



<!DOCTYPE html>
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
 <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" >
 
 <meta name="ROBOTS" content="NOARCHIVE">
 
 <link rel="icon" type="image/vnd.microsoft.icon" href="https://ssl.gstatic.com/codesite/ph/images/phosting.ico">
 
 
 <script type="text/javascript">
 
 
 
 
 var codesite_token = null;
 
 
 var CS_env = {"projectName": "toadlet", "domainName": null, "assetVersionPath": "https://ssl.gstatic.com/codesite/ph/3197964839662303775", "token": null, "profileUrl": null, "assetHostPath": "https://ssl.gstatic.com/codesite/ph", "loggedInUserEmail": null, "relativeBaseUrl": "", "projectHomeUrl": "/p/toadlet"};
 var _gaq = _gaq || [];
 _gaq.push(
 ['siteTracker._setAccount', 'UA-18071-1'],
 ['siteTracker._trackPageview']);
 
 _gaq.push(
 ['projectTracker._setAccount', 'UA-8904603-10'],
 ['projectTracker._trackPageview']);
 
 (function() {
 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
 (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
 })();
 
 </script>
 
 
 <title>LocateStaticLibraries.cmake - 
 toadlet -
 
 
 The Toadlet Engine Toolkit - Google Project Hosting
 </title>
 <link type="text/css" rel="stylesheet" href="https://ssl.gstatic.com/codesite/ph/3197964839662303775/css/core.css">
 
 <link type="text/css" rel="stylesheet" href="https://ssl.gstatic.com/codesite/ph/3197964839662303775/css/ph_detail.css" >
 
 
 <link type="text/css" rel="stylesheet" href="https://ssl.gstatic.com/codesite/ph/3197964839662303775/css/d_sb.css" >
 
 
 
<!--[if IE]>
 <link type="text/css" rel="stylesheet" href="https://ssl.gstatic.com/codesite/ph/3197964839662303775/css/d_ie.css" >
<![endif]-->
 <style type="text/css">
 .menuIcon.off { background: no-repeat url(https://ssl.gstatic.com/codesite/ph/images/dropdown_sprite.gif) 0 -42px }
 .menuIcon.on { background: no-repeat url(https://ssl.gstatic.com/codesite/ph/images/dropdown_sprite.gif) 0 -28px }
 .menuIcon.down { background: no-repeat url(https://ssl.gstatic.com/codesite/ph/images/dropdown_sprite.gif) 0 0; }
 
 
 
  tr.inline_comment {
 background: #fff;
 vertical-align: top;
 }
 div.draft, div.published {
 padding: .3em;
 border: 1px solid #999; 
 margin-bottom: .1em;
 font-family: arial, sans-serif;
 max-width: 60em;
 }
 div.draft {
 background: #ffa;
 } 
 div.published {
 background: #e5ecf9;
 }
 div.published .body, div.draft .body {
 padding: .5em .1em .1em .1em;
 max-width: 60em;
 white-space: pre-wrap;
 white-space: -moz-pre-wrap;
 white-space: -pre-wrap;
 white-space: -o-pre-wrap;
 word-wrap: break-word;
 font-size: 1em;
 }
 div.draft .actions {
 margin-left: 1em;
 font-size: 90%;
 }
 div.draft form {
 padding: .5em .5em .5em 0;
 }
 div.draft textarea, div.published textarea {
 width: 95%;
 height: 10em;
 font-family: arial, sans-serif;
 margin-bottom: .5em;
 }

 
 .nocursor, .nocursor td, .cursor_hidden, .cursor_hidden td {
 background-color: white;
 height: 2px;
 }
 .cursor, .cursor td {
 background-color: darkblue;
 height: 2px;
 display: '';
 }
 
 
.list {
 border: 1px solid white;
 border-bottom: 0;
}

 
 </style>
</head>
<body class="t4">
<script type="text/javascript">
 window.___gcfg = {lang: 'en'};
 (function() 
 {var po = document.createElement("script");
 po.type = "text/javascript"; po.async = true;po.src = "https://apis.google.com/js/plusone.js";
 var s = document.getElementsByTagName("script")[0];
 s.parentNode.insertBefore(po, s);
 })();
</script>
<div class="headbg">

 <div id="gaia">
 

 <span>
 
 
 <a href="#" id="projects-dropdown" onclick="return false;"><u>My favorites</u> <small>&#9660;</small></a>
 | <a href="https://www.google.com/accounts/ServiceLogin?service=code&amp;ltmpl=phosting&amp;continue=https%3A%2F%2Fcode.google.com%2Fp%2Ftoadlet%2Fsource%2Fbrowse%2Fcmake%2FModules%2FLocateStaticLibraries.cmake&amp;followup=https%3A%2F%2Fcode.google.com%2Fp%2Ftoadlet%2Fsource%2Fbrowse%2Fcmake%2FModules%2FLocateStaticLibraries.cmake" onclick="_CS_click('/gb/ph/signin');"><u>Sign in</u></a>
 
 </span>

 </div>

 <div class="gbh" style="left: 0pt;"></div>
 <div class="gbh" style="right: 0pt;"></div>
 
 
 <div style="height: 1px"></div>
<!--[if lte IE 7]>
<div style="text-align:center;">
Your version of Internet Explorer is not supported. Try a browser that
contributes to open source, such as <a href="http://www.firefox.com">Firefox</a>,
<a href="http://www.google.com/chrome">Google Chrome</a>, or
<a href="http://code.google.com/chrome/chromeframe/">Google Chrome Frame</a>.
</div>
<![endif]-->



 <table style="padding:0px; margin: 0px 0px 10px 0px; width:100%" cellpadding="0" cellspacing="0"
 itemscope itemtype="http://schema.org/CreativeWork">
 <tr style="height: 58px;">
 
 
 
 <td id="plogo">
 <link itemprop="url" href="/p/toadlet">
 <a href="/p/toadlet/">
 
 
 <img src="/p/toadlet/logo?cct=1393352939"
 alt="Logo" itemprop="image">
 
 </a>
 </td>
 
 <td style="padding-left: 0.5em">
 
 <div id="pname">
 <a href="/p/toadlet/"><span itemprop="name">toadlet</span></a>
 </div>
 
 <div id="psum">
 <a id="project_summary_link"
 href="/p/toadlet/"><span itemprop="description">The Toadlet Engine Toolkit</span></a>
 
 </div>
 
 
 </td>
 <td style="white-space:nowrap;text-align:right; vertical-align:bottom;">
 
 <form action="/hosting/search">
 <input size="30" name="q" value="" type="text">
 
 <input type="submit" name="projectsearch" value="Search projects" >
 </form>
 
 </tr>
 </table>

</div>

 
<div id="mt" class="gtb"> 
 <a href="/p/toadlet/" class="tab ">Project&nbsp;Home</a>
 
 
 
 
 
 
 <a href="/p/toadlet/w/list" class="tab ">Wiki</a>
 
 
 
 
 
 <a href="/p/toadlet/issues/list"
 class="tab ">Issues</a>
 
 
 
 
 
 <a href="/p/toadlet/source/checkout"
 class="tab active">Source</a>
 
 
 
 
 
 
 
 
 <a href="https://code.google.com/export-to-github/export?project=toadlet">
 <button>Export to GitHub</button>
 
 </a>
 
 
 
 
 
 <div class=gtbc></div>
</div>
<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0" class="st">
 <tr>
 
 
 
 
 
 
 <td class="subt">
 <div class="st2">
 <div class="isf">
 
 <form action="/p/toadlet/source/browse" style="display: inline">
 
 Repository:
 <select name="repo" id="repo" style="font-size: 92%" onchange="submit()">
 <option value="default">default</option><option value="wiki">wiki</option>
 </select>
 </form>
 
 


 <span class="inst1"><a href="/p/toadlet/source/checkout">Checkout</a></span> &nbsp;
 <span class="inst2"><a href="/p/toadlet/source/browse/">Browse</a></span> &nbsp;
 <span class="inst3"><a href="/p/toadlet/source/list">Changes</a></span> &nbsp;
 <span class="inst4"><a href="/p/toadlet/source/clones">Clones</a></span> &nbsp; 
 
 
 
 
 
 
 </form>
 <script type="text/javascript">
 
 function codesearchQuery(form) {
 var query = document.getElementById('q').value;
 if (query) { form.action += '%20' + query; }
 }
 </script>
 </div>
</div>

 </td>
 
 
 
 <td align="right" valign="top" class="bevel-right"></td>
 </tr>
</table>


<script type="text/javascript">
 var cancelBubble = false;
 function _go(url) { document.location = url; }
</script>
<div id="maincol"
 
>

 




<div class="expand">
<div id="colcontrol">
<style type="text/css">
 #file_flipper { white-space: nowrap; padding-right: 2em; }
 #file_flipper.hidden { display: none; }
 #file_flipper .pagelink { color: #0000CC; text-decoration: underline; }
 #file_flipper #visiblefiles { padding-left: 0.5em; padding-right: 0.5em; }
</style>
<table id="nav_and_rev" class="list"
 cellpadding="0" cellspacing="0" width="100%">
 <tr>
 
 <td nowrap="nowrap" class="src_crumbs src_nav" width="33%">
 <strong class="src_nav">Source path:&nbsp;</strong>
 <span id="crumb_root">
 
 <a href="/p/toadlet/source/browse/">hg</a>/&nbsp;</span>
 <span id="crumb_links" class="ifClosed"><a href="/p/toadlet/source/browse/cmake/">cmake</a><span class="sp">/&nbsp;</span><a href="/p/toadlet/source/browse/cmake/Modules/">Modules</a><span class="sp">/&nbsp;</span>LocateStaticLibraries.cmake</span>
 
 

 <span class="sourcelabel">Download
 <a href="//toadlet.googlecode.com/archive/addfe68edc48e132f00cbf9741e81cb0c564f20d.zip" rel="nofollow">zip</a> | <a href="//toadlet.googlecode.com/archive/addfe68edc48e132f00cbf9741e81cb0c564f20d.tar.gz" rel="nofollow">tar.gz</a>
 </span>


 </td>
 
 
 <td nowrap="nowrap" width="33%" align="right">
 <table cellpadding="0" cellspacing="0" style="font-size: 100%"><tr>
 
 
 <td class="flipper">
 <ul class="leftside">
 
 <li><a href="/p/toadlet/source/browse/cmake/Modules/LocateStaticLibraries.cmake?r=b506d90b37aa5391b9c6ac5050a364071ee7c975" title="Previous">&lsaquo;b506d90b37aa</a></li>
 
 </ul>
 </td>
 
 <td class="flipper"><b>addfe68edc48</b></td>
 
 </tr></table>
 </td> 
 </tr>
</table>

<div class="fc">
 
 
 
<style type="text/css">
.undermouse span {
 background-image: url(https://ssl.gstatic.com/codesite/ph/images/comments.gif); }
</style>
<table class="opened" id="review_comment_area"
><tr>
<td id="nums">
<pre><table width="100%"><tr class="nocursor"><td></td></tr></table></pre>
<pre><table width="100%" id="nums_table_0"><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_1"

><td id="1"><a href="#1">1</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_2"

><td id="2"><a href="#2">2</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_3"

><td id="3"><a href="#3">3</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_4"

><td id="4"><a href="#4">4</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_5"

><td id="5"><a href="#5">5</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_6"

><td id="6"><a href="#6">6</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_7"

><td id="7"><a href="#7">7</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_8"

><td id="8"><a href="#8">8</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_9"

><td id="9"><a href="#9">9</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_10"

><td id="10"><a href="#10">10</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_11"

><td id="11"><a href="#11">11</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_12"

><td id="12"><a href="#12">12</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_13"

><td id="13"><a href="#13">13</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_14"

><td id="14"><a href="#14">14</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_15"

><td id="15"><a href="#15">15</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_16"

><td id="16"><a href="#16">16</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_17"

><td id="17"><a href="#17">17</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_18"

><td id="18"><a href="#18">18</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_19"

><td id="19"><a href="#19">19</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_20"

><td id="20"><a href="#20">20</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_21"

><td id="21"><a href="#21">21</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_22"

><td id="22"><a href="#22">22</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_23"

><td id="23"><a href="#23">23</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_24"

><td id="24"><a href="#24">24</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_25"

><td id="25"><a href="#25">25</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_26"

><td id="26"><a href="#26">26</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_27"

><td id="27"><a href="#27">27</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_28"

><td id="28"><a href="#28">28</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_29"

><td id="29"><a href="#29">29</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_30"

><td id="30"><a href="#30">30</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_31"

><td id="31"><a href="#31">31</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_32"

><td id="32"><a href="#32">32</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_33"

><td id="33"><a href="#33">33</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_34"

><td id="34"><a href="#34">34</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_35"

><td id="35"><a href="#35">35</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_36"

><td id="36"><a href="#36">36</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_37"

><td id="37"><a href="#37">37</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_38"

><td id="38"><a href="#38">38</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_39"

><td id="39"><a href="#39">39</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_40"

><td id="40"><a href="#40">40</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_41"

><td id="41"><a href="#41">41</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_42"

><td id="42"><a href="#42">42</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_43"

><td id="43"><a href="#43">43</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_44"

><td id="44"><a href="#44">44</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_45"

><td id="45"><a href="#45">45</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_46"

><td id="46"><a href="#46">46</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_47"

><td id="47"><a href="#47">47</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_48"

><td id="48"><a href="#48">48</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_49"

><td id="49"><a href="#49">49</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_50"

><td id="50"><a href="#50">50</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_51"

><td id="51"><a href="#51">51</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_52"

><td id="52"><a href="#52">52</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_53"

><td id="53"><a href="#53">53</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_54"

><td id="54"><a href="#54">54</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_55"

><td id="55"><a href="#55">55</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_56"

><td id="56"><a href="#56">56</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_57"

><td id="57"><a href="#57">57</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_58"

><td id="58"><a href="#58">58</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_59"

><td id="59"><a href="#59">59</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_60"

><td id="60"><a href="#60">60</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_61"

><td id="61"><a href="#61">61</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_62"

><td id="62"><a href="#62">62</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_63"

><td id="63"><a href="#63">63</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_64"

><td id="64"><a href="#64">64</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_65"

><td id="65"><a href="#65">65</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_66"

><td id="66"><a href="#66">66</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_67"

><td id="67"><a href="#67">67</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_68"

><td id="68"><a href="#68">68</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_69"

><td id="69"><a href="#69">69</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_70"

><td id="70"><a href="#70">70</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_71"

><td id="71"><a href="#71">71</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_72"

><td id="72"><a href="#72">72</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_73"

><td id="73"><a href="#73">73</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_74"

><td id="74"><a href="#74">74</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_75"

><td id="75"><a href="#75">75</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_76"

><td id="76"><a href="#76">76</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_77"

><td id="77"><a href="#77">77</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_78"

><td id="78"><a href="#78">78</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_79"

><td id="79"><a href="#79">79</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_80"

><td id="80"><a href="#80">80</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_81"

><td id="81"><a href="#81">81</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_82"

><td id="82"><a href="#82">82</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_83"

><td id="83"><a href="#83">83</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_84"

><td id="84"><a href="#84">84</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_85"

><td id="85"><a href="#85">85</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_86"

><td id="86"><a href="#86">86</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_87"

><td id="87"><a href="#87">87</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_88"

><td id="88"><a href="#88">88</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_89"

><td id="89"><a href="#89">89</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_90"

><td id="90"><a href="#90">90</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_91"

><td id="91"><a href="#91">91</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_92"

><td id="92"><a href="#92">92</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_93"

><td id="93"><a href="#93">93</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_94"

><td id="94"><a href="#94">94</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_95"

><td id="95"><a href="#95">95</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_96"

><td id="96"><a href="#96">96</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_97"

><td id="97"><a href="#97">97</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_98"

><td id="98"><a href="#98">98</a></td></tr
><tr id="gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_99"

><td id="99"><a href="#99">99</a></td></tr
></table></pre>
<pre><table width="100%"><tr class="nocursor"><td></td></tr></table></pre>
</td>
<td id="lines">
<pre><table width="100%"><tr class="cursor_stop cursor_hidden"><td></td></tr></table></pre>
<pre ><table id="src_table_0"><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_1

><td class="source"># This macro looks for static library versions of a list of libraries<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_2

><td class="source"># STATIC - the output list of static libraries<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_3

><td class="source"># LIBRARIES - the input list of libraries you would like as static<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_4

><td class="source"># FALSE(default)/TRUE - No import libraries. This option only has effect on windows platforms. <br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_5

><td class="source">#                       If FALSE(default), then import libraries will be returned as static along with a warning.<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_6

><td class="source">#                       If TRUE, then import libraries will be ignored and only fully static libraries will be returned.<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_7

><td class="source">#<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_8

><td class="source"># Example usage:<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_9

><td class="source">#    LOCATE_STATIC_LIBRARIES (MYLIBS_S &quot;${MYLIBS}&quot;)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_10

><td class="source">#      or<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_11

><td class="source">#    LOCATE_STATIC_LIBRARIES (MYLIBS_S &quot;${MYLIBS}&quot; TRUE) - will not allow import libraries on windows platforms.<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_12

><td class="source">#<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_13

><td class="source"># NOTE: When you call this script, make sure you quote the argument to LIBRARIES if it is a list!<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_14

><td class="source"><br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_15

><td class="source">macro (LOCATE_STATIC_LIBRARIES STATIC LIBRARIES) #FALSE/TRUE)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_16

><td class="source">	unset (${STATIC})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_17

><td class="source"><br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_18

><td class="source">	if (TOADLET_PLATFORM_WIN32)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_19

><td class="source">		# Are import libraries acceptable?<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_20

><td class="source">		set (NO_IMPORT FALSE)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_21

><td class="source">		if (${ARGC} EQUAL 3)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_22

><td class="source">			if (${ARGV2})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_23

><td class="source">				set (NO_IMPORT TRUE)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_24

><td class="source">			endif (${ARGV2})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_25

><td class="source">		endif (${ARGC} EQUAL 3)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_26

><td class="source">	<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_27

><td class="source">		# On windows we use the &#39;lib&#39; tool to try and determine if we have found a fully static or an import library<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_28

><td class="source">		find_program (LIBEXE lib)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_29

><td class="source"><br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_30

><td class="source">		if (EXISTS ${LIBEXE})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_31

><td class="source">			foreach (LIBRARY ${LIBRARIES})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_32

><td class="source">				# If a .dll was passed in, try the .lib extension<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_33

><td class="source">				get_filename_component (SUFFIX ${LIBRARY} EXT)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_34

><td class="source">				if (${SUFFIX} STREQUAL &quot;.dll&quot;)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_35

><td class="source">					string (REPLACE &quot;.dll&quot; &quot;.lib&quot; LIBRARY ${LIBRARY})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_36

><td class="source">				endif (${SUFFIX} STREQUAL &quot;.dll&quot;)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_37

><td class="source"><br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_38

><td class="source">				if (EXISTS ${LIBRARY})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_39

><td class="source">					# Use the lib.exe tool to dump the .lib contents to a file and search for the string &quot;.dll&quot; in that file<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_40

><td class="source">					message(&quot;Using:${LIBEXE} to list:${LIBRARY} and dump to file: libout-${LIBNAME}&quot;)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_41

><td class="source">					get_filename_component (LIBNAME ${LIBRARY} NAME_WE)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_42

><td class="source">					execute_process (<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_43

><td class="source">						COMMAND ${LIBEXE} /list ${LIBRARY} WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR} OUTPUT_FILE libout-${LIBNAME}<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_44

><td class="source">					)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_45

><td class="source">					file (READ ${CMAKE_CURRENT_BINARY_DIR}/libout-${LIBNAME} LIBOUT)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_46

><td class="source">					message(&quot;Read found libout=|${LIBOUT}| when scanning:|${CMAKE_CURRENT_BINARY_DIR}/libout-${LIBNAME}&quot;)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_47

><td class="source">					string (FIND ${LIBOUT} &quot;.dll&quot; HASDLL)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_48

><td class="source"><br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_49

><td class="source">					if (${HASDLL} GREATER -1)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_50

><td class="source">						# If a path to a .dll is present, this is an import library<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_51

><td class="source">						if (${NO_IMPORT})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_52

><td class="source">							message (STATUS &quot;WARNING: A fully static build of ${LIBRARY} could not be found. ${STATIC} will be incomplete.&quot;)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_53

><td class="source">						else (${NO_IMPORT})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_54

><td class="source">							message (STATUS &quot;WARNING: ${LIBRARY} is an IMPORT library. The corresponding .dll(s) will be required.&quot;)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_55

><td class="source">							set (${STATIC} ${${STATIC}} ${LIBRARY})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_56

><td class="source">						endif (${NO_IMPORT})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_57

><td class="source">					else (${HASDLL} GREATER -1)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_58

><td class="source">						# We have found a fully static library<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_59

><td class="source">						set (${STATIC} ${${STATIC}} ${LIBRARY})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_60

><td class="source">					endif (${HASDLL} GREATER -1)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_61

><td class="source">				else (EXISTS ${LIBRARY})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_62

><td class="source">					message (STATUS &quot;WARNING: A static build of ${LIBRARY} could not be found. ${STATIC} will be incomplete.&quot;)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_63

><td class="source">				endif (EXISTS ${LIBRARY})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_64

><td class="source">			endforeach (LIBRARY ${LIBRARIES})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_65

><td class="source">		else (EXISTS ${LIBEXE})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_66

><td class="source">			message (STATUS &quot;WARNING: &#39;lib.exe&#39; could not be found, libraries:${LIBRARIES} will be assumed static&quot;)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_67

><td class="source">			set (${STATIC} ${LIBRARIES})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_68

><td class="source">		endif (EXISTS ${LIBEXE})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_69

><td class="source">	else (TOADLET_PLATFORM_WIN32)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_70

><td class="source">		if (TOADLET_PLATFORM_OSX)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_71

><td class="source">			# Both iOS and OSX use the dylib extension<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_72

><td class="source">			set (DYNEXT &quot;.dylib&quot;)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_73

><td class="source">		else (TOADLET_PLATFORM_OSX)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_74

><td class="source">			# Everyone else uses .so<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_75

><td class="source">			set (DYNEXT &quot;.so&quot;)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_76

><td class="source">		endif (TOADLET_PLATFORM_OSX)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_77

><td class="source"><br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_78

><td class="source">		foreach (LIBRARY ${LIBRARIES})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_79

><td class="source">			get_filename_component (SUFFIX ${LIBRARY} EXT)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_80

><td class="source">			if (${SUFFIX} STREQUAL &quot;.a&quot;)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_81

><td class="source">				# Library is already static<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_82

><td class="source">				set (${STATIC} ${${STATIC}} ${LIBRARY})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_83

><td class="source">			elseif (${SUFFIX} STREQUAL ${DYNEXT})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_84

><td class="source">				string (REPLACE ${DYNEXT} &quot;.a&quot; LIB_S ${LIBRARY})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_85

><td class="source">				if (EXISTS ${LIB_S})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_86

><td class="source">					set (${STATIC} ${${STATIC}} ${LIB_S})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_87

><td class="source">				else (EXISTS ${LIB_S})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_88

><td class="source">					# Do a last ditch search for a static library<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_89

><td class="source">					find_file (LIB_S_FOUND ${LIB_S})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_90

><td class="source">					if (LIB_S_FOUND)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_91

><td class="source">						set (${STATIC} ${${STATIC}} ${LIB_S_FOUND})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_92

><td class="source">					else (LIB_S_FOUND)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_93

><td class="source">						message (STATUS &quot;WARNING: A static build of ${LIBRARY} could not be found. ${STATIC} will be incomplete.&quot;)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_94

><td class="source">					endif (LIB_S_FOUND)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_95

><td class="source">				endif (EXISTS ${LIB_S})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_96

><td class="source">			endif (${SUFFIX} STREQUAL &quot;.a&quot;)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_97

><td class="source">		endforeach (LIBRARY ${LIBRARIES})<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_98

><td class="source">	endif (TOADLET_PLATFORM_WIN32)<br></td></tr
><tr
id=sl_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_99

><td class="source">endmacro (LOCATE_STATIC_LIBRARIES)<br></td></tr
></table></pre>
<pre><table width="100%"><tr class="cursor_stop cursor_hidden"><td></td></tr></table></pre>
</td>
</tr></table>

 
<script type="text/javascript">
 var lineNumUnderMouse = -1;
 
 function gutterOver(num) {
 gutterOut();
 var newTR = document.getElementById('gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_' + num);
 if (newTR) {
 newTR.className = 'undermouse';
 }
 lineNumUnderMouse = num;
 }
 function gutterOut() {
 if (lineNumUnderMouse != -1) {
 var oldTR = document.getElementById(
 'gr_svnaddfe68edc48e132f00cbf9741e81cb0c564f20d_' + lineNumUnderMouse);
 if (oldTR) {
 oldTR.className = '';
 }
 lineNumUnderMouse = -1;
 }
 }
 var numsGenState = {table_base_id: 'nums_table_'};
 var srcGenState = {table_base_id: 'src_table_'};
 var alignerRunning = false;
 var startOver = false;
 function setLineNumberHeights() {
 if (alignerRunning) {
 startOver = true;
 return;
 }
 numsGenState.chunk_id = 0;
 numsGenState.table = document.getElementById('nums_table_0');
 numsGenState.row_num = 0;
 if (!numsGenState.table) {
 return; // Silently exit if no file is present.
 }
 srcGenState.chunk_id = 0;
 srcGenState.table = document.getElementById('src_table_0');
 srcGenState.row_num = 0;
 alignerRunning = true;
 continueToSetLineNumberHeights();
 }
 function rowGenerator(genState) {
 if (genState.row_num < genState.table.rows.length) {
 var currentRow = genState.table.rows[genState.row_num];
 genState.row_num++;
 return currentRow;
 }
 var newTable = document.getElementById(
 genState.table_base_id + (genState.chunk_id + 1));
 if (newTable) {
 genState.chunk_id++;
 genState.row_num = 0;
 genState.table = newTable;
 return genState.table.rows[0];
 }
 return null;
 }
 var MAX_ROWS_PER_PASS = 1000;
 function continueToSetLineNumberHeights() {
 var rowsInThisPass = 0;
 var numRow = 1;
 var srcRow = 1;
 while (numRow && srcRow && rowsInThisPass < MAX_ROWS_PER_PASS) {
 numRow = rowGenerator(numsGenState);
 srcRow = rowGenerator(srcGenState);
 rowsInThisPass++;
 if (numRow && srcRow) {
 if (numRow.offsetHeight != srcRow.offsetHeight) {
 numRow.firstChild.style.height = srcRow.offsetHeight + 'px';
 }
 }
 }
 if (rowsInThisPass >= MAX_ROWS_PER_PASS) {
 setTimeout(continueToSetLineNumberHeights, 10);
 } else {
 alignerRunning = false;
 if (startOver) {
 startOver = false;
 setTimeout(setLineNumberHeights, 500);
 }
 }
 }
 function initLineNumberHeights() {
 // Do 2 complete passes, because there can be races
 // between this code and prettify.
 startOver = true;
 setTimeout(setLineNumberHeights, 250);
 window.onresize = setLineNumberHeights;
 }
 initLineNumberHeights();
</script>

 
 
 <div id="log">
 <div style="text-align:right">
 <a class="ifCollapse" href="#" onclick="_toggleMeta(this); return false">Show details</a>
 <a class="ifExpand" href="#" onclick="_toggleMeta(this); return false">Hide details</a>
 </div>
 <div class="ifExpand">
 
 
 <div class="pmeta_bubble_bg" style="border:1px solid white">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <div id="changelog">
 <p>Change log</p>
 <div>
 <a href="/p/toadlet/source/detail?spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d&amp;r=762045a80ab42e590097815fa1aab84fdbabc457">762045a80ab4</a>
 by Alan Fischer &lt;a...@lightningtoads.com&gt;
 on Apr 30, 2014
 &nbsp; <a href="/p/toadlet/source/diff?spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d&r=762045a80ab42e590097815fa1aab84fdbabc457&amp;format=side&amp;path=/cmake/Modules/LocateStaticLibraries.cmake&amp;old_path=/cmake/Modules/LocateStaticLibraries.cmake&amp;old=b506d90b37aa5391b9c6ac5050a364071ee7c975">Diff</a>
 </div>
 <pre>Fix vc2013 issues</pre>
 </div>
 
 
 
 
 
 
 <script type="text/javascript">
 var detail_url = '/p/toadlet/source/detail?r=762045a80ab42e590097815fa1aab84fdbabc457&spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d';
 var publish_url = '/p/toadlet/source/detail?r=762045a80ab42e590097815fa1aab84fdbabc457&spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d#publish';
 // describe the paths of this revision in javascript.
 var changed_paths = [];
 var changed_urls = [];
 
 changed_paths.push('/cmake/Modules/FindD3D9.cmake');
 changed_urls.push('/p/toadlet/source/browse/cmake/Modules/FindD3D9.cmake?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/cmake/Modules/LocateStaticLibraries.cmake');
 changed_urls.push('/p/toadlet/source/browse/cmake/Modules/LocateStaticLibraries.cmake?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 var selected_path = '/cmake/Modules/LocateStaticLibraries.cmake';
 
 
 changed_paths.push('/examples/data/lt.tmsh');
 changed_urls.push('/p/toadlet/source/browse/examples/data/lt.tmsh?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/examples/main/toadletApp.cpp');
 changed_urls.push('/p/toadlet/source/browse/examples/main/toadletApp.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/examples/randisle/res/frog.tmsh');
 changed_urls.push('/p/toadlet/source/browse/examples/randisle/res/frog.tmsh?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/examples/randisle/res/tall_grass.tmsh');
 changed_urls.push('/p/toadlet/source/browse/examples/randisle/res/tall_grass.tmsh?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/examples/randisle/src/Resources.h');
 changed_urls.push('/p/toadlet/source/browse/examples/randisle/src/Resources.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/Types.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/Types.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/egg/Collection.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/egg/Collection.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/egg/Types.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/egg/Types.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/egg/math/Vector3.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/egg/math/Vector3.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/peeper/BlendState.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/peeper/BlendState.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/peeper/DepthState.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/peeper/DepthState.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/peeper/FogState.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/peeper/FogState.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/peeper/GeometryState.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/peeper/GeometryState.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/peeper/LightState.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/peeper/LightState.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/peeper/MaterialState.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/peeper/MaterialState.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/peeper/RasterizerState.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/peeper/RasterizerState.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/peeper/SamplerState.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/peeper/SamplerState.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/peeper/TextureFormatConversion.cpp');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/peeper/TextureFormatConversion.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/peeper/TextureState.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/peeper/TextureState.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/peeper/plugins/d3d10renderdevice/D3D10Buffer.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/peeper/plugins/d3d10renderdevice/D3D10Buffer.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/peeper/plugins/glrenderdevice/GLRenderDevice.cpp');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/peeper/plugins/glrenderdevice/GLRenderDevice.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/peeper/plugins/glrenderdevice/GLVertexFormat.cpp');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/peeper/plugins/glrenderdevice/GLVertexFormat.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/tadpole/ArchiveManager.cpp');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/tadpole/ArchiveManager.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/tadpole/ResourceCache.cpp');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/tadpole/ResourceCache.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/tadpole/ResourceManager.cpp');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/tadpole/ResourceManager.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/tadpole/ResourceManager.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/tadpole/ResourceManager.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/tadpole/bsp/BSP30Map.cpp');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/tadpole/bsp/BSP30Map.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/tadpole/bsp/BSP30Map.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/tadpole/bsp/BSP30Map.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/tadpole/material/RenderVariables.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/tadpole/material/RenderVariables.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/tadpole/plugins/TMSHStreamer.cpp');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/tadpole/plugins/TMSHStreamer.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/source/cpp/toadlet/tadpole/plugins/TMSHStreamer.h');
 changed_urls.push('/p/toadlet/source/browse/source/cpp/toadlet/tadpole/plugins/TMSHStreamer.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/tools/shared/NvTriStrip/NvTriStripObjects.cpp');
 changed_urls.push('/p/toadlet/source/browse/tools/shared/NvTriStrip/NvTriStripObjects.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/tools/smd2tmsh/main.cpp');
 changed_urls.push('/p/toadlet/source/browse/tools/smd2tmsh/main.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/tools/tmsh2m3g/main.cpp');
 changed_urls.push('/p/toadlet/source/browse/tools/tmsh2m3g/main.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/tools/tmsh2tmsh/main.cpp');
 changed_urls.push('/p/toadlet/source/browse/tools/tmsh2tmsh/main.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/tools/tmshModifier/main.cpp');
 changed_urls.push('/p/toadlet/source/browse/tools/tmshModifier/main.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/tools/tmshOptimizer/main.cpp');
 changed_urls.push('/p/toadlet/source/browse/tools/tmshOptimizer/main.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/tools/tmshVertexLighter/main.cpp');
 changed_urls.push('/p/toadlet/source/browse/tools/tmshVertexLighter/main.cpp?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 changed_paths.push('/tools/tmshViewer/Viewer.h');
 changed_urls.push('/p/toadlet/source/browse/tools/tmshViewer/Viewer.h?r\x3d762045a80ab42e590097815fa1aab84fdbabc457\x26spec\x3dsvnaddfe68edc48e132f00cbf9741e81cb0c564f20d');
 
 
 function getCurrentPageIndex() {
 for (var i = 0; i < changed_paths.length; i++) {
 if (selected_path == changed_paths[i]) {
 return i;
 }
 }
 }
 function getNextPage() {
 var i = getCurrentPageIndex();
 if (i < changed_paths.length - 1) {
 return changed_urls[i + 1];
 }
 return null;
 }
 function getPreviousPage() {
 var i = getCurrentPageIndex();
 if (i > 0) {
 return changed_urls[i - 1];
 }
 return null;
 }
 function gotoNextPage() {
 var page = getNextPage();
 if (!page) {
 page = detail_url;
 }
 window.location = page;
 }
 function gotoPreviousPage() {
 var page = getPreviousPage();
 if (!page) {
 page = detail_url;
 }
 window.location = page;
 }
 function gotoDetailPage() {
 window.location = detail_url;
 }
 function gotoPublishPage() {
 window.location = publish_url;
 }
</script>

 
 <style type="text/css">
 #review_nav {
 border-top: 3px solid white;
 padding-top: 6px;
 margin-top: 1em;
 }
 #review_nav td {
 vertical-align: middle;
 }
 #review_nav select {
 margin: .5em 0;
 }
 </style>
 <div id="review_nav">
 <table><tr><td>Go to:&nbsp;</td><td>
 <select name="files_in_rev" onchange="window.location=this.value">
 
 <option value="/p/toadlet/source/browse/cmake/Modules/FindD3D9.cmake?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >/cmake/Modules/FindD3D9.cmake</option>
 
 <option value="/p/toadlet/source/browse/cmake/Modules/LocateStaticLibraries.cmake?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 selected="selected"
 >...ules/LocateStaticLibraries.cmake</option>
 
 <option value="/p/toadlet/source/browse/examples/data/lt.tmsh?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >/examples/data/lt.tmsh</option>
 
 <option value="/p/toadlet/source/browse/examples/main/toadletApp.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >/examples/main/toadletApp.cpp</option>
 
 <option value="/p/toadlet/source/browse/examples/randisle/res/frog.tmsh?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >/examples/randisle/res/frog.tmsh</option>
 
 <option value="/p/toadlet/source/browse/examples/randisle/res/tall_grass.tmsh?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...les/randisle/res/tall_grass.tmsh</option>
 
 <option value="/p/toadlet/source/browse/examples/randisle/src/Resources.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >/examples/randisle/src/Resources.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/Types.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >/source/cpp/toadlet/Types.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/egg/Collection.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...rce/cpp/toadlet/egg/Collection.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/egg/Types.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >/source/cpp/toadlet/egg/Types.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/egg/math/Vector3.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...e/cpp/toadlet/egg/math/Vector3.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/peeper/BlendState.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >.../cpp/toadlet/peeper/BlendState.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/peeper/DepthState.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >.../cpp/toadlet/peeper/DepthState.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/peeper/FogState.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...ce/cpp/toadlet/peeper/FogState.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/peeper/GeometryState.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...p/toadlet/peeper/GeometryState.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/peeper/LightState.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >.../cpp/toadlet/peeper/LightState.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/peeper/MaterialState.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...p/toadlet/peeper/MaterialState.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/peeper/RasterizerState.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...toadlet/peeper/RasterizerState.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/peeper/SamplerState.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...pp/toadlet/peeper/SamplerState.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/peeper/TextureFormatConversion.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...eper/TextureFormatConversion.cpp</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/peeper/TextureState.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...pp/toadlet/peeper/TextureState.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/peeper/plugins/d3d10renderdevice/D3D10Buffer.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >.../d3d10renderdevice/D3D10Buffer.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/peeper/plugins/glrenderdevice/GLRenderDevice.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...lrenderdevice/GLRenderDevice.cpp</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/peeper/plugins/glrenderdevice/GLVertexFormat.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...lrenderdevice/GLVertexFormat.cpp</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/tadpole/ArchiveManager.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...adlet/tadpole/ArchiveManager.cpp</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/tadpole/ResourceCache.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...oadlet/tadpole/ResourceCache.cpp</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/tadpole/ResourceManager.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...dlet/tadpole/ResourceManager.cpp</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/tadpole/ResourceManager.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...oadlet/tadpole/ResourceManager.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/tadpole/bsp/BSP30Map.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...toadlet/tadpole/bsp/BSP30Map.cpp</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/tadpole/bsp/BSP30Map.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...p/toadlet/tadpole/bsp/BSP30Map.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/tadpole/material/RenderVariables.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...dpole/material/RenderVariables.h</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/tadpole/plugins/TMSHStreamer.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...tadpole/plugins/TMSHStreamer.cpp</option>
 
 <option value="/p/toadlet/source/browse/source/cpp/toadlet/tadpole/plugins/TMSHStreamer.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...t/tadpole/plugins/TMSHStreamer.h</option>
 
 <option value="/p/toadlet/source/browse/tools/shared/NvTriStrip/NvTriStripObjects.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >...NvTriStrip/NvTriStripObjects.cpp</option>
 
 <option value="/p/toadlet/source/browse/tools/smd2tmsh/main.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >/tools/smd2tmsh/main.cpp</option>
 
 <option value="/p/toadlet/source/browse/tools/tmsh2m3g/main.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >/tools/tmsh2m3g/main.cpp</option>
 
 <option value="/p/toadlet/source/browse/tools/tmsh2tmsh/main.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >/tools/tmsh2tmsh/main.cpp</option>
 
 <option value="/p/toadlet/source/browse/tools/tmshModifier/main.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >/tools/tmshModifier/main.cpp</option>
 
 <option value="/p/toadlet/source/browse/tools/tmshOptimizer/main.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >/tools/tmshOptimizer/main.cpp</option>
 
 <option value="/p/toadlet/source/browse/tools/tmshVertexLighter/main.cpp?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >/tools/tmshVertexLighter/main.cpp</option>
 
 <option value="/p/toadlet/source/browse/tools/tmshViewer/Viewer.h?r=762045a80ab42e590097815fa1aab84fdbabc457&amp;spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d"
 
 >/tools/tmshViewer/Viewer.h</option>
 
 </select>
 </td></tr></table>
 
 
 



 <div style="white-space:nowrap">
 Project members,
 <a href="https://www.google.com/accounts/ServiceLogin?service=code&amp;ltmpl=phosting&amp;continue=https%3A%2F%2Fcode.google.com%2Fp%2Ftoadlet%2Fsource%2Fbrowse%2Fcmake%2FModules%2FLocateStaticLibraries.cmake&amp;followup=https%3A%2F%2Fcode.google.com%2Fp%2Ftoadlet%2Fsource%2Fbrowse%2Fcmake%2FModules%2FLocateStaticLibraries.cmake"
 >sign in</a> to write a code review</div>


 
 </div>
 
 
 </div>
 <div class="round1"></div>
 <div class="round2"></div>
 <div class="round4"></div>
 </div>
 <div class="pmeta_bubble_bg" style="border:1px solid white">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <div id="older_bubble">
 <p>Older revisions</p>
 
 
 <div class="closed" style="margin-bottom:3px;" >
 <a class="ifClosed" onclick="return _toggleHidden(this)"><img src="https://ssl.gstatic.com/codesite/ph/images/plus.gif" ></a>
 <a class="ifOpened" onclick="return _toggleHidden(this)"><img src="https://ssl.gstatic.com/codesite/ph/images/minus.gif" ></a>
 <a href="/p/toadlet/source/detail?spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d&r=b506d90b37aa5391b9c6ac5050a364071ee7c975">b506d90b37aa</a>
 by Andrew Fischer &lt;and...@lightningtoads.com&gt;
 on Dec 8, 2011
 &nbsp; <a href="/p/toadlet/source/diff?spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d&r=b506d90b37aa5391b9c6ac5050a364071ee7c975&amp;format=side&amp;path=/cmake/Modules/LocateStaticLibraries.cmake&amp;old_path=/cmake/Modules/LocateStaticLibraries.cmake&amp;old=053fa1665701a61bca6fcafa21dbda3f8d70a4e4">Diff</a>
 <br>
 <pre class="ifOpened">LocateStaticLibraries now accepts an
optional 3rd argument. If FALSE
(default),
then import libraries are OK on
windows. If TRUE, then only fully
...</pre>
 </div>
 
 <div class="closed" style="margin-bottom:3px;" >
 <a class="ifClosed" onclick="return _toggleHidden(this)"><img src="https://ssl.gstatic.com/codesite/ph/images/plus.gif" ></a>
 <a class="ifOpened" onclick="return _toggleHidden(this)"><img src="https://ssl.gstatic.com/codesite/ph/images/minus.gif" ></a>
 <a href="/p/toadlet/source/detail?spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d&r=053fa1665701a61bca6fcafa21dbda3f8d70a4e4">053fa1665701</a>
 by Andrew Fischer &lt;and...@lightningtoads.com&gt;
 on Dec 8, 2011
 &nbsp; <a href="/p/toadlet/source/diff?spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d&r=053fa1665701a61bca6fcafa21dbda3f8d70a4e4&amp;format=side&amp;path=/cmake/Modules/LocateStaticLibraries.cmake&amp;old_path=/cmake/Modules/LocateStaticLibraries.cmake&amp;old=b257d36d70a1465d38c07a12807531110f456c49">Diff</a>
 <br>
 <pre class="ifOpened">LocateStaticLibraries script now looks
for import libraries on windows and
warns the user if they are being used
for static library linking.</pre>
 </div>
 
 <div class="closed" style="margin-bottom:3px;" >
 <a class="ifClosed" onclick="return _toggleHidden(this)"><img src="https://ssl.gstatic.com/codesite/ph/images/plus.gif" ></a>
 <a class="ifOpened" onclick="return _toggleHidden(this)"><img src="https://ssl.gstatic.com/codesite/ph/images/minus.gif" ></a>
 <a href="/p/toadlet/source/detail?spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d&r=b257d36d70a1465d38c07a12807531110f456c49">b257d36d70a1</a>
 by Andrew Fischer &lt;and...@lightningtoads.com&gt;
 on Dec 2, 2011
 &nbsp; <a href="/p/toadlet/source/diff?spec=svnaddfe68edc48e132f00cbf9741e81cb0c564f20d&r=b257d36d70a1465d38c07a12807531110f456c49&amp;format=side&amp;path=/cmake/Modules/LocateStaticLibraries.cmake&amp;old_path=/cmake/Modules/LocateStaticLibraries.cmake&amp;old=96cdde1f3d5b6c2cd1259a5dbcc3c4dabae8cc0f">Diff</a>
 <br>
 <pre class="ifOpened">Minor cleanup in merge and locate
static library scripts</pre>
 </div>
 
 
 <a href="/p/toadlet/source/list?path=/cmake/Modules/LocateStaticLibraries.cmake&r=762045a80ab42e590097815fa1aab84fdbabc457">All revisions of this file</a>
 </div>
 </div>
 <div class="round1"></div>
 <div class="round2"></div>
 <div class="round4"></div>
 </div>
 
 <div class="pmeta_bubble_bg" style="border:1px solid white">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <div id="fileinfo_bubble">
 <p>File info</p>
 
 <div>Size: 4298 bytes,
 99 lines</div>
 
 <div><a href="//toadlet.googlecode.com/hg/cmake/Modules/LocateStaticLibraries.cmake">View raw file</a></div>
 </div>
 
 </div>
 <div class="round1"></div>
 <div class="round2"></div>
 <div class="round4"></div>
 </div>
 </div>
 </div>


</div>

</div>
</div>


<script src="https://ssl.gstatic.com/codesite/ph/3197964839662303775/js/source_file_scripts.js"></script>

 <script type="text/javascript" src="https://ssl.gstatic.com/codesite/ph/3197964839662303775/js/kibbles.js"></script>
 <script type="text/javascript">
 var lastStop = null;
 var initialized = false;
 
 function updateCursor(next, prev) {
 if (prev && prev.element) {
 prev.element.className = 'cursor_stop cursor_hidden';
 }
 if (next && next.element) {
 next.element.className = 'cursor_stop cursor';
 lastStop = next.index;
 }
 }
 
 function pubRevealed(data) {
 updateCursorForCell(data.cellId, 'cursor_stop cursor_hidden');
 if (initialized) {
 reloadCursors();
 }
 }
 
 function draftRevealed(data) {
 updateCursorForCell(data.cellId, 'cursor_stop cursor_hidden');
 if (initialized) {
 reloadCursors();
 }
 }
 
 function draftDestroyed(data) {
 updateCursorForCell(data.cellId, 'nocursor');
 if (initialized) {
 reloadCursors();
 }
 }
 function reloadCursors() {
 kibbles.skipper.reset();
 loadCursors();
 if (lastStop != null) {
 kibbles.skipper.setCurrentStop(lastStop);
 }
 }
 // possibly the simplest way to insert any newly added comments
 // is to update the class of the corresponding cursor row,
 // then refresh the entire list of rows.
 function updateCursorForCell(cellId, className) {
 var cell = document.getElementById(cellId);
 // we have to go two rows back to find the cursor location
 var row = getPreviousElement(cell.parentNode);
 row.className = className;
 }
 // returns the previous element, ignores text nodes.
 function getPreviousElement(e) {
 var element = e.previousSibling;
 if (element.nodeType == 3) {
 element = element.previousSibling;
 }
 if (element && element.tagName) {
 return element;
 }
 }
 function loadCursors() {
 // register our elements with skipper
 var elements = CR_getElements('*', 'cursor_stop');
 var len = elements.length;
 for (var i = 0; i < len; i++) {
 var element = elements[i]; 
 element.className = 'cursor_stop cursor_hidden';
 kibbles.skipper.append(element);
 }
 }
 function toggleComments() {
 CR_toggleCommentDisplay();
 reloadCursors();
 }
 function keysOnLoadHandler() {
 // setup skipper
 kibbles.skipper.addStopListener(
 kibbles.skipper.LISTENER_TYPE.PRE, updateCursor);
 // Set the 'offset' option to return the middle of the client area
 // an option can be a static value, or a callback
 kibbles.skipper.setOption('padding_top', 50);
 // Set the 'offset' option to return the middle of the client area
 // an option can be a static value, or a callback
 kibbles.skipper.setOption('padding_bottom', 100);
 // Register our keys
 kibbles.skipper.addFwdKey("n");
 kibbles.skipper.addRevKey("p");
 kibbles.keys.addKeyPressListener(
 'u', function() { window.location = detail_url; });
 kibbles.keys.addKeyPressListener(
 'r', function() { window.location = detail_url + '#publish'; });
 
 kibbles.keys.addKeyPressListener('j', gotoNextPage);
 kibbles.keys.addKeyPressListener('k', gotoPreviousPage);
 
 
 }
 </script>
<script src="https://ssl.gstatic.com/codesite/ph/3197964839662303775/js/code_review_scripts.js"></script>
<script type="text/javascript">
 function showPublishInstructions() {
 var element = document.getElementById('review_instr');
 if (element) {
 element.className = 'opened';
 }
 }
 var codereviews;
 function revsOnLoadHandler() {
 // register our source container with the commenting code
 var paths = {'svnaddfe68edc48e132f00cbf9741e81cb0c564f20d': '/cmake/Modules/LocateStaticLibraries.cmake'}
 codereviews = CR_controller.setup(
 {"projectName": "toadlet", "domainName": null, "assetVersionPath": "https://ssl.gstatic.com/codesite/ph/3197964839662303775", "token": null, "profileUrl": null, "assetHostPath": "https://ssl.gstatic.com/codesite/ph", "loggedInUserEmail": null, "relativeBaseUrl": "", "projectHomeUrl": "/p/toadlet"}, '', 'svnaddfe68edc48e132f00cbf9741e81cb0c564f20d', paths,
 CR_BrowseIntegrationFactory);
 
 codereviews.registerActivityListener(CR_ActivityType.REVEAL_DRAFT_PLATE, showPublishInstructions);
 
 codereviews.registerActivityListener(CR_ActivityType.REVEAL_PUB_PLATE, pubRevealed);
 codereviews.registerActivityListener(CR_ActivityType.REVEAL_DRAFT_PLATE, draftRevealed);
 codereviews.registerActivityListener(CR_ActivityType.DISCARD_DRAFT_COMMENT, draftDestroyed);
 
 
 
 
 
 
 
 var initialized = true;
 reloadCursors();
 }
 window.onload = function() {keysOnLoadHandler(); revsOnLoadHandler();};

</script>
<script type="text/javascript" src="https://ssl.gstatic.com/codesite/ph/3197964839662303775/js/dit_scripts.js"></script>

 
 
 
 <script type="text/javascript" src="https://ssl.gstatic.com/codesite/ph/3197964839662303775/js/ph_core.js"></script>
 
 
 
 
</div> 

<div id="footer" dir="ltr">
 <div class="text">
 <a href="/projecthosting/terms.html">Terms</a> -
 <a href="http://www.google.com/privacy.html">Privacy</a> -
 <a href="/p/support/">Project Hosting Help</a>
 </div>
</div>
 <div class="hostedBy" style="margin-top: -20px;">
 <span style="vertical-align: top;">Powered by <a href="http://code.google.com/projecthosting/">Google Project Hosting</a></span>
 </div>

 
 


 
 </body>
</html>

