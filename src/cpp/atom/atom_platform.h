/**
 * @file platform.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 6, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef PLATFORM_H_
#define PLATFORM_H_

#undef __ANDROID__
#undef __LINUX__
#undef __IOS__
#undef __MACOSX__
#undef __WIN32__
#undef __WP8__

#if defined(linux) || defined(__linux) || defined(__linux__)
#define __LINUX__	1
#endif

#if defined(ANDROID)
#undef  __LINUX__   /*do we need to do this?*/
#define __ANDROID__ 1
#endif

#if defined(__APPLE__)
/* lets us know what version of Mac OS X we're compiling on */
#include "AvailabilityMacros.h"
#include "TargetConditionals.h"
#ifndef MAC_OS_X_VERSION_10_4
#define MAC_OS_X_VERSION_10_4 1040
#endif
#ifndef MAC_OS_X_VERSION_10_5
#define MAC_OS_X_VERSION_10_5 1050
#endif
#ifndef MAC_OS_X_VERSION_10_6
#define MAC_OS_X_VERSION_10_6 1060
#endif
#if TARGET_OS_IPHONE
/* if compiling for iPhone */
#define __IOS__ 1
#undef __MACOSX__
#else
/* if not compiling for iPhone */
#undef __MACOSX__
#define __MACOSX__	1
#endif /* TARGET_OS_IPHONE */
#endif /* defined(__APPLE__) */

#if defined(WIN32) || defined(_WIN32)
#include <winapifamily.h>
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP)
// This code is for Win32 desktop apps
# undef  __WIN32__
# define __WIN32__	1
#else
// This code is for WinRT Windows Store apps
# if defined(WINAPI_FAMILY) && WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP
/// This code is for Windows phone 8
#  undef  __WIN32__
#  define __WP8__ 	1
# endif
#endif

#endif



#endif /* PLATFORM_H_ */
