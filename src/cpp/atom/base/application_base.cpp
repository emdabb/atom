#include "application.h"

namespace atom {

application_base::~application_base() 
{
}

void* application_base::get_display() const 
{
    return nullptr;
}

void* application_base::get_surface() const 
{
    return nullptr;
}

}
