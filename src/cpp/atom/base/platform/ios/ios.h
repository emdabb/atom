#pragma once

#define IOS_VERSION_LOWER_THAN_8    (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1)

#define IOS_IS_LANDSCAPE            (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))

#define SCREEN_WIDTH    (IOS_VERSION_LOWER_THAN_8 \
                        ? (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) \
                        ? [[UIScreen mainScreen] bounds].size.width \
                        : [[UIScreen mainScreen] bounds].size.height) \
                        : [[UIScreen mainScreen] bounds].size.width)

#define SCREEN_HEIGHT   (IOS_VERSION_LOWER_THAN_8 \
                        ? (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) \
                        ? [[UIScreen mainScreen] bounds].size.height \
                        : [[UIScreen mainScreen] bounds].size.width) \
                        : [[UIScreen mainScreen] bounds].size.height)
