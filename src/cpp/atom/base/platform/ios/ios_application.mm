#include "ios.h"
#include "ios_application.h"
#include <iostream>
#import <UIKit/UIKit.h>

using namespace atom;

/**
* @brief 
*/
@interface application_view : UIView 
{
    ios_application* _app;
    NSTimer*         _timer;
    uint64_t         _last_time;
    CGPoint          _last_pos;
}

/**
* @brief 
*/
- (id)      initWithApplication:(ios_application*) application frame:(CGRect)rc;
/**
* @brief 
*/
- (void)    update;
@end

@implementation application_view
+ (Class)layerClass 
{
    return [CAEAGLLayer class];
}

-(id)   initWithApplication : (ios_application*)app_ frame:(CGRect)rect_
{
    if((self = [super initWithFrame:rect_]))
    {
        self.opaque = YES;
        _last_time = 0;
        _app = app_;
    }
    return self;
}

-(void) layoutSubviews { /* todo */ }

-(void) dealloc { /*todo*/ }

-(void) update { /* todo */ }

-(void) onStart { /* todo */ }

-(void) onStop { /* todo */ }
/**
* @brief Handles the start of a touch
*/
- (void) touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event{
    CGRect fr = [self frame];
    CGRect rc = [self bounds];
    UITouch* touch      = [[event touchesForView:self] anyObject];
    CGPoint location    = [touch locationInView:self];
    _last_pos           = location;
    _app->mouse_press(location.x,location.y,0);
}
/**
* @brief Handles the continuation of a touch.
*/
- (void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event{
    UITouch* touch  = [[event touchesForView:self] anyObject];
    CGPoint location= [touch locationInView:self];
    _last_pos   = location;
    _app->mouse_move(location.x,location.y, 0);
}

/**
* @brief Handles the end of a touch event when the touch is a tap.
*/
- (void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event{
    UITouch* touch  =[[event touchesForView:self] anyObject];
    CGPoint location=[touch locationInView:self];
    if(location.x!=_last_pos.x || location.y!=_last_pos.y){
        _app->mouse_move(location.x,location.y, 0);
    }
    _app->mouse_release(location.x,location.y,0);
}
/**
* @brief Handles the end of a touch event.
*/
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch* touch=[[event touchesForView:self] anyObject];
    CGPoint location=[touch locationInView:self];
    if(location.x!=_last_pos.x || location.y!=_last_pos.y){
        _app->mouse_move(location.x,location.y, 0);
    }
    _app->mouse_release(location.x,location.y,0);
}

@end

/**
 * @brief
 */
@interface view_controller : UIViewController
{
@public
    application_view* __view;
    ios_application*  _app;
}
-(CGRect)   getScreenRect;
-(id)       initWithApplication:(ios_application*)application;
@end


@implementation view_controller
-(id) initWithApplication:(ios_application*) application
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    _app = application;
    return self;
}

-(BOOL) preferStatusBarHidden
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    return YES;
}

-(void) loadView
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    self.view = [[UIView alloc] initWithFrame : [UIScreen mainScreen].bounds];
}

-(void) viewDidLoad
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    [super viewDidLoad];
    
    CGRect screenRect = [self getScreenRect];
    __view = [[application_view alloc] initWithApplication: _app frame: screenRect];
    [self.view addSubview : (UIView*)__view];
    
    _app->post_create();
}

-(void) viewDidAppear:(BOOL)animated
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    /* empty */
}

-(CGRect) getScreenRect
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    CGRect screenSize = [UIScreen mainScreen].bounds;
    if(IOS_VERSION_LOWER_THAN_8 && IOS_IS_LANDSCAPE)
    {
        std::swap(screenSize.size.width, screenSize.size.height);
    }
    return screenSize;
}

@end

ios_application::ios_application()
: _win(nullptr)
, _view(nullptr)
, _viewController(nullptr)
, _pool(nullptr)
{
    
}

ios_application::~ios_application() {
    
}

bool  ios_application::create() {
    
    CGRect screenRect = [UIScreen mainScreen].bounds;
    
    if(!_win) {
        
        if(IOS_VERSION_LOWER_THAN_8 && IOS_IS_LANDSCAPE)
            std::swap(screenRect.size.width, screenRect.size.height);
        _win = [[UIWindow alloc] initWithFrame : screenRect];
        
        CGPoint center;
        if(IOS_IS_LANDSCAPE) {
            ((UIWindow*)_win).center = CGPointMake(screenRect.size.height / 2.f, screenRect.size.width / 2.f);
            [(UIWindow*)_win setTransform:CGAffineTransformMakeRotation(M_PI/2.f)];
        }
        else {
            ((UIWindow*)_win).center = CGPointMake(screenRect.size.width / 2.f, screenRect.size.height / 2.f);
        }
    }
    
    _viewController = [(void*)[view_controller alloc] initWithApplication : this];
    
    [(UIWindow*)_win setRootViewController : (view_controller*)_viewController];
    
    [(UIWindow*)_win makeKeyAndVisible];
}
bool  ios_application::destroy() {
    
}
int   ios_application::start() {
    
}
void  ios_application::stop() {
    
}
void  ios_application::move(int32_t, int32_t) {
}
void  ios_application::resize(uint32_t, uint32_t) {
}

void* ios_application::get_display() const {
    return nullptr;
}
void* ios_application::get_surface() const {
    return (CAEAGLLayer*)[(UIView*)_view layer];
}
void  ios_application::set_applet(applet* app) {
    _app = app;
    
}
void  ios_application::mouse_move(int, int, int) {
    
}
void  ios_application::mouse_press(int, int, int) {
    
}
void  ios_application::mouse_release(int, int, int) {
    
}

void ios_application::post_create()
{
    _view = (void*)[((view_controller*)_viewController)->__view retain];
}

const std::string& ios_application::api() const
{
    return "eagl";
}

static struct registerIosApplication {
    registerIosApplication() {
        register_factory<application, ios_application> reg("ios");
    }
    
    ~registerIosApplication() {
        
    }
} _;

