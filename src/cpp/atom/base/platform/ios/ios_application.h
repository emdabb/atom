#pragma once

#include <atom/base/application.h>
#include <atom/base/applet.h>

namespace atom {

class __export ios_application : public application
{
    void* _win;
    void* _view;
    void* _viewController;
    void* _pool;
    applet* _app;
public:
    ios_application();
    virtual ~ios_application();
    
    virtual bool create();
    virtual bool destroy();

    virtual int start();
    virtual void stop();

    virtual void move(int32_t, int32_t);
    virtual void resize(uint32_t, uint32_t);

    virtual void* get_display() const;
    virtual void* get_surface() const;
    virtual void set_applet(applet*);
    
    virtual const std::string& api() const;

    void mouse_move(int, int, int);
    void mouse_press(int, int, int);
    void mouse_release(int, int, int);
    
    void post_create();
};

}
