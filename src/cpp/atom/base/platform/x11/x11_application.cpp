#include <atom/base/application.h>
#include <atom/base/applet.h>
#include <atom/core/factory.h>
#include <atom/core/rect.h>

//#include <X11/Xlib.h>
//#include <X11/Xutil.h>
#include <cassert>
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <chrono>
#include <X11/Xlib-xcb.h>

namespace atom {

struct graphics_context;
struct graphics_device;

namespace impl {

class x11_application 
: public application 
{
    Display*    _dpy;
    Window      _win;
    Cursor      _blank;
    rect        _rc;
    bool        _running;
    applet*     _app;
    graphics_device*    _device;
    graphics_context*   _context;
protected:
    
    void error(const char* format, ...) {
        printf("error: ");
        va_list va;
        va_start(va, format);
        vprintf(format, va);
        va_end(va);
        printf("\n");
        exit(1);
    }
    
    bool create_window() 
    {
        if((_dpy = XOpenDisplay(getenv("DISPLAY"))) == NULL) {
            error("failed to open X display.");
        }

        int screen = DefaultScreen(_dpy);

        _win = XCreateSimpleWindow(
            _dpy, 
            RootWindow(_dpy, screen), 
            _rc.x, _rc.y,
            _rc.w, _rc.h,
            1,
            BlackPixel(_dpy, screen),
            WhitePixel(_dpy, screen)
        );

        XSelectInput(_dpy, _win, ExposureMask | KeyPressMask);

        XMapWindow(_dpy, _win);

        return true;
    }

    void create_cursor() 
    {
        static char data[1] = {0};
        XColor color;
        Pixmap pixmap   = XCreateBitmapFromData(_dpy, _win, data, 1, 1);
        _blank          = XCreatePixmapCursor(_dpy, pixmap, pixmap, &color, &color, 0, 0);
        XFreePixmap(_dpy, pixmap);
    }

    void events() 
    {
    }

    virtual void set_applet(applet* app_) {
        _app = app_;
    }

    int run() 
    {
        using clock = std::chrono::high_resolution_clock;
        using milliseconds = std::chrono::milliseconds;
        using std::chrono::duration_cast;

        milliseconds dt = milliseconds(16);
        
        auto start  = clock::now();
        auto end    = clock::now();
        uint64_t diff = duration_cast<milliseconds>(end - start).count();

        auto accum_start = clock::now();
        while(_running){

            events();

            start = clock::now();
            
            diff = duration_cast<milliseconds>(end - start).count();
            


            if(duration_cast<milliseconds>(clock::now() - accum_start) >= dt){
                // do render updates every 60th of a second
                accum_start = clock::now();
            }

            end = clock::now();
        } 
        return 0;
    }

public:
    x11_application()
    : _dpy(NULL)
    , _win(None)
    , _blank(None)
    , _rc({-1, -1, 0, 0})
    , _running(false)
    , _app(NULL)
 
    {     
    }

    ~x11_application() 
    {
        if(_blank)
            XFreeCursor(_dpy, _blank);
        
        if(_dpy)
            XSync(_dpy, true);

        if(_win)
            XDestroyWindow(_dpy, _win);

        if(_dpy)
            XCloseDisplay(_dpy);
    }

    virtual bool create(const std::string& api_) 
    {
        
        try {
            create_window();
            create_cursor();
                      
            _context = factory<graphics_context>::instance().create(api_);
            
            _device  = factory<graphics_device>::instance().create(api_);
            
        } catch(...) {
            return false;
        }
        return true;
    }
    
    virtual bool destroy()
    {
       return false; 
    }

    virtual void* get_display() const 
    {
        return static_cast<void*>(_dpy);
    }
    virtual void* get_surface() const 
    {
        return reinterpret_cast<void*>(_win);
    }

    virtual void move(int32_t x_, int32_t y_) 
    {
        _rc.x = x_;
        _rc.y = y_;
        if (_win != None) {
            XMoveWindow(_dpy, _win, x_, y_);
            XFlush(_dpy);
        }
    }

    virtual void resize(uint32_t w_, uint32_t h_) {
        _rc.w = w_;
        _rc.h = h_;
        if (_win != None) {
            XResizeWindow(_dpy, _win, w_, h_);
            XFlush(_dpy);
        }
    } 
    
    virtual int start() {
        _running = true;
        return this->run();
    } 

    virtual void stop() {
        this->_running = false;
    }

    virtual void load_plugin(const std::string& fn) {
        /**
        void* lib = dlopen(fn.c_str());
        pfn_createGraphicsContext pfn_context = (pfnCreateGraphicsContext)dlsym(lib, "create_graphics_context");
        pfn_createGraphicsDevice  pfn_device  = (pfnCreateGraphicsDevice) dlsym(lib, "create_graphics_device");
        pfn_getApiString pfn_api              = (pfnGetApiString)dlsym("api_string");
       
        std::string api = (*pfn_api)();
        
        register_plugin(api, new graphics_plugin(pfn_context, pfn_device));
        */
    }

    virtual void register_plugin(const std::string& api_, graphics_plugin* plugin_)
    {
        //register_factory<graphics_context, graphics_context> reg()
        factory<graphics_context>::instance().register_class(api_, plugin_->_create_context);
        factory<graphics_device>::instance().register_class(api_, plugin_->_create_device);
    }

    virtual graphics_context& get_graphics_context() {
        return *_context;
    }
    virtual graphics_device&  get_graphics_device() 
    {
        return *_device;
    }

};

static struct add_x11_application_factory {
    add_x11_application_factory() 
    {
        register_factory<application, x11_application> reg("x11");
    }

    ~add_x11_application_factory()
    {
    }
} _;

}

}
