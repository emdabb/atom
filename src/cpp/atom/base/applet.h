#pragma once

namespace atom {

struct applet {
    virtual ~applet() = default;

    virtual bool create() = 0;
    virtual bool destroy() = 0;
    virtual void update(float, float) = 0;
    virtual void draw() = 0;
};

}
