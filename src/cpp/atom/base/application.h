#pragma once

#include <atom/core/factory.h>
#include <atom/core/event_handler.h>

namespace atom {
struct graphics_device;
struct graphics_context;

struct applet;

struct engine {
    graphics_context* _context;
    graphics_device* _device;
};

struct graphics_plugin {
    using context_fn_t = graphics_context*(*)();
    using device_fn_t  = graphics_device*(*)();

    context_fn_t _create_context;
    device_fn_t _create_device;

    graphics_plugin(context_fn_t create_context_, device_fn_t create_device_) 
    : _create_context(create_context_)
    , _create_device(create_device_)
    {
    }

    ~graphics_plugin() 
    {
    }
};

struct application {
    virtual ~application() = default;
    /**
    * @brief 
    *
    * @return 
    */
    virtual bool create(const std::string&) = 0;
    
    /**
    * @brief 
    *
    * @return 
    */
    virtual bool destroy() = 0;
     
    /**
    * @brief 
    *
    * @return 
    */
    virtual int start() = 0;
    virtual void stop() = 0;

    virtual void move(int32_t, int32_t) = 0;
    virtual void resize(uint32_t, uint32_t) = 0;

    virtual void* get_display() const = 0;
    virtual void* get_surface() const = 0;
    virtual void set_applet(applet*) = 0;
    
    virtual const std::string& api() const = 0;

    virtual void load_plugin(const std::string&) = 0;
    virtual void register_plugin(const std::string&, graphics_plugin*) = 0;

    virtual graphics_context& get_graphics_context() = 0;
    virtual graphics_device&  get_graphics_device() = 0;

    event_handler<void, int16_t, int16_t> on_mouse_press;
};

struct application_base : public application {
    virtual ~application_base();
    virtual void* get_display() const;
    virtual void* get_surface() const;
};

typedef factory<application> application_factory;

}
