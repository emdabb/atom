#ifndef ATOM_CONFIG_H_
#define ATOM_CONFIG_H_

#cmakedefine _DEBUG
#cmakedefine NDEBUG

#define SYSTEM_NAME     "@CMAKE_SYSTEM_NAME@"
#define PACKAGE_NAME    "@PROJECT_NAME@"
#define VERSION         "@VERSION@"


#ifndef             ATOM_PLATFORM_POSIX
#cmakedefine        ATOM_PLATFORM_POSIX
#endif

#ifndef             ATOM_PLATFORM_ANDROID
#cmakedefine        ATOM_PLATFORM_ANDROID
#endif

#ifndef             ATOM_PLATFORM_IOS
#cmakedefine        ATOM_PLATFORM_IOS
#endif

#ifndef             ATOM_PLATFORM_WINRT
#cmakedefine        ATOM_PLATFORM_WINRT
#endif

#cmakedefine        ANDROID_NDK_API_LEVEL   ${ANDROID_NDK_API_LEVEL}

#cmakedefine        ATOM_FIXED_POINT
#cmakedefine        ATOM_RTTI
#cmakedefine        ATOM_EXCEPTIONS
#cmakedefine        ATOM_HAS_SSE

#endif
