#pragma once

/**
 *
 * Type promotion templates.
 *
 */
template<typename B, typename U = void>
struct promote_type {
    //#ifdef _MSC_VER
    //   typedef promote_typetype_not_specialized_for_thistype type;
    //#endif
};

template<typename U>
struct promote_type<int8_t, U> {
    typedef int16_t type;
};

template<typename U>
struct promote_type<uint8_t, U> {
    typedef uint16_t type;
};

template<typename U>
struct promote_type<int16_t, U> {
    typedef int32_t type;
};

template<typename U>
struct promote_type<uint16_t, U> {
    typedef uint32_t type;
};

template<typename U>
struct promote_type<int32_t, U> {
    typedef int64_t type;
};

template<typename U>
struct promote_type<uint32_t, U> {
    typedef uint64_t type;
};


#if(ATOM_PRECISION == ATOM_PRECISION_FIXED)
#include <atom/core/fixed.h>
namespace atom {
    typedef atom::fixed<int32_t, 16> real;
}
#elif(ATOM_PRECISION == ATOM_PRECISION_SINGLE)
namespace atom {
    typedef float real;
}
#elif(ATOM_PRECISION == ATOM_PRECISION_DOUBLE)
namespace atom {
    typedef double real;
}
#else
namespace atom {
    typedef float real;
}
#endif

