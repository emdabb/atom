#pragma once

namespace atom {
class graphics_device {
    SDL_Window*   _win;
    SDL_GLContext _ctx;
public:
    graphics_device(SDL_Window* win) {
        _ctx = SDL_GL_CreateContext(win);
    }

};
}
