#pragma once

namespace atom {

class sprite_batch {
    struct sprite_info {
        SDL_Texture* tex;
        SDL_Rect     src;
        SDL_Rect     dst;
        SDL_Color    color;
        float        rotation;
    };

    std::vector<sprite_info>    _info;
    SDL_Renderer*               _ren;
public:
    sprite_batch(SDL_Renderer*);
    void begin();
    void draw(const SDL_Texture*, const SDL_Rect& dst, SDL_Rect* src, const SDL_Color& color);
    void drawString();
    void end();
protected:
    void flush() {

        SDL_Texture* tex = NULL)
        unsigned int begin = 0;
        for(unsigned int i=0; i < _info.size(); i++) {
            if(tex != _info[i].tex) {
                if(i > begin) {
                    renderBatch(tex, begin, i - begin);
                }
                tex   = _info[i].tex;
                begin = i;
            }
        }
        drawBatch(tex, begin, _info.size() - begin);
        _info.clear();
    }
    void drawBatch(SDL_Texture* tex, unsigned int beg, unsigned int end) {
    }
};

}
