#pragma once

#include <atom/atom.h>

namespace atom {

struct graphics_context {
    virtual ~graphics_context() = default;

    virtual bool create(void*, void*) = 0;
    virtual bool destroy() = 0;
    virtual void swap_buffers() = 0;
    virtual bool bind() = 0;
};

__apicall __export graphics_context* new_gfx_context();
}
