#include <atom/atom.h>
#include <atom/gfx/graphics_context.h>
#include <atom/core/factory.h>
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#include <OpenGLES/EAGL.h>
#include <OpenGLES/EAGLDrawable.h>

#import <UIKit/UIKit.h>
#include <QuartzCore/QuartzCore.h>

namespace atom {
namespace impl {

class eagl_graphics_context
: public graphics_context
{
    CAEAGLLayer*    _surface;
    EAGLContext*    _context;
    GLuint          _fbo;
    GLuint          _depthid;
    GLuint          _colorid;
public:
    virtual bool create(void* dpy, void* surface) {
        _surface = (__bridge CAEAGLLayer*)surface;

        int width = [_surface bounds].size.width;
        int height= [_surface bounds].size.height;

        _surface.drawableProperties = [
            NSDictionary dictionaryWithObjectsAndKeys :
                [NSNumber numberWithBool:YES],
                kEAGLDrawablePropertyRetainedBacking,
                kEAGLColorFormatRGBA8, // kEAGLColorFormatRGB565
                kEAGLDrawablePropertyColorFormat,
                nil
        ];

        EAGLRenderingAPI api = kEAGLRenderingAPIOpenGLES2;
        
        _context = [[EAGLContext alloc] initWithAPI:api];

        [EAGLContext setCurrentContext:_context];
        
        glGenFramebuffers(1, &_fbo);
        glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
        glGenRenderbuffers(1, &_colorid);
        glBindRenderbuffer(GL_RENDERBUFFER, _colorid);
        [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:_surface];
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorid);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
        glGenRenderbuffers(1, &_depthid);
        glBindRenderbuffer(GL_RENDERBUFFER, _depthid);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depthid);
        glCheckFramebufferStatus(GL_FRAMEBUFFER);
        
    }
    virtual bool destroy() {
        if(_context != nil) {
            EAGLContext* oldContext = [EAGLContext currentContext];
            if(oldContext != _context) {
                [EAGLContext setCurrentContext:_context];
            }

            if(_fbo != 0) {
                glDeleteFramebuffers(1, &_fbo);
            }

            if(_colorid !=0 ) {
                glDeleteRenderbuffers(1, &_colorid);
            }
            if(_depthid != 0) {
                glDeleteRenderbuffers(1, &_depthid);
            }

            if(oldContext != _context) {
                [EAGLContext setCurrentContext:oldContext];
            }
            [_context release];
            _context = nil;
        }
    }
    virtual void swap_buffers() {
        glBindRenderbuffer(GL_RENDERBUFFER, _colorid);
        return [_context presentRenderbuffer:GL_RENDERBUFFER];
    }
    virtual bool bind() {
        bool res = [EAGLContext setCurrentContext:_context] == YES;
        if(res != false) 
            glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
        return res;
    } 
};
    
static struct __export registerEaglContextFactory {
    registerEaglContextFactory()
    {
        register_factory<graphics_context, eagl_graphics_context> reg("eagl");
    }
} _ ;

}
}

__apicall __export atom::graphics_context* create_eagl_graphics_context()
{
    return new atom::impl::eagl_graphics_context;
}
