#include <atom/gfx/gfx_context.h>
#include <EGL/egl.h>
#include <cstdarg>
#include <stdexcept>

namespace atom 
{

class egl_graphics_context : public gfx_context {
    EGLContext  _context;
    EGLDisplay  _dpy;
    EGLSurface  _surface;
    EGLConfig   _config;
protected:
    void error_fatal(const char* format, ...) 
    {
        printf("error: ");

        va_list va;
        va_start(va, format);
        vprintf(format, va);
        va_end(va);

        printf("\n");
//        exit(1);
        throw std::runtime_error(format);
    }

public:
    egl_graphics_context()
    {
    }

    virtual egl_graphics_context()
    {
        destroy();
    }

    virtual bool create(void* dpy, void* surface) 
    {
        const EGLint egl_config_attribs[] = {
            EGL_COLOR_BUFFER_TYPE,     EGL_RGB_BUFFER,
            EGL_BUFFER_SIZE,           32,
            EGL_RED_SIZE,              8,
            EGL_GREEN_SIZE,            8,
            EGL_BLUE_SIZE,             8,
            EGL_ALPHA_SIZE,            8,

            EGL_DEPTH_SIZE,            24,
            EGL_STENCIL_SIZE,          8,

            EGL_SAMPLE_BUFFERS,        0,
            EGL_SAMPLES,               0,

            EGL_SURFACE_TYPE,          EGL_WINDOW_BIT,
            EGL_RENDERABLE_TYPE,       EGL_OPENGL_ES2_BIT,

            EGL_NONE,
        };

        const EGLint egl_context_attribs[] = {
            EGL_CONTEXT_CLIENT_VERSION, 2,
            EGL_NONE,
        };

        const EGLint egl_surface_attribs[] = {
            EGL_RENDER_BUFFER, EGL_BACK_BUFFER,
            EGL_NONE,
        };

        EGLint ignore;
        EGLBoolean ok;

        const EGLint api = EGL_OPENGL_ES_API;

        ok = eglBindAPI(api);
        if (!ok)
            error_fatal("eglBindAPI(0x%x) failed", api);

        _dpy = eglGetDisplay(dpy);
        if (_dpy == EGL_NO_DISPLAY)
            error_fatal("eglGetDisplay() failed");

        ok = eglInitialize(_dpy, &ignore, &ignore);
        if (!ok)
            error_fatal("eglInitialize() failed");

        EGLint configs_size = 256;
        EGLConfig* configs = new EGLConfig[configs_size];
        EGLint num_configs;
        ok = eglChooseConfig(
            _dpy,
            egl_config_attribs,
            configs,
            configs_size, // num requested configs
            &num_configs); // num returned configs
        if (!ok)
            error_fatal("eglChooseConfig() failed");
        if (num_configs == 0)
            error_fatal("failed to find suitable EGLConfig");
        _config = configs[0];
        delete [] configs;

        _context = eglCreateContext(
            _dpy,
            _config,
            EGL_NO_CONTEXT,
            egl_context_attribs);
        if (!_context)
            error_fatal("eglCreateContext() failed");

#if defined(ATOM_PLATFORM_ANDROID)
        EGLint nativeVisualId;
        if(!eglGetConfigAttrib(_dpy, _config, EGL_NATIVE_VISUAL_ID, &nativeVisualId)) {
            throw std::runtime_error("eglGetConfigAttrib() failed!");
        }
        ANativeWindow_setBuffersGeometry((ANativeWindow*)surface, 0, 0, nativeVisualId);
#endif

        _surface = eglCreateWindowSurface(
            _dpy,
            _config,
            surface,
            egl_surface_attribs);
        if (!surface)
            error_fatal("eglCreateWindowSurface() failed");

        ok = eglMakeCurrent(_dpy, _surface, _surface, _context);
        if (!ok)
            error_fatal("eglMakeCurrent() failed");

        // Check if surface is double buffered.
        EGLint render_buffer;
        ok = eglQueryContext(
            _dpy,
            _context,
            EGL_RENDER_BUFFER,
            &render_buffer);
        if (!ok)
            error_fatal("eglQueyContext(EGL_RENDER_BUFFER) failed");
        if (render_buffer == EGL_SINGLE_BUFFER)
            printf("warn: EGL surface is single buffered\n");

    }

    virtual bool destroy() 
    {
    }

    virtual void swap_buffers()
    {
        eglSwapBuffers(_dpy, _surface);
    }

    virtual void bind() 
    {
        return eglMakeCurrent(_dpy, _surface, _surface, _context) != EGL_FALSE;
    }
    
};

    static struct registerEglGraphicsContext {
        registerEglGraphicsContext() {
            register_factory<graphics_context, egl_graphics_context> reg("egl");
        }
    };
    
}

