#ifndef ATOM_CORE_LEXICAL_CAST_H_
#define ATOM_CORE_LEXICAL_CAST_H_

#include <string>
#include <sstream>

namespace atom {

    template <typename T>
    T lexical_cast(const std::string& what) {
        std::stringstream ss(what);
        T res;
        ss >> res >> std::ws;
        return res;
    }

    template <typename T>
    std::string lexical_cast(const T& what) {
        std::stringstream ss;
        ss << what;
        return ss.str();
    }

    template <typename T>
    std::string lexical_cast(T& what) {
        std::stringstream ss;
        ss << what;
        return ss.str();
    }
}

#endif
