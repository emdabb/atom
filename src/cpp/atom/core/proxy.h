#pragma once

namespace atom {

template <typename T>
struct proxy {
    proxy(T* p_) 
    : _p(p_)
    {
    }

    T* operator -> () {
        return _p;
    }
protected:
    T* _p;
};
    
}
