#ifndef PTREE_H_
#define PTREE_H_

template <typename K>
struct basic_ptree {
    typedef K           key_type;
    typedef any         value_type;

    typedef std::map<key_type, value_type> tree_type;

    tree_type _data;

    struct path {
        std::queue<std::string> _p;

        path(const char* str) : path(std::string(str)) {}

        path(const std::string& str) 
        {
            set(str);
        }

        void set(const std::string& str) {
            size_t start = 0;
            while(start < str.size()) {
                size_t pos = str.find(".", start);
                if(pos == std::string::npos) 
                    pos = str.size();
                _p.push(str.substr(start, pos - start));
                start = pos + 1;
            }
        }

        std::string pop() {
            if(_p.empty()) {
                return std::string();
            } 
            key_type res = _p.front();
            _p.pop();
            return res;
        }

        inline const bool empty() const {
            return _p.empty();
        }
    };

    const bool is_valid(const path& path_) const {
        path copy(path_);

        key_type p = copy.pop();
        typename tree_type::const_iterator it = _data.find(p);
        if(it != _data.end()) {
            if(copy.empty()) {
                return true;
            } else {
                const any& a = it->second;
                const basic_ptree* child = a.is_null() ? nullptr : &a.as<const basic_ptree>();
                if(child) {
                    return child->is_valid(copy);
                }
            }
        }
    }

    struct bad_path : std::runtime_error {
        bad_path() : std::runtime_error("bad path") {}
    };
    
    template <typename T>
    const T& get(const path& path_) const {
        path copy(path_);
        key_type p = copy.pop();
        typename tree_type::const_iterator it = _data.find(p);
        if(it != _data.end()) {
            if(copy.empty()) {
                const any& a = it->second;
                bool valid = a.is<T>();
                return a.as<T>();
            } else {
                const any& a = it->second;
                const basic_ptree* child = a.is_null() ? nullptr : &a.as<basic_ptree>();
                if(child) {
                    return child->get<T>(copy);
                } else {
                    throw bad_path();
                }
            }
        } else {
            throw bad_path();
        }
    }

    template <typename T>
    T& get(const path& path_) {
        return const_cast<T&>(static_cast<const basic_ptree*>(this)->get<T>(path_));
    }

    template <typename T>
    void put(const path& path_, const T& value) {
        if(path_.empty()) 
            return;

        path copy(path_);
        key_type p = copy.pop();
        any& a = _data[p];
        if(copy.empty()) {
            a = value;//lexical_cast<T>(value);
            assert(a.is<T>());
        } else {
            basic_ptree* child = a.is_null() ? nullptr : &a.as<basic_ptree>();

            if(!child) {
                a = basic_ptree();
                bool res = a.is<basic_ptree>();
                child = &a.as<basic_ptree>();
            }
            assert(child);
            child->put(copy, value);
        }
    }

    template <typename T>
    void push(const path& path_, const T& value) {
        if(path_.empty()) 
            return;

        path copy(path_);
        key_type p = copy.pop();
        any& a = _data[p];
        if(copy.empty()) {
            std::vector<T>* list = a.is_null() ? nullptr : &a.as<std::vector<T> >();
            if(list) {
                list->push_back(value);
            } else {
                a = std::vector<T>(1, value);
            }
        } else {
//            basic_ptree* child = a.as<basic_ptree*>();
            basic_ptree* child = a.is_null() ? nullptr : &a.as<basic_ptree>();

            if(!child) {
                a = basic_ptree();
                child = a.as<basic_ptree*>();
            }
            assert(child);
            child->push(copy, value);
        }
    }

    cJSON* serialize() {
        cJSON* json = cJSON_CreateObject();

        return json;
    }

};

typedef basic_ptree<std::string> ptree;


#endif

