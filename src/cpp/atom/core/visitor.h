#pragma once

namespace atom {

    struct visitor_base {
        virtual ~visitor_base() = default;
    };

    template <typename... Types>
    struct visitor;

    template <typename T, typename... Types>
    struct visitor<T, Types...> 
    : visitor<Types...> {
        using visitor<Types...>::visit;
        virtual void visit(const T&) = 0;
    };

    template <typename T>
    struct visitor<T> {
        virtual void visit(const T&) = 0;
    };

    struct visitable {
#define VISITABLE()\
        virtual void accept(visitor_base& vis) {\
            accept_as(*this, vis);\
        }
        /*
        virtual void accept(visitor_base& vis) {
            accept_as(*this, vis);
        }
        */
        VISITABLE();
    protected:
        template <typename T, typename... Types>
        void accept_as(T& visited, visitor_base& vis) {
            if(visitor<T, Types...>* p = dynamic_cast<visitor<T, Types...>*>(&vis)) {
                p->visit(visited);
            }
        }
    };

       
}
