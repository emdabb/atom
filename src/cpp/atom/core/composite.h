#pragma once

#include <vector>

namespace atom {

struct component {
    virtual ~component() {
    }
};

template <typename T>
struct leaf {
    
    operator T* () { return _val; }
    
    T* val() {
        return _val;
    }
protected:
    T* _val;
};

struct composite {
    void del(component* c_) {

    }
    void add(component* c_) {
        _elements.push_back(c_);
    }
//protected:
    std::vector<component*> _elements;
};

}
