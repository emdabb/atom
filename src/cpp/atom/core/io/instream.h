#ifndef ATOM_IO_ONSTREAM_H_
#define ATOM_IO_ONSTREAM_H_

#include <atom/net/tcp.h>
#include <iostream>
#include <atom/core/log.h>

namespace atom {
    template <typename T, typename Traits = std::char_traits<T> >
    class basic_inbuffer : public std::basic_streambuf<T, Traits> {
        typedef std::basic_streambuf<T, Traits> this_type;
        typedef typename Traits::int_type int_type;
        const static size_t char_size = sizeof(T);
        const static size_t BUFFER_SIZE = 256;
        T gbuf[BUFFER_SIZE];
        int sockfd;
    public:
        basic_inbuffer()
            : sockfd(0)
        {
            this_type::setg(gbuf, gbuf, gbuf);
        }

        ~basic_inbuffer() {
        }

        int_type underflow() {
            if(this_type::gptr() < this_type::egptr())
                return *this_type::gptr();
            int n;
            if((n = tcp::receive(sockfd, static_cast<void*>(gbuf), BUFFER_SIZE * char_size)) == 0)
                    return Traits::eof();
            this_type::setg(gbuf, gbuf, gbuf + n);
            return *this_type::gptr();
        }
        
        bool listen(uint16_t port) {
            //DEBUG_METHOD();
            return tcp::listen(sockfd, port, nullptr);
        }
    };

    template <typename T, typename Traits = std::char_traits<T> >
    class basic_instream : public std::basic_istream<T, Traits> {
        typedef T char_type;
        typedef std::basic_istream<T, Traits> this_type;
        typedef basic_inbuffer<T, Traits> buf_type;

        buf_type buf;
    public:
        basic_instream()
        : this_type(&buf) 
        , buf(buf_type())          
        {
        }
        
        bool listen(uint16_t port) {
            return buf.listen(port);
        }

        void close() {
        }
        
    };

    typedef basic_instream<char> instream;
}

#endif
