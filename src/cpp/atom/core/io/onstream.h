#ifndef ATOM_CORE_IO_ONSTREAM_H_
#define ATOM_CORE_IO_ONSTREAM_H_

#include <atom/net/tcp.h>
#include <iostream>

namespace atom {

    template <typename T, typename Traits = std::char_traits<T> >
    class basic_onbuffer : public std::basic_streambuf<T, Traits>
    {
        typedef std::basic_streambuf<T, Traits> this_type;
        typedef typename Traits::int_type int_type;
        static const size_t char_size = sizeof(T);
        const static size_t BUFFER_SIZE = 256;
        T pbuf[BUFFER_SIZE];
        int sockfd;
    public:
        basic_onbuffer()
            : sockfd(0)
        {
        }

        ~basic_onbuffer() {
            sync();
        }

        int flush() {
            int n = this_type::pptr() - this_type::pbase();
            if(tcp::send(sockfd, (const void*)pbuf, n * char_size) != n) 
                return Traits::eof();
            this_type::pbump(-n);
            return n;
        }
        
        int_type overflow(int_type c = Traits::eof()) {
            if(c == Traits::eof()) {
                *this_type::pptr() = c;
                 this_type::pbump(1);
            }
            if(flush() == Traits::eof())
                return Traits::eof();
            return c;
        }

        int sync() {
            if(flush() == Traits::eof()) 
                return Traits::eof();
            return 0;
        }

        int& socket() {
            return sockfd;
        }
    };

    template <typename T, typename Traits = std::char_traits<T> >
    class basic_onstream : public std::basic_ostream<T, Traits>  {
        typedef std::basic_ostream<T, Traits> this_type;
        typedef basic_onbuffer<T, Traits> buf_type;

        buf_type buf;
    public:
        basic_onstream()
            : this_type(&buf) 
        {
        }

        ~basic_onstream() 
        {
        }

        void open(const char* ip, uint16_t port) {
            tcp::open(buf.socket(), ip, port);
        }
    };

    typedef basic_onstream<char> onstream;
}

#endif


