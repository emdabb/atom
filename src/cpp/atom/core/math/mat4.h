#ifndef ATOM_CORE_MATH_MAT4_H_
#define ATOM_CORE_MATH_MAT4_H_

//#ifdef ATOM_HAS_SSE
#include <xmmintrin.h>
//#endif

namespace atom {


    static void matmul4_c(const float* m0, const float* m1, float* d);
    struct mat4f {
        union {
            struct {
                float m11, m12, m13, m14;
                float m21, m22, m23, m24;
                float m31, m32, m33, m34;
                float m41, m42, m43, m44;
            };
            float cell[16];
            __m128 row[4];
        };

        mat4f() 
            : m11(1.0f), m12(0.0f), m13(0.0f), m14(0.0f)
              , m21(0.0f), m22(1.0f), m23(0.0f), m24(0.0f)
              , m31(0.0f), m32(0.0f), m33(1.0f), m34(0.0f)
              , m41(0.0f), m42(0.0f), m43(0.0f), m44(1.0f)
        {
        }


        static mat4f ortho_offcenter(float left, float right, float top, float bottom, float near, float far) {
            mat4f res;
            res.m11 = 2.0f / (right - left);
            res.m22 = 2.0f / (top - bottom);
            res.m33 = 2.0f / (far - near);
            res.m41 = (left + right) / (left - right);
            res.m42 = (bottom + top) / (bottom - top);
            res.m43 = near / (near - far);
            return res;
        }

        static mat4f translation(float x, float y, float z) {
            mat4f res;
            res.m41 = x;
            res.m42 = y;
            res.m43 = z;
            return res;
        }

        static mat4f transpose(const mat4f& in) {
            mat4f res;
            res.m11 = in.m11;
            res.m12 = in.m21;
            res.m13 = in.m31;
            res.m14 = in.m41;

            res.m21 = in.m12;
            res.m22 = in.m22;
            res.m23 = in.m32;
            res.m24 = in.m42;

            res.m31 = in.m13;
            res.m32 = in.m23;
            res.m33 = in.m33;
            res.m34 = in.m43;

            res.m41 = in.m14;
            res.m42 = in.m24;
            res.m43 = in.m34;
            res.m44 = in.m44;

            return res;
        }

        static mat4f mul(const mat4f& a, const mat4f& b) {
            mat4f res;
            matmul4_c(&a.cell[0], &b.cell[0], &res.cell[0]);
            return res;
        }

    };

    static inline __m128 lincomb_sse(const __m128 &a, const mat4f &B)
    {
        __m128 result;
        result = _mm_mul_ps(_mm_shuffle_ps(a, a, 0x00), B.row[0]);
        result = _mm_add_ps(result, _mm_mul_ps(_mm_shuffle_ps(a, a, 0x55), B.row[1]));
        result = _mm_add_ps(result, _mm_mul_ps(_mm_shuffle_ps(a, a, 0xaa), B.row[2]));
        result = _mm_add_ps(result, _mm_mul_ps(_mm_shuffle_ps(a, a, 0xff), B.row[3]));
        return result;
    }


    static void matmul4_sse(const mat4f& A, const mat4f& B, mat4f* out) {
        __m128 out0x = lincomb_sse(A.row[0], B);
        __m128 out1x = lincomb_sse(A.row[1], B);
        __m128 out2x = lincomb_sse(A.row[2], B);
        __m128 out3x = lincomb_sse(A.row[3], B);

        out->row[0] = out0x;
        out->row[1] = out1x;
        out->row[2] = out2x;
        out->row[3] = out3x;

    }

}


#endif 
