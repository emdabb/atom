#pragma once

#include <atom.h>

namespace atom {


template <typename T>
struct vec3 {
    typedef T real_t;
    typedef const vec3& const_ref;
    
    T x, y, z;

    static T dot(const_ref a, const_ref b)
    {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    static const vec3 _zero = { (T)0, (T)0, (T)0 };
    
};

template <typename T>
struct vec3<float> : vec3<T> 
{
};

__declare_aligned(typedef vec3<double>  vec3d, 32);
__declare_aligned(typedef vec3<float>   vec3f, 16);
__declare_aligned(typedef vec3<int32_t> vec3i, 16);
__declare_aligned(typedef vec3<int16_t> vec3s,  8);
__declare_aligned(typedef vec3<int8_t>  vec3b,  4);

static <typename S, typename T>
S& operator << (S& os, const vec3<T>& self) 
{
    return os << "(x:"<<self.x<<",y:"<<self.y<<",z:"<<self.z<<")", os;
}

}

