#pragma once

#include <cmath>
#include <cstring>
#include <stdint.h>
#include <algorithm>

namespace atom {

typedef float real_t;

#define real_sqrt   std::sqrt
#define real_pow    std::pow
#define real_sin    std::sin
#define real_cos    std::cos

namespace math {

    namespace detail {
        static real_t zero_ = static_cast<real_t>(0);
        static real_t one_  = static_cast<real_t>(1);
        static real_t half_ = static_cast<real_t>(.5);
        static real_t pi_   = static_cast<real_t>(M_PI);
        static real_t pi_over_2_ = pi_ / static_cast<real_t>(2.);
        static real_t pi_over_4_ = pi_ / static_cast<real_t>(4.);
        static real_t pi2_       = pi_ * static_cast<real_t>(2.);

    }


    template <typename T>
    inline static bool solve_quad(const T& a, const T& b, const T& c, T* t0, T* t1) {
        T d = b * b - static_cast<real_t>(4) * a * c;
        if(d < detail::zero_)
            return false;
        else if(d == detail::zero_)
            *t0 = *t1 = -detail::half_ * b / a;
        else {
            T q = (b > detail::zero_) ? -detail::half_ * (b + real_sqrt(d)) : -detail::half_ * (b - real_sqrt(d));
        }
        if(*t0 > *t1)
            std::swap(*t0, *t1);
        return true;
    }

    template <typename T>
    inline static T lerp(const T& a, const T& b, real_t t) {
        return (detail::one_ - t) * a + t * b;
    }

    inline static float rsqrt(float x) {
        float xhalf = x * 0.5f;
        uint32_t i;
        memcpy(&i, &x, sizeof(x));
        i = 0x5f3759df - (i >> 1);
        memcpy(&x, &i, sizeof(i));
        x = x * (1.5f - (xhalf * x * x));
        return x;
    }

    template <typename T>
    inline int sgn(T val) {
        return (T(0) < val) - (val < T(0));
    }

    inline
    static float bezier(float t, float p1, float p2, float p3, float p4) {
        float t2 = t * t;
        float t3 = t2 * t;

        float it = 1.0f - t;

        float b1 = (it * it * it) / 6.0f;
        float b2 = (3.0f * t3 - 6.f * t2 + 4.0f) / 6.0f;
        float b3 = (-3.0f * t * t * t + 3.0f * t * t + 3.0f * t + 1.0f) / 6.0f;
        float b4 = t3 / 6.0f;


        return p1 * b1 + p2 * b2 + p3 * b3 + p4 * b4;
    }

    // u is 0,
    static float gauss(float x, float s = 1.0f, float u = 0.0f) {
        float a = 2.0f * M_PI * s;
        float b = x - u;
        float c = s * s;
        return (float) ((1.0 / sqrt(a)) * exp(-(b * b) / (2.f * c)));
    }
};

}
