#pragma once

namespace atom {

template <typename S, typename T>
struct prototype {

    typedef S base_t;
    typedef T derived_t;
#if 0    
    static base_t* clone() {
        return new derived_t(*this);
    }
#else
    virtual base_t* clone() = 0;
};

}
