#ifndef ATOM_CORE_LOG_H_
#define ATOM_CORE_LOG_H_

#include <iostream>
#include <string>
#include <time.h>
#include <atom/atom.h>

#define DEBUG_USING_NAMESPACE using namespace atom
#define DEBUG_SET_STREAM(stream) { atom::log::set_stream(stream); }
#define DEBUG_METHOD() atom::log _debugLog(__FUNCTION__);
#define DEBUG_MESSAGE(debug_message) { _debugLog.message(debug_message); }
#define DEBUG_VALUE_OF(variable) { _debugLog.value_of(#variable, variable, false); }
#define DEBUG_VALUE_AND_TYPE_OF(variable) { _debugLog.value_of(#variable, variable, true); }
#define DEBUG_VALUE_OF_COLLECTION(variable) { _debugLog.value_of_collection(#variable, variable, 0, all, false); }
#define DEBUG_VALUE_OF_TOP_COLLECTION(variable, maxCount) { _debugLog.value_of_collection(#variable, variable, maxCount, top, false); }
#define DEBUG_VALUE_OF_BOTTOM_COLLECTION(variable, maxCount) { _debugLog.value_of_collection(#variable, variable, maxCount, bottom, false); }
#define DEBUG_VALUE_AND_TYPE_OF_COLLECTION(variable) { _debugLog.value_of_collection(#variable, variable, 0, all, true); }
#define DEBUG_VALUE_AND_TYPE_OF_TOP_COLLECTION(variable, maxCount) { _debugLog.value_of_collection(#variable, variable, maxCount, top, true); }
#define DEBUG_VALUE_AND_TYPE_OF_BOTTOM_COLLECTION(variable, maxCount) { _debugLog.value_of_collection(#variable, variable, maxCount, bottom, true); }

namespace atom {
    class log {
        static int indentation;
        static std::ostream* stream;
        const std::string context;
#ifdef DEBUG_LOG_ENABLE_TIMING
        const clock_t start_time;
#endif
        void write_timestamp();
        void write_indentation();
        void write_indentation(const char prefix);
    public:     // Methods
        enum list_segment
        {
            all,
            top,
            bottom
        };

        void message(const std::string& message);
        template<class T> void value_of(const std::string& name, const T& value, const bool outputTypeInformation);
        template<class T> void value_of_collection(const std::string& name, const T& collection, const typename T::size_type max, const list_segment segment, const bool outputTypeInformation);
        static void set_stream(std::ostream& stream);
                            public:     // Constructor, Destructor
        log(const std::string& context);
        ~log();
    };

    template<class T> void log::value_of(const std::string& name, const T& value, const bool outputTypeInformation) {
        write_indentation();
        *stream << name;
#ifdef DEBUG_LOG_ENABLE_TYPE_OUTPUT
        if (outputTypeInformation) {
            *stream << "(" << typeid(value).name() << ")";
        }
#endif
        *stream << "=[" << value << "]" << std::endl;
        stream->flush();
    }

    template<typename T> 
    void log::value_of_collection(const std::string& name, const T& collection, const typename T::size_type max, const list_segment segment, const bool outputTypeInformation)
    {
        typedef typename T::size_type size_type;
        const size_type limit = max != 0 ? std::min<T::size_type>(max, collection.size()) : collection.size();
              size_type startIndex = 0;
              switch(segment) {
                  case all:
                  case top:
                      startIndex = 0; 
                      break; 
                  case bottom:
                      startIndex = collection.size() - limit; 
                      break;
              }
              const size_type endIndex = startIndex + limit;
              write_indentation();
              *stream << "collection(" << name; 
#ifdef DEBUG_LOG_ENABLE_TYPE_OUTPUT
              if (outputTypeInformation) {
                  *stream << "(" << typeid(collection).name() << ")";
              }
#endif
              *stream << ", " << collection.size() << " items)" << std::endl; 
              write_indentation();
              *stream << "{" << std::endl;
              if (startIndex != 0) {
                  write_indentation();
                  *stream << "   ..." << std::endl;
              }
              for(size_type i = startIndex; i < endIndex; ++i) {
                  write_indentation();
                  *stream << "   [" << i << "]=[" << collection[i] << "]" << std::endl;
              }
              if (endIndex != collection.size()) {
                  write_indentation();
                  *stream << "   ..." << std::endl;
              }
              write_indentation();
              *stream << "}" << std::endl;
              stream->flush();
    }
}
#endif
