#pragma once

namespace atom {

template <typename T>
struct decorator : public T {
    virtual void decorate() = 0;
};

}
