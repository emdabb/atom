#ifndef ANY_H_
#define ANY_H_

#include <type_traits>
#include <utility>
#include <typeinfo>
#include <string>
#include <cassert>

namespace atom {

struct any {

    template <typename T>
    using storage_type = typename std::decay<T>::type;



    struct base_type {
        virtual ~base_type() = default;
        virtual base_type* clone() const = 0;
    };

    template <typename T>
    struct derived_type : base_type {
        T _value;

        template <typename S>
        derived_type(S&& val) 
        : _value(std::forward<S>(val)) {}

        base_type* clone() const { return new derived_type<T>(_value); }
    };

    base_type* _ptr;

    base_type* clone() const {
        return _ptr ? _ptr->clone() : nullptr; 
    }

    const bool is_null() const { return !_ptr; }

    template <typename S>
    const bool is() const {
        typedef storage_type<S> T;
        return dynamic_cast<derived_type<T>*>(_ptr) != nullptr;
    }

    template <typename S>
    const storage_type<S>& as() const {
        typedef storage_type<S> T;
        auto res = dynamic_cast<derived_type<T>* >(_ptr);
        if(!res)
            throw std::bad_cast();
        return res->_value;
    }

    template <typename S>
    storage_type<S>& as() {
        return const_cast<S&>(static_cast<const any*>(this)->as<S>());
    }

    template <typename S>
    operator S () {
        return as<storage_type<S> >();
    }

    any() : _ptr(nullptr) {}
    any(any& that) : _ptr(that._ptr) {}
    any(const any& that) : _ptr(that.clone()) {}
    any(any&& that) : _ptr(std::move(that._ptr)) {}
    any(const any&& that) : _ptr(that.clone()) {}

    template <typename S>
    any(S&& val) 
    : _ptr(new derived_type<storage_type<S> >(std::forward<S>(val)))
    {

    }


    any& operator = (const any& a) {
        if(_ptr == a._ptr) 
            return *this;
        auto old = _ptr;
        _ptr = a.clone();
        if(old)
            delete old;
        return *this;
    }

    any& operator = (any&& a) {
        if(_ptr == a._ptr) 
            return *this;
        std::swap(_ptr, a._ptr);
        return *this;
    }

    ~any() {
        if(_ptr) {
            delete _ptr;
            _ptr = nullptr;
        }
    }
};
}


#endif

