#pragma once

#include <cstdint>

namespace atom {

struct rect {
    /*
    rect() = delete;
    rect(const rect& other) 
    : x(other.x)
    , y(other.y)
    , w(other.w)
    , h(other.h)
    {
    }
    */

    rect& operator = (const rect& other) {
        using std::swap;
        rect copy(*this);
        swap(*this, copy);
        return *this;
    }

    int32_t  x, y;
    uint32_t w, h;
};

}
