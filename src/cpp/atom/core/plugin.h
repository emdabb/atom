#pragma once

#if defined(MSC_VER)
# include <windows.h>
# define RTLD_NOW
# define dlopen(x, y)   ((void*)::LoadLibrary(y.c_str());
# define dlsym(x, y)    ((void*)::GetProcAddress((HMODULE)x, y.c_str()))
# define dlclose(x)     ((int)::FreeLibrary(x))

#elif defined(__GNUC__)
# include <dlfcn.h>
#endif

namespace atom {

namespace dlib {
    
    std::string error() {
        return std::string(dlerror());
    }
    
    void* open(const std::string& name) {
        void* res = dlopen(name.c_str(), RTLD_NOW);
        if(!res) {
            throw std::runtime_error(error());
        }
    }

    void close(void* lib)
    {
        if(dlclose(lib)) 
            throw std::runtime_error(error());
    }
    
    template <typename T>
    T* sym(void* lib, const std::string& name) {
        T* res = reinterpret_cast<T*>(dlsym(lib, name.c_str()));
        if(!res) {
            throw std::runtime_error(error());
        }
    }
    // sugar
    template <typename T>
    T* load_from(const std::string& lib, const std::string& fun) {
        void* dl = dlib::open(lib);
        T* res   = dlib::sym(dl, fun);
        dlib::close(dl);
        return res;
    }

}

}
