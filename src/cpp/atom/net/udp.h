#ifndef ATOM_NET_UDP_H_
#define ATOM_NET_UDP_H_

#include <string>
#include <stdint.h>

namespace atom {
    namespace udpc {
        bool init(int& socket);
        bool connect(int& socket, const std::string& ip, uint16_t port, int timeout = 5000);
        bool send(int socket, const void*, ssize_t);
        bool recv(int socket, void*, ssize_t&, int timeout = 0);
        bool is_connected(int socket);
        bool close(int& socket);
        void sleep(int ms);
        //void set_reliable(bool);
    }

    namespace udps {
        struct event {
            int type;
            int peer;

            uint8_t data[256];
            ssize_t len;
        };



        enum {
            TYPE_OK,
            TYPE_ERR,
            TYPE_JOIN,
            TYPE_DATA,
            TYPE_LEAVE
        };

        bool poll(int& socket, event& ev, int timeout);
        bool echo(int socket, const void*, ssize_t, int peer);
        bool listen(int& socket, uint16_t port, int clients = 128);
        bool stop(int& socket);
        bool recv(int socket, void*, ssize_t&, int timeout = 0);
        bool send(int socket, const void*, ssize_t, int peer);
        bool broadcast(int socket, const void*, ssize_t);
        bool broadcast_but(int socket, const void*, ssize_t, int peer);
        void sleep(int ms);
	    //void set_reliable(bool);
        namespace sugar {
            bool say(int socket, const void*, ssize_t, int peer);
            bool echo(int socket, const void*, ssize_t, int peer);
            bool shout(int socket, const void*, ssize_t);
        }
    }
}


#endif /* end of include guard: ATOM_NET_UDP_H_ */
