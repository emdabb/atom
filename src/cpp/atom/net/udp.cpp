#include "udp.h"
#include <enet/enet.h>
#include <map>
#include <chrono>
#include <thread>
#include <cstring>

namespace atom {

namespace udpc {
    struct client_t {
        ENetHost*   client;
        ENetPeer*   peer;
        ENetAddress addr;
        ENetEvent   event;
//      ENetPacket* packet;

        bool connected;
        uint8_t data[256];
    };
    


    std::map<int, client_t> clients;
    static int id = 0;

//void set_reliable(bool val) {
//}

bool init(int& socket) {
    static struct once {
        once() {
            fprintf(stderr, "initializing ENet... ");
            if(enet_initialize() != 0) {
                fprintf(stderr, "error!\n");
            }
            fprintf(stderr, "done!\n");
        }

        ~once() {
            fprintf(stderr, "tearing down ENet...\n");
            enet_deinitialize();
        }
    } _;

    socket = ++id;
    client_t& cl = (clients[socket] = clients[socket]);
    cl.client    = enet_host_create(NULL, 1, 2, 0, 0);
    cl.connected = false;

    if(!cl.client)
        return "could not create client.", false;
    return true;
}

bool connect(int& socket, const std::string& ip, uint16_t port, int timeout) {

    if(!udpc::init(socket))
        return fprintf(stderr, "cannot init socket"), false;

    client_t& cl = clients[socket];

    enet_address_set_host(&cl.addr, ip.c_str());
    cl.addr.port = port;

    cl.peer = enet_host_connect(cl.client, &cl.addr, 2, 0);

    if(!cl.peer)
        return fprintf(stderr, "could not connect to server\n"), false;

    fprintf(stderr, "connecting to host...\n");

    if(enet_host_service(cl.client, &cl.event, timeout) > 0)
        if(cl.event.type == ENET_EVENT_TYPE_CONNECT)
            return cl.connected = true;

    return fprintf(stderr, "could not connect to host\n"), false;
}

bool send(int socket, const void* src, ssize_t len) {
    if(socket <= 0)
        return "invalid socket", false;

    client_t& cl = clients[socket];

    if(!cl.connected)
        return "socket is not connected", false;

    if(!len)
        return "invalid data size (0)", false;

    ENetPacket* packet = enet_packet_create(src, len, ENET_PACKET_FLAG_RELIABLE);
    enet_peer_send(cl.peer, 0, packet);

    return true;
}

bool recv(int socket, void* dst, ssize_t& len, int timeout) {

    if(socket <= 0)
        return "invalid socket", false;

    client_t& cl = clients[socket];

    if(!cl.connected)
        return "client is not connected", false;

    if(enet_host_service(cl.client, &cl.event, timeout) > 0) {
        switch(cl.event.type) {
            case ENET_EVENT_TYPE_RECEIVE:
                len = cl.event.packet->dataLength;
                memcpy(dst, cl.event.packet->data, len);
                return true;
            break;
            case ENET_EVENT_TYPE_DISCONNECT:
                cl.connected = false;
                return fprintf(stderr, "disconnected from host.\n"), false;
            break;
            default:
            break;
        }
    }

    return false;
}

bool is_connected(int socket) {
    if(socket <= 0)
        return false;

    client_t& cl = clients[socket];

    return cl.connected;
}

bool close(int& socket) {
    if(socket <= 0)
        return false;

    client_t& cl = clients[socket];

    if(cl.connected) {
        enet_peer_disconnect(cl.peer, 0);
        cl.connected = false;
    }

    enet_peer_reset(cl.peer);

    socket = 0;

    return true;
}

void sleep(int ms) {
    std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

}


namespace udps {
    struct server {
        ENetEvent   event;
        ENetHost*   self;
        int         port;
        int         peers;
        bool        ready;
    };


    std::map<int, server> servers;
    int id = 0;

    //void set_reliable(bool) {
    //}

    bool listen(int& socket, uint16_t port, int peers) {
        socket = ++id;

        server& s = (servers[socket] = servers[socket]);

        ENetAddress addr;
        addr.host   = ENET_HOST_ANY;
        addr.port   = port;

        s.port = port;
        s.peers= peers;
        s.self = enet_host_create(&addr, peers, 2, 0, 0);

        if(!s.self)
            return false;

        fprintf(stderr, "server socket is ready.\n");
        return s.ready = true;
    }

    bool stop(int& socket) {
        if(socket <= 0)
            return false;

        server& s = servers[socket];

        enet_host_destroy(s.self);

        servers.erase(servers.find(socket));

        socket = 0;

        return true;
    }

    bool recv(int socket, void* dst, ssize_t& len, int timeout) {
        if(socket <= 0)
            return false;

        server& s = servers[socket];

        if(s.event.packet && s.event.packet->data && s.event.packet->dataLength > 0) {
            len = s.event.packet->dataLength;
            memcpy(dst, s.event.packet->data, len);
            return true;
        }
        return false;
    }

    bool send(int socket, const void* src, ssize_t len, int peer) {
        if(socket <= 0)
            return false;

        if(!len || !src)
            return false;

        server& s = servers[socket];

        ENetPacket* packet = enet_packet_create(src, len, ENET_PACKET_FLAG_RELIABLE);

        enet_peer_send(&s.self->peers[peer], 0, packet);
        enet_host_flush(s.self);

        return true;
    }

    bool broadcast(int socket, const void* src, ssize_t len) {
        if(socket <= 0)
            return false;

        if(!len || !src)
            return false;

        server& s = servers[socket];

        ENetPacket* packet = enet_packet_create(src, len, ENET_PACKET_FLAG_RELIABLE);
        enet_host_broadcast(s.self, 1, packet);
        enet_host_flush(s.self);

        return true;
    }

    bool broadcast_but(int socket, const void* src, ssize_t len, int but) {
        if(socket <= 0)
            return false;

        if(!len || !src)
            return false;

        server& s = servers[socket];

        ENetPacket* packet = enet_packet_create(src, len, ENET_PACKET_FLAG_RELIABLE);

        for(int i=0; i < s.peers; i++)
            if(i != but)
                enet_peer_send(&s.self->peers[i], 0, packet);

        enet_host_flush(s.self);

        return true;
    }

    bool poll(int& socket, event& e, int timeout) {
        if(socket <= 0)
            return false;

        server& s = servers[socket];

        int res;
        if((res = enet_host_service(s.self, &s.event, timeout)) < 0) {
            e.type = TYPE_ERR;
            return false;
        }

        if(res == 0) {
            //fprintf(stderr, "no events.\n");
            e.type = TYPE_OK;
            return true;
        }

        e.peer = 0;
        e.len  = 0;
        memset(e.data, 0, sizeof(e.data));

        for(int i=0; i < s.peers; i++)
            if(&s.self->peers[i] == s.event.peer)
                e.peer = i;

        if(s.event.type == ENET_EVENT_TYPE_CONNECT) {
            fprintf(stderr, "client connected.\n");
            e.type = TYPE_JOIN;
            return true;
        }
        else if(s.event.type == ENET_EVENT_TYPE_RECEIVE) {
            udps::recv(socket, e.data, e.len, 0);
            if(!e.len) {
                e.type = TYPE_JOIN;
                return true;
            }
            else {
                e.type = TYPE_DATA;
                return true;
            }
        }
        else if(s.event.type == ENET_EVENT_TYPE_DISCONNECT) {
            fprintf(stderr, "client disconnected.");
            e.type = TYPE_LEAVE;
            return true;
        } else {
            fprintf(stderr, "unhandled event.\n");
            e.type = TYPE_OK;
            return true;
        }
        return true;
    }

    bool say(int socket, const void* src, ssize_t len, int peer) {
        return send(socket, src, len, peer);
    }

    bool echo(int socket, const void* src, ssize_t len, int peer) {
        return broadcast_but(socket, src, len, peer);
    }

    bool shout(int socket, const void* src, ssize_t len) {
        return broadcast(socket, src, len);
    }

    static struct once {
        once() {
            if(!enet_initialize())
                return;
            fprintf(stderr, "could not initialize ENet.");
            exit(1);
        }

        ~once() {
            for( auto &it : servers )
                if(it.second.ready) {
                    int socket = it.first;
                    fprintf(stderr, "closing server @port=[%u]\n", it.second.port);
                    stop(socket);
                }
            enet_deinitialize();
        }
    } _;
}
}// atom
