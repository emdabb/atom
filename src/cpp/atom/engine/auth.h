#ifndef AUTH_H_
#define AUTH_H_

#include <string>

namespace auth {

class session;

namespace provider {
    std::string salt();
    std::string hash(const std::string&);
    unsigned long long now();
    unsigned long long timeout();
    std::string serialize(const session&);
}

class session {
    bool    _valid = false;
    size_t  _timestamp = 0;
    std::string _user, _pass, _id, _publickey, _passphrase;
public:
    session() {
        set_publickey(std::string());
    }

    void setup(const std::string& name,
            const std::string& pass,
            const std::string& context = std::string(),
            const std::string& pubkey = std::string()) {
        _user = name;
        _id   = _user + ';' + context;
        _pass = provider::hash(pass);
        set_publickey(pubkey.empty() ? provider::salt() : pubkey);
    }

    void touch() {
        _timestamp = provider::now();
    }

    const bool is_timedout() const {
        return provider::now() > _timestamp + provider::timeout();
    }

    const bool is_valid() const {
        return _valid != false;
    }

    void invalidate() {
        _valid = false;
    }

    void reset() {
        _valid = true;
        _passphrase = _publickey;
        mutate();
    }

    void mutate() {
        touch();
        _passphrase = provider::hash(_passphrase + _pass);
    }

    void set_publickey(const std::string& key) {
        _publickey = key;
        reset();
    }

    const std::string& get_publickey() const {
        return _publickey;
    }

    const std::string& get_passphrase() const {
        return _passphrase;
    }

    const std::string& next_passphrase() const {
        const_cast<session*>(this)->mutate();
        return get_passphrase();
    }

    template <typename T>
    friend inline T& operator << (T& ostream, const session& self) {
        return ostream << provider::serialize(self);
    }
};
} // auth

#endif
