#ifndef ATOM_PROPERTIES_H_
#define ATOM_PROPERTIES_H_

#include <map>
#include <string>
#include <stdexcept>
#include <sstream>
#include <iomanip>
#include <cassert>
#include <vector>
#include <cjson/cJSON.h>

namespace atom {

template <typename T>
T lexical_cast(const std::string& what) {
    std::stringstream ss(what);
    T res;
    ss >> res >> std::ws;
    return res;
}

template <typename T>
std::string lexical_cast(const T& what) {
    std::stringstream ss;
    ss << what;
    return ss.str();
}

template <typename T>
std::string lexical_cast(T& what) {
    std::stringstream ss;
    ss << what;
    return ss.str();
}

class properties {
    typedef std::multimap<std::string, std::string> dictionary_t;
    dictionary_t mDictionary;
public:
    properties() {

    }
    virtual ~properties() {
    }

    
    template <typename T>
    T get(const std::string& key, T defaultValue) const {
        try {
            return get<T>(key);
        } catch(...) {
            return defaultValue;
        }
    }

    template <typename T>
    T get(const std::string& key) const {

        if(!has(key)) {
            throw std::runtime_error(std::string("property not found: ") + key);
        }

        return lexical_cast<T>((*mDictionary.find(key)).second);
    }

    template <typename T>
    void get(const std::string& key, std::vector<T>* res) {
        assert(res);
        res->clear();

        if(!has(key)) {
            throw std::runtime_error(std::string("property not found: ") + key);
        }

        dictionary_t::iterator it, beg, end;
        beg = mDictionary.lower_bound(key);
        end = mDictionary.upper_bound(key);

        for(it = beg; it != end; it++) {
            res->emplace_back((*it).second);
        }
    }

    template <typename T>
    void set(const std::string& key, const T& val) {
        if(!has(key)) {
            add(key, lexical_cast<T>(val));
            return;
        }
        dictionary_t::iterator it, beg, end;
        beg = mDictionary.lower_bound(key);
        end = mDictionary.upper_bound(key);

        for(it = beg; it != end; it++) {
            it->second = lexical_cast<T>(val);
        }
    }

    void add(const std::string& key, const std::string& val) {
        mDictionary.insert(std::pair<std::string, std::string>(key, val));
    }

    const bool has(const std::string& key) const {
        dictionary_t::const_iterator it = mDictionary.find(key);
        return it != mDictionary.end() && mDictionary.size() > 0;
    }
}; // properties
}  // atom

#endif
