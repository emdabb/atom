#pragma once

#include <SDL.h>
#include <atom/core/threadpool.h>
#include <atom/core/event_handler.h>
#include <iostream>
#include "cleanup.hpp"
//struct SDL_Window;
//struct SDL_Renderer;

namespace atom {

struct applet {
    virtual ~applet() = default;
    virtual int create() = 0;
    virtual int destroy() = 0;
    virtual void update(int, int) = 0;
    virtual void draw() = 0;
};

class application {
    applet*         _applet;
    SDL_Window*     _win;
    SDL_Renderer*   _ren;
    bool            _running;
    int             _width;
    int             _height;
    
public:
    application()
    : _applet(nullptr)
    , _win(nullptr)
    , _ren(nullptr)
    , _running(false)
    {
        if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_EVENTS) != 0) {
            std::cout << "SDL_Init() Error: " << SDL_GetError() << std::endl;
            SDL_Quit();
        }
        
        SDL_DisplayMode dm;
        SDL_GetDesktopDisplayMode(0, &dm);
        _width = dm.w;
        _height= dm.h;
    }

    ~application()
    {
        destroy();
    }

    void setApplet(applet* applet_)
    {
        _applet = applet_;
    }

    SDL_Window* getWindow() const {
        return _win;
    }

    SDL_Renderer* getRenderer() const {
        return _ren;
    }
public:
    int start()
    {
        _running = true;
        //auto res = threadpool::instance().enqueue(&application::run, this);
        return run();//res.get();
    }
    void stop()
    {
        _running = false;
    }

    void move(int x, int y) {
        if(_win) {
            SDL_SetWindowPosition(_win, x, y);
        }
    }

    void resize(int w, int h) {
        _width  = w;
        _height = h;
        if(_win) {
            SDL_SetWindowSize(_win, w, h);
        }
    }
    
    int create()
    {
        if(!_applet)
            return 1;

        _win = SDL_CreateWindow(NULL, 0, 0, _width, _height, SDL_WINDOW_OPENGL
#if defined(ATOM_PLATFORM_IOS) | defined(ATOM_PLATFORM_ANDROID)
        | SDL_WINDOW_FULLSCREEN | SDL_WINDOW_BORDERLESS
#else
	| SDL_WINDOW_RESIZABLE
#endif
);

        if(!_win) {
            std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
            SDL_Quit();
            return 1;
        }

        auto gl = SDL_GL_CreateContext(_win);
    
        _ren = SDL_CreateRenderer(_win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        if (_ren == nullptr){
            SDL_DestroyWindow(_win);
            std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
            SDL_Quit();
            return 1;
        }

        return _applet->create();
    }
    void destroy() {
        stop();
        sdl2::cleanup(_win, _ren);
    }
    int  getWidth() { return _width; }
    int  getHeight() { return _height; }
protected:

    void handleWindowEvent(const SDL_WindowEvent& window) {
        switch(window.event) {
        case SDL_WINDOWEVENT_SHOWN:
            SDL_Log("Window %d shown", window.windowID);
            break;
        case SDL_WINDOWEVENT_HIDDEN:
            SDL_Log("Window %d hidden", window.windowID);
            break;
        case SDL_WINDOWEVENT_EXPOSED:
            SDL_Log("Window %d exposed", window.windowID);
            break;
        case SDL_WINDOWEVENT_MOVED:
            SDL_Log("Window %d moved to %d,%d",
                    window.windowID, window.data1,
                    window.data2);
            break;
        case SDL_WINDOWEVENT_RESIZED:
            SDL_Log("Window %d resized to %dx%d",
                    window.windowID, window.data1,
                    window.data2);
                resize(window.data1, window.data2);
            break;
        case SDL_WINDOWEVENT_SIZE_CHANGED:
            SDL_Log("Window %d size changed to %dx%d",
                    window.windowID, window.data1,
                    window.data2);
            break;
        case SDL_WINDOWEVENT_MINIMIZED:
            SDL_Log("Window %d minimized", window.windowID);
            break;
        case SDL_WINDOWEVENT_MAXIMIZED:
            SDL_Log("Window %d maximized", window.windowID);
            break;
        case SDL_WINDOWEVENT_RESTORED:
            SDL_Log("Window %d restored", window.windowID);
            break;
        case SDL_WINDOWEVENT_ENTER:
            SDL_Log("Mouse entered window %d",
                    window.windowID);
            break;
        case SDL_WINDOWEVENT_LEAVE:
            SDL_Log("Mouse left window %d", window.windowID);
            break;
        case SDL_WINDOWEVENT_FOCUS_GAINED:
            SDL_Log("Window %d gained keyboard focus",
                    window.windowID);
            break;
        case SDL_WINDOWEVENT_FOCUS_LOST:
            SDL_Log("Window %d lost keyboard focus",
                    window.windowID);
            break;
        case SDL_WINDOWEVENT_CLOSE:
            SDL_Log("Window %d closed", window.windowID);
            break;
        default:
            SDL_Log("Window %d got unknown event %d",
                    window.windowID, window.event);
            break;
        }
    }

    void internal_GestureEvent(const SDL_MultiGestureEvent& ev) {
    }

    void internal_FingerEvent(const SDL_TouchFingerEvent& ev) 
    {
        SDL_Finger* finger = SDL_GetTouchFinger(ev.touchId, 0);
    }
    
    void internal_MouseMotionEvent(const SDL_MouseMotionEvent& motion) {
        OnMouseMove(motion.x, motion.y, motion.xrel, motion.yrel);
    }
    
    void internal_MouseButtonEvent(const SDL_MouseButtonEvent& button) {
        if(button.type == SDL_MOUSEBUTTONDOWN)
            OnMouseDown(button.x, button.y);
        else 
            OnMouseRelease(button.x, button.y);
    }
    
    void internal_TextInputEvent(const SDL_TextInputEvent& ev) {
        std::cout << "text input: " << ev.text << std::endl;
        OnTextInput(ev.text);
    }
    
    void internal_TextEditingEvent(const SDL_TextEditingEvent& ev) {
                std::cout << "text editing: " << ev.text << std::endl;
        OnTextInput(ev.text);
    }
    
    void internal_KeyPressed(const SDL_KeyboardEvent& ev) {
        if(ev.state == SDL_PRESSED) {
            OnKeyPressed((uint16_t)ev.keysym.sym);
        }
    }

    int processInput() {
        SDL_Event ev;
        SDL_PumpEvents();
        while(SDL_PollEvent(&ev)) {
            switch(ev.type) {
            case SDL_QUIT:
                    SDL_Log("SDL_QUIT");
                SDL_Quit();
                stop();
                break;
            case SDL_WINDOWEVENT:
                handleWindowEvent(ev.window);
                break;
                case SDL_KEYDOWN:
                    internal_KeyPressed(ev.key);
                    break;
            case SDL_TEXTINPUT:
                    internal_TextInputEvent(ev.text);
                break;
            case SDL_TEXTEDITING:
                    internal_TextEditingEvent(ev.edit);
                break;
            case SDL_MULTIGESTURE:
                internal_GestureEvent(ev.mgesture);
                break;
                case SDL_FINGERDOWN:
                case SDL_FINGERUP:
                case SDL_FINGERMOTION:
                    internal_FingerEvent(ev.tfinger);
                    break;
                case SDL_MOUSEMOTION:
                    internal_MouseMotionEvent(ev.motion);
                    break;
                case SDL_MOUSEBUTTONDOWN:
                case SDL_MOUSEBUTTONUP:
                    internal_MouseButtonEvent(ev.button);
                    break;
            }
        }
        return 0;
    }

    int run() {
        int t =0;
        int dt = (int)(1000. / 60.);
        int timeCurrentMs = 0;
        int timeAccumulatedMs = dt;
        int timeDeltaMs = 0;
        int timeLastMs = SDL_GetTicks();

        if(!_applet) 
            return 1;
        
        while(_running) {

                processInput();
                timeCurrentMs = SDL_GetTicks();
                timeDeltaMs = timeCurrentMs - timeLastMs;
                timeAccumulatedMs += timeDeltaMs;
                while(timeAccumulatedMs >= dt) {
                    _applet->update(t, dt);
                    timeAccumulatedMs -= dt;
                    t += dt;
                }
                _applet->draw();
                timeLastMs = timeCurrentMs;
            }
        
        return _applet->destroy();
    }
public:
    event_handler<void(int, int)> OnMouseDown;
    event_handler<void(int, int)> OnMouseRelease;
    event_handler<void(int, int, int, int)> OnMouseMove;
    event_handler<void(std::string)> OnTextInput;
    event_handler<void(uint16_t)> OnKeyPressed;
};

}
