#pragma once

#include <atom/core/factory.h>
#include <atom/core/event_handler.h>

namespace atom {

struct AdEventArgs {
};

class Advertisement {
public:
    virtual ~Advertisement() = default;
    virtual void fetch(const std::string&, const std::string&) = 0;
    virtual void show() = 0;
    virtual void hide() = 0;
    virtual void dismiss() = 0;
    virtual bool isReady() = 0;
    virtual const std::string& getPublisherId() const = 0;
    virtual const std::string& getAppId() const = 0;
public:
    event_handler<void(const AdEventArgs&)> onAdWasFetched;
    event_handler<void(const AdEventArgs&)> onAdWasShown;
    event_handler<void(const AdEventArgs&)> onAdWasHidden;
    event_handler<void(const AdEventArgs&)> onAdWasDismissed;
    event_handler<void(const AdEventArgs&)> onAdIsReady;
};

typedef factory<Advertisement> AdFactory;

} 
