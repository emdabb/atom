#pragma once

#include <atom/core/singleton.h>
#include <stack>
#include <functional>

namespace atom {

template <typename T>
class state
{
public:
    virtual void enter(T&) = 0;
    virtual void update(T&, int, int) = 0;
    virtual void leave(T&) = 0;
};

template <typename T>
struct fsm
{
    typedef state<T> state_type;
    typedef std::stack<state<T>* > stack_type;

    stack_type _stack;
    T& _self;

    explicit fsm(T& self_)
    : _self(self_)
    {
    }

    virtual ~fsm()
    {
        while(_stack.top())
        {
            _stack.top()->leave(_self);
            _stack.pop();
        }
    }

    void push(state_type* state)
    {
        if(_stack.size()) {
            _stack.top()->leave(_self);
            //_stack.pop();
        }
        _stack.push(state);
        _stack.top()->enter(_self);
    }

    void pop()
    {
        if(_stack.size())
        {
            _stack.top()->leave(_self);
            _stack.pop();
        }
        if(_stack.size()) {
            _stack.top()->enter(_self);
        }
    }

    void update(int t, int dt)
    {
        if(_stack.size())
        {
            _stack.top()->update(_self, t, dt);
        }
    }

    bool is_current(state<T>* which) {
        return which == _stack.top();
    }
};

}
