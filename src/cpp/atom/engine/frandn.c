#include "frandn.h"
#include <stdlib.h>

float frandn(float u, float s) {
    float u1, u2, w, mult;
    static float x1, x2;
    static int call = 0;

    if(call) {
        call = !call;
        return (u + s * x2);
    }
    do {
        u1 = ((float)rand() / RAND_MAX) * 2.0f - 1.0f;
        u2 = ((float)rand() / RAND_MAX) * 2.0f - 1.0f;
        w  = u1 * u1 + u2 * u2;
    } while(w >= 1.f || w == 0.0f);

    mult = sqrtf((-2.f * logf(w)) / w);
    x1 = u1 * mult;
    x2 = u2 * mult;

    call = !call;
    return u + s * x1;
}

