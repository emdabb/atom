#include "lua_script.h"
#include "lua_vm.h"
#include "lua_this.h"

extern "C" {

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

namespace atom {

struct lua_restore_stack {
    lua_vm& _vm;
    int     _top;
    lua_restore_stack(lua_vm& vm_)
    : _vm(vm_)
    , _top(-1)
    {
        _top = lua_gettop(static_cast<lua_State*>(_vm));
    }

    ~lua_restore_stack()
    {
        lua_settop(static_cast<lua_State*>(_vm), _top);
    }
};

lua_script::lua_script(lua_vm& vm_)
: _vm(vm_) 
{
    lua_State* state = static_cast<lua_State*>(_vm);
    lua_newtable(state);
    _this = luaL_ref(state, LUA_REGISTRYINDEX);
    lua_restore_stack rs(_vm);
    lua_rawgeti(state, LUA_REGISTRYINDEX, _this);
    lua_pushlightuserdata(state, static_cast<void*>(this));
    lua_rawseti(state, -2, 0);

}

lua_script::~lua_script() 
{
    lua_State* state = static_cast<lua_State*>(_vm);
    lua_restore_stack rs(_vm);
    lua_rawgeti(state, LUA_REGISTRYINDEX, _this);
    lua_pushnil(state);
    lua_rawseti(state, -2, 0);
    
}

bool lua_script::compile(const std::string& src) {
    lua_this this_(_vm, _this);
    return _vm.load(src); 
}

bool lua_script::has(const std::string& fn)
{
    lua_restore_stack rs(_vm);

    lua_State* state = static_cast<lua_State*>(_vm);
    
    lua_rawgeti(state, LUA_REGISTRYINDEX, _this);

    lua_pushstring(state, fn.c_str());
    
    lua_rawget(state, -2);
    lua_remove(state, -2);
    
    return lua_isfunction(state, -1);
}

void lua_script::register_function(const std::string& fn)
{
   int i = -1;

   lua_restore_stack rs(_vm);
}
/**
void lua_script::go(const std::string& fn) {
    lua_State* l = static_cast<lua_State*>(_vm);
    
    lua_rawgeti(l, LUA_REGISTRYINDEX, _this);
    
    lua_pushstring(l, fn.c_str());

    lua_rawget(l, -2);
    
    lua_remove(l, -2);
    
    lua_rawgeti(l, LUA_REGISTRYINDEX, _this);

    if(!lua_isfunction(l, -2)) {
        lua_pop(l, 2);
    } else {
        _vm.call<>(fn);
    }
}
*/
namespace detail {
    template <typename T>
    struct _id {};

    template <typename T>
    inline T* _get(_id<T*>, lua_State* l, const int i) {
        auto t = lua_topointer(l, i);
        return static_cast<T*>(t);
    }


    inline bool _get(_id<bool>, lua_State* l, const int i) {
        return lua_toboolean(l, i) != false;
    }

    inline int _get(_id<int>, lua_State* l, const int i) {
        return static_cast<int>(lua_tointeger(l, i));
    }

    inline unsigned int _get(_id<unsigned int>, lua_State* l, const int i) {
        return static_cast<unsigned>(lua_tointeger(l, i));
    }

    inline float _get(_id<float>, lua_State* l, const int i) {
        return static_cast<float>(lua_tonumber(l, i));
    }

    inline std::string _get(_id<std::string>, lua_State* l, const int i) {
        size_t sz;
        const char* buf = lua_tolstring(l, i, &sz);
        return std::string { buf, sz };
    }



    template <std::size_t... I>
    struct _indices {
        // empty
    };

    template <std::size_t N, std::size_t... I>
    struct _indices_builder : _indices_builder<N-1, N-1, I...> {
        // empty
    };

    template <size_t... I>
    struct _indices_builder<0, I...> {
        using type = _indices<I...>;
    };

    // template <typename... T, std::size_t... N>
    // std::tuple<T> _get_args(lua_State* l, _indices<N...>) {
    //
    // }
}

}
