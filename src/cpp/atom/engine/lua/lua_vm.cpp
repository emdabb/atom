#include "lua_vm.h"
#include "lua_debug.h"
#include <iostream>

namespace atom {

lua_vm::lua_vm()
: _state(nullptr)
, _ok(true)
, _dbg(nullptr)
{
    _state = luaL_newstate();
    lua_atpanic(_state, (lua_CFunction) lua_vm::panic);
}
lua_vm::lua_vm(lua_vm&& other)
: _state(other._state)
{
    other._state = nullptr;
}
lua_vm::~lua_vm()
{
    if(_state == nullptr)
        return;
    lua_close(_state);
}

void lua_vm::panic(lua_State* l) {
    std::cout << "lua ** panic" << std::endl;
}

bool lua_vm::valid() const {
    return _ok != false;
}

bool lua_vm::load(const std::string& buf) {
    int err = 0;
    if((err = luaL_loadbuffer(_state, buf.c_str(), buf.length(), buf.c_str())) == 0) {
        if((err = lua_pcall(_state, 0, LUA_MULTRET, 0)) == 0) {
            return true;
        } else {
            if(_dbg) {
                _dbg->error(err);
            }
        }
    }
    return false;
}

void lua_vm::attach(lua_debug* dbg_) {
    _dbg = dbg_;
}

void lua_vm::push() {

}

void lua_vm::push(const int val) {
    lua_pushinteger(_state, val);
}

void lua_vm::push(const float val) {
    lua_pushnumber(_state, val);
}

void lua_vm::push(const bool val) {
    lua_pushboolean(_state, val);
}

void lua_vm::push(const std::string& val) {
    //lua_pushstring(_state, val.c_str());
    push<const char*>(val.c_str());
}

void lua_vm::push(const char* val) {
    lua_pushstring(_state, val);
}

void lua_vm::error(int err) {
    std::cout << "lua ** error code:"<< err << std::endl;
    if(_dbg != NULL) {
        _dbg->error(err);
    }
}

}
