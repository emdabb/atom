#include "lua_this.h"
#include "lua_vm.h"

extern "C" {

}

using namespace atom;

lua_this::lua_this(lua_vm& vm_, int ref_)
: _ref(0)
, _vm(vm_)
{
     lua_State* state = static_cast<lua_State*>(_vm);
     if(_vm.valid())
     {
         lua_getglobal(state, "this");

         _ref = luaL_ref(state, LUA_REGISTRYINDEX);

         lua_rawgeti(state, LUA_REGISTRYINDEX, ref_);

         lua_setglobal(state, "this");
     }
}

lua_this::~lua_this()
{
    lua_State* state = static_cast<lua_State*>(_vm);
    if(_ref && _vm.valid())
    {
        lua_rawgeti(state, LUA_REGISTRYINDEX, _ref);

        lua_setglobal(state, "this");

        luaL_unref(state, LUA_REGISTRYINDEX, _ref);
    }
}
