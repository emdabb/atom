#ifndef ATOM_ENGINE_LUA_DEBUGGER_H_
#define ATOM_ENGINE_LUA_DEBUGGER_H_

namespace atom {
    class lua_vm;
    class lua_debug {
        lua_vm& _vm;
        int     _mask;
    public:
        explicit lua_debug(lua_vm&);
        ~lua_debug();
        void hook(int);
        void set_count(int);
        void error(int);
    };
}

#endif /* end of include guard: ATOM_ENGINE_LUA_DEBUGGER_H_ */
