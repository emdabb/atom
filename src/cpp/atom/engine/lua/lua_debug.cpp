#include "lua_debug.h"
#include "lua_vm.h"
#include <iostream>
extern "C" {
    #include <lua.h>
}

namespace atom {

lua_debug::lua_debug(lua_vm& vm_)
: _vm(vm_){
    _vm.attach(this);
//    lua_sethook(L, lua_helper::lua_)
}

lua_debug::~lua_debug() {
}

void lua_debug::hook(int) {

}

void lua_debug::set_count(int) {

}

void lua_debug::error(int val) {
    switch(val) {
    case LUA_ERRRUN:
        std::cout << "lua ** run error: ";
        break;
    case LUA_ERRMEM:
        std::cout << "lua ** memory error: ";
        break;
    case LUA_ERRERR:
        std::cout << "lua ** error: ";
        break;
    case LUA_ERRSYNTAX:
        std::cout << "lua ** syntax error: ";
        break;
    case LUA_ERRFILE:
        std::cout << "lua ** file error: ";
        break;
    }
    std::cout << lua_tostring(static_cast<lua_State*>(_vm), -1) << std::endl;
}

}
