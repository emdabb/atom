#pragma once

#include <atom/atom.h>
#include <tuple>
#include <string>

extern "C" {
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
}

namespace atom {

    namespace detail {

        template <typename T>
        static T read_(lua_State*, int);

        template <>
        float read_(lua_State* state_, int i)  {
            return lua_tonumber(state_, i);
        }

        template <>
        int read_(lua_State* state_, int i)  {
            return lua_tointeger(state_, i);
        }

        template <>
        bool read_(lua_State* state_, int i)  {
            return lua_toboolean(state_, i);
        }

        template <>
        std::string read_(lua_State* state_, int i) {
            return std::string(lua_tostring(state_, i));
        }

        template <size_t, typename... Types>
        struct pop_ {
            typedef std::tuple<Types...> type;

            template <typename T>
            static std::tuple<T> worker(const lua_State* L, const int i) {
                return std::make_tuple(read_<T>(L, i));
            }

            template <typename T1, typename T2, typename... Rest>
            static std::tuple<T1, T2, Rest...> worker(const lua_State* L, const int i) {
                std::tuple<T1> head = std::make_tuple(read_<T1>(L, i));
                return std::tuple_cat(head, worker<T2, Rest...>(L, i + 1));
            }

            static type apply(lua_State* L) {
                auto ret = worker<Types...>(L, 1);
                lua_pop(L, sizeof...(Types));
                return ret;
            }
        };

        template <typename T>
        struct pop_<1, T> {
            typedef T type;
            static type apply(lua_State* L) {
                T ret = read_<T>(L, -1);
                lua_pop(L, 1);
                return ret;
            }
        };

        template <typename... Types>
        struct pop_<0, Types...> {
            typedef void type;
            static type apply(lua_State* L) {
                // empty
            }
        };

    }
    class lua_debug;
    class lua_vm
    {
        lua_State*  _state;
        bool        _ok;
        lua_debug*  _dbg;
    public:
        DISALLOW_COPY_AND_ASSIGN(lua_vm);
        lua_vm();
        lua_vm(lua_vm&& other);
        ~lua_vm();

        template <typename... Types>
        typename detail::pop_<sizeof...(Types), Types...>::type
        pop() {
            return detail::pop_<sizeof...(Types), Types...>::apply(*this);
        }

        template <typename... Ret, typename... Args>
        typename detail::pop_<sizeof...(Ret), Ret...>::type
        call(const std::string& fn, const Args&... args) {
            lua_getglobal(_state, fn.c_str());

            const int num_args = sizeof...(Args);
            const int num_rets = sizeof...(Ret);

            push(args...);
            int err = 0;
            if((err = lua_pcall(_state, num_args, num_rets, 0)) != 0) {
                error(err);
            }
            return pop<Ret...>();
        }

        template <typename T, typename... Types>
        void push(T&& value, Types&&... values) {
            push(std::forward<T>(value));
            push(std::forward<Types>(values)...);
        }

        template <typename T>
        T read(int i) const {
            return detail::read_<T>(_state, i);
        }

        void error(int);
        void push();
        void push(const int val);
        void push(const float val);
        void push(const bool val);
        void push(const std::string& val);
        void push(const char* val);
        bool load(const std::string& buf);
        bool valid() const;
        void attach(lua_debug*);

        operator lua_State* () {
            return _state;
        }

        static void panic(lua_State*);
    };


}
