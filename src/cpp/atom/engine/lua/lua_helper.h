#pragma once

namespace atom {
    class lua_vm;

    struct lua_helper {
        static int print(lua_vm&);
        static void hook_call(lua_vm&);
        static void hook_ret(lua_vm&);
        static void hook_line(lua_vm&);
        static void hook_count(lua_vm&);
        static void hook(lua_vm&);
        static void callback(lua_vm&);

    };
}
