#include "lua_helper.h"
#include "lua_vm.h"
#include <iostream>


namespace atom {

int  lua_helper::print(lua_vm& vm_) 
{
    lua_State* L = static_cast<lua_State*>(vm_);

    std::string msg = lua_tostring(L, 1);

    lua_Debug ar = { 0 };

   
    lua_getstack(L, 1, &ar);
    lua_getinfo(L, "Snl", &ar);
    std::cout << "lua ** " << msg << "@" << ar.source << ":" << ar.currentline << std::endl;
    return 0;

}

void lua_helper::hook_call(lua_vm& vm_) 
{
    lua_State* L = static_cast<lua_State*>(vm_);
    std::cout << "-- call stack --" << std::endl;
    lua_Debug ar;

    for(int i=0; lua_getstack(L, i, &ar) != 0; i++) 
        if(lua_getinfo(L, "Snlu", &ar) != 0)
            std::cout << i << " " << ar.namewhat << " " << ar.name << " " << ar.nups << "@" << ar.linedefined << " " << ar.short_src << std::endl;
}

void lua_helper::hook_ret(lua_vm&)
{
}

void lua_helper::hook_line(lua_vm&)
{
}



void lua_helper::callback(lua_vm&)
{
}

}
