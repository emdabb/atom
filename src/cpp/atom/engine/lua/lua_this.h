#pragma once

extern "C" {
#include <lua.h>
}

namespace atom {

class lua_vm;

class lua_this {
    int     _ref;
    lua_vm& _vm;
public:
    lua_this(lua_vm&, int); 
    ~lua_this();
};

}
