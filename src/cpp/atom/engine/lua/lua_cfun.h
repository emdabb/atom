#pragma once

namespace atom {

struct luacfun_base {
    virtual ~lua_cfun_base() {}
    virtual int apply(lua_State*) = 0;
};

namespace detail {
    int _lua_dispatch(lua_State* L) {
        luacfun_base* fun = static_cast<luacfun_base*>(lua_touserdata(L, lua_upvalueindex(1)));
        return fun->apply(L);
    };
}
}

template <int N, typename Ret, typename... Args>
class lua_cfun : public luacfun_base 
{
    using fn_type = std::function<Ret(Args...)>;
    fn_type     _fun;
    std::string _name;
    lua_State** _L;
public:
    DISALLOW_COPY_AND_ASSIGN(lua_cfun);
    lua_cfun(lua_State* &l, const std::string& name, fn_type fun)
    : _fun(fun)
    , _name(name)
    , _L(&l)
    {
    }

    lua_cfun(lua_State* &l, const std::string& name, Ret(*fun)(Args...))
    : lua_cfun(l, name, fn_type{fun})
    {
    }

    lua_cfun(lua_State* &L, const std::string& name, fn_type fun)
    : _fun(fun)
    , _name(name)
    , _L(&L)
    {
        lua_pushlightuserdata(L, (void*)static_cast<luacfun_base*>(this));
        
        lua_pushclosure(L, &detail::_lua_dispatch, 1);

        lua_setglobal(L, name.c_str());
    }

    lua_cfun(lua_cfun&& other) 
    : _fun(other._fun)
    , _name(other._name)
    , _L(other._L)
    {
        *other._L = nullptr;
    }

    ~lua_cfun() 
    {
        if(_L != nullptr && *_L != nullptr) 
        {
            lua_pushnil(*_L);
            lua_setglobal(*_L, _name.c_str());
        } 
    }
};

}
