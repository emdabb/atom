#pragma once

#include "lua_vm.h"
#include <string>

namespace atom {

class lua_vm;

class lua_script {
    lua_vm& _vm;
    int     _this;
public:
    explicit lua_script(lua_vm&);
    virtual ~lua_script();

    void register_function(const std::string& fn);

    bool has(const std::string&);

    bool compile(const std::string&);
    
    template <typename... Ret, typename... Args>
    typename detail::pop_<sizeof...(Ret), Ret...>::type
    go(const std::string& fn, const Args&... args) 
    {
        lua_State* L = static_cast<lua_State*>(_vm);
        lua_rawgeti(L, LUA_REGISTRYINDEX, _this);
        lua_pushstring(L, fn.c_str());
        lua_rawget(L, -2);
        lua_remove(L, -2);

        lua_rawgeti(L, LUA_REGISTRYINDEX, _this);

        if(!lua_isfunction(L, -2)) {
            lua_pop(L, 2);
            return false;
        }

        const int num_args = sizeof...(Args) + 1;
        const int num_rets = sizeof...(Ret);

        _vm.push(args...);
        int err = 0;
        if((err = lua_pcall(L, num_args, num_rets, 0)) != 0) {
            _vm.error(err);
        }
        return _vm.pop<Ret...>();
    }


public:
    // virtual bool init() = 0;
    // virtual bool call(lua_vm&, int) = 0;
    // virtual bool ret(lua_vm&, const string&) = 0;
};

}
