#ifndef STRUTIL_H_
#define STRUTIL_H_

namespace atom {

struct strutil {
    void find_and_replace(std::string& source, const std::string& find, const std::string& replace) {
        for(std::string::size_type i = 0; (i = source.find(find, i)) != std::string::npos;) {
            source.replace(i, find.length(), replace);
            i += replace.length();
        }
    }

    std::string rand_string(size_t size)
    {
        std::string str;
        str.resize(size);

        static const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJK...";
        if (size) {
            //--size;
            for (size_t n = 0; n < size; n++) {
                int key = rand() % (int) (sizeof charset - 1);
                str[n] = charset[key];
            }
        }
        return str;
    }
    
    int get_next_line_and_split(std::istream& is, std::vector<std::string>* res, char delim) {
        res->clear();
        std::string line;
        std::getline(is, line);

        size_t last = 0;
        size_t next = 0;

        while((next = line.find(delim, last)) != std::string::npos) {
            std::string word = line.substr(last, next - last);

            res->push_back(word);
            last = next + 1;
        }
        std::string word = line.substr(last);

        res->push_back(word);
        return (int)res->size() - 1;
    }

    std::array<char, 8> utf8(int c) {
        std::array<char, 8> seq;
        int n = 0;
        if (c < 0x80) n = 1;
        else if (c < 0x800) n = 2;
        else if (c < 0x10000) n = 3;
        else if (c < 0x200000) n = 4;
        else if (c < 0x4000000) n = 5;
        else if (c <= 0x7fffffff) n = 6;
        seq[n] = '\0';
        switch (n) {
            case 6: seq[5] = 0x80 | (c & 0x3f); c = c >> 6; c |= 0x4000000;
            case 5: seq[4] = 0x80 | (c & 0x3f); c = c >> 6; c |= 0x200000;
            case 4: seq[3] = 0x80 | (c & 0x3f); c = c >> 6; c |= 0x10000;
            case 3: seq[2] = 0x80 | (c & 0x3f); c = c >> 6; c |= 0x800;
            case 2: seq[1] = 0x80 | (c & 0x3f); c = c >> 6; c |= 0xc0;
            case 1: seq[0] = c;
        }
        return seq;
    }
};

}

#endif
