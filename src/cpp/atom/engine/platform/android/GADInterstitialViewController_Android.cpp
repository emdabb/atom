#include <jni.h>

static pthread_key_t    mThreadKey;
static JavaVM*          mJavaVM;
static jmethodID        mIdFetch;
static jmethodID        mIdShow;
static jmethodID        mIdDismiss;

JNIEXPORT int JNI_SetupThread() {
    JNI_GetEnv();
    return 1;
}

JNIEXPORT JNIEnv* JNI_GetEnv(void) {
    JNIEnv *env;
    int status = mJavaVM->AttachCurrentThread(&env, NULL);
    if(status < 0) {
        LOGE("failed to attach current thread");
        return 0;
    }

    pthread_setspecific(mThreadKey, (void*) env);

    return env;
}

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void*) {
	JNIEnv* env;
	mJavaVM = vm;

    if((*mJavaVM)->GetEnv(mJavaVM, (void**)&env, JNI_VERSION_1_4) != JNI_OK) {
        return -1;
    }

    if(pthread_key_create(&mThreadKey, JNI_ThreadDestroyed)) {
        return -1;
    }

    JNI_SetupThread();

    return JNI_VERSION_1_4;
}

static void JNI_ThreadDestroyed(void* val) {
    JNIEnv* env = (JNIEnv*)val;
    if(env != NULL) {
        (*mJavaVM)->DetachCurrentThread(mJavaVM);
        pthread_setspecific(mThreadKey, NULL);
    }
}
