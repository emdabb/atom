#ifndef GADBANNER_VIEW_CONTROLLER_H_
#define GADBANNER_VIEW_CONTROLLER_H_

#include <UIKit/UIKit.h>
#include <GoogleMobileAds/GoogleMobileAds.h>

@interface GADBannerViewController : UIViewController <GADBannerViewDelegate>

- (void) viewDidLoad;

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView;

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error;

- (void)adViewWillPresentScreen:(GADBannerView *)bannerView;

- (void)adViewDidDismissScreen:(GADBannerView *)bannerView;

- (void)adViewWillDismissScreen:(GADBannerView *)bannerView;

- (void)adViewWillLeaveApplication:(GADBannerView *)bannerView;

@end

#endif
 
