#include "BannerView.h"

@implementation BannerView

@synthesize banner;

- (void)viewDidLoad {
    NSLog(@"BannerView.viewDidLoad");
    [super viewDidLoad];
}

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    NSLog(@"BannerView.adViewDidReceiveAd");
    [UIView beginAnimations:@"BannerSlide" context:nil];
    bannerView.frame = CGRectMake(0.0,
                                  self.view.frame.size.height -
                                  bannerView.frame.size.height,
                                  bannerView.frame.size.width,
                                  bannerView.frame.size.height);
    [UIView commitAnimations];
}

- (UIViewController *)viewControllerForPresentingModalView {
    //return UIWindow.viewController;
    return self;
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"BannerView.didFailToReceiveAdWithError");
}

- (void)adViewWillPresentScreen:(GADBannerView *)bannerView {
    NSLog(@"BannerView.adViewWillPresentScreen");
}

- (void)adViewDidDismissScreen:(GADBannerView *)bannerView {
    NSLog(@"BannerView.adViewDidDismissScreen");
}

- (void)adViewWillDismissScreen:(GADBannerView *)bannerView {
    NSLog(@"BannerView.adViewWillDismissScreen");
}

- (void)adViewWillLeaveApplication:(GADBannerView *)bannerView {
    NSLog(@"BannerView.adViewWillLeaveApplication");
}

- (void) createAndLoadBanner:(NSString*) str {
    NSLog(@"BannerView.createAndLoadBanner");
    NSLog(@"Loading ad-unit: %@", str);
    
    UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
    UIViewController* rc = [mainWindow rootViewController];
    

    self.banner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
//    self.banner.delegate = self;
    [self.banner setAdUnitID:str];
    [self.banner setRootViewController:rc];
    [self.banner setDelegate:self];
//    [self.banner setNeedsLayout];
    GADRequest* req = [GADRequest request];
    [self.banner loadRequest:req];
}


@end

#include "../Classes/ads.h"

namespace ads {

    class GADBannerAd : public Advertisement {
        BannerView* _view;
    public:
        GADBannerAd() {
            _view = [[BannerView alloc] init];
        }
        
        virtual void request() {
            [_view createAndLoadBanner:[NSString stringWithUTF8String:_appId.c_str()]];
        }
        
    public:
        static Advertisement* create() {
            return new GADBannerAd;
        }
    };
    
    static class RegisterGADBannerAd {
    public:
        RegisterGADBannerAd() {
            atom::factory<Advertisement>::instance().register_class("admob.banner", &GADBannerAd::create);
        }
    } gRegisterGADBannerAd;

}
