#ifndef ACHIEVEMENT_H_
#define ACHIEVEMENT_H_

#include <map>
#include <string>
#include <cstring>
#include <initializer_list>
#include <cjson/cJSON.h>
#include <atom/engine/properties.h>
#include <regex>
#include "variant.h"

using var = variant<std::string, int, float>;

enum class ECondition {
    Never,
    Less,
    Equal,
    LessEqual,
    Greater,
    NotEqual,
    GreaterEqual,
    Always
};


struct condition {
    
    var _v_activationValue;
    std::string _key, _activationValue;
    ECondition _cond;

    cJSON* serialize() {
        cJSON* json = cJSON_CreateObject();
        {
            cJSON_AddStringToObject(json, "key", _key.c_str());
            cJSON_AddStringToObject(json, "activation-value", _activationValue.c_str());
            cJSON_AddNumberToObject(json, "compare", static_cast<int>(_cond));
        }
        return json;
    }
    
    void deserialize(cJSON* json) {
        cJSON* ptr = json->child;

        while(ptr) {
            if(!strcmp(ptr->string, "key")) _key = ptr->valuestring;
            if(!strcmp(ptr->string, "activation-value")) _activationValue = ptr->valuestring;
            if(!strcmp(ptr->string, "compare")) _cond = static_cast<ECondition>(ptr->valueint);
            ptr = ptr->next;
        }
    }

};

struct achievement {
    
    std::string _name;
    std::string _text;
    std::vector<condition> _cond;
    bool _unlocked;

    template <typename T>
    bool compare(const std::string& s1, const std::string &s2, const ECondition comp) {
        
        T a = atom::lexical_cast<T>(s1);
        T b = atom::lexical_cast<T>(s2);
        switch(comp) {

            case ECondition::Always:        return true;
            case ECondition::Greater:       return a > b; 
            case ECondition::GreaterEqual:  return a >= b; 
            case ECondition::Equal:         return a == b;
            case ECondition::Less:          return a < b; 
            case ECondition::LessEqual:     return a <= b;
            case ECondition::NotEqual:      return a != b; 
            case ECondition::Never:         return false;
            default:                        return false;
        }
        
        return false;
    }

    
    achievement& withCondition(const std::string& s1, const ECondition comp, const std::string& s2) {
        condition item;
        item._key               = s1;
        item._activationValue   = s2;
        item._cond              = comp;
        _cond.push_back(item);
        return *this;
    }

    cJSON* serialize() {
        cJSON* json = cJSON_CreateObject();
        cJSON* arr = nullptr;
        {
            cJSON_AddStringToObject(json, "name", _name.c_str());//cJSON_AddStringToObject(json, "key", _key);
            cJSON_AddStringToObject(json, "text", _text.c_str());
            cJSON_AddBoolToObject(json, "unlocked", _unlocked);
            cJSON_AddItemToObject(json, "conditions", arr = cJSON_CreateArray());

            for(auto& c : _cond) 
                cJSON_AddItemToArray(arr, c.serialize());
        }
        return json;
    }
    
    void deserialize(cJSON* json) {
        
        cJSON* ptr = json->child;
        while(ptr) {
            if(!strcmp(ptr->string, "name")) _name=ptr->valuestring;
            if(!strcmp(ptr->string, "text")) _text=ptr->valuestring;
            if(!strcmp(ptr->string, "unlocked")) _unlocked = ptr->valueint;
            if(!strcmp(ptr->string, "conditions")) {
                int n = cJSON_GetArraySize(ptr);
                for(int i=0; i < n; i++) {
                    condition c;
                    c.deserialize(cJSON_GetArrayItem(ptr, i));
                    _cond.push_back(c);
                }
            }


            ptr = ptr->next;
        }
        
        
        
        
        
        
    }
};

struct achieve { 

    template <typename T>
    struct dictionary {
        typedef std::map<std::string, T> type;
    };

    dictionary<achievement*>::type _ach;
    dictionary<std::string>::type _props;
    dictionary<var>::type _vars;

//    atom::properties        _props;
    


    void get_all(std::vector<achievement*>* list) {
        for(auto it = _ach.begin(); it != _ach.end(); it++) {
            achievement* a = (*it).second;
            list->push_back(a);
        }
    }

    void get_unlocked(std::vector<achievement*>* list) {
        for(auto it = _ach.begin(); it != _ach.end(); it++) {
            achievement* a = (*it).second;
            if(a->_unlocked)
                list->push_back(a);
        }
    }

    void check_achievements(std::vector<achievement*>* list) {
        static std::regex integer("(\\+|-)?[[:digit:]]+");
//      static std::regex fp("[-+]?[0-9]*\.?[0-9]+");

        list->clear();
    
        for(auto it = _ach.begin(); it != _ach.end(); it++ ) {

            achievement* a = (*it).second;
            if(!a->_unlocked) {
                int activeProps = 0;
    
                for(auto& c : a->_cond) {
                    
                    std::string& value = _props[c._key];

                    if(std::regex_match(value, integer)) {
                        if(std::regex_match(c._activationValue, integer)) {
                            if(a->compare<int>(value, c._activationValue, c._cond)) {
                                activeProps++;
                            }
                        } else {
                            throw std::runtime_error("invalid types for operand");
                        }
                    } else {
                        if(a->compare<std::string>(value, c._activationValue, c._cond)) {
                            activeProps++;
                        }
                    }
                }
                if(activeProps == a->_cond.size()) {
                    a->_unlocked = true;
                    list->push_back(a);
                }
            }
        }
    }
    
    achievement& def_achievement(const std::string& name) {
        achievement* res = new achievement;
        res->_name      = name;
        res->_unlocked  = false;
        _ach.insert(std::pair<std::string, achievement*>(name, res));
        return *res;
    }

    void def_property(const std::string& key, const std::string& val) {
        _props[key] = val;
    }
    
    void deserialize_properties(cJSON* json) {
        
    }

    cJSON* serialize_properties() {
        cJSON* json = cJSON_CreateObject();

        for(auto it=_props.begin(); it!=_props.end(); it++) {
            cJSON_AddStringToObject(json, (*it).first.c_str(), (*it).second.c_str());
        }

        return json;
    }
    
    void deserialize(cJSON* json) {
        int n = cJSON_GetArraySize(json);
        for(int i=0; i < n; i++) {
            achievement* a = new achievement();
            a->deserialize(cJSON_GetArrayItem(json, i));
            _ach[a->_name] = a;
        }
    }

    cJSON* serialize() {
        cJSON* json = cJSON_CreateArray();
        {
            for(auto it=_ach.begin(); it != _ach.end(); it++) {
               cJSON_AddItemToArray(json, (*it).second->serialize());
            }
        }
        return json;
    }
};
   

#endif
