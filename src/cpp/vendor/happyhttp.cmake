
cmake_minimum_required(VERSION 3.0)

project(happyhttp C CXX)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}/happyhttp
)

set(happyhttp_src
    ${CMAKE_CURRENT_SOURCE_DIR}/happyhttp/happyhttp.cpp
)

add_library(happyhttp ${happyhttp_src})