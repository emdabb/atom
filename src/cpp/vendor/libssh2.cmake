#cmake_minimum_required(VERSION 3.0)

#project(libssh2 C CXX)

#include(ExternalProject)
#ExternalProject_Add(libssh2
#    SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/libssh2/src
#    CONFIGURE_COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/libssh2/configure
#    BUILD_COMMAND ${MAKE}
#)

#include_directories(
#    ${CMAKE_CURRENT_SOURCE_DIR}/libssh2/include
#    ${CMAKE_CURRENT_SOURCE_DIR}/libssh2/src
#)
#file(GLOB libssh2_src
#    ${CMAKE_CURRENT_SOURCE_DIR}/libssh2/src/*.c
#)

#add_library(libssh2 ${libssh2_src})

set(BUILD_EXAMPLES OFF CACHE BOOL "Build libssh2 examples"   FORCE)
set(BUILD_TESTING  OFF CACHE BOOL "Build libssh2 test suite" FORCE)

add_subdirectory(libssh2)
