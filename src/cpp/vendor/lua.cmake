
CMAKE_MINIMUM_REQUIRED(VERSION 3.0)
project (LUA C)

# the following two variables are defined for the use of packages
# that wish to link or compile against lua
set (LUA_INCLUDE_DIRS "${CMAKE_CURRENT_SOURCE_DIR}/lua")
set (LUA_LIBRARIES lua)
file (GLOB LUA_CORE_SRCS
    ${LUA_INCLUDE_DIRS}/*.c
 )

list(REMOVE_ITEM LUA_CORE_SRCS "${LUA_INCLUDE_DIRS}/lbitlib.c")
if(ATOM_PLATFORM_IOS)
    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} -std=gnu99 -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_USE_IOS" )
endif(ATOM_PLATFORM_IOS)

#if(ATOM_PLATFORM_ANDROID)
#    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} -std=gnu99 -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_USE_LINUX" )
    #add_definitions(-Dlua_getlocaledecpoint='.' -DLUA_USE_LINUX)
#endif(ATOM_PLATFORM_ANDROID)

if(ATOM_PLATFORM_X11)
    set(CMAKE_C_FLAGS	"${CMAKE_C_FLAGS} -std=gnu99 -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_USE_LINUX" )
endif(ATOM_PLATFORM_X11)

add_library (liblua STATIC ${LUA_CORE_SRCS})
target_link_libraries (liblua)
install (TARGETS liblua DESTINATION .)
