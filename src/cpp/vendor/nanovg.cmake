cmake_minimum_required(VERSION 3.0)

project(nanovg C)

include_directories(
    nanovg/src
)

file(GLOB nanovg_sources

    nanovg/src/*.c
)

if(ATOM_PLATFORM_IOS)

    add_definitions(-DNANOVG_GLES2_IMPLEMENTATION)
endif(ATOM_PLATFORM_IOS)

add_library(nanovg ${nanovg_sources})

link_libraries(nanovg)
