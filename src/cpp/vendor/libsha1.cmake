cmake_minimum_required(VERSION 3.0)

project(libsha1 C)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}/libsha1
)

set(libsha1_src
    ${CMAKE_CURRENT_SOURCE_DIR}/libsha1/sha1.c
)

add_library(libsha1 ${libsha1_src})
