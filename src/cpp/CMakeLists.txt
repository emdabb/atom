option (ATOM_BUILD_DYNAMIC	"Build dynamic libraries" ON)
option (ATOM_BUILD_STATIC	"Build static libraries" ON)

include_directories(
	${CMAKE_CURRENT_SOURCE_DIR}
)

if(ATOM_PLATFORM_ANDROID)
	set( lib/${ANDROID_ABI})
	set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/${ANDROID_ABI}/${CMAKE_BUILD_TYPE} )
	set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/${ANDROID_ABI}/${CMAKE_BUILD_TYPE} )
	set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin/${ANDROID_ABI}/${CMAKE_BUILD_TYPE} )
	set(ATOM_BUILD_STATIC OFF CACHE BOOL "Build static libraries" FORCE)
    set(ATOM_BUILD_DYNAMIC ON CACHE BOOL "Build dynamic libraries" FORCE)
    set(ATOM_LIBTYPE SHARED)
#    set(BUILD_SHARED_LIBS ON CACHE BOOL FORCE)
#    set(BUILD_STATIC_LIBS OFF CACHE BOOL FORCE)
	option(ATOM_RTTI "Enable RTTI" ON)
	option(ATOM_EXCEPTIONS "Enable exceptions" ON)
	if(ATOM_RTTI)
		message(STATUS "RTTI enabled")
	else(ATOM_RTTI)
		message(STATUS "RTTI disabled")
	endif(ATOM_RTTI)

elseif(ATOM_PLATFORM_IOS)
    message(STATUS "atom ** IOS ARCH=" ${IOS_ARCH} ":" ${CMAKE_BUILD_TYPE} )
	set(ATOM_BUILD_DYNAMIC OFF CACHE BOOL "Build dynamic libraries" FORCE)
    set(BUILD_SHARED_LIBS OFF)
    set(ATOM_LIBTYPE STATIC)
	set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/${CMAKE_CONFIGURATION_TYPE} )
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/${CMAKE_CONFIGURATION_TYPE} )
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin/${CMAKE_CONFIGURATION_TYPE} )
elseif(ATOM_PLATFORM_POSIX)
    set(ATOM_LIBTYPE SHARED)
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/${CMAKE_BUILD_TYPE} )
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/${CMAKE_BUILD_TYPE} )
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin/${CMAKE_BUILD_TYPE} )
endif(ATOM_PLATFORM_ANDROID)

if(ATOM_PLATFORM_WINDOWS_PHONE)
    set(ATOM_LIBTYPE STATIC)
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/${CMAKE_BUILD_TYPE} )
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/${CMAKE_BUILD_TYPE} )
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin/${CMAKE_BUILD_TYPE} )
    link_directories("${CMAKE_CURRENT_SOURCE_DIR}/vendor/angle/winrt/8.1/windowsphone/build/Debug_ARM")
endif(ATOM_PLATFORM_WINDOWS_PHONE)


message(STATUS "atom ** archive output to: " ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY})
message(STATUS "atom ** runtime output to: " ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
message(STATUS "atom ** library output to: " ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})

add_subdirectory(vendor)
add_subdirectory(atom)
add_subdirectory(tests)



