//
//  CBInterstitialDelegate.h
//  bouncy
//
//  Created by emiel Dam on 12/17/15.
//
//

#ifndef CBInterstitialDelegate_h
#define CBInterstitialDelegate_h

#import <UIKit/UIKit.h>
#import <Chartboost/Chartboost.h>

@interface CBInterstitialViewController : UIViewController<ChartboostDelegate>

@end

#endif /* CBInterstitialDelegate_h */
