//
//  InterstitialView.m
//  bouncy
//
//  Created by emiel Dam on 12/3/15.
//
//

#import <Foundation/Foundation.h>
#include "GADInterstitialViewController.h"
#include "GADBase.h"

@implementation AdmobInterstitialViewController

atom::Advertisement* _ad;

- (void)createAndLoadInterstitial:(NSString*)str {
    NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
    NSLog(@"Loading ad-unit: %@", str);
    
    self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:str];
    self.interstitial.delegate = self;
    
    
    GADRequest *request = [GADRequest request];
    // Requests test ads on test devices.
    //request.testDevices = @[@"7a6f80ec7bf34abd68a374108d996d4c"];
    //request.testDevices = @[ @"7a6f80ec7bf34abd68a374108d996d4c" ];
    [self.interstitial loadRequest:request];
}

- (void)presentInterstitial {
    UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
    UIViewController* rc = [mainWindow rootViewController];
    [self.interstitial presentFromRootViewController:rc];
}

/// Called when an interstitial ad request succeeded.
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    NSLog(@"GADInterstitialViewController.interstitialDidReceiveAd");
    atom::AdEventArgs args = { _ad };
    _ad->onAdWasFetched(args);
}

/// Called when an interstitial ad request failed.
- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"GADInterstitialViewController.interstitialDidFailToReceiveAdWithError: %@", [error localizedDescription]);
    [self restoreController];
}

/// Called just before presenting an interstitial.
- (void)interstitialWillPresentScreen:(GADInterstitial *)ad {
    NSLog(@"GADInterstitialViewController.interstitialWillPresentScreen");
}

/// Called before the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"GADInterstitialViewController.interstitialWillDismissScreen");
}

/// Called just after dismissing an interstitial and it has animated off the screen.
- (void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    NSLog(@"GADInterstitialViewController.interstitialDidDismissScreen");
    [self restoreController];
    atom::AdEventArgs args = { _ad };
    _ad->onAdWasDismissed(args);
}

/// Called just before the app will background or terminate because the user clicked on an
/// ad that will launch another app (such as the App Store).
- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {
    NSLog(@"GADInterstitialViewController.interstitialWillLeaveApplication");
}

- (void) restoreController {
    [self.view removeFromSuperview];
}

- (instancetype) initWithInterstitial:(void*)ad {
    _ad = (atom::Advertisement*)ad;
    return super.init;
}

@end




namespace atom {
    
    class GADInterstitialAd : public GADBase {
        AdmobInterstitialViewController* _view;
        bool _isReady;
    public:
        GADInterstitialAd() : _view(nullptr), _isReady(false) {
            _view = [[AdmobInterstitialViewController alloc] initWithInterstitial:(this)];
            this->onAdWasFetched += [&] (const AdEventArgs& args) {
                _isReady = true;
            };
        }
        
        virtual void fetch() {
            std::string adUnit = this->getAdUnitId();
            
            [_view createAndLoadInterstitial:[NSString stringWithUTF8String:adUnit.c_str()]];
        }
        
        virtual void show() {
            [_view presentInterstitial];
        }
        
        virtual void dismiss() {
            _isReady = false;
            [_view restoreController];
        }
        
        virtual bool isReady() {
            return _isReady != false;
        }
        
    public:
        static Advertisement* create() {
            return new GADInterstitialAd;
        }
    };
    
    static class RegisterGADInterstitialAd {
    public:
        RegisterGADInterstitialAd() {
            atom::factory<Advertisement>::instance().register_class("admob.interstitial", &GADInterstitialAd::create);
        }
    } gRegisterGADInterstitialAd;
    
}


