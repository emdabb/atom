//
//  SDL_Shader.c
//  bouncy
//
//  Created by emiel Dam on 12/31/15.
//
//

#include <stdio.h>
#include <SDL.h>
#include "SDL_Ext.h"
#include "SDL_Shader.h"
#include "SDL_VertexBuffer.h"
#if   defined(__IOS__)
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#elif defined(__ANDROID__)
#elif defined(__X11__)
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#endif

struct SDL_Shader {
    GLuint id;
    GLuint vs;
    GLuint ps;
};


C_API void SDL_ShaderSetMatrix4x4(const SDL_Shader* sha, const char* name, float* val) {
    GLint id = glGetUniformLocation(sha->id, name);
    if(id >= 0) {
        glUniformMatrix4fv(id, 1, GL_FALSE, val);
    }
}

C_API int SDL_SetShader(const SDL_Shader* sha) {
    //DEBUG_LOG("enabling shader %d...", sha->id);
    glUseProgram(sha->id);
    //DEBUG_LOG("error: 0x%x\n", glGetError());
     
}

C_API void SDL_ShaderSetTexture(const SDL_Shader* sha, const char* name, int unit, SDL_Texture* tex) {
#if 1
    GLint id = glGetUniformLocation(sha->id, name);
    if(id >= 0) {
        glActiveTexture(GL_TEXTURE0 + unit);
        
        if(SDL_GL_BindTexture(tex, NULL, NULL)) {
            DEBUG_LOG("%s\n", SDL_GetError());
        }
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      
        glUniform1i(id, unit); 
    }
#else
    glActiveTexture(GL_TEXTURE0 + mValue[i].SamplerID);
    mValue[i].TextureRef->getImpl().lock();
    glUniform1i(mHandle, mValue[i].SamplerID);
#endif
    //} else {
    //    glBindTexture(GL_TEXTURE_2D, 0);
    //}
}

int SDL_SetShaderSource(const SDL_Shader* sha, const char* src) {
	   
    const char* pSourceVS[3]= {
	    "#undef 	FRAGMENT	 \n",
        "#define 	VERTEX		1\n",
        src
    };

	const char* pSourcePS[3]= {
    	"#define	FRAGMENT	1\n",
	    "#undef		VERTEX		 \n",
    	src
	};

	glShaderSource(sha->vs, 3, pSourceVS, NULL);
    glShaderSource(sha->ps, 3, pSourcePS, NULL);


	return 0;
}

SDL_Shader* SDL_CreateShader(SDL_Renderer* ren) {
	DEBUG_LOG("%s", "creating shader...\n");
    SDL_Shader* res = (SDL_Shader*)malloc(sizeof(SDL_Shader));
    
    res->id = glCreateProgram();
    
    res->vs = glCreateShader(GL_VERTEX_SHADER);
    res->ps = glCreateShader(GL_FRAGMENT_SHADER);
    
        
    return res;
}

int SDL_GL_CheckError() {
    return glGetError() != GL_NO_ERROR ? -1 : 0;
}

int SDL_GL_CompileShader(GLuint id) {
    GLint isCompiled = GL_FALSE;
    glCompileShader(id);
    glGetShaderiv(id, GL_COMPILE_STATUS, &isCompiled);
    if(isCompiled != GL_TRUE) {
        GLchar errorLog[1024] = { 0 };
        glGetShaderInfoLog(id, 1024, NULL, &errorLog[0]);
        fprintf(stderr, "%s\n", errorLog);
        return -1;
    }
    
    return 0;
}

int SDL_PreCompileShaders(const SDL_Shader* val) {
    return 0;
}

int SDL_PostCompileShaders(const SDL_Shader* val) {
    return 0;
}


int SDL_CompileShader(const SDL_Shader* val) {
	//printf("%s[SDL_ext] %s compiling shader...\n", TERM_BOLD_RED, TERM_DEFAULT);	
//	DEBUG_LOG("pre-compiling shaders...\n");
    if(!SDL_PreCompileShaders(val) ) {
		//DEBUG_LOG("compiling vertex shader...\n");

        if(!SDL_GL_CompileShader(val->vs)) {
			//DEBUG_LOG("compiling fragment shader...\n");

            if(!SDL_GL_CompileShader(val->ps)) {
				//DEBUG_LOG("post-compiling shaders...\n");
                return SDL_PostCompileShaders(val);
            }
        }
    }
    return -1;
}

int SDL_PreLinkShader(const SDL_Shader* val) {

    static const char* SEMANTIC[] = {
            "VertexCoord",
            "BlendWeight",
            "Normal",
            "Color0",
            "Color1",
            "FogCoord",
            "PointSize",
            "BlendIndex0",
            "TexCoord0",
            "TexCoord1",
            "TexCoord2",
            "TexCoord3",
            "TexCoord4",
            "TexCoord5",
            "TexCoord6",
            "TexCoord7"
    };



    for(int i=0; i < SDL_VERTEX_STREAM_MAX; i++) {
        DEBUG_LOG("binding vertex attribute %d to semantic %s\n", i, SEMANTIC[i]);
    
        glBindAttribLocation(val->id, i, SEMANTIC[i]);
        
        if(glGetError() != GL_NO_ERROR) {
			DEBUG_LOG("error binding vertex attribute %d to semantic %s!\n", i, SEMANTIC[i]);
            return -1;
        }
    }
    return 0;
}

int SDL_PostLinkShader(const SDL_Shader* val) {
	DEBUG_LOG("%s", "validating shader...\n");
    GLint isValid = GL_FALSE;
    glValidateProgram(val->id);
    glGetProgramiv(val->id, GL_VALIDATE_STATUS, &isValid);
    if(isValid != GL_TRUE) {
        char errorLog[1024] = { 0 };
        glGetProgramInfoLog(val->id, 1024, NULL, &errorLog[0]);
        fprintf(stderr, "%s\n", errorLog);
        return -1;
    }
    
    return 0;
}

int SDL_LinkShader(const SDL_Shader* val) {


	glAttachShader(val->id, val->vs);
    glAttachShader(val->id, val->ps);


    if(!SDL_PreLinkShader(val)) {
		DEBUG_LOG("%s", "linking shader program...\n");
	        
        glLinkProgram(val->id);
        
        GLint isLinked;
        glGetProgramiv(val->id, GL_LINK_STATUS, &isLinked);
        if(isLinked != GL_TRUE) {
            char errorLog[1024] = { 0 };
            glGetProgramInfoLog(val->id, 1024, NULL, &errorLog[0]);
            fprintf(stderr, "%s\n", errorLog);
            return -1;
        }
        return SDL_PostLinkShader(val);
    }
    return -1;
}


SDL_Shader* SDL_CreateShader_RW(SDL_Renderer* ren) {
    return NULL;
}

void SDL_FreeShader(SDL_Shader* shader) {
    
}
