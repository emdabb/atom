//
//  screens.h
//  coil
//
//  Created by emiel Dam on 11/6/15.
//
//

#ifndef screens_h
#define screens_h

#include "gamescreen.h"
#include <atom/core/event_handler.h>
#include "ads.h"
#include "world.h"
#include <map>
#include <string>
#include <atom/core/math/curve.h>

struct TrailRenderer;


class screen_gameover : public gamescreen {
    atom::Advertisement* ad;
    bool _buttonsAreEnabled;
    char _buf[1024];
protected:
    void saveHighscore(const std::string&);
public:
    screen_gameover();
    virtual ~screen_gameover();
    virtual void update(int dt, bool a, bool b);
    virtual void draw(SDL_Renderer* ren);
public:
    atom::event_handler<void(int)> OnButtonPressed;
};

class GameEntity;

class screen_play : public gamescreen
{
    static const int _spawnTimeout = 3000;
    int _currentTime;
    World _world;
    int _score;
    int mRw, mRh;
public:
    screen_play();
    ~screen_play();
    virtual void load_content();
    virtual void update(int dt, bool a, bool b);
    virtual void draw(SDL_Renderer* ren);
};

class screen_menu : public gamescreen
{
    int _time;
    static const int _timeout;
    float _factor;
    atom::curve2d* _curve;
public:
    screen_menu();
    virtual void update(int dt, bool a, bool b);
    virtual void draw(SDL_Renderer* ren);
public:
    atom::event_handler<void(int)> OnButtonPressed;
};


class screen_splash : public gamescreen
{
    int _currentScreen;
    int _maxScreens;
    int _visibleTimeout;
    int _visibleTime;
public:
    screen_splash();
    virtual ~screen_splash();
    virtual void load_content();
    virtual void update(int dt, bool a, bool b);
    virtual void draw(SDL_Renderer* ren);
};

class screen_highscore : public gamescreen {
    std::map<int, std::string> _list;
public:
    screen_highscore();
    ~screen_highscore();
    virtual void load_content();
    virtual void update(int, bool, bool);
    virtual void draw(SDL_Renderer*);
};


#endif /* screens_h */
