#pragma once

class resolution {
    int mVirtualResolution[2];
    int mResolution[2];
public:
    void setVirtualResolution(int w, int h) {
        mVirtualResolution[0] = w;
        mVirtualResolution[1] = h;
    }
    
    void setResolution(int w, int h) {
        mResolution[0] = w;
        mResolution[1] = h;
    }
    
    void getTransform(int* x, int* y, int* w, int* h) {
        float aspectRatio = (float)mVirtualResolution[0] / (float)mVirtualResolution[1];
        
        int width = mResolution[0];
        int height = (int)(width / aspectRatio + 0.5f);
        
        if(height > mResolution[1]) {
            height = mResolution[1];
            width = (int)(height * aspectRatio + 0.5f);
        }
        
        *x = mResolution[0] - width;
        *y = mResolution[1] - height;
        *w = width;
        *h = height;
        
//        *tx = (float)mVirtualResolution[0] / (float)mResolution[0];
//        *ty = (float)mVirtualResolution[1] / (float)mResolution[1];
    }
};