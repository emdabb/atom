//
//  SDL_Ext.h
//  bouncy
//
//  Created by emiel Dam on 12/18/15.
//
//

#ifndef SDL_Ext_h
#define SDL_Ext_h

#include <SDL.h>

#define TERM_BOLD_RED	"\033[1;31m"
#define TERM_DEFAULT	"\033[0m"



#define DEBUG_LOG(format, ...)	SDL_Debug(__FILE__, __LINE__, format, __VA_ARGS__) //printf("%s [SDL_ext] %s %s" format, TERM_BOLD_RED, TERM_DEFAULT, ##__VA_ARGS__)




struct SDL_VertexBuffer;

#if defined(__cplusplus)
extern "C" {
#endif
    //void SDL_DrawPrimitives(const SDL_PrimitiveType, int, ssize_t, ssize_t);
    //void SDL_DrawIndexedPrimitives(const SDL_PrimitiveType, int, ssize_t);
	void SDL_Debug(const char* file, int line, const char* format, ...);
#if defined(__cplusplus)
}
#endif

#endif /* SDL_Ext_h */
