//
//  screen_menu.cpp
//  coil
//
//  Created by emiel Dam on 11/6/15.
//
//

#include "screens.h"
#include "imgui.h"
#include "asset_manager.h"
#include "graphics.h"
#include <atom/core/event_handler.h>
#include <atom/engine/easing.h>
#include <SDL.h>

using namespace atom;


const int screen_menu::_timeout = 1000;

screen_menu::screen_menu()
    : _time(0)
    , _factor(0.f)
    {
        _time_on = 1000;
        _time_off= 1000;
        
        OnExit += [&] (gamescreen&, gamescreen_manager&) {
            getOwner()->add(new screen_play);
        };
        
//        OnButtonPressed.add(delegate<void(int)>([this] (int id) {
//            this->quit();
//            this->getOwner()->add(new screen_play);
//        }));
        
        _curve = new curve2d;
        _curve->add(0.f, 128.0f, 128.0f);
        _curve->add(1.0f, 64.0f, 64.0f);
        _curve->add(2.0f, 128.0f + 64.f, 64.0f);
        /**
        atom::ad_manager::instance().getAdByName("chartboostInterstitial")->fetch();
        atom::ad_manager::instance().getAdByName("admobIntersitial")->fetch();
        */
    }
    
void screen_menu::update(int dt, bool a, bool b) {
        //if(state() == E_STATE_ACTIVE) {
        _factor = (float)_time / _timeout;
        _time = std::min(_timeout, _time + dt);
        //}
        gamescreen::update(dt, a, b);
    }
    
void screen_menu::draw(SDL_Renderer* ren)
    {
        int rw, rh;
        int tw, th;
        SDL_GetRendererOutputSize(ren, &rw, &rh);
        
        int id = atom::getTextureId("@menu");
        
        int x, y, w, h;

        
        atom::getTextureById(id);
        
        atom::imgui_reset();
        atom::imgui_setAlpha(1.0f - transition());
        
         int bw = rw >> 2;
         int bh = rh / 8;
        
        // int px = (int)(atom::easing::ease_elastic_in_out(1.0f - transition()) * (rw / 2 - bw / 2));
        
        float t = atom::easing::ease_elastic_out(1.0f - transition());
        
        float a0 = -bw;
        float b0 =  bw / 2;
        
        float a1 = rw + bw;
        float b1 = rw - bw - bw / 2;
        
        int a2 = -bh;
        int b2 = rh - bh * 2;
//        float t2 = atom::easing::ease_elastic_out(1.0f - transition());
        int c2 = (int)(a2 + (b2 - a2) * t);
        
        if(imgui_button(1, ren,
                        {
                            (int)b0,//(int)(a0 + (b0 - a0) * t),
                            c2, //rh - bh * 2,
                            bw,
                            bh
                        }, 0, "new game"))
        {
            
            quit();
        }
        
        if(imgui_button(2, ren,
                        {
                            (int)b1,//(int)(a1 + (b1 - a1) * t),
                            c2, //rh - bh * 2,
                            bw,
                            bh
                        }, 0, "highscore"))
        {
            OnExit += [&] (gamescreen&, gamescreen_manager&) {
                getOwner()->add(new screen_highscore);
            };
            quit();
        }
        
        SDL_Color color = { 255, 255, 255, 255 };
        
        SDL_RenderDrawCurve(ren, *_curve, &color, 128);
        
        
        gamescreen::draw(ren);
    }




