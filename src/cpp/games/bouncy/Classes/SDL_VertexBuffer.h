//
//  SDL_VertexBuffer.h
//  bouncy
//
//  Created by emiel Dam on 12/31/15.
//
//

#ifndef SDL_VertexBuffer_h
#define SDL_VertexBuffer_h

#ifndef BUFFER_OFFSET
#define BUFFER_OFFSET(i) ((char *)NULL + (i))
#endif

typedef enum {
    SDL_POINTS,
    SDL_LINES,
    SDL_LINE_LOOP,
    SDL_TRIANGLES,
    SDL_TRIANGLE_STRIP,
    SDL_TRIANGLE_FAN
} SDL_PrimitiveType;

typedef enum {
	SDL_POSITION,
	SDL_BLENDWEIGHT,
	SDL_NORMAL,
	SDL_COLOR0,
	SDL_COLOR1,
	SDL_FOGCOORD,
	SDL_POINTSIZE,
	SDL_BLENDINDEX0,
	SDL_TEXCOORD0,
	SDL_TEXCOORD1,
	SDL_TEXCOORD2,
	SDL_TEXCOORD3,
	SDL_TEXCOORD4,
	SDL_TEXCOORD5,
	SDL_TEXCOORD6,
	SDL_TEXCOORD7,
	SDL_VERTEX_STREAM_MAX
} SDL_VertexElementUsage;

typedef enum {
    SDL_VERTEX_TYPE_FLOAT,
    SDL_VERTEX_TYPE_FLOAT2,
    SDL_VERTEX_TYPE_FLOAT3,
    SDL_VERTEX_TYPE_FLOAT4
} SDL_VertexType;

typedef enum {
    SDL_SINGLE,
    SDL_VECTOR2,
    SDL_VECTOR3,
    SDL_VECTOR4,
    SDL_HALF_VECTOR2,
    SDL_HALF_VECTOR4,
    SDL_COLOR,
    SDL_NORMALIZED_SHORT2,
    SDL_NORMALIZED_SHORT4,
    SDL_SHORT2,
    SDL_SHORT4,
    SDL_BYTE4
} SDL_VertexElementFormat;

struct SDL_Renderer;

struct SDL_VertexBuffer;
typedef struct SDL_VertexBuffer SDL_VertexBuffer;

struct SDL_VertexDeclaration;
typedef struct SDL_VertexDeclaration SDL_VertexDeclaration;

struct SDL_VertexElement;
typedef struct SDL_VertexElement SDL_VertexElement;

#ifdef __cplusplus
extern "C" {
#endif
    
SDL_VertexBuffer* SDL_CreateVertexBuffer(ssize_t);
SDL_VertexDeclaration* SDL_CreateVertexDeclaration(const SDL_VertexElement** el, ssize_t nel, ssize_t stride);
SDL_VertexElement* SDL_CreateVertexElement(int off, SDL_VertexElementFormat format, SDL_VertexElementUsage usage);
int  SDL_LockVertexBuffer(SDL_VertexBuffer*);
int  SDL_UpdateVertexBuffer(const SDL_VertexBuffer*, int offset, ssize_t len, const void* src);
int  SDL_UnlockVertexBuffer(SDL_VertexBuffer*);
void SDL_SetVertexStreamSource(const SDL_VertexBuffer*);
void SDL_SetVertexDeclaration(SDL_VertexBuffer*, const SDL_VertexDeclaration* decl);
void SDL_DrawVertexBuffer(SDL_VertexBuffer* vbo, const SDL_PrimitiveType prim, int start, ssize_t count, ssize_t primCount);
//void SDL_VertexBufferSetData(SDL_VertexBuffer*, const void*, ssize_t);
    
#ifdef __cplusplus
}
#endif



#endif /* SDL_VertexBuffer_h */
