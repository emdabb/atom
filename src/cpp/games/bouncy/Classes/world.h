#pragma once

#define MAX_ENTITIES        1024

#include <SDL.h>
#include <Box2D/Box2D.h>
#include <atom/core/event_handler.h>
#include <atom/engine/easing.h>
#include "vec2.h"
#include "particles.h"

#ifndef PTM_RATIO
#define PTM_RATIO   (40.f)
#endif

#define WORLD_TO_SCREEN(x)  ((x) * (PTM_RATIO))
#define SCREEN_TO_WORLD(x)  ((x) / (PTM_RATIO))

struct World;

typedef atom::delegate<void(World*, intptr_t, SDL_Renderer*)> PFNDEBUGDRAWCALLBACK;
typedef atom::delegate<void(World*, intptr_t)> PFNINPUTCALLBACK;
typedef atom::delegate<void(World*, intptr_t)> PFNTIMEOUTCALLBACK;

namespace sugar {
    template <typename T>
    inline vec2<T> toWorld(const vec2<T>& src) {
        vec2<T> dst;
        dst.x = SCREEN_TO_WORLD(src.x);
        dst.y = SCREEN_TO_WORLD(src.y);
        return dst;
    }

    template <typename T>
    inline vec2<T> toScreen(const vec2<T>& src) {
        vec2<T> dst;
        dst.x = WORLD_TO_SCREEN(src.x);
        dst.y = WORLD_TO_SCREEN(src.y);
        return dst;
    }
}

enum {
    E_COMPONENT_NIL         = 0,
    E_COMPONENT_VISUAL      = 1<<0,
    E_COMPONENT_TRANSFORM   = 1<<1,
    E_COMPONENT_PHYSICS     = 1<<2,
    E_COMPONENT_COLLIDER    = 1<<3,
    E_COMPONENT_NESTED      = 1<<4,
    E_COMPONENT_INPUT       = 1<<5,
    E_COMPONENT_PARTICLE_EMITTER = 1 << 6,
    E_COMPONENT_TIMEOUT     = 1<<7,
    E_COMPONENT_CAMERAFOLLOW= 1<<8,
	E_COMPONENT_TWEEN		= 1<<9,
	E_COMPONENT_TEXT		= 1<<10
};

struct CollisionEventArgs {
    World* world;
    intptr_t self, other;
    vec2f n;
};

struct Collider {
    atom::event_handler<void(const CollisionEventArgs&)> OnCollisionEnter;
    atom::event_handler<void(const CollisionEventArgs&)> OnCollisionLeave;

    b2Fixture* fixture;
};

struct Physics {
    b2Body* body;
};

struct Nested {
    int parent;
};

struct Transform {
    vec2f pos;
    float rot;
};

struct Visual {
    SDL_Color   color;
    int         texid;
};

struct Text {
    char str[256];
    int  fontId;
};

struct Tween {
//	atom::delegate<const TweenEventArgs&> ease;
    atom::easing::easing_t ease;
    float* dst;
    float a, b;
};

struct Timeout {
    PFNTIMEOUTCALLBACK onTimeout;
    int time;
    int maxTime;
};

class CollisionHandler;

struct World {
    CollisionHandler* collisions;
    b2World*          sim;
    uint32_t          mask[MAX_ENTITIES];
    Transform         transform[MAX_ENTITIES];
    Physics           physics[MAX_ENTITIES];
    Collider          collider[MAX_ENTITIES];
    PFNDEBUGDRAWCALLBACK   drawDebug[MAX_ENTITIES];
    PFNINPUTCALLBACK  input[MAX_ENTITIES];
    Timeout           timeout[MAX_ENTITIES];
    Visual            visual[MAX_ENTITIES];
    Nested            nested[MAX_ENTITIES];
    bool              cleanup[MAX_ENTITIES];
    particles_base*   particles[MAX_ENTITIES];
    b2Joint*          joint[MAX_ENTITIES];
    Text              text[MAX_ENTITIES];
    Tween             tween[MAX_ENTITIES];
    vec2f cameraPos;
};

void World_clear(World*);
void World_update(World*, int);
void World_draw(NVGcontext*);

intptr_t World_createEntity(World* world);
void World_cleanup(World* world);
void World_destroyEntity(World* world, intptr_t id) ;
bool World_hasComponent(World* world, intptr_t id, uint32_t mask);

class CollisionHandler : public b2ContactListener {
    World* _world;
public:
    explicit CollisionHandler(World*);
    virtual ~CollisionHandler();

    virtual void BeginContact(b2Contact* contact);

    virtual void EndContact(b2Contact* contact);

    virtual void BeginContact(b2ParticleSystem* particleSystem, b2ParticleBodyContact* particleBodyContact);

    virtual void EndContact(b2Fixture* fixture, b2ParticleSystem* particleSystem, int32 index);

    virtual void BeginContact(b2ParticleSystem* particleSystem, b2ParticleContact* particleContact);

    virtual void EndContact(b2ParticleSystem* particleSystem, int32 indexA, int32 indexB);
};

