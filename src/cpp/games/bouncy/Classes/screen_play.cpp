//
//  screen_play.cpp
//  coil
//
//  Created by emiel Dam on 11/6/15.
//
//


#include <SDL.h>
#include "screens.h"
#include "sprite_font.h"
#include "asset_manager.h"
#include "input.h"
#include "particle_spawn.h"
#include <atom/core/math/math.h>
#include "http_request.h"
#include <cjson/cJSON.h>
#include <iostream>


#define RESTITUTION_TERRAIN         (0.1f)
#define RESTITUTION_CARWHEEL        (0.1f)
#define RESTITUTION_CARBODY         (0.3f)
#define FRICTION_TERRAIN            (1.f)
#define FRICTION_CARWHEEL           (1.f)
#define FRICTION_CARBODY            (0.5f)
#define DENSITY_CARBODY             (128.0f)
#define DENSITY_CARWHEEL            (16.0f)
#define MAX_MOTOR_TORQUE            (128.0f)
#define IDLE_MOTOR_TORQUE           (64.f)
#define MAX_MOTOR_SPEED             (8.0f * M_PI)


extern SDL_Renderer* g_Renderer;




enum {
    E_FILTER_NONE       = 0,
    E_FILTER_TERRAIN    = 1 << 0,
    E_FILTER_CARBODY    = 1 << 1,
    E_FILTER_CARWHEEL   = 1 << 2,
    E_FILTER_SAFEZONE   = 1 << 3,
    E_FILTER_ALL        = -1
};

#define MASK_DEFAULT    (E_COMPONENT_PHYSICS | E_COMPONENT_COLLIDER | E_COMPONENT_VISUAL)

struct trail {
    static trail defaultValue;
};

trail trail::defaultValue = {} ;

static void update_trail(particles<trail>* group, particles<trail>::particle& p, int, int) {

}

SDL_Rect cameraRect = {
    0, 0, 640, 480
};

static b2Vec2 rotate(const b2Vec2& p, const b2Vec2& at, float t) {
    b2Vec2 res;
    float c = cos((double)t);
    float s = sin((double)t);

    res.x = at.x + (p.x * c - p.y * s);
    res.y = at.y + (p.x * s + p.y * c);

    return res;
}

static void b2DrawEdgeShape(World* world, intptr_t id, SDL_Renderer* vg) {
    if((world->mask[id] & E_COMPONENT_VISUAL) != E_COMPONENT_VISUAL) {
        return;
    }

    Collider* c  = &world->collider[id];
    Visual* v    = &world->visual[id];

    //nvgBeginPath(vg);

    SDL_SetRenderDrawColor(vg, v->color.r, v->color.g, v->color.b, v->color.a);

    b2Fixture* ptr = c->fixture;
    while(ptr) {

        b2EdgeShape* shape = (b2EdgeShape*)ptr->GetShape();

        int x1, y1, x2, y2;
        x1 = WORLD_TO_SCREEN(shape->m_vertex1.x) - cameraRect.x + cameraRect.w / 2;
        y1 = WORLD_TO_SCREEN(shape->m_vertex1.y) - cameraRect.y + cameraRect.h / 2;
        x2 = WORLD_TO_SCREEN(shape->m_vertex2.x) - cameraRect.x + cameraRect.w / 2;
        y2 = WORLD_TO_SCREEN(shape->m_vertex2.y) - cameraRect.y + cameraRect.h / 2;

        SDL_Rect rc = {
            x1 - 2, y1 - 2,
            4,
            4
        };

        SDL_RenderDrawLine(vg, x1, y1, x2, y2);
        SDL_RenderDrawRect(vg, &rc);

        ptr = ptr->GetNext();
    }
//    nvgStrokeWidth(vg, 2.0f);
//    nvgStrokeColor(vg, v->color);
//    nvgStroke(vg);
}

static void b2DrawPolygonShape(World* world, intptr_t id, SDL_Renderer* vg) {

    if((world->mask[id] & E_COMPONENT_VISUAL) != E_COMPONENT_VISUAL) {
        return;
    }

    Collider* c  = &world->collider[id];
    Visual* v    = &world->visual[id];

    SDL_SetRenderDrawColor(vg, v->color.r, v->color.g, v->color.b, v->color.a);

    b2PolygonShape* shape = (b2PolygonShape*)c->fixture->GetShape();
    int n = shape->GetVertexCount();

    b2Body* body = world->physics[id].body;

    b2Vec2 worldPos = body->GetPosition();
    worldPos.x -= SCREEN_TO_WORLD(cameraRect.x - cameraRect.w / 2);
    worldPos.y -= SCREEN_TO_WORLD(cameraRect.y - cameraRect.h / 2);

    float r = body->GetAngle();

    for(int i=0; i < n - 1; i++) {
        const b2Vec2& v0 = rotate(shape->GetVertex(i + 0), worldPos, r);
        const b2Vec2& v1 = rotate(shape->GetVertex(i + 1), worldPos, r);

        int x1, y1, x2, y2;
        x1 = (int)(WORLD_TO_SCREEN(v0.x) + 0.5f);
        y1 = (int)(WORLD_TO_SCREEN(v0.y) + 0.5f);
        x2 = (int)(WORLD_TO_SCREEN(v1.x) + 0.5f);
        y2 = (int)(WORLD_TO_SCREEN(v1.y) + 0.5f);

        SDL_RenderDrawLine(vg, x1, y1, x2, y2);

        //nvgLineTo(vg, WORLD_TO_SCREEN(v0.x), WORLD_TO_SCREEN(v0.y));
    }

    //nvgLineTo(vg, WORLD_TO_SCREEN(v1.x), WORLD_TO_SCREEN(v1.y));

    //nvgStroke(vg);
}

void SDL_RenderDrawCircle(SDL_Renderer* ren, int x, int y, int r, float rot) {
    static const int n = 15;

    float rad = (M_PI * 2.0f) / (float)n;

    for(int i=0; i < n - 1; i++) {
        float x0 = x + (float)cos((double)rot + rad * i) * (float)r;
        float y0 = y + (float)sin((double)rot + rad * i) * (float)r;

        float x1 = x + (float)cos((double) rot + rad * (i + 1)) * (float)r;
        float y1 = y + (float)sin((double) rot + rad * (i + 1)) * (float)r;

        SDL_RenderDrawLine(ren, (int)(x0 + 0.5f), (int)(y0 + 0.5f), (int)(x1 + 0.5f), (int)(y1 + 0.5f));
    }
}

static void b2DrawCircle(World* world, intptr_t id, SDL_Renderer* vg) {
    if((world->mask[id] & E_COMPONENT_VISUAL) != E_COMPONENT_VISUAL) {
        return;
    }

    const b2Vec2& worldPos = world->physics[id].body->GetPosition();

    b2Fixture* fix          = world->collider[id].fixture;
    b2CircleShape* shape    = (b2CircleShape*)fix->GetShape();

    float px = WORLD_TO_SCREEN(worldPos.x) - cameraRect.x + cameraRect.w / 2;
    float py = WORLD_TO_SCREEN(worldPos.y) - cameraRect.y + cameraRect.h / 2;
    float r  = WORLD_TO_SCREEN(shape->m_radius);
    float rot= fix->GetBody()->GetAngle();

    Visual* v    = &world->visual[id];

    SDL_SetRenderDrawColor(vg, v->color.r, v->color.g, v->color.b, v->color.a);

    SDL_RenderDrawCircle(vg, px, py, r, rot);
}

struct DustParticle {
    float scale;
    
    typedef particles<vec2f> particles_t;
    
    static DustParticle defaultValue;
    
    static void update(particles_t* group, particles_t::particle& p, int t, int dt) {
        float fDt = (float)dt * 1.f;
        p.pos.x += p.state.x * fDt;
        p.pos.y += p.state.y * fDt;
        
        p.state.x *= 0.98f;
        p.state.y *= 0.98f;
        
        p.percentLife   = std::max(0.0f, p.percentLife - fDt / p.duration);
        p.currentScale  = p.scale * atom::easing::ease_elastic_out(1.0f - p.percentLife);
        p.tint.a        = (uint8_t)(p.percentLife * 255.0f);
        p.angle         = p.percentLife * M_PI * 2.0f;
    }
};

DustParticle DustParticle::defaultValue = {
    0.f
};

struct Terrain
{

    static intptr_t create(World* world, vec2f* v, size_t n) {

        intptr_t id = World_createEntity(world);
        world->mask[id] = MASK_DEFAULT;
        


        {
            b2BodyDef bdef;
            bdef.type = b2_staticBody;
            bdef.position.Set(0.f, 0.f);//SCREEN_TO_WORLD(x), SCREEN_TO_WORLD(y));
            world->physics[id].body = world->sim->CreateBody(&bdef);
        }

        {
            b2EdgeShape shape;
            b2FixtureDef fdef;
            fdef.userData           = (void*)id;
            fdef.restitution        = RESTITUTION_TERRAIN;
            fdef.friction           = FRICTION_TERRAIN;
            fdef.shape              = &shape;
            fdef.filter.categoryBits= E_FILTER_TERRAIN;
            fdef.filter.maskBits    = E_FILTER_ALL;
            fdef.userData           = nullptr;

            for(size_t i=0; i < n - 1; i++) {
                const vec2f& v0 = v[i];
                const vec2f& v1 = v[i + 1];
                shape.Set({SCREEN_TO_WORLD(v0.x), SCREEN_TO_WORLD(v0.y)},
                          {SCREEN_TO_WORLD(v1.x), SCREEN_TO_WORLD(v1.y)});
                world->collider[id].fixture           = world->physics[id].body->CreateFixture(&fdef);
                world->collider[id].OnCollisionEnter += [&] (const CollisionEventArgs& args) {
                    //std::cout << "terrain.collision: self=[" << args.self << "], other=[" << args.other << "]" << std::endl;
                };
            }
        }
        world->visual[id].color = {255,255,255,255};
        world->drawDebug[id]         = &b2DrawEdgeShape;
        return id;
    }
};

struct Portal {
    static intptr_t create(World* world, intptr_t otherEnd, float x0, float y0, float x1, float y1) {
        intptr_t id = World_createEntity(world);

        world->mask[id] = MASK_DEFAULT;

        world->nested[id].parent = otherEnd;

        world->collider[id].OnCollisionEnter += [&] (const CollisionEventArgs& args) {
            intptr_t other = world->nested[args.self].parent;
        };

        return id;
    }
};

struct SafeZone {

    static intptr_t create(World* world, float x0, float y0, float x1, float y1) {


        intptr_t id = World_createEntity(world);
        world->mask[id] = MASK_DEFAULT;

        {
            b2BodyDef bd;
            bd.type = b2_staticBody;
            bd.allowSleep = false;
            bd.position.Set(0.0f, 0.0f);//SCREEN_TO_WORLD(0.0f), SCREEN_TO_WORLD(0.0f));
            world->physics[id].body = world->sim->CreateBody(&bd);
        }
        {
            b2EdgeShape shape;
            b2FixtureDef fdef;
            fdef.userData           = (void*)id;
            fdef.restitution        = 1.0f;
            fdef.shape              = &shape;
            fdef.filter.categoryBits= E_FILTER_TERRAIN;
            fdef.filter.maskBits    = E_FILTER_ALL;

            shape.Set( {SCREEN_TO_WORLD(x0), SCREEN_TO_WORLD(y0) },
                       {SCREEN_TO_WORLD(x1), SCREEN_TO_WORLD(y1) }
                      );

            world->collider[id].fixture = world->physics[id].body->CreateFixture(&fdef);
            world->collider[id].OnCollisionEnter += [&] (const CollisionEventArgs& args) {
                World_destroyEntity(args.world, args.other);
            };
        }
        world->visual[id].color = {0, 255, 0, 255};
        world->drawDebug[id]         = &b2DrawEdgeShape;
        return id;
    }


};

struct PopupText {
    static intptr_t create(World* world, float x, float y) {
        intptr_t id = World_createEntity(world);
        
        world->mask[id] =
        E_COMPONENT_TEXT    |
        E_COMPONENT_VISUAL  |
        E_COMPONENT_TIMEOUT |
        E_COMPONENT_TWEEN   |
        E_COMPONENT_TRANSFORM;
        
        {

        }
        
        world->transform[id].pos = { x, y };
        world->transform[id].rot = 0.0f;
        
        world->drawDebug[id] = [&] (World* world, intptr_t i, SDL_Renderer* ren) {
            atom::sprite_font* font = atom::getFontById(world->text[i].fontId);
            font->drawString(ren,
                             {   (int)(world->transform[i].pos.x + 0.5f) - cameraRect.x + cameraRect.w / 2,
                                 (int)(world->transform[i].pos.y + 0.5f) - cameraRect.y + cameraRect.h / 2
                             },
                             world->text[i].str,
                             &world->visual[i].color);
        };
        
        world->visual[id].color = { 255, 255, 255, 255 };
        
        world->text[id].fontId = 0;
        strcpy(world->text[id].str, "footest");
        
        world->timeout[id].time = 0;
        world->timeout[id].maxTime = 2000;
        world->timeout[id].onTimeout = [&] (World* world, intptr_t i) {
            World_destroyEntity(world, i);
        };
        
        world->tween[id].a   = y;
        world->tween[id].b   = y + 20;
        world->tween[id].dst = &world->transform[id].pos.y;
        world->tween[id].ease = &atom::easing::ease_back_out;
        
        return id;
    }
};

struct CarBody {
    static intptr_t create(World* world, float x, float y, float size_x, float size_y) {
        intptr_t id = World_createEntity(world);

        world->mask[id] =
        E_COMPONENT_COLLIDER |
        E_COMPONENT_NESTED |
        E_COMPONENT_TRANSFORM |
        E_COMPONENT_PHYSICS |
        E_COMPONENT_VISUAL |
        E_COMPONENT_CAMERAFOLLOW |
        E_COMPONENT_PARTICLE_EMITTER;

        world->transform[id].pos = { x, y };
        world->transform[id].rot = 0.0f;

        {
            b2BodyDef bd;
            bd.type = b2_dynamicBody;
            bd.position.Set(SCREEN_TO_WORLD(x), SCREEN_TO_WORLD(y));

            world->physics[id].body = world->sim->CreateBody(&bd);
        }

        {
            b2PolygonShape shape;
            shape.SetAsBox(SCREEN_TO_WORLD(size_x), SCREEN_TO_WORLD(size_y));

            b2FixtureDef fd;
            fd.userData = (void*)id;
            fd.filter.maskBits      = ~E_FILTER_CARWHEEL;
            fd.filter.categoryBits  = E_FILTER_CARBODY;
            fd.restitution          = RESTITUTION_CARBODY;
            fd.friction             = FRICTION_CARBODY;
            fd.density              = DENSITY_CARBODY;
            fd.shape                = &shape;
            world->collider[id].fixture = world->physics[id].body->CreateFixture(&fd);

        }

        world->collider[id].OnCollisionEnter += [&] (const CollisionEventArgs& args) {
            const b2Vec2& wp    = args.world->physics[args.self].body->GetWorldCenter();
            intptr_t tid        = PopupText::create(args.world, WORLD_TO_SCREEN(wp.x), WORLD_TO_SCREEN(wp.y));
            Text* text          = &args.world->text[tid];
            
            strcpy(&text->str[0], "crash!");
            
            //const b2Vec2& wp = args.world->physics[args.self].body->GetWorldCenter();
            const vec2f& vec = args.world->transform[args.self].pos;
            
//            args.world->particles[args.self]->emit({ vec.x, vec.y }, rand() % 1000, { 255, 255, 255, 255 });
            float baseAngle = 0.f;
            
            float angle = atan2(-args.n.y, args.n.x);
            
            burst(vec,                    // at
                  16,                     // count
                  180.0f * M_PI / 180.f,  // spread
                  angle,
                  0.1f,                   // speed
                  2.0f,                   // speedVariance
                  args.world->particles[args.self],
                  1.f,                    // scale
                  3.5f                     // scaleVariance
                  );


        };
        

        world->particles[id] = new particles<vec2f>(64, &DustParticle::update);

        //world->visual[id].texid = atom::getTextureId("@body");
        world->visual[id].color = {255, 255, 255, 255};
        world->drawDebug[id] = &b2DrawPolygonShape;

        return id;
    }
};

struct CarSuspension {
    static intptr_t create(World* world, float x, float y, intptr_t parentID) {
        intptr_t id = World_createEntity(world);

        b2Body* bodyA = world->physics[parentID].body;


        const b2Vec2& worldPos = bodyA->GetWorldCenter();

        world->mask[id] = E_COMPONENT_NESTED | E_COMPONENT_PHYSICS;// | E_COMPONENT_VISUAL;

        {
            b2BodyDef bd;
            bd.type = b2_dynamicBody;

            bd.position.Set(worldPos.x + SCREEN_TO_WORLD(x), worldPos.y + SCREEN_TO_WORLD(y));
            world->physics[id].body = world->sim->CreateBody(&bd);
        }
        b2Body* bodyB = world->physics[id].body;

        {
            b2FixtureDef fd;
            b2CircleShape shape;
            //shape.SetAsBox(SCREEN_TO_WORLD(10.0f), SCREEN_TO_WORLD(10.0f));
            shape.m_p = { 0.0f, 0.0f };
            shape.m_radius = SCREEN_TO_WORLD(10.0f);
            fd.filter.maskBits = 0;
            fd.filter.categoryBits = 0;
            fd.shape = &shape;
            fd.friction = 0.0f;
            fd.density = 15.0f;
            bodyB->CreateFixture(&fd);
        }

        {
            b2PrismaticJointDef jd;

            jd.bodyA        = bodyA;
            jd.bodyB        = bodyB;

            jd.localAnchorA = { SCREEN_TO_WORLD(x), SCREEN_TO_WORLD(y) };
            jd.localAnchorB = { 0.0f, 0.0f };
            jd.localAxisA   = { 0.0f, 1.0f };

            jd.maxMotorForce    = 100.f;
            jd.motorSpeed       = 6.f;
            jd.enableMotor      = false; // ?
            jd.enableLimit      = true;
            jd.upperTranslation = SCREEN_TO_WORLD(-1.0f);
            jd.lowerTranslation = SCREEN_TO_WORLD( 1.0f);

            world->joint[id] = world->sim->CreateJoint(&jd);
        }

//        world->visual[id].color = { 0, 0, 255, 255 };
//        world->draw[id] = &b2DrawEdgeShape;

        return id;
    }
};

struct CarWheel {
    static intptr_t create(World* world, float x, float y, float wheelRadius, intptr_t parentID) {
        intptr_t id = World_createEntity(world);

        world->mask[id] =
            E_COMPONENT_COLLIDER |
            E_COMPONENT_NESTED |
            E_COMPONENT_TRANSFORM |
            E_COMPONENT_PHYSICS |
            E_COMPONENT_VISUAL;

        b2Vec2 parentPos = world->physics[parentID].body->GetWorldCenter();//world->physics[parentID].body->GetTransform().p;
//        b2Vec2 parentCenter = ;

        {
            b2BodyDef bd;
            bd.position.Set(parentPos.x + SCREEN_TO_WORLD(x), parentPos.y + SCREEN_TO_WORLD(y));
            bd.type = b2_dynamicBody;
            world->physics[id].body = world->sim->CreateBody(&bd);
        }

        {
            b2FixtureDef fd;
            b2CircleShape shape;
            shape.m_p.Set(0.0f, 0.0f);
            shape.m_radius = SCREEN_TO_WORLD(wheelRadius);

            fd.shape                = &shape;
            fd.userData             = (void*)id;
            fd.restitution          = RESTITUTION_CARWHEEL;
            fd.friction             = FRICTION_CARWHEEL;
            fd.density              = DENSITY_CARWHEEL;
            fd.filter.categoryBits  = E_FILTER_CARWHEEL;
            fd.filter.maskBits      = ~E_FILTER_CARBODY;

            world->collider[id].fixture = world->physics[id].body->CreateFixture(&fd);
        }

        {
            b2Body* bodyA = world->physics[parentID].body;
            b2Body* bodyB = world->physics[id].body;

            b2Vec2 worldPos = bodyA->GetWorldCenter();

            {
                b2RevoluteJointDef jd;
                jd.Initialize(bodyA, bodyB, { worldPos.x + SCREEN_TO_WORLD(x), worldPos.y + SCREEN_TO_WORLD(y) } );

                jd.maxMotorTorque   = MAX_MOTOR_TORQUE;
                jd.enableMotor      = false;
                jd.motorSpeed       = M_PI * 16.0f;

                world->joint[id] = world->sim->CreateJoint(&jd);
            }
        }

        world->collider[id].OnCollisionEnter += [&] (const CollisionEventArgs& args) {
        };
        
        
        
        //world->visual[id].texid = atom::getTextureId("@wheel");
        world->visual[id].color = {255, 255, 255, 255};
        world->drawDebug[id] = &b2DrawCircle;

        return id;
    }
};

void drawDebugText(World* world, intptr_t id, SDL_Renderer* ren) {
}

static void carwheel_input(World* world, intptr_t id) {
    
    int rw;
    SDL_GetRendererOutputSize(g_Renderer, &rw, nullptr);
    int px =atom::SDL_GetMousePosition().x;
    
    int sgn = atom::math::sgn(px - rw / 2);
    
    b2RevoluteJoint* jj = ((b2RevoluteJoint*)world->joint[id]);
    
    jj->SetMaxMotorTorque(atom::SDL_IsMouseDown() ? MAX_MOTOR_TORQUE : IDLE_MOTOR_TORQUE);
    
    jj->EnableMotor(true);//atom::SDL_IsMouseDown());
    
    jj->SetMotorSpeed(atom::SDL_IsMouseDown() ? sgn * MAX_MOTOR_SPEED : 0.f);
}

struct Car {

    static intptr_t create(World* world, float x, float y, float size_x, float size_y)
    {

        intptr_t bodyID = CarBody::create(world, x, y, size_x, size_y);
#if 1
        static const float suspensionHeight = 1.5f;
        intptr_t sa = CarSuspension::create(world, -size_x, size_y * suspensionHeight, bodyID);
        intptr_t sb = CarSuspension::create(world,  size_x, size_y * suspensionHeight, bodyID);

        intptr_t a   = CarWheel::create(world, 0.0f, 0.0f, 15.0f, sa);
        intptr_t b   = CarWheel::create(world, 0.0f, 0.0f, 15.0f, sb);
#else

        intptr_t a   = CarWheel::create(world, -size_x, size_y, 15.0f, bodyID);
        intptr_t b   = CarWheel::create(world,  size_x, size_y, 15.0f, bodyID);
#endif

        //((b2RevoluteJoint*)world->joint[a])->EnableMotor(true);

        world->mask[b] |= E_COMPONENT_INPUT;
        world->mask[a] |= E_COMPONENT_INPUT;
        world->input[b] = &carwheel_input;
        world->input[a] = &carwheel_input;

        return bodyID;

    }
    static void input(World* world, intptr_t id) {

        const b2Vec2& b2Pos = world->physics[id].body->GetPosition();

        if(atom::SDL_IsMouseDown()) {
            const SDL_Point& p = atom::SDL_GetMousePosition();
            vec2f v0 = { (float)p.x, WORLD_TO_SCREEN(b2Pos.y) };
            world->physics[id].body->SetTransform({ SCREEN_TO_WORLD(v0.x), SCREEN_TO_WORLD(v0.y) }, 0.0f);
        }
    }
};

screen_play::screen_play()
{
    _score          = 0;
    _currentTime    = 0;
    mRw = 100;
    mRh = 100;
//    _world.trailParticles = new particles<trail>(128, &update_trail);
    atom::Advertisement* banner = atom::ad_manager::instance().getAdByName("admobBanner");
    if(banner != nullptr) {
        banner->onAdWasFetched += [&] (const atom::AdEventArgs& args) {
            args.ad->show();
        };
    }
}

screen_play::~screen_play()
{
}

static void fractal(const vec2f& beg, const vec2f& end, int n, std::vector<vec2f>* list) {

    vec2f dir = vec2f::normalize(end - beg);
    float len = vec2f::length(end - beg);

    float segmentLength = len / (float)n;

    for(int i=0; i < n; i++)
    {
        vec2f vec;
        vec.x = beg.x + dir.x * (i * segmentLength);
        vec.y = beg.y + dir.y * (i * segmentLength);
        list->push_back(vec);
    }

    for(int i=0; i < list->size() - 1; i++) {
        vec2f a = list->at(i);
        vec2f b = list->at(i + 1);
        vec2f d = vec2f::normalize(b - a);
        vec2f normal = { d.y, -d.x };
        list->at(i).x += normal.x * randf() * segmentLength * 0.5f;
        list->at(i).y += normal.y * randf() * segmentLength * 0.5f;
    }
}


struct remote_database {
    struct entry {
        std::string name;//"name": "strongworld_912x1280_v4",
        std::string game;//"game": "Strong World D.",
        std::string androidid;//"androidID": "com.mtr.sw",
        std::string appleid;//"appleID": "1017242667",
        std::string path;//"picture": "uploads/Banners/1817-1sl3emj.png",
        std::string id;//"_id": "5652e4b63e1d491907dd139a"
        int clickcount;//"clickcount": "9",
    };
    
    std::vector<entry*> _entry;
    
    void parseJsonNode(cJSON* json, entry* en) {
        if(json->string && json->valuestring) {
            std::cout << json->string << ":" << json->valuestring << std::endl;
            
            if(!strcmp(json->string, "name")) {
                en->name = json->valuestring;
            }
            if(!strcmp(json->string, "game")) {
                en->game = json->valuestring;
            }
            if(!strcmp(json->string, "androidID")) {
                en->androidid = json->valuestring;
            }
            if(!strcmp(json->string, "appleID")) {
                en->appleid = json->valuestring;
            }
            if(!strcmp(json->string, "picture")) {
                en->path = json->valuestring;
            }
            if(!strcmp(json->string, "clickcount")) {
                en->clickcount = std::atoi(json->valuestring);
            }
            if(!strcmp(json->string, "_id")) {
                en->id = json->valuestring;
            }
        }
        
        if(json->child) {
            parseJsonNode(json->child, en);
        }
        
        if(json->next) {
            parseJsonNode(json->next, en);
        }
        
    }
    
    void parseJsonArray(cJSON* json) {
        
        int n = cJSON_GetArraySize(json);
        entry* en = new entry;
        
        for(int i=0; i < n; i++) {
            parseJsonNode(cJSON_GetArrayItem(json->child, i), en);
            _entry.push_back(en);
        }
    }
    
    void collect() {
        http_request* req = new http_request("pic.tfjoy.com", 3000, "/database");
        std::cout << this << std::endl;
        
        req->setRequestDoneCallback([&] (const HttpEventArgs& args) {
            std::cout << args.req->bytes() << std::endl;
            
            cJSON* node = cJSON_Parse((const char*)args.req->bytes());
            if(node) {
                this->parseJsonArray(node);
            }
            
        });
        req->fetch();
    }
};

extern SDL_Renderer* g_Renderer;

void screen_play::load_content() {
    
//    remote_database db;
//    db.collect();

//    http_request* req = new http_request("pic.tfjoy.com", 3000, "/" + db._entry[2]->path);//uploads/Banners/1817-1sl3emj.png");
//    req->OnRequestIsDone += [&] (const HttpEventArgs& args) {
//        SDL_RWops* ops    = SDL_RWFromMem((char*)args.req->bytes(), args.req->size());
//        SDL_Surface* surf = IMG_Load_RW(ops, 0);
//        SDL_Texture* tex  = SDL_CreateTextureFromSurface(g_Renderer, surf);
//        atom::addTexture("remote", tex);
//        //std::cout << args.req->bytes() << std::endl;
//    };
//    req->fetch();
//
    
    //World_destroyEntity(i);
    for(int i=0; i < MAX_ENTITIES; i++) {
        _world.mask[i] = 0;
        _world.physics[i].body = nullptr;
    }
    
    _world.collisions = new CollisionHandler(&_world);
    _world.sim = new b2World({0.0f, 9.8f});
    _world.sim->SetContactListener(_world.collisions);
    _world.cameraPos = { 0.0f, 0.0f };

    std::vector<vec2f> list;

    vec2f a = { 0.0f, 50.0f };
    vec2f b = { 8192.f, 2048.f };

    fractal(a, b, 120, &list);


    Terrain::create(&_world, &list[0], list.size());

    Car::create(&_world, 128.f, 0.0f, 30.0f, 10.0f);
    
    
    
}

void screen_play::update(int dt, bool a, bool b)
{
    static int t = 0;
    _currentTime += dt;
    t += dt;
    
    cameraRect.w = mRw;
    cameraRect.h = mRh;

    
    if(t >= 2000) {
        PopupText::create(&_world, 256.f, 128.f);
        t = 0;
    }

    float fDt = (float)dt * 0.001f;
    
    for(int i=0; i < MAX_ENTITIES; i++) {
        if(World_hasComponent(&_world, i, E_COMPONENT_TRANSFORM | E_COMPONENT_PHYSICS)) {
            const vec2f& p = _world.transform[i].pos;
            float r = _world.transform[i].rot;
            if(_world.physics[i].body != nullptr) {
                _world.physics[i].body->SetTransform({ SCREEN_TO_WORLD(p.x), SCREEN_TO_WORLD(p.y) }, r);
            }
        }
    }
    
    _world.sim->Step(fDt, 8, 3, 4);
    
    for(int i=0; i < MAX_ENTITIES; i++) {
        if(World_hasComponent(&_world, i, E_COMPONENT_TRANSFORM | E_COMPONENT_PHYSICS)) {
            const b2Transform b2t = _world.physics[i].body->GetTransform();
            const b2Vec2& vec = b2t.p;
            
            _world.transform[i].pos.x = WORLD_TO_SCREEN(vec.x);
            _world.transform[i].pos.y = WORLD_TO_SCREEN(vec.y);
            _world.transform[i].rot   = _world.physics[i].body->GetAngle();
        }
    }

    float xx = cameraRect.x;
    float yy = cameraRect.y;
    
    float maxx = -FLT_MAX;
    float minx =  FLT_MAX;
    float maxy = -FLT_MAX;
    float miny =  FLT_MAX;
    
    
    int count = 0;
    
    for(int i=0; i < MAX_ENTITIES; i++) {
        if(World_hasComponent(&_world, i, E_COMPONENT_CAMERAFOLLOW | E_COMPONENT_TRANSFORM)) {
            maxx = std::max(maxx, _world.transform[i].pos.x);
            maxy = std::max(maxy, _world.transform[i].pos.y);
            minx = std::min(minx, _world.transform[i].pos.x);
            miny = std::min(miny, _world.transform[i].pos.y);
            count++;
        }
    }
    
    if(count) {
        float cx = (minx + maxx) * 0.5f;
        float cy = (miny + maxy) * 0.5f;
        
        xx = xx + (cx - xx) * 0.1f;
        yy = yy + (cy - yy) * 0.1f;
        
        cameraRect.x = (int)(xx + 0.5f);
        cameraRect.y = (int)(yy + 0.5f);
    }
    
    for(int i=0; i < MAX_ENTITIES; i++) {
        if((_world.mask[i] & E_COMPONENT_INPUT) == E_COMPONENT_INPUT) {
            _world.input[i](&_world, i);
        }

        if((_world.mask[i] & E_COMPONENT_PARTICLE_EMITTER) == E_COMPONENT_PARTICLE_EMITTER) {
            _world.particles[i]->update(_currentTime, dt);
        }

        if((_world.mask[i] & E_COMPONENT_TIMEOUT) == E_COMPONENT_TIMEOUT) {
            _world.timeout[i].time += dt;
            if(_world.timeout[i].time >= _world.timeout[i].maxTime) {
                _world.timeout[i].onTimeout(&_world, i);
                _world.timeout[i].time = 0;
            }
        }
        
        
        
        if(World_hasComponent(&_world, i, E_COMPONENT_TWEEN | E_COMPONENT_TIMEOUT)) {
            Tween* tt   = &_world.tween[i];
            Timeout* to = &_world.timeout[i];
            
            to->time += dt;
            
            *(tt->dst) = tt->a + (tt->b - tt->a) * tt->ease((float)to->time / (float)to->maxTime);
        
        }
    }

    World_cleanup(&_world);

    gamescreen::update(dt, a, b);
}

void screen_play::draw(SDL_Renderer* ren) //SDL_Renderer* ren)
{
    //nvgMoveTo(ren, -_world.cameraPos.x, -_world.cameraPos.y);

    SDL_GetRendererOutputSize(ren, &mRw, &mRh);
    
    SDL_Texture* tex = atom::getTextureByName("remote");
    if(tex) {
        int w, h;
        SDL_QueryTexture(tex, nullptr, nullptr, &w, &h);
        SDL_Rect dst = { -cameraRect.x, -cameraRect.y, w, h};
        SDL_RenderCopy(ren, tex, nullptr, &dst );
    }

    for(int i=0; i < MAX_ENTITIES; i++) {
        if(World_hasComponent(&_world, i, E_COMPONENT_VISUAL)) {
            _world.drawDebug[i](&_world, i, ren);
        }
        if(World_hasComponent(&_world, i, E_COMPONENT_VISUAL | E_COMPONENT_TRANSFORM)) {
            
            SDL_Texture* tex = atom::getTextureById(_world.visual[i].texid);
            SDL_Rect dst;
            SDL_QueryTexture(tex, nullptr, nullptr, &dst.w, &dst.h);
            dst.x = (int)(_world.transform[i].pos.x + 0.5f) - cameraRect.x + cameraRect.w / 2;
            dst.y = (int)(_world.transform[i].pos.y + 0.5f) - cameraRect.y + cameraRect.h / 2;
            dst.w >>= 2;
            dst.h >>= 2;
            
            dst.x -= dst.w / 2;
            dst.y -= dst.h / 2;
            
            float angle = _world.transform[i].rot;
            SDL_RenderCopyEx(ren, tex, nullptr, &dst, angle * 180.0f / M_PI, nullptr, SDL_FLIP_NONE);
        }

        if((_world.mask[i] & E_COMPONENT_PARTICLE_EMITTER) == E_COMPONENT_PARTICLE_EMITTER) {

            _world.particles[i]->draw(ren, atom::getTextureByName("@smoke"), cameraRect);
        }
    }
    gamescreen::draw(ren);
}

