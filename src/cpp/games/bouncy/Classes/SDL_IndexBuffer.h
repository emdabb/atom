//
//  SDL_IndexBuffer.h
//  bouncy
//
//  Created by emiel Dam on 12/31/15.
//
//

#ifndef SDL_IndexBuffer_h
#define SDL_IndexBuffer_h

#include <stdint.h>

struct SDL_IndexBuffer;
typedef struct SDL_IndexBuffer SDL_IndexBuffer;

struct SDL_Renderer;

typedef enum {
    SDL_INDEX_TYPE_UINT8,
    SDL_INDEX_TYPE_INT8,
    SDL_INDEX_TYPE_UINT16,
    SDL_INDEX_TYPE_INT16,
    SDL_INDEX_TYPE_UINT32,
    SDL_INDEX_TYPE_INT32
} SDL_IndexType;

static const ssize_t SDL_IndexSize[] = {
    sizeof(uint8_t),
    sizeof(int8_t),
    sizeof(uint16_t),
    sizeof(int16_t),
    sizeof(uint32_t),
    sizeof(int32_t)
};

SDL_IndexBuffer* SDL_CreateIndexBuffer(struct SDL_Renderer*, ssize_t);
int  SDL_LockIndexBuffer(const SDL_IndexBuffer*);
void SDL_UpdateIndexBuffer(const SDL_IndexBuffer*, int, ssize_t, const void*);
int  SDL_UnlockIndexBuffer(const SDL_IndexBuffer*);
void SDL_SetIndexBuffer(const SDL_IndexBuffer*);


#endif /* SDL_IndexBuffer_h */
