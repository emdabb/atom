//
//  SDL_VertexBuffer_GLES2.c
//  bouncy
//
//  Created by emiel Dam on 12/31/15.
//
//

#include <stdio.h>
#include "SDL_VertexBuffer.h"
#include "SDL_Ext.h"
#include <SDL.h>
#if   defined(__IOS__)
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#elif defined(__ANDROID__)
#elif defined(__X11__)
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#endif
#include <string.h>
#include <assert.h>


struct SDL_VertexElement {
    int offset;
    SDL_VertexElementFormat format;
    SDL_VertexElementUsage  usage;
};

struct SDL_VertexDeclaration {
    SDL_VertexElement** element;
    ssize_t numElements;
    ssize_t stride;
};

struct SDL_VertexBuffer {
    GLuint  id;
    size_t  size;
    void*   ptr;
    SDL_VertexDeclaration* decl;
};


static int glVertexElementFormatType[] = {
    GL_FLOAT,//SDL_SINGLE,
    GL_FLOAT,//SDL_VECTOR2,
    GL_FLOAT,//SDL_VECTOR3,
    GL_FLOAT,//SDL_VECTOR4,
    GL_HALF_FLOAT_OES,//SDL_HALF_VECTOR2,
    GL_HALF_FLOAT_OES,//SDL_HALF_VECTOR4,
    GL_UNSIGNED_BYTE,//SDL_COLOR,
    GL_SHORT,//SDL_NORMALIZED_SHORT2,
    GL_SHORT,//SDL_NORMALIZED_SHORT4,
    GL_SHORT,//SDL_SHORT2,
    GL_SHORT,//SDL_SHORT4,
    GL_BYTE,//SDL_BYTE4
};

static int glVertexElementFormatSize[] = {
    1,//SDL_SINGLE,
    2,//SDL_VECTOR2,
    3,//SDL_VECTOR3,
    4,//SDL_VECTOR4,
    2,//SDL_HALF_VECTOR2,
    4,//SDL_HALF_VECTOR4,
    4,//SDL_COLOR,
    2,//SDL_NORMALIZED_SHORT2,
    4,//SDL_NORMALIZED_SHORT4,
    2,//SDL_SHORT2,
    4,//SDL_SHORT4,
    4,//SDL_BYTE4
};

static GLboolean glAttribIsNormalized[] = {
    GL_FALSE,//SDL_SINGLE,
    GL_FALSE,//SDL_VECTOR2,
    GL_FALSE,//SDL_VECTOR3,
    GL_FALSE,//SDL_VECTOR4,
    GL_FALSE,//SDL_HALF_VECTOR2,
    GL_FALSE,//SDL_HALF_VECTOR4,
    GL_FALSE,//SDL_COLOR,
    GL_TRUE,//SDL_NORMALIZED_SHORT2,
    GL_TRUE,//SDL_NORMALIZED_SHORT4,
    GL_FALSE,//SDL_SHORT2,
    GL_FALSE,//SDL_SHORT4,
    GL_FALSE,//SDL_BYTE4
};

static const GLenum SDL_GL_Primitive[] = {
    GL_POINTS,//    SDL_POINTS,
    GL_LINES,//SDL_LINES,
    GL_LINE_LOOP,//SDL_LINE_LOOP,
    GL_TRIANGLES,//SDL_TRIANGLES,
    GL_TRIANGLE_STRIP,//
    GL_TRIANGLE_FAN

};

/**
void SDL_VertexBufferSetData(SDL_VertexBuffer* vbo, const void* src, ssize_t size) {

}
*/

SDL_VertexDeclaration* SDL_CreateVertexDeclaration(const SDL_VertexElement** el, ssize_t ni, ssize_t stride) {
    DEBUG_LOG("%s\n", "creating vertex declaration");

    SDL_VertexDeclaration* res = NULL;
    res = (SDL_VertexDeclaration*)malloc(sizeof(SDL_VertexDeclaration));
    res->element = NULL;
    res->element = (SDL_VertexElement**)malloc(sizeof(SDL_VertexElement*) * ni);
    for(int i=0; i < ni; i++) {
        res->element[i] = (SDL_VertexElement*)malloc(sizeof(SDL_VertexElement));
        memcpy((void*)res->element[i], (void*)el[i], sizeof(SDL_VertexElement));
    }
    res->stride = stride;
	res->numElements = ni;
    
    return res;
}

SDL_VertexElement* SDL_CreateVertexElement(int off, SDL_VertexElementFormat format, SDL_VertexElementUsage usage) {
    DEBUG_LOG("%s\n", "creating vertex element");
    SDL_VertexElement* res = (SDL_VertexElement*)malloc(sizeof(SDL_VertexElement));
    res->offset = off;
    res->format = format;
    res->usage  = usage;
    return res;
}

SDL_VertexBuffer* SDL_CreateVertexBuffer(ssize_t len) {
    DEBUG_LOG("%s\n", "creating vertex buffer...");
    SDL_VertexBuffer* res = (SDL_VertexBuffer*)malloc(sizeof(SDL_VertexBuffer));
    GLuint old = 0;
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &old);
    glGenBuffers(1, &res->id);
    glBindBuffer(GL_ARRAY_BUFFER, res->id);
    glBufferData(GL_ARRAY_BUFFER, len, NULL, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, old);

	res->size = len;
    
    return res;
}

int SDL_LockVertexBuffer(SDL_VertexBuffer* buf) {
//    assert(g_VertexBufferLock == 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, buf->id);
    //buf->ptr = glMapBufferOES(GL_ARRAY_BUFFER, GL_MAP_WRITE_BIT_EXT);
    return buf->ptr ? 0 : -1;
    
}

int SDL_UnlockVertexBuffer(SDL_VertexBuffer* buf) {
    if(buf->ptr != NULL) {
        GLboolean res = GL_FALSE;//glUnmapBufferOES(GL_ARRAY_BUFFER);
        return res != GL_FALSE;
    }
    return -1;
}

#define SDL_GL_ASSERT(fun) { \ 
	fun;\
	GLint err=  glGetError(); \
	if(err != GL_NO_ERROR) {\
		DEBUG_LOG("opengl error: 0x%x\n", err); \
		exit(-128);\
	}\
}
	

int SDL_UpdateVertexBuffer(const SDL_VertexBuffer* dst, int off, ssize_t len, const void* src) {
#if 1
    GLuint old = 0;
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &old);
	assert(glGetError() == GL_NO_ERROR);

    glBindBuffer(GL_ARRAY_BUFFER, dst->id);
	assert(glGetError() == GL_NO_ERROR);

    glBufferData(GL_ARRAY_BUFFER, dst->size, NULL, GL_STATIC_DRAW);
	assert(glGetError() == GL_NO_ERROR);

    DEBUG_LOG("SDL_UpdateVertexBuffer -> bufferSize[%d], size=[%d], offset=[%d], src=[0x%x]\n", dst->size, len, off, src);
    SDL_GL_ASSERT(glBufferSubData(GL_ARRAY_BUFFER, off, len, src));

    glBindBuffer(GL_ARRAY_BUFFER, old);
	assert(glGetError() == GL_NO_ERROR);    
#else
    if(dst->ptr) {
        uint8_t* ptr = (uint8_t*)dst->ptr;
        memcpy(&ptr[off], src, len);
        return 0;
    }
#endif
    return 0;
}

void SDL_DrawVertexBuffer(SDL_VertexBuffer* vbo, const SDL_PrimitiveType prim, int start, ssize_t count, ssize_t primCount) {
	GLuint old = 0;
	glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &old);
	glBindBuffer(GL_ARRAY_BUFFER, vbo->id);	
    for(int i=0; i < primCount; i++) {
        glDrawArrays(SDL_GL_Primitive[prim], start + count * i, count);
    }
	glBindBuffer(GL_ARRAY_BUFFER, old);
}

void SDL_SetVertexDeclaration(SDL_VertexBuffer* vbo, const SDL_VertexDeclaration* decl) {
    GLint old = 0;
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &old);
    glBindBuffer(GL_ARRAY_BUFFER, vbo->id);
    
    for(int i=0; i < decl->numElements; i++) {
        SDL_VertexElement* ev = decl->element[i];

		GLint size 	= glVertexElementFormatSize[ev->format];
		GLenum type	= glVertexElementFormatType[ev->format];
		GLboolean is_normalized = glAttribIsNormalized[ev->format];
		const GLvoid* ptr = BUFFER_OFFSET(ev->offset);
        
		glEnableVertexAttribArray(ev->usage);
        glVertexAttribPointer(ev->usage, size, type, is_normalized, (GLsizei)decl->stride, ptr);
    }
    glBindBuffer(GL_ARRAY_BUFFER, old);
}
