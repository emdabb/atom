//
//  SDL_IndexBuffer_GLES2.c
//  bouncy
//
//  Created by emiel Dam on 12/31/15.
//
//

#include <stdio.h>
#include "SDL_IndexBuffer.h"
#include <SDL.h>

#if defined(__IOS__)
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#elif defined(__ANDROID__)
#elif defined(__X11__)
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#endif

struct SDL_IndexBuffer {
    GLuint id;
    size_t size;
};

SDL_IndexBuffer* SDL_CreateIndexBuffer(SDL_Renderer* ren, ssize_t len){
    SDL_IndexBuffer* res = (SDL_IndexBuffer*)malloc(sizeof(SDL_IndexBuffer));
    res->size = len;
    
    {
        glGenBuffers(1, &res->id);
        glBindBuffer(res->id, GL_ELEMENT_ARRAY_BUFFER);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, len, NULL, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
    
    return res;
}
int  SDL_LockIndexBuffer(const SDL_IndexBuffer* buf) {
    return -1;
    
}
void SDL_UpdateIndexBuffer(const SDL_IndexBuffer* buf, int start, ssize_t count, const void* src) {
    
}
int  SDL_UnlockIndexBuffer(const SDL_IndexBuffer* buf) {
    return -1;
}
