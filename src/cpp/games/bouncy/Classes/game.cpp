#include <atom/engine/application.h>
#include <atom/engine/fsm.h>
#include <atom/engine/easing.h>
#include <atom/engine/ecs/kult.hpp>
#include "gamescreen.h"
#include "asset_manager.h"
#include "imgui.h"
#include "input.h"
#include "particles.h"
#include "particle_spawn.h"
#include "color.h"
#include "sprite_font.h"
#include "grid.h"
#include "trail.h"
#include "world.h"
#include "particle_update.h"

#include "ads.h"
#include "screens.h"
#include "resolution.h"
#include "SDL_Ext.h"

#include <SDL_ttf.h>
#include <cstdlib>
#include <cstring>
#include <atom/core/math/mat4.h>

#include "../Classes/SDL_Ext.h"
#include "../Classes/SDL_Shader.h"
#include "../Classes/SDL_SpriteBatch.h"
#include "../Classes/SDL_VertexBuffer.h"
#include "../Classes/SDL_IndexBuffer.h"

using namespace atom;

//#define DOT_TIMEOUT     5000
//#define BAD_TIMEOUT     7000

SDL_Renderer* g_Renderer;

class game_coil;
class gamescreen;

template <typename R, typename... A>
delegate<R(A...)> make_delegate(R(*pfn)(A...)) {
    return delegate<R(A...)>{pfn};
}

static class register_gamescreens
{
    public:
        register_gamescreens()
        {
            atom::register_factory<gamescreen, screen_splash>("SplashScreen");
        }
} _;


#include <fstream>
#include "sprite_font.h"
#include <python3.5/Python.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

struct VertexP2T2 {
	vec2f pos, tc;
	static const SDL_VertexElement* elem[];
	static const SDL_VertexDeclaration* decl;
};
const SDL_VertexElement* VertexP2T2::elem[] = {
	SDL_CreateVertexElement(offsetof(VertexP2T2, pos), SDL_VECTOR2, SDL_POSITION),
	SDL_CreateVertexElement(offsetof(VertexP2T2, tc),  SDL_VECTOR2, SDL_TEXCOORD0)
};
const SDL_VertexDeclaration* VertexP2T2::decl = SDL_CreateVertexDeclaration(VertexP2T2::elem, 2, sizeof(VertexP2T2));

class game_coil
: public applet
{
    typedef fsm<game_coil> fsm_type;
    application*    _app;
    gamescreen_manager* _man;

public:
    game_coil(application* app)
    : _app(app)
    {
        _man = new gamescreen_manager();

        app->OnMouseDown.add(make_delegate(&input_OnMousePressed));
        app->OnMouseMove.add(make_delegate(&input_OnMouseMoved));
        app->OnMouseRelease.add(make_delegate(&input_OnMouseReleased));
        app->OnKeyPressed.add(make_delegate(&input_OnKeyPressed));
    }

    virtual ~game_coil()
    {
        //delete _fsm;
        delete _man;
    }

    std::string from_file(const char* fn) {
		std::ifstream is;
		is.open(fn);
		if(is.is_open()) {
            std::istreambuf_iterator<char> eos;
            std::string s(std::istreambuf_iterator<char>(is), eos);
            is.close();
            return s;
        }
        return std::string();
	}

	SDL_VertexBuffer* vbo;
	SDL_Shader* sha;
	
    virtual int create()
    {
        SDL_Renderer* g_ren = _app->getRenderer();
		SDL_Renderer* ren = _app->getRenderer();


		std::ifstream is;
        is.open(atom::getResourcePath() + "assets/textures.json");
        if(is.is_open()) {
            atom::loadTextureLibrary(ren, is);
            is.close();
        }


        std::string fontPath = atom::getResourcePath() + "assets/Lack.ttf";

        is.open(fontPath);
        if(is.is_open()) {
            atom::loadFont("@Lack_16pt", is, 16, ren);
            is.close();
        }

        //imgui_loadFont(_nvg, fontPath.c_str(), "Lack.ttf");

        is.open(atom::getResourcePath() + "assets/ads.json");
        if(is.is_open()) {
            atom::ad_manager::instance().cacheFrom(is);
            is.close();
        }
		std::string pyres;
#if 1 
        PyObject* pName, *pModule, *pDict, *pFunc;
        PyObject* pArgs, *pValue[2];
        PyObject* pRet;

		Py_Initialize();

        PyRun_SimpleString("import sys");
        PyRun_SimpleString("sys.path.append('assets')");

        pName = PyUnicode_DecodeFSDefault("cg2glsl");

        pModule = PyImport_Import(pName);
        Py_DECREF(pName);
        if(pModule != nullptr) {
            pFunc = PyObject_GetAttrString(pModule, "convert");
            if(pFunc && PyCallable_Check(pFunc)) {
                pArgs = PyTuple_New(2);

                pValue[0] = PyUnicode_DecodeFSDefault("assets/shaders/shader.cg");
                pValue[1] = PyUnicode_DecodeFSDefault("assets/shaders/shader.glsl");

                PyTuple_SetItem(pArgs, 0, pValue[0]);
                PyTuple_SetItem(pArgs, 1, pValue[1]);

                pRet = PyObject_CallObject(pFunc, pArgs);
                Py_DECREF(pArgs);

                if(pValue != nullptr) {
                    char* utf8 = PyUnicode_AsUTF8(pRet);
                    //std::cout << "result of call: " << utf8 << std::endl;
					pyres = utf8;
                    Py_DECREF(pRet);
                } else {

                }
            }
        }

        Py_Finalize();
#else
		pyres = from_file("assets/shaders/shader.glsl");

#endif
		//std::cout << "*****pyres ******\n" << pyres << "****************" << std::endl;

        assert(glGetError() == GL_NO_ERROR);
        sha = SDL_CreateShader(g_ren);
        assert(glGetError() == GL_NO_ERROR);

        SDL_SetShaderSource(sha, pyres.c_str());
        assert(glGetError() == GL_NO_ERROR);

		SDL_CompileShader(sha);
        assert(glGetError() == GL_NO_ERROR);

		SDL_LinkShader(sha);
        assert(glGetError() == GL_NO_ERROR);


       	static const float ONE = 320.f; 

        VertexP2T2 vertices[] = {
            { {0.f, 0.f}, {0.f, 0.f} },
            { {ONE, 0.f}, {1.f, 0.f} },
            { {ONE, ONE}, {1.f, 1.f} },
            { {0.f, ONE}, {0.f, 1.f} }
        };

        //SDL_SetShaderParameter(sha, "MVP", &mat);

		vbo = SDL_CreateVertexBuffer(sizeof(VertexP2T2) * 4);
		assert(glGetError() == GL_NO_ERROR);

       	SDL_SetVertexDeclaration(vbo, VertexP2T2::decl);
		assert(glGetError() == GL_NO_ERROR);
 
        SDL_UpdateVertexBuffer(vbo, 0, 4 * sizeof(VertexP2T2), &vertices);
		assert(glGetError() == GL_NO_ERROR);

		assert(vbo);

        int rw, rh;
        SDL_GetRendererOutputSize(g_ren, &rw, &rh);

        g_Renderer = g_ren;


        

        SDL_Texture* _whitePixel = SDL_CreateTexture(ren, SDL_PIXELFORMAT_RGB565, SDL_TEXTUREACCESS_STATIC, 1, 1);
        atom::addTexture("@whitePixel", _whitePixel);
        uint16_t pixel = 0xffff;
        SDL_Rect rc = { 0, 0, 1, 1 };
        SDL_UpdateTexture(_whitePixel, &rc, &pixel, sizeof(pixel));
        
        
                
        {
        
            atom::Advertisement* ad = atom::ad_manager::instance().getAdByName("chartboostInterstitial");
            if(ad != nullptr) {
                ad->onAdWasFetched += [&] (const atom::AdEventArgs& args) {
                    args.ad->show();
                };
            }
        }
        
        {
            atom::Advertisement* ad = atom::ad_manager::instance().getAdByName("admobBanner");
            if(ad != nullptr) {
                ad->onAdWasFetched += [&] (const atom::AdEventArgs& args) {
                    args.ad->show();
                };
                ad->fetch();
            }
        }
        

        _man->add(new screen_play);

        return 0;
    }

    virtual int destroy()
    {
        return 0;
    }

    virtual void update(int t, int dt)
    {
        //_man->update(t, dt);
    }

    virtual void draw()
    {
        static float x = 0.0f;

        x = fmodf(x + 1.0f, 340.0f);
		
        atom::mat4f world, view, proj, ortho, viewProj;
        view = atom::mat4f::translation(x, 0.0f, 0.0f);
        proj = atom::mat4f::ortho_offcenter(0.0f, 640.0f, 0.0f, 480.0f, -1.0f, 1.0f);
        //ortho = atom::mat4f::transpose(atom::mat4f::mul(world, atom::mat4f::mul(view, proj)));
        
        viewProj = atom::mat4f::mul(view, proj);
        ortho = atom::mat4f::transpose(viewProj);//atom::mat4f::mul(view, proj));


		glDisable(GL_CULL_FACE);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
		glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO);
        SDL_Renderer* ren = _app->getRenderer();
        int rw, rh;
        SDL_GetRendererOutputSize(ren, &rw, &rh);
        SDL_SetRenderDrawColor(ren, 128, 128, 128, 255);
        SDL_RenderClear(ren);
        //_man->draw(ren);
		SDL_SetShader(sha);
		assert(glGetError() == GL_NO_ERROR);

		SDL_ShaderSetTexture(sha, "BaseSampler", 0, atom::getTextureByName("@splash01"));
		assert(glGetError() == GL_NO_ERROR);

		SDL_ShaderSetMatrix4x4(sha, "_MVP", &ortho.cell[0]);
		assert(glGetError() == GL_NO_ERROR);

		
		SDL_DrawVertexBuffer(vbo, SDL_TRIANGLE_FAN, 0, 4, 1);
		assert(glGetError() == GL_NO_ERROR);

        SDL_RenderPresent(ren); 
		
    } 

    gamescreen_manager& getGamescreenManager()
    {
        return *_man;
    }
};

extern "C" applet* new_game(application* app) {
    return new game_coil(app);
}
