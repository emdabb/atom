//
//  SDL_Ext.c
//  bouncy
//
//  Created by emiel Dam on 12/18/15.
//
//

#include "SDL_Ext.h"
#include "SDL_VertexBuffer.h"
#if   defined(__IOS__)
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#elif defined(__ANDROID__)
#elif defined(__X11__)
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#endif

#define STRCAT(x,y)	x##y

#include <assert.h>
#include <stdio.h>



void SDL_Debug(const char* file, int line, const char* format, ...) {
	char buffer[1024] = { '\0' };
	char res[1024] = { '\0' };
		
	va_list args;
	va_start(args, format);//STRCAT("%s", format));
	vsnprintf(buffer, sizeof(buffer), format, args);
	sprintf(res, "%s[SDL] %s%s", TERM_BOLD_RED, TERM_DEFAULT, buffer);
	printf(res);
	va_end(args);
}



static int glPrimitiveType[] = {
    GL_POINTS,          //SDL_PRIMITIVE_POINTS,
    GL_LINES,           //SDL_PRIMITIVE_LINES,
    GL_LINE_LOOP,       //SDL_PRIMITIVE_LINE_LOOP,
    GL_TRIANGLES,       //SDL_PRIMITIVE_TRIANGLES,
    GL_TRIANGLE_STRIP,  //SDL_PRIMITIVE_TRIANGLE_STRIP,
    GL_TRIANGLE_FAN     //SDL_PRIMITIVE_TRIANGLE_FAN
};

//static SDL_IndexBuffer*  g_IndexSource  = NULL;

int g_VertexBufferLock = 0;
//static SDL_VertexBuffer* g_VertexBufferLock = NULL;

//void SDL_SetVertexStreamSource(const SDL_VertexBuffer* buf) {
//    if(g_StreamSource != buf)
//    {
//        g_StreamSource = (SDL_VertexBuffer*)buf;
//        
//        SDL_ValidateVertexBuffer(buf);
//    }
//}

//void SDL_SetIndexBuffer(const SDL_IndexBuffer* buf) {
//}


//void SDL_DrawPrimitives(const SDL_Primitive prim, int first, ssize_t count){
//    glDrawArrays(glPrimitiveType[prim], (GLint)first, (GLsizei)count);
//}

//void SDL_DrawIndexedPrimitives(const SDL_Primitive prim, int first, ssize_t count) { 
//    ssize_t offset = first * sizeof(uint16_t);
//    glDrawElements(glPrimitiveType[prim], (GLsizei)count, GL_UNSIGNED_SHORT, BUFFER_OFFSET(offset));
//}

