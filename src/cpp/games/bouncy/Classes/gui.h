//
//  gui.h
//  application_name
//
//  Created by emiel Dam on 10/12/15.
//
//

#ifndef gui_h
#define gui_h

#include <SDL.h>
#include <vector>

namespace ui {
    
    struct container;
    struct widget;
    
    struct layout {
        virtual ~layout() = default;
        virtual void layoutContainer(container*) = 0;
        virtual void addLayoutComponent(widget*, int) = 0;
        virtual void removeLayoutComponent(widget*) = 0;
    };
    
    struct widget {
        widget*  _parent;
        SDL_Rect _rect;
        bool     _valid;
        
        virtual const bool isValid() const {
            return _valid != false;
        }
        virtual void validate() {
            _valid = true;
        }
        
        virtual void invalidate() {
            _valid = false;
        }
        
        void setParent(widget* w) {
            _parent = w;
        }
        
        const SDL_Rect& getBounds() const {
            return _rect;
        }
        
        void setBounds(const SDL_Rect& rc) {
            _rect = { rc.x, rc.y, rc.w, rc.h };
        }
    };
    
    struct container : public widget {
        std::vector<widget*> _children;
        layout* _layout;
        
        void add(widget* w, int constraint) {
            w->setParent(this);
            if(_layout != NULL) {
                _layout->addLayoutComponent(w, constraint);
            }
            _children.push_back(w);
            this->invalidate();
        }
        
        void remove(widget* w) {
        }
        
        void validateTree() {
            if(!isValid()) {
                if(_layout && _children.size()) {
                    _layout->layoutContainer(this);
                    
                    for(auto& i : _children) {
                        if(!i->isValid()) {
                            i->validate();
                        }
                    }
                }
            }
        }
        
        virtual void validate() {
            this->validateTree();
            widget::validate();
        }
        
        const int getNumberOfChildren() const {
            return (int)_children.size();
        }
    };
    
    struct abstract_button {
        
    };
    
    struct button : public abstract_button {
        
    };
    
    struct label : public widget {
        
    };
    
    struct text_input : public widget {
        
    };
    
    
    
    struct layout_grid
    {
        int _nrows;
        int _ncols;
        
        layout_grid(int nrows, int ncols)
        : _nrows(nrows)
        , _ncols(ncols)
        {
            
        }
        
        virtual ~layout_grid()
        {
        }
        
        virtual void layoutContainer(container* parent)
        {
            int ncomp = parent->getNumberOfChildren();
            int nrows = _nrows;
            int ncols = _ncols;
            
            if(nrows > 0)
                ncols = (ncomp + nrows - 1) / nrows;
            else
                nrows = (ncomp + ncols - 1) / ncols;
            
            int w = parent->getBounds().w;
            int h = parent->getBounds().h;
            int x = 0;
            int y = 0;
            
            w = (w - ncols - 1) / ncols;
            h = (h - nrows - 1) / nrows;
            
            int i = 1;
            
            for(auto& comp : parent->_children) {
                comp->setBounds({x, y, w, h});
                
                if(ncols > nrows) {
                    if(i >= ncols) {
                        y += h;
                        x  = 0;
                        i  = 0;
                    } else {
                        x += w;
                    }
                } else {
                    if(i >= nrows) {
                        x += w;
                        y  = 0;
                        i  = 0;
                    } else {
                        y += h;
                    }
                }
                i++;
            }
        }
    };
    
    struct layout_flow {
        
        virtual ~layout_flow()
        {
            
        }
        
        virtual void layoutContainer(container*)
        {
            
        }
    };
    
}

#endif /* gui_h */
