//
//  screen_gameover.cpp
//  coil
//
//  Created by emiel Dam on 11/6/15.
//
//

#include "screens.h"
#include "imgui.h"
#include "asset_manager.h"
#include "graphics.h"
#include <string>
#include <fstream>
#include <iostream>
#include <SDL.h>

#define DEFAULT_NAMESTRING "enter your name"

using namespace atom;


extern int g_score;

void screen_gameover::saveHighscore(const std::string& name) {

//   if(name.compare(DEFAULT_NAMESTRING) == 0) {
//       return;
//   }
//
//   NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
//                                                              NSUserDomainMask, YES);
//
//   NSString *documentsPath = [searchPaths objectAtIndex:0];
//
//   std::string respath( [ documentsPath UTF8String ] ) ;
//
//   std::string outpath = respath + std::string("/highscore.bin");
//
//   std::ofstream of;
//
//   of.open(outpath, std::ios::out | std::ios::app);
//   if(of.is_open()) {
//       std::cout << "highscore file opened" << std::endl;
//       //of.write();
//       of << "{" << name << ":" << g_score << "}" << std::endl;
//       of.close();
//   } else {
//       std::cout << "error opening: " << outpath << std::endl;
//   }
}

screen_gameover::screen_gameover()
{
    strcpy(_buf, DEFAULT_NAMESTRING);

        _popup = false;
        ad = atom::ad_manager::instance().getAdByName("admobAd_0");
        //                ad->request();
    
        ad->onAdWasDismissed.add(delegate<void(const AdEventArgs&)>([&] (const AdEventArgs& args) {
            ad->fetch();

            printf("delegate fired\n");
        }));

        ad->onAdWasFetched.add(delegate<void(const AdEventArgs&)>([&] (const AdEventArgs& args) {

        }));

    this->OnExit += [&] (gamescreen& screen, gamescreen_manager& man) {
        this->getOwner()->add(new screen_highscore);
    };
}


screen_gameover::~screen_gameover()
{
}

void screen_gameover::update(int dt, bool a, bool b)
{
    gamescreen::update(dt, a, b);
}

void screen_gameover::draw(SDL_Renderer* ren)
{
    int rw,rh;
    int tw,th;

    SDL_GetRendererOutputSize(ren, &rw, &rh);

    imgui_reset();
    imgui_setAlpha(1.0f - transition());
    imgui_setTextColor(250, 250, 250);

    std::string msg = "foo$bar";
    //char buf[256] = "enter your name";

    tw = std::max(tw, 128);
    th = std::max(th,  32);

    int x, y;
    x = rw / 2 - tw / 2;
    y = rh / 4 - th / 2;





        int bw = rw >> 3;
        int bh = rh >> 4;

    if(imgui_textinput(1, ren, 0, (SDL_Rect) {
            rw / 2 - bw / 2 - bw,
            rh / 2 - bh * 3,
            bw * 3,
            bh
    }, _buf)) {
        memset(_buf, 0, 256 * sizeof(char));
    }

        {
            if(imgui_button(2, ren, (SDL_Rect) {
                rw / 2 - bw / 2 - bw,
                rh / 2 - bh / 2,
                bw,
                bh
            }, 0, "save")) {

//                if(ad->isReady()) {
//                    ad->show();
//                }
                this->saveHighscore(_buf);
                quit();
            }

            if(imgui_button(3, ren, (SDL_Rect) {
                rw / 2 - bw / 2 + bw,
                rh / 2 - bh / 2,
                bw,
                bh
            }, 0, "exit")) {

                quit();
        }
    }
}
