//
//  assets.h
//  application_name
//
//  Created by emiel Dam on 9/29/15.
//
//

#ifndef atom_asset_manager_h
#define atom_asset_manager_h

#include "sprite_font.h"
#include <SDL.h>
#include <SDL_image.h>
#include <atom/core/singleton.h>
#include <map>
#include <string>

namespace atom {

    
    struct assets : public singleton<assets> {
        
    };
    
    
    
    std::string getResourcePath(const std::string &subDir = "");
    
    int addTexture(const std::string& name, SDL_Texture* tex);
    int addSurface(const std::string& name, SDL_Surface* surf);
    int loadSurface(const std::string& name, const std::string& filename);
    int addFont(const std::string& name, sprite_font* font);
    int loadFont(const std::string& name, std::istream& is, int size, SDL_Renderer* ren);
    int getFontId(const std::string& name);
    sprite_font* getFontById(int id);
    sprite_font* getFontByName(const std::string& name);
    int getSurfaceId(const std::string& name);
    SDL_Surface* getSurfaceById(int id);
    SDL_Surface* getSurfaceByName(const std::string& name);
    int getTextureId(const std::string& name);
    SDL_Texture* getTextureById(int id);
    SDL_Texture* getTextureByName(const std::string& name);
    
    void loadTextureLibrary(SDL_Renderer*, std::istream&);
}

//int SDL_LoadTexture(const char*, const std::istream&);
//int SDL_GetTexture(const char*);


#endif /* assets_h */
