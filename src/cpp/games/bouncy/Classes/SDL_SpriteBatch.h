#pragma once

#include "vec2.h"
#include <SDL.h>

struct SDL_VertexDeclaration;
struct SDL_VertexElement;

#define VERTICES_PER_SPRITE     4
#define MAX_BATCH_SIZE          1024
#define MAX_VERTEX_COUNT        MAX_BATCH_SIZE * VERTICES_PER_SPRITE

namespace SDL {

    struct vec4f {
        float x, y, z, w;
    };

    struct SpriteInfo {
        vec4f src, dst;
        vec2f origin;
        float rot, depth;
        int fx;
        SDL_Color color;
        SDL_Texture* texture;
    };

    struct SpriteVertex {
        vec2f position;
        vec2f texcoord;
        SDL_Color color;

        static const SDL_VertexDeclaration* declaration;
        static const SDL_VertexElement* elements[];
    };

    class SpriteBatch {
        ssize_t _num;
        SpriteInfo _q[MAX_BATCH_SIZE];
        SpriteVertex _v[MAX_VERTEX_COUNT];
    public:
        void draw(const SDL_Texture*, const SDL_Rect& dst, SDL_Rect* src);
    protected:
        void copy(const SpriteInfo&, SpriteVertex*);
        void flush();
         
    };

}
