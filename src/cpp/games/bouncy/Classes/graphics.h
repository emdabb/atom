//
//  graphics.h
//  bouncy
//
//  Created by emiel Dam on 12/14/15.
//
//

#ifndef graphics_h
#define graphics_h

#include <SDL.h>
#include <atom/core/math/curve.h>

void SDL_RenderDrawCurve(SDL_Renderer* ren, atom::curve2d& curve, const SDL_Color* color, int nsegments);


#endif /* graphics_h */
