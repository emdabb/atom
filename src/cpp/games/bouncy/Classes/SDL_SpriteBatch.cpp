#include "SDL_SpriteBatch.h"

using namespace SDL;

void SpriteBatch::copy(const SpriteInfo& info, SpriteVertex* vertex) {
    int tw, th;
    SDL_QueryTexture(info.texture, nullptr, nullptr, &tw, &th);
    float dx = 1.0f / tw;
    float dy = 1.0f / th;

    static const vec2f corner_offset[] = {
        { 0.0f, 0.0f },
        { 1.0f, 0.0f },
        { 1.0f, 1.0f },
        { 0.0f, 1.0f }
    };

    vec2f tx = info.rot != 1.0f ?
    (vec2f){ cosf(info.rot), sinf(info.rot) } :
    (vec2f){ 1.0f, 0.0f };
}

void SpriteBatch::flush() {
    for(ssize_t i=0; i < _num; i++) {
        copy(_q[i], &_v[i * VERTICES_PER_SPRITE]);
    }
}
