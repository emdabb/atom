//
//  SDL_Shader.h
//  bouncy
//
//  Created by emiel Dam on 12/31/15.
//
//

#ifndef SDL_Shader_h
#define SDL_Shader_h

#ifdef __cplusplus
#define C_API extern "C"
#else
#define C_API
#endif

struct SDL_Shader;
typedef struct SDL_Shader SDL_Shader;

struct SDL_Texture;

C_API SDL_Shader* SDL_CreateShader(SDL_Renderer*);
C_API SDL_Shader* SDL_CreateShader_RW(SDL_Renderer*);
C_API void SDL_FreeShader(SDL_Shader*);
C_API int SDL_CompileShader(const SDL_Shader* val);
C_API int SDL_LinkShader(const SDL_Shader* val);
C_API int SDL_SetShaderSource(const SDL_Shader* sha, const char* src);
C_API int SDL_SetShader(const SDL_Shader*);
C_API void SDL_ShaderSetTexture(const SDL_Shader*, const char*, int, SDL_Texture*);
C_API void SDL_ShaderSetMatrix4x4(const SDL_Shader*, const char*, float*);


#endif /* SDL_Shader_h */
