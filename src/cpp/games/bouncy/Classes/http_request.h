//
//  http_request.h
//  bouncy
//
//  Created by emiel Dam on 12/10/15.
//
//

#ifndef http_request_h
#define http_request_h

#include <happyhttp/happyhttp.h>
#include <atom/core/event_handler.h>

class http_request;

struct HttpEventArgs {
    http_request* req;
    int status;
};

class http_request {
    static void OnBegin( const happyhttp::Response* r, void* userdata )
    {
        http_request* req = (http_request*)userdata;
        printf( "BEGIN (%d %s)\n", r->getstatus(), r->getreason() );
        req->reset();
        req->OnRequestIsStarted( { req, r->getstatus() } );
    }
    
    // invoked to process response body data (may be called multiple times)
    static void OnData( const happyhttp::Response* r, void* userdata, const unsigned char* data, int n )
    {
        http_request* req = (http_request*)userdata;
        printf( "DATA (%d %s)\n", r->getstatus(), r->getreason() );
        req->inc(data, n);
        req->OnRequestIsProcessing( { req, r->getstatus() } );
    }
    
    // invoked when response is complete
    static void OnComplete( const happyhttp::Response* r, void* userdata )
    {
        printf( "COMPLETE (%d %s)\n", r->getstatus(), r->getreason() );
        http_request* req = (http_request*)userdata;
//        printf( "COMPLETE (%d bytes)\n", count );
        req->OnRequestIsDone( { req, r->getstatus() } );
    }
    
    static const int datasize = 1024 * 1024 * 8 * sizeof(uint8_t);

    int _nbytesReceived;
    void* _bytes;
    
    std::string _addr;
    int _port;
    std::string _get;
    
    std::function<void(const HttpEventArgs&)> OnRequestIsStarted;
    std::function<void(const HttpEventArgs&)> OnRequestIsProcessing;
    std::function<void(const HttpEventArgs&)> OnRequestIsDone;

public:
    http_request(const std::string& addr, int port, const std::string& get)
    : _addr(addr)
    , _port(port)
    , _get(get)
    {
        _bytes = malloc(datasize);
    }
    
    virtual ~http_request() {
        free(_bytes);
    }
    
    void fetch() {
        reset();
        try {
            std::cout << "running GET on " << _addr.c_str() << ":" << _port << _get << std::endl;
            happyhttp::Connection conn(_addr.c_str(), _port);
            conn.setcallbacks(OnBegin, OnData, OnComplete, this);
            conn.request("GET", _get.c_str());
            while(conn.outstanding()) {
                conn.pump();
            }
        } catch(happyhttp::Wobbly& w) {
            std::cout << "caught wobbly: " << w.what() << std::endl;
        } catch(...) {
            std::cout << "uncaught wobbly" << std::endl;
        }
    }
    
    void reset() {
        memset(_bytes, 0, datasize);
        _nbytesReceived = 0;
    }
    
    void inc(const uint8_t* data, int n) {
        memcpy((uint8_t*)_bytes + _nbytesReceived, data, n);
        _nbytesReceived += n;
    }
    
    const uint8_t* bytes() const {
        return (uint8_t*)_bytes;
    }
    
    const int size() const {
        return _nbytesReceived;
    }

    void setRequestStartedCallback(const std::function<void(const HttpEventArgs&)>& val) {
        OnRequestIsStarted = val;
    }
    void setRequestProcessingCallback(const std::function<void(const HttpEventArgs&)>& val) {
        OnRequestIsProcessing= val;
    }
    void setRequestDoneCallback(const std::function<void(const HttpEventArgs&)>& val) {
        OnRequestIsDone = val;
    }

    
//public:
    
    
    
};


#endif /* http_request_h */
