//
//  screen_highscore.cpp
//  coil
//
//  Created by emiel Dam on 11/18/15.
//
//

#include "screens.h"
#include "imgui.h"
#include "asset_manager.h"
#include <fstream>
#include <atom/engine/easing.h>

using namespace atom;

extern SDL_Renderer* g_ren;

screen_highscore::screen_highscore()
{
    OnExit += [&] (gamescreen&, gamescreen_manager&) {
        this->getOwner()->add(new screen_menu);
    };
}

screen_highscore::~screen_highscore()
{

}

void screen_highscore::load_content()
{
//    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
//                                                               NSUserDomainMask, YES);
//
//    NSString *documentsPath = [searchPaths objectAtIndex:0];
//
//    std::string respath( [ documentsPath UTF8String ] ) ;
//
//    std::string outpath = respath + std::string("/highscore.bin");
//
//    std::ifstream is;
//    is.open(outpath, std::ios::in);
//    if(is.is_open()) {
//        std::string line;
//        while(std::getline(is, line)) {
//            std::string::size_type a = line.find_first_of("{");
//            std::string::size_type b = line.find_first_of(":");
//            std::string::size_type c = line.find_first_of("}");
//            std::string name = line.substr(a + 1, b - 1);
//            std::string score= line.substr(b + 1, c);
//
//            int iscore = std::atoi(score.c_str());
//
//            _list.insert(std::pair<int, std::string>(iscore, name));
//        }
//    }
}

void screen_highscore::update(int dt, bool a, bool b)
{
    gamescreen::update(dt, a, b);
}

void screen_highscore::draw(SDL_Renderer* vg)
{
    int rw, rh;
    SDL_GetRendererOutputSize(vg, &rw, &rh);

    int bgid = atom::getTextureId("@highscore_bg");



//    NVGpaint paint = nvgImagePattern(vg, 0.0f, 0.0f, rw, rh, 0.0f, bgid, 1.0f - transition());
//
//    nvgBeginPath(vg);
//    nvgRect(vg, 0.0f, 0.0f, rw, rh);
//    nvgFillPaint(vg, paint);
//    nvgFill(vg);

    imgui_reset();
    imgui_setAlpha(1.0f - transition());

    int bw = rw / 8;
    int bh = rh / 8;

    int a = -bh;
    int b = rh - bh * 2;
    float t = atom::easing::ease_elastic_out(1.0f - transition());
    int c = (int)(a + (b - a) * t);

    if(imgui_button(1, vg, (SDL_Rect) { rw / 2 - bw / 2, c, bw, bh }, 0, "exit")) {
        quit();
    }



    std::map<int, std::string>::reverse_iterator it = _list.rbegin();

    for(int i=0; i < 5 && it != _list.rend(); i++, it++) {

        std::string scoreString;
        std::stringstream ss;
        ss << (*it).first;
        scoreString = ss.str();
        imgui_label(2 + i,      vg, 0, (SDL_Point) { 128, (i + 1) * 32 }, (*it).second.c_str(), 16.f);
        imgui_label(2 + 5 * i,  vg, 0, (SDL_Point) { 128 + 256, (i + 1) * 32 }, scoreString.c_str(), 16.f);

    }

    gamescreen::draw(vg);
}
