//
//  color.h
//  application_name
//
//  Created by emiel Dam on 10/1/15.
//
//

#ifndef color_h
#define color_h

#include <SDL.h>

struct ColorPreset {
    static const SDL_Color Base03;
    static const SDL_Color Base02;
    static const SDL_Color Base01;
    static const SDL_Color Base00;
    static const SDL_Color Base0;
    static const SDL_Color Base1;
    static const SDL_Color Base2;
    static const SDL_Color Base3;
    static const SDL_Color Yellow;
    static const SDL_Color Orange;
    static const SDL_Color Red;
    static const SDL_Color Magenta;
    static const SDL_Color Violet;
    static const SDL_Color Blue;
    static const SDL_Color Cyan;
    static const SDL_Color Green;
};

namespace detail {
    static SDL_Color SDL_MakeColor(uint8_t r, uint8_t g, uint8_t b)
    {
        return (SDL_Color) { r, g, b, 255 };
    }
    static
    SDL_Color SDL_MakeColorHSV(uint8_t h, uint8_t s, uint8_t v)
    {
        if(s == 0)
            return SDL_MakeColor(v, v, v);
        uint8_t region  = h / 43;
        uint8_t fpart   = (h - (region * 43)) * 6;
        uint8_t p       = (v * (255 - s)) >> 8;
        uint8_t q       = (v * (255 - ((s * fpart) >> 8))) >> 8;
        uint8_t t       = (v * (255 - ((s * (255 - fpart)) >> 8))) >> 8;
        
        switch(region) {
            case 0: return  SDL_MakeColor(v, t, p);
            case 1: return  SDL_MakeColor(q, v, p);
            case 2: return  SDL_MakeColor(p, v, t);
            case 3: return  SDL_MakeColor(p, q, v);
            case 4: return  SDL_MakeColor(t, p, v);
            default: return SDL_MakeColor(v, p, q);
        }
        return SDL_MakeColor(0, 0, 0);
    }
}

#endif /* color_h */
