//
//  screen_play.cpp
//  coil
//
//  Created by emiel Dam on 11/6/15.
//
//


#include <SDL.h>
#include <SDL_mixer.h>
#include "screens.h"
#include "sprite_font.h"
#include "asset_manager.h"
#include "input.h"
#include <atom/core/math/math.h>
#include "http_request.h"
#include "particle_spawn.h"
#include "particle_update.h"
#include <atom/core/math/curve.h>
#include <cjson/cJSON.h>
#include <iostream>
#include <float.h>
#include "input.h"
#include "color.h"

#define RESTITUTION_TERRAIN         (0.1f)
#define RESTITUTION_CARWHEEL        (0.1f)
#define RESTITUTION_CARBODY         (0.3f)
#define FRICTION_TERRAIN            (1.f)
#define FRICTION_CARWHEEL           (1.f)
#define FRICTION_CARBODY            (0.5f)
#define DENSITY_CARBODY             (128.0f)
#define DENSITY_CARWHEEL            (16.0f)
#define MAX_MOTOR_TORQUE            (128.0f)
#define IDLE_MOTOR_TORQUE           (64.f)
#define MAX_MOTOR_SPEED             (8.0f * M_PI)
#define DOT_SIZE                    (32.0f)


extern SDL_Renderer* g_Renderer;
struct remote_database {
    struct entry {
        std::string name;//"name": "strongworld_912x1280_v4",
        std::string game;//"game": "Strong World D.",
        std::string androidid;//"androidID": "com.mtr.sw",
        std::string appleid;//"appleID": "1017242667",
        std::string path;//"picture": "uploads/Banners/1817-1sl3emj.png",
        std::string id;//"_id": "5652e4b63e1d491907dd139a"
        int clickcount;//"clickcount": "9",
    };
    
    std::vector<entry*> _entry;
    
    void parseJsonNode(cJSON* json, entry* en) {
        if(json->string && json->valuestring) {
            std::cout << json->string << ":" << json->valuestring << std::endl;
            
            if(!strcmp(json->string, "name")) {
                en->name = json->valuestring;
            }
            if(!strcmp(json->string, "game")) {
                en->game = json->valuestring;
            }
            if(!strcmp(json->string, "androidID")) {
                en->androidid = json->valuestring;
            }
            if(!strcmp(json->string, "appleID")) {
                en->appleid = json->valuestring;
            }
            if(!strcmp(json->string, "picture")) {
                en->path = json->valuestring;
            }
            if(!strcmp(json->string, "clickcount")) {
                en->clickcount = std::atoi(json->valuestring);
            }
            if(!strcmp(json->string, "_id")) {
                en->id = json->valuestring;
            }
        }
        
        if(json->child) {
            parseJsonNode(json->child, en);
        }
        
        if(json->next) {
            parseJsonNode(json->next, en);
        }
        
    }
    
    void parseJsonArray(cJSON* json) {
        
        int n = cJSON_GetArraySize(json);
        entry* en = new entry;
        
        for(int i=0; i < n; i++) {
            parseJsonNode(cJSON_GetArrayItem(json->child, i), en);
            _entry.push_back(en);
        }
    }
    
    void collect() {
        http_request* req = new http_request("pic.tfjoy.com", 3000, "/database");
        std::cout << this << std::endl;
        
        req->OnRequestIsDone += [&] (const HttpEventArgs& args) {
            std::cout << args.req->bytes() << std::endl;
            
            cJSON* node = cJSON_Parse((const char*)args.req->bytes());
            if(node) {
                this->parseJsonArray(node);
            }
            
        };
        req->fetch();
    }
};


enum {
    E_FILTER_NONE       = 0,
    E_FILTER_TERRAIN    = 1 << 0,
    E_FILTER_CARBODY    = 1 << 1,
    E_FILTER_CARWHEEL   = 1 << 2,
    E_FILTER_SAFEZONE   = 1 << 3,
    E_FILTER_ALL        = -1
};

const int screen_play::_timeToMaxSpawnRate = 120000;

#define MASK_DEFAULT    (E_COMPONENT_PHYSICS | E_COMPONENT_COLLIDER | E_COMPONENT_VISUAL)

struct trail {
    static trail defaultValue;
};

trail trail::defaultValue = {} ;

static void update_trail(particles<trail>* group, particles<trail>::particle& p, int, int) {

}

SDL_Rect cameraRect = {
    0, 0, 640, 480
};

static vec2f rotate(const vec2f& p, const vec2f& at, float t) {
    vec2f res;
    float c = cos((double)t);
    float s = sin((double)t);

    res.x = at.x + (p.x * c - p.y * s);
    res.y = at.y + (p.x * s + p.y * c);

    return res;
}

struct RegularDot {
    
    static
    void createCircle(World* world, float x0, float y0, float r, int nseg) {
        atom::curve2d cc;

        intptr_t id = World_createEntity(world);
        world->mask[id] = E_COMPONENT_GROUP;
        
        float step = (2 * M_PI) / (nseg - 1);
        
        for(int i=1; i < nseg; i++) {
            float xx = x0 + cos((double)i * step) * r;
            float yy = y0 + sin((double)i * step) * r;
            
            intptr_t dot = create(world, xx, yy);
            world->mask[dot] |= E_COMPONENT_NESTED;
            world->nested[dot].parent = id;
        }
        world->group[id].size = nseg;
        world->group[id].scored = 0;
        world->group[id].timedOut = 0;
    }
    
    static
    void createAlongCurve(World* world, float x0, float y0, float x1, float y1, int nseg) {
        atom::curve2d cc;
        
        intptr_t id = World_createEntity(world);
        world->mask[id] = E_COMPONENT_GROUP;
        
        cc.add(0.f, x0, y0);
//        cc.add(1.f, x0 + randf() * 128.f, y0);
//        cc.add(2.f, x0, y0 + randf() * 128.f);
        cc.add(3.f, x1, y1);
        
        float range = cc.timerange();
        
        vec2f q0, q1;
        cc.at(0.0f, &q0.x, &q0.y);
        for(int i=1; i < nseg; i++)
        {
            float r = range * ((float)i / (float)nseg);
            cc.at(r, &q1.x, &q1.y);
            intptr_t dot = RegularDot::create(world, q1.x, q1.y);
            world->mask[dot] |= E_COMPONENT_NESTED;
            world->nested[dot].parent = id;
        }
        world->group[id].size = nseg;
        world->group[id].scored = 0;
        world->group[id].timedOut = 0;
    }
    
    static intptr_t create(World* world, float x, float y)
    {
        intptr_t id = World_createEntity(world);
        
        world->mask[id] =   E_COMPONENT_TWEEN |
                            E_COMPONENT_TIMEOUT |
                            E_COMPONENT_TRANSFORM |
                            E_COMPONENT_VISUAL |
                            E_COMPONENT_TRAILOBSERVER |
                            E_COMPONENT_PARTICLE_EMITTER |
                            E_COMPONENT_SCORE;
        
        {
            Transform* t = &world->transform[id];
            t->pos.x = x;
            t->pos.y = y;
        }
        
        {
            Visual* v = &world->visual[id];
            v->texid = atom::getTextureId("@ballGrey");
            v->color = detail::SDL_MakeColorHSV(rand() % 255, 255, 255);

        }
        
        {
            Tween* t = &world->tween[id];
            t->dst = &world->transform[id].scale;
            t->a   = 0.0f;
            t->b   = 1.0f;
            t->ease= &atom::easing::ease_elastic_out;
        }
        
        {
            Timeout* t = &world->timeout[id];
            t->maxTime = 3000;
            t->time    = 0;
            t->onTimeout = [&] (World* world, intptr_t i) {
                if(World_hasComponent(world, i, E_COMPONENT_NESTED)) {
                    intptr_t parent = world->nested[i].parent;
                    if(World_hasComponent(world, parent, E_COMPONENT_GROUP)) {
                        
                        Group* g = &world->group[i];
                        
                        g->timedOut++;
                        
                        if(g->timedOut == g->size - 1) {
                            
                            World_destroyEntity(world, parent);
                            
                        }
                    }
                }
                world->totalScore -= world->score[i];
                
                world->numDotsMissed++;
                
                World_destroyEntity(world, i);
            };
        }
        
        {
            world->particles[id] = E_PARTICLE_SYSTEM_EXPLODE;
        }
        
        {
            world->score[id] = 1000;
        }
        
        world->numDotsSpawned++;
        
        return id;
    }
    
    static float score(World* world, intptr_t id) {
        return (float)world->score[id] * ((float)world->timeout[id].time / (float)world->timeout[id].maxTime);
    }
};

struct PopupText {
    static intptr_t create(World* world, float x, float y, const std::string& what, int fontId, int maxTime) {
        intptr_t id = World_createEntity(world);
        
        world->mask[id] =
        E_COMPONENT_TEXT    |
        E_COMPONENT_TIMEOUT |
        E_COMPONENT_TWEEN   |
        E_COMPONENT_TRANSFORM;
        
        {

        }
        
        world->transform[id].pos = { x, y };
        world->transform[id].rot = 0.0f;
        
        world->drawDebug[id] = [&] (World* world, intptr_t i, SDL_Renderer* ren) {
            atom::sprite_font* font = atom::getFontById(world->text[i].fontId);
            
            float a = 1.0f - (float)world->timeout[i].time / world->timeout[i].maxTime;
            
            world->visual[i].color.a = (uint8_t)(a * 255.f);
            
            font->drawString(ren,
                             {   (int)(world->transform[i].pos.x + 0.5f),// - cameraRect.x + cameraRect.w / 2,
                                 (int)(world->transform[i].pos.y + 0.5f),// - cameraRect.y + cameraRect.h / 2
                             },
                             world->text[i].str,
                             &world->visual[i].color);
        };
        
        world->visual[id].color = { 255, 255, 255, 255 };
        
        world->text[id].fontId = fontId;
        strcpy(world->text[id].str, what.c_str());
        
        world->timeout[id].time = 0;
        world->timeout[id].maxTime = maxTime;
        world->timeout[id].onTimeout = [&] (World* world, intptr_t i) {
            World_destroyEntity(world, i);
        };
        
        world->tween[id].a   = y;
        world->tween[id].b   = y - 40;
        world->tween[id].dst = &world->transform[id].pos.y;
        world->tween[id].ease= &atom::easing::ease_elastic_out;
        
        return id;
    }
};

screen_play::screen_play()
{
    _world.totalScore           = 0;
    _world.numDotsScored        = 0;
    _world.numDotsMissed        = 0;
    _world.numDotsSpawned       = 0;
    
    _currentTime    = 0;
    mRw = 100;
    mRh = 100;
//    _world.trailParticles = new particles<trail>(128, &update_trail);
    
    int rw, rh;
    SDL_GetRendererOutputSize(g_Renderer, &rw, &rh);
    _world.grid.create((float)rw / GRID_W, (float)rh / GRID_H);
    
    srand(SDL_GetTicks());
}

screen_play::~screen_play()
{
}

static void musicIsDone() {
    
}

static void postMix(void* udata, uint8_t* stream, int len) {
    
}

void screen_play::load_content() {
    
    remote_database db;
    db.collect();

//    http_request* req = new http_request("pic.tfjoy.com", 3000, "/" + db._entry[2]->path);//uploads/Banners/1817-1sl3emj.png");
//    req->OnRequestIsDone += [&] (const HttpEventArgs& args) {
//        SDL_RWops* ops    = SDL_RWFromMem((char*)args.req->bytes(), args.req->size());
//        SDL_Surface* surf = IMG_Load_RW(ops, 0);
//        SDL_Texture* tex  = SDL_CreateTextureFromSurface(g_Renderer, surf);
//        atom::addTexture("remote", tex);
//        //std::cout << args.req->bytes() << std::endl;
//    };
//    req->fetch();
    
    
    _world.particleTextureId[E_PARTICLE_SYSTEM_EXPLODE] = atom::getTextureId("@star");
    _world.particleGroup[E_PARTICLE_SYSTEM_EXPLODE] = new particles<vec2f>(128, &update_boom);
    
    atom::OnMouseMoved += atom::delegate<void(int, int, int, int)>(&_world.trail, &TrailRenderer::mouse_move);
    atom::OnMousePressed += atom::delegate<void(int, int)>(&_world.trail, &TrailRenderer::mouse_press);
    atom::OnMouseReleased += atom::delegate<void(int, int)>(&_world.trail, &TrailRenderer::mouse_release);
    
#if 0
    Mix_Music* mus = atom::getMusicByName("@music_play");
    if(mus)
    {
        if(!Mix_PlayingMusic()) {
            Mix_PlayMusic(mus, 0);
            Mix_HookMusicFinished(&musicIsDone);
            Mix_SetPostMix(&postMix, this);
        }
    }
#endif
}

static void destroyDot(World* world, intptr_t id) {

    
    const vec2f& pos = world->transform[id].pos;

    burst(pos, 8, M_PI * 2.0f, 0.0f, 1.0f, 1.0f, world->particleGroup[E_PARTICLE_SYSTEM_EXPLODE], 0.4f, 0.6f);
    std::stringstream ss;
    ss << (int)(RegularDot::score(world, id) + 0.5f);
    PopupText::create(world, pos.x, pos.y, ss.str(), 0, 1000);
    
    world->numDotsScored++;
    
    if(World_hasComponent(world, id, E_COMPONENT_NESTED)) {
        intptr_t parent = world->nested[id].parent;
        world->group[parent].scored++;
        if(world->group[parent].scored == world->group[parent].size - 1) {
            std::string str = "perfect!";
            int tw, th;
            int rw, rh;
            SDL_GetRendererOutputSize(g_Renderer, &rw, &rh);
            
            atom::getFontById(1)->measureString(str, &tw, &th);
            
            PopupText::create(world, pos.x, pos.y, str, 1, 4000);//rw/2.f - tw / 2.0f, rh/2.f - th / 2.f, str, 1, 4000);
            
            int cc = Mix_PlayChannelTimed(-1, atom::getSoundById(0), 0, -1);
        } else {
//            int cc = Mix_PlayChannelTimed(-1, atom::getSoundByName("@tap"), 0, -1);
        }
    } else {
        int cc = Mix_PlayChannelTimed(-1, atom::getSoundById(1 + (rand() % 12)), 0, -1);
    }
    World_destroyEntity(world, id);
}

void screen_play::update(int dt, bool a, bool b)
{
    static int t = 0;
    static const int MAX_SPAWNRATE_TIMEOUT = 40000;
    _currentTime += dt;
    _currentTime = std::min(_currentTime, MAX_SPAWNRATE_TIMEOUT);
    t += dt;
    
    _world.trail.update(dt);

    static const int minSpawnRate = 1500;
    static const int maxSpawnRate = 400;
    static int timeToSpawn = minSpawnRate;
    static const float margin = 64.f;
    
    cameraRect.w = mRw;
    cameraRect.h = mRh;
    
    if(t >= timeToSpawn) {


        
        float fRand = randf();
        
        if(fRand > 0.95f) {

            float x0 = margin * 0.5f;//margin * 0.5f + (std::max(0.5f, randf()) * (mRw - margin));// / 2.0f;
            float y0 = margin * 0.5f + (std::max(0.5f, randf()) * (mRh - margin));// / 2.0f;
            float x1 = mRw - margin * 0.5f;//margin * 0.5f + (std::max(0.5f, randf()) * (mRw - margin));// / 2.0f;
            float y1 = margin * 0.5f + (std::max(0.5f, randf()) * (mRh - margin));// / 2.0f;
        
            RegularDot::createAlongCurve(&_world, x0, y0, x1, y1, 8);
        }
        else if(fRand > 0.97f) {
            float r  = 64.f + randf() * 64.f;
            float x0 = r * 0.5f + (randf() * (mRw - r));
            float y0 = r * 0.5f + (randf() * (mRh - r));
            RegularDot::createCircle(&_world, x0, y0, r, 8);
        }
        else {
            float x0 = margin * 0.5f + (randf() * (mRw - margin));
            float y0 = margin * 0.5f + (randf() * (mRh - margin));
            RegularDot::create(&_world, x0, y0);
        }
        
        float a = (float)minSpawnRate;
        float b = (float)maxSpawnRate;
        float c = (float)_currentTime / (float)MAX_SPAWNRATE_TIMEOUT;
        float d = atom::easing::ease_quadratic_in(c);
        
        float fCurrentSpawnRate = atom::math::lerp(a, b, d);
        
        timeToSpawn = (int)(fCurrentSpawnRate + 0.5f);

        t = 0;
    }
    
    for(int i=0; i < E_PARTICLE_SYSTEM_MAX; i++) {
        _world.particleGroup[i]->update(t, dt);
    }



    float xx = cameraRect.x;
    float yy = cameraRect.y;
    
    float maxx = -FLT_MAX;
    float minx =  FLT_MAX;
    float maxy = -FLT_MAX;
    float miny =  FLT_MAX;
    
    
    int count = 0;
    
    for(int i=0; i < MAX_ENTITIES; i++) {
        if(World_hasComponent(&_world, i, E_COMPONENT_CAMERAFOLLOW | E_COMPONENT_TRANSFORM)) {
            maxx = std::max(maxx, _world.transform[i].pos.x);
            maxy = std::max(maxy, _world.transform[i].pos.y);
            minx = std::min(minx, _world.transform[i].pos.x);
            miny = std::min(miny, _world.transform[i].pos.y);
            count++;
        }
    }
    
    if(count) {
        float cx = (minx + maxx) * 0.5f;
        float cy = (miny + maxy) * 0.5f;
        
        xx = xx + (cx - xx) * 0.1f;
        yy = yy + (cy - yy) * 0.1f;
        
        cameraRect.x = (int)(xx + 0.5f);
        cameraRect.y = (int)(yy + 0.5f);
    }
    
    for(int i=0; i < MAX_ENTITIES; i++) {
        if((_world.mask[i] & E_COMPONENT_INPUT) == E_COMPONENT_INPUT) {
            _world.input[i](&_world, i);
        }

        if(World_hasComponent(&_world, i, E_COMPONENT_TRAILOBSERVER)) {
            const vec2f& pos = _world.transform[i].pos;
            
//            float score = RegularDot::score(&_world, i);

            if(_world.trail.containsPoint(
            {
                (int)(DOT_SIZE * 0.5f + pos.x + .5f),
                (int)(DOT_SIZE * 0.5f + pos.y + .5f)}
            )) {
//                _score += RegularDot::score(&_world, i);
//                World_destroyEntity(&_world, i);
//                explode(pos, 8, 2.0f, _world.particleGroup[E_PARTICLE_SYSTEM_EXPLODE]);
                destroyDot(&_world, i);
                _world.totalScore += (int)(RegularDot::score(&_world, i) + 0.5f);
            }
            
            int mx, my;
            int mstate = SDL_GetMouseState(&mx, &my);

            if(atom::SDL_IsMouseDown()) {
                vec2f dif = { pos.x - (float)mx, pos.y - (float)my };
                float distSq = dif.x * dif.x + dif.y * dif.y;
                if(distSq <= ((DOT_SIZE) * (DOT_SIZE))) {
                    //_world.destroy[i](&_world, i);
                    destroyDot(&_world, i);
                    _world.totalScore += (int)(RegularDot::score(&_world, i) + 0.5f);
                }
            }

        }

        if((_world.mask[i] & E_COMPONENT_TIMEOUT) == E_COMPONENT_TIMEOUT) {
            _world.timeout[i].time += dt;
            if(_world.timeout[i].time >= _world.timeout[i].maxTime) {
                _world.timeout[i].onTimeout(&_world, i);
                _world.timeout[i].time = 0;
            }
        }
        
        if(World_hasComponent(&_world, i, E_COMPONENT_TWEEN | E_COMPONENT_TIMEOUT)) {
            Tween* tt   = &_world.tween[i];
            Timeout* to = &_world.timeout[i];
            
            to->time += dt;
            
            float to_cur = std::min((float)to->maxTime, to->time * 2.f);
            float to_max = to->maxTime;
            
            *(tt->dst) = tt->a + (tt->b - tt->a) * tt->ease(to_cur / to_max);
        
        }
    }

    World_cleanup(&_world);

    gamescreen::update(dt, a, b);
}

void screen_play::draw(SDL_Renderer* ren) //SDL_Renderer* ren)
{
    //nvgMoveTo(ren, -_world.cameraPos.x, -_world.cameraPos.y);

    SDL_GetRendererOutputSize(ren, &mRw, &mRh);
    
    SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
//    _world.grid.draw(ren);
    
    SDL_Texture* tex = atom::getTextureByName("remote");
//    if(tex) {
//        int w, h;
//        SDL_QueryTexture(tex, nullptr, nullptr, &w, &h);
//        SDL_Rect dst = { -cameraRect.x, -cameraRect.y, w, h};
//        SDL_RenderCopy(ren, tex, nullptr, &dst );
//    }
    
    

    for(int i=0; i < MAX_ENTITIES; i++) {
        if(World_hasComponent(&_world, i, E_COMPONENT_TEXT)) {
            _world.drawDebug[i](&_world, i, ren);
        }
        if(World_hasComponent(&_world, i, E_COMPONENT_VISUAL | E_COMPONENT_TRANSFORM)) {

            Transform* tt = &_world.transform[i];
            SDL_Rect dst = {
                (int)(tt->pos.x - DOT_SIZE * 0.5f * tt->scale + 0.5f),
                (int)(tt->pos.y - DOT_SIZE * 0.5f * tt->scale + 0.5f),
                (int)(DOT_SIZE * tt->scale + 0.5f),
                (int)(DOT_SIZE * tt->scale + 0.5f)
            };
            
            SDL_Color* color = &_world.visual[i].color;
            
            SDL_Texture* tex = atom::getTextureById(_world.visual[i].texid);
            SDL_SetTextureColorMod(tex, color->r, color->g, color->b);
            
            SDL_RenderCopyEx(ren, tex, nullptr, &dst, 0.0f, nullptr, SDL_FLIP_NONE);
        }
    }
    
    for(int i=0; i < E_PARTICLE_SYSTEM_MAX; i++) {
        _world.particleGroup[i]->draw(ren, atom::getTextureById(_world.particleTextureId[i]), cameraRect);
    }
    
    {
        std::stringstream ss;
        ss << "score:" << _world.totalScore;
        std::string scoreString = ss.str();
    
        SDL_Color color = { 255, 255, 255, 255 };
        atom::getFontById(0)->drawString(ren, { 0, 0}, scoreString, &color);
    }
    
    {
        std::stringstream ss;
        float fRatio = _world.numDotsScored > 0 ? (float)_world.numDotsScored / (float)_world.numDotsSpawned : 0.0f;
        ss << "ratio:=[" << fRatio << "]";
        std::string scoreString = ss.str();
        
        SDL_Color color = { 255, 255, 255, 255 };
        atom::getFontById(0)->drawString(ren, { 0, 64}, scoreString, &color);
    }
    
    
    _world.trail.draw(ren, { 255, 255, 255, 255} );
    
    gamescreen::draw(ren);
}

