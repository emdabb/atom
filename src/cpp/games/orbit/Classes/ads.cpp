//
//  ads.m
//  application_name
//
//  Created by emiel Dam on 10/14/15.
//
//

//#import <Foundation/Foundation.h>
#include <stdint.h>

#include "ads.h"
#include <iostream>
#include <cstring>
#include <cjson/cJSON.h>

namespace ads {

    std::map<std::string, Advertisement*> _ads;

//    static void AdManager_cacheNext(Advertisement* ad) {
//        ad->request();
//    }

    void AdManager_buildAd(cJSON* json, Advertisement* ad) {
        cJSON* ptr = json;
        while(ptr) {
            if(!strcmp(ptr->string, "location")) {
                ad->setLocation(ptr->valuestring);
            }
            if(!strcmp(ptr->string, "publisher-id")) {
                ad->setPublisherId(ptr->valuestring);
            }
            if(!strcmp(ptr->string, "app-id")) {
                ad->setAppId(ptr->valuestring);
//                ad->request();
                //ad->onAdIsDismissed.add(atom::delegate<void(Ad*)>(&AdManager_cacheNext));
            }
            ptr = ptr->next;
        }
    }

    void AdManager_Parse(cJSON* json) {
        static std::string name;
        if(json->string) {
            if(!json->valuestring) {
                name = json->string;
            }
            if(!strcmp(json->string, "type")) {
                std::cout << "creating ad: " << name << std::endl;
                Advertisement* ad = AdFactory::instance().create(json->valuestring);
                AdManager_buildAd(json, ad);
                //_ads.push_back(ad);
                //ad->request();
                _ads[name] = ad;
            }

        }

        if(json->child) {
            AdManager_Parse(json->child);
        }

        if(json->next) {
            AdManager_Parse(json->next);
        }
    }



    void AdManager::cacheFrom(std::istream &is) {
        std::istreambuf_iterator<char> eos;
        std::string rw(std::istreambuf_iterator<char>(is), {});

        cJSON* json = cJSON_Parse(rw.c_str());
        if(!json)
            std::cout << "cJSON error: " << cJSON_GetErrorPtr() << std::endl;
        else
            AdManager_Parse(json);
    }

    Advertisement* AdManager::getAdByName(const std::string& name) {
        std::map<std::string, Advertisement*>::iterator it = _ads.find(name);
        if(it != _ads.end()) {
            return (*it).second;
        }
        return NULL;
    }
}
