#include "imgui.h"
#include "input.h"
#include "sprite_font.h"
#include "asset_manager.h"
#include <SDL.h>
#include <cstring>

namespace atom 
{
    static struct guistate {
        int hot;
        int active;
        SDL_Color baseColor;
        SDL_Color hotColor;
        SDL_Color activeColor;
        SDL_Color textColor;
        uint16_t keyChar;
        int kbditem;
    } uistate;

    int  imgui_loadFont(SDL_Renderer* vg, const char* path, const char* name) {
//        int res = nvgCreateFont(vg, name, path);
        //return res;
        return 0;
    }
    bool SDL_IntersectRectAndPoint(const SDL_Rect& rc, const SDL_Point* p)
    {
        
        return ((p->x >= rc.x) &&
                (p->x <= (rc.x + rc.w)) &&
                (p->y >= rc.y) &&
                (p->y <= (rc.y + rc.h)));
    }
    
    void imgui_setAlpha(float alpha) {
        uistate.hotColor.a = alpha;
        uistate.textColor.a = alpha;
        uistate.activeColor.a = alpha;
        uistate.baseColor.a = alpha;
    }
    
    void imgui_setTextColor(uint8_t r, uint8_t g, uint8_t b) {
        uistate.textColor = { r, g, b, 255} ;
    }

    void imgui_reset() 
    {
        uistate.hot = uistate.active = 0;
        uistate.baseColor = {181,137,  0, 255};
        uistate.hotColor  = {220, 50, 47, 255};
        uistate.activeColor= {211, 54,130, 255};
        uistate.textColor  ={0,0,0, 255};
        uistate.keyChar = input_GetLastKeyPressed();
        uistate.kbditem = -1;
    }
    
    void imgui_label(int id, SDL_Renderer* ren, int fontId, const SDL_Point& at, const char* text, float size) {
        //sprite_font* font = asset_manager
//        nvgBeginPath(ren);
//        nvgFontSize(ren, size);
//        nvgFillColor(ren, uistate.textColor);
//        nvgFontFaceId(ren, fontId);
//        nvgTextAlign(ren, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
//        nvgText(ren, at.x, at.y, text, NULL);
        
    }

    bool imgui_button(int id, SDL_Renderer* ren, const SDL_Rect& dst, int fontId, const char* text)
    {
        const SDL_Point& mousep = SDL_GetMousePosition();
        if(SDL_IntersectRectAndPoint(dst, &mousep)) {
            uistate.hot = id;
            if(uistate.active == 0 && SDL_IsMouseDown()) 
            {
                uistate.active = id;
                input_resetMouseButton();
            }
        }
        
        SDL_Color col;

        if(uistate.hot == id)
            if(uistate.active == id)
                col  = uistate.activeColor;//SDL_SetTextureColor(tex, &uistate.activeColor);
            else
                col  = uistate.hotColor;//SDL_SetTextureColor(tex, &uistate.hotColor);
        else
             col  = uistate.baseColor;//SDL_SetTextureColor(tex, &uistate.baseColor);
        
        SDL_RenderDrawRect(ren, &dst);
        sprite_font* font = atom::getFontById(fontId);
        font->drawString(ren, {dst.x, dst.y}, text, &col);
        
//        nvgBeginPath(ren);
//        nvgFillColor(ren, col);
//        nvgRoundedRect(ren, dst.x, dst.y, dst.w, dst.h, 5.0f);
//        nvgFill(ren);
//        
//        nvgBeginPath(ren);
//        nvgStrokeColor(ren, col);
//        nvgRoundedRect(ren, dst.x - 8, dst.y - 8, dst.w + 16, dst.h + 16, 5.0f);
//        nvgStroke(ren);
//        
//        nvgFontFaceId(ren, fontId);
//        nvgTextAlign(ren, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
//        nvgFillColor(ren, uistate.textColor);
//        nvgText(ren, dst.x + dst.w / 2, dst.y + dst.h / 2, text, NULL);

        return uistate.hot == id && uistate.active == id;
    }

    bool imgui_textinput(int id, SDL_Renderer* ren, int fontId, const SDL_Rect& dst, char* str)
    {
        const SDL_Point& p = SDL_GetMousePosition();
        if(SDL_IntersectRectAndPoint(dst, &p)) {
            uistate.hot = id;
            if(uistate.active == 0 && SDL_IsMouseDown())
            {
                uistate.active = id;
                SDL_StartTextInput();
            }
        } else {
            SDL_StopTextInput();
            uistate.keyChar = 0;
        }
        
        if(uistate.kbditem == -1) uistate.kbditem = id;
        
        size_t len = strlen(str);
        
        if(uistate.kbditem == id) {
            if(uistate.keyChar == 8)
            {
                str[--len] = 0;
            }
            
            if(uistate.keyChar == SDL_SCANCODE_SPACE) {
                
            }
            
            if(uistate.keyChar >= 32 && uistate.keyChar < 127) {
                str[len++] = uistate.keyChar;
                str[len] = 0;
            }
            
            uistate.keyChar = 0;
            input_resetKey();
        }
        
        SDL_Color col;
        
        if(uistate.hot == id)
            if(uistate.active == id)
                col = uistate.activeColor;//SDL_SetRenderDrawColor(ren, uistate.activeColor.r, uistate.activeColor.g, uistate.activeColor.b, 255 );
            else
                col = uistate.hotColor;//SDL_SetRenderDrawColor(ren, uistate.hotColor.r, uistate.hotColor.g, uistate.hotColor.b, 255 );
        else
            col = uistate.baseColor;//SDL_SetRenderDrawColor(ren, uistate.baseColor.r, uistate.baseColor.g, uistate.baseColor.b, 255);

        int tw, th;
        
        float bounds[4] = { 0.f };
//        nvgTextBounds(ren, dst.x, dst.y, str, nullptr, bounds);
        tw = bounds[2] - bounds[0];
        th = bounds[3] - bounds[1];

        //font->measure_string(str, &tw, &th);
        SDL_Rect box = {
            dst.x,
            dst.y,
            dst.w,
            dst.h
        };
        SDL_RenderDrawRect(ren, &box);
        atom::getFontById(fontId)->drawString(ren, {dst.x, dst.y}, str, &uistate.hotColor);
     
        
        return uistate.active == id && uistate.hot == id && uistate.kbditem == id;
    }
}
