//
//  world.cpp
//  coil
//
//  Created by emiel Dam on 11/6/15.
//
//

#include "world.h"


void World_clear(World* world)
{
    for(int i=0; i < MAX_ENTITIES; i++)
    {
        World_destroyEntity(world, i);
    }
}

intptr_t World_createEntity(World* world) {
    for(int i=0; i < MAX_ENTITIES; i++) {
        if(world->mask[i] == E_COMPONENT_NIL) {
            world->cleanup[i] = false;
            return i;
        }
    }
    return MAX_ENTITIES;
}

void World_destroyEntity(World* world, intptr_t id) {
    
    world->mask[id] = E_COMPONENT_NIL;
    world->cleanup[id] = true;
}

void World_cleanup(World* world) {
//    for(int i=0; i < MAX_ENTITIES; i++) {
//        if(world->cleanup[i] != false) {
//            if(World_hasComponent(world, i, E_COMPONENT_PHYSICS)) {
//                if(World_hasComponent(world, i, E_COMPONENT_COLLIDER)) {
//
//                    b2Fixture* ptr = world->physics[i].body->GetFixtureList();
//                    while(ptr) {
//                        world->physics[i].body->DestroyFixture(ptr);
//                        ptr = ptr->GetNext();
//                    }
//                }
//                world->sim->DestroyBody(world->physics[i].body);
//                world->physics[i].body  = NULL;
//                world->cleanup[i]       = false;
//            }
//            
//            if(world->particles[i] != NULL) {
//                delete world->particles[i];
//                world->particles[i] = NULL;
//            }
//        }
//    }
}

bool World_hasComponent(World* world, intptr_t id, uint32_t mask) {
    return (world->mask[id] & mask) == mask;
}
