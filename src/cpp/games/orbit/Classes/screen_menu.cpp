//
//  screen_menu.cpp
//  coil
//
//  Created by emiel Dam on 11/6/15.
//
//

#include "screens.h"
#include "imgui.h"
#include "asset_manager.h"
#include "graphics.h"
#include <atom/core/event_handler.h>
#include <atom/engine/easing.h>
#include <SDL.h>

using namespace atom;


const int screen_menu::_timeout = 1000;

screen_menu::screen_menu()
    : _time(0)
    , _factor(0.f)
    {
        _time_on = 1000;
        _time_off= 1000;
        
        OnExit += [&] (gamescreen&, gamescreen_manager&) {
            getOwner()->add(new screen_play);
        };
        
//        OnButtonPressed.add(delegate<void(int)>([this] (int id) {
//            this->quit();
//            this->getOwner()->add(new screen_play);
//        }));
        
    }
    
void screen_menu::update(int dt, bool a, bool b) {
        //if(state() == E_STATE_ACTIVE) {
        _factor = (float)_time / _timeout;
        _time = std::min(_timeout, _time + dt);
        //}
        gamescreen::update(dt, a, b);
    }
    
void screen_menu::draw(SDL_Renderer* ren)
    {
        int rw, rh;
        int tw, th;
        SDL_GetRendererOutputSize(ren, &rw, &rh);
        //SDL_RenderGetLogicalSize(ren, &rw, &rh);
        
        SDL_Texture* tex = atom::getTextureByName("@menu");
        SDL_QueryTexture(tex, nullptr, nullptr, &tw, &th);
        
        atom::imgui_reset();
        atom::imgui_setAlpha(1.0f - transition());
        
         int bw = rw >> 2;
         int bh = rh / 8;
        
        SDL_Rect dst = {
            0, 0,
            rw, rh
        };
        
        SDL_RenderCopy(ren, tex, nullptr, &dst);
        
        
        // int px = (int)(atom::easing::ease_elastic_in_out(1.0f - transition()) * (rw / 2 - bw / 2));
        
        float t = atom::easing::ease_elastic_out(1.0f - transition());
        
        float a0 = -bw;
        float b0 =  bw / 2;
        
        float a1 = rw + bw;
        float b1 = rw - bw - bw / 2;
        
        int a2 = -bh;
        int b2 = rh - bh * 2;
//        float t2 = atom::easing::ease_elastic_out(1.0f - transition());
        int c2 = (int)(a2 + (b2 - a2) * t);
        
        if(imgui_button(1, ren,
                        {
                            (int)b0,//(int)(a0 + (b0 - a0) * t),
                            c2, //rh - bh * 2,
                            bw,
                            bh
                        }, 0, "new game"))
        {
            
            quit();
        }
        
        if(imgui_button(2, ren,
                        {
                            (int)b1,//(int)(a1 + (b1 - a1) * t),
                            c2, //rh - bh * 2,
                            bw,
                            bh
                        }, 0, "highscore"))
        {
            OnExit += [&] (gamescreen&, gamescreen_manager&) {
                getOwner()->add(new screen_highscore);
            };
            quit();
        }
        
        SDL_Color color = { 255, 255, 255, 255 };
        
        gamescreen::draw(ren);
    }




