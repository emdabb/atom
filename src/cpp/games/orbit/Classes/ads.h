//
//  ads.h
//  application_name
//
//  Created by emiel Dam on 10/14/15.
//
//

#ifndef ads_h
#define ads_h

//#include <GoogleMobileAds/GoogleMobileAds.h>
#include <atom/core/factory.h>
#include <atom/core/event_handler.h>
#include <atom/core/singleton.h>
#include <future>

namespace ads {
    struct Advertisement {
        
        std::string _location;
        std::string _publisherId;
        std::string _appId;
        
        virtual ~Advertisement() = default;
        virtual void request() = 0;
        virtual void show() = 0;
        virtual void hide() = 0;
//        virtual bool isReady() = 0;
        
        void setPublisherId(const char* str) {
            _publisherId = str;
        }
        
        void setAppId(const char* str) {
            _appId = str;
        }
        
        const char* getLocation() const {
            return _location.c_str();
        }
        
        void setLocation(const char* str) {
            _location = str;
        }
        
        atom::event_handler<void(Advertisement*)> onAdWasReceived;
        atom::event_handler<void(Advertisement*)> onAdWasDismissed;
    };
    
    struct AdManager : public atom::singleton<AdManager> {
        void cacheFrom(std::istream&);
        Advertisement* getAdByName(const std::string&);
    };
    
    typedef atom::factory<Advertisement> AdFactory;
}

//extern "C" ads::Ad* createAd();


#endif /* ads_h */
