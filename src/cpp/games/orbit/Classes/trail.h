#pragma once

#include <SDL.h>
#include <vector>

struct NVGcontext;
struct NVGcolor;

struct TrailRenderer {
    
    const static int _trailSegmentTimeout = 500;
    
    struct TrailPoint {
        SDL_Point   point;
        int         time;
    };
    
    std::vector<TrailPoint> _trail;
    
    void mouse_move(int x, int y, int dx, int dy);
    
    void mouse_press(int x, int y);
    
    void mouse_release(int x, int y);
    
    void update(int dt);
    
    void draw(SDL_Renderer* ren, const SDL_Color&);
    
    bool onSegment(const SDL_Point& p, const SDL_Point& q, const SDL_Point& r);
    
    int orientation(const SDL_Point& p, const SDL_Point& q, const SDL_Point& r);
    
    bool lineIntersect(const SDL_Point& p1,
                       const SDL_Point& q1,
                       const SDL_Point& p2,
                       const SDL_Point& q2);
    
    bool isClosedShape();
    
    int containsPoint(const SDL_Point& p);
};

