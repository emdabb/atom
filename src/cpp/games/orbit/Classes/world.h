#pragma once

#define MAX_ENTITIES        1024
#define MAX_PARTICLE_SYSTEMS 16

#include <SDL.h>
#include <atom/core/event_handler.h>
#include <atom/engine/easing.h>
#include "vec2.h"
#include "particles.h"
#include "grid.h"
#include "trail.h"

#ifndef PTM_RATIO
#define PTM_RATIO   (40.f)
#endif

#define WORLD_TO_SCREEN(x)  ((x) * (PTM_RATIO))
#define SCREEN_TO_WORLD(x)  ((x) / (PTM_RATIO))

struct World;

typedef atom::delegate<void(World*, intptr_t, SDL_Renderer*)> PFNDRAWCALLBACK;
typedef atom::delegate<void(World*, intptr_t)> PFNINPUTCALLBACK;
typedef atom::delegate<void(World*, intptr_t)> PFNTIMEOUTCALLBACK;

namespace sugar {
    template <typename T>
    inline vec2<T> toWorld(const vec2<T>& src) {
        vec2<T> dst;
        dst.x = SCREEN_TO_WORLD(src.x);
        dst.y = SCREEN_TO_WORLD(src.y);
        return dst;
    }

    template <typename T>
    inline vec2<T> toScreen(const vec2<T>& src) {
        vec2<T> dst;
        dst.x = WORLD_TO_SCREEN(src.x);
        dst.y = WORLD_TO_SCREEN(src.y);
        return dst;
    }
}

enum {
    E_COMPONENT_NIL         = 0,
    E_COMPONENT_VISUAL      = 1<<0,
    E_COMPONENT_TRANSFORM   = 1<<1,
    E_COMPONENT_TRAILOBSERVER= 1<<2,
    E_COMPONENT_SCORE       = 1<<3,
    E_COMPONENT_NESTED      = 1<<4,
    E_COMPONENT_INPUT       = 1<<5,
    E_COMPONENT_PARTICLE_EMITTER = 1 << 6,
    E_COMPONENT_TIMEOUT     = 1<<7,
    E_COMPONENT_CAMERAFOLLOW= 1<<8,
	E_COMPONENT_TWEEN		= 1<<9,
	E_COMPONENT_TEXT		= 1<<10,
    E_COMPONENT_GROUP       = 1<<11
};

enum {
    E_PARTICLE_SYSTEM_EXPLODE,
    E_PARTICLE_SYSTEM_MAX
};

struct Group {
    int scored;
    int size;
    int timedOut;
};

struct Nested {
    intptr_t parent;
};

struct Transform {
    vec2f pos;
    float rot;
    float scale;
};

struct Visual {
    SDL_Color color;
    int texid;
};

struct Text {
    char str[256];
    int fontId;
};

struct Tween {
//	atom::delegate<const TweenEventArgs&> ease;
    atom::easing::easing_t ease;
    float* dst;
    float a, b;
};

struct Timeout {
    PFNTIMEOUTCALLBACK onTimeout;
    int time;
    int maxTime;
};

struct World {
    Grid              grid;
    TrailRenderer     trail;
    uint32_t          mask[MAX_ENTITIES];
    Transform         transform[MAX_ENTITIES];
    PFNDRAWCALLBACK   drawDebug[MAX_ENTITIES];
    PFNINPUTCALLBACK  input[MAX_ENTITIES];
    Timeout           timeout[MAX_ENTITIES];
    Visual            visual[MAX_ENTITIES];
    Nested            nested[MAX_ENTITIES];
    bool              cleanup[MAX_ENTITIES];
    Text              text[MAX_ENTITIES];
    Tween             tween[MAX_ENTITIES];
    int               particles[MAX_ENTITIES];
    int               score[MAX_ENTITIES];
    Group             group[MAX_ENTITIES];
    
    vec2f cameraPos;
    atom::delegate<void(World*, intptr_t)> destroy[MAX_ENTITIES];
    particles_base*   particleGroup[E_PARTICLE_SYSTEM_MAX];
    int               particleTextureId[E_PARTICLE_SYSTEM_MAX];
    int numDotsSpawned;
    int numDotsMissed;
    int numDotsScored;
    int totalScore;
};

void World_clear(World*);
void World_update(World*, int);
void World_draw(NVGcontext*);

intptr_t World_createEntity(World* world);
void World_cleanup(World* world);
void World_destroyEntity(World* world, intptr_t id) ;
bool World_hasComponent(World* world, intptr_t id, uint32_t mask);


