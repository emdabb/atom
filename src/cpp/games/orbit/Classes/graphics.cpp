//
//  graphics.cpp
//  bouncy
//
//  Created by emiel Dam on 12/14/15.
//
//

#include <stdio.h>
#include "graphics.h"
#include "vec2.h"

void SDL_RenderDrawCurve(SDL_Renderer* ren, atom::curve2d& curve, const SDL_Color* color, int nsegments) {
    vec2f q0;
    curve.at(0.0f, &q0.x, &q0.y);
    SDL_SetRenderDrawColor(ren, color->r, color->g, color->b, color->b);
    
    float range = curve.timerange();
    
    for(int i=1; i < nsegments; i++) {
        float t = ((float)i / nsegments) * range;
        vec2f q1;
        curve.at(t, &q1.x, &q1.y);
        
        SDL_RenderDrawLine(ren,
                           (int)(q0.x + 0.5f),
                           (int)(q0.y + 0.5f),
                           (int)(q1.x + 0.5f),
                           (int)(q1.y + 0.5f)
                           );
        q0 = q1;
    }
}