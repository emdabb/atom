#include <atom/engine/application.h>
#include <atom/engine/fsm.h>
#include <atom/engine/easing.h>
#include <atom/engine/ecs/kult.hpp>
#include "gamescreen.h"
#include "asset_manager.h"
#include "imgui.h"
#include "input.h"
#include "particles.h"
#include "particle_spawn.h"
#include "color.h"
#include "sprite_font.h"
#include "grid.h"
#include "trail.h"
#include "world.h"
#include "particle_update.h"

#include "ads.h"
#include "screens.h"

#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <cstdlib>
#include <cstring>

extern "C" float randf() {
    return (float)rand() / RAND_MAX;
}

using namespace atom;

//#define DOT_TIMEOUT     5000
//#define BAD_TIMEOUT     7000

SDL_Renderer* g_Renderer;

class game_coil;
class gamescreen;

template <typename R, typename... A>
delegate<R(A...)> make_delegate(R(*pfn)(A...)) {
    return delegate<R(A...)>{pfn};
}

static class register_gamescreens
{
    public:
        register_gamescreens()
        {
            atom::register_factory<gamescreen, screen_splash>("SplashScreen");
        }
} _;


#include <fstream>
#include "sprite_font.h"

class game_coil
: public applet
{
    typedef fsm<game_coil> fsm_type;
    application*    _app;
    gamescreen_manager* _man;

public:
    game_coil(application* app)
    : _app(app)
    {
        _man = new gamescreen_manager();

        app->OnMouseDown.add(make_delegate(&input_OnMousePressed));
        app->OnMouseMove.add(make_delegate(&input_OnMouseMoved));
        app->OnMouseRelease.add(make_delegate(&input_OnMouseReleased));
        app->OnKeyPressed.add(make_delegate(&input_OnKeyPressed));
    }

    virtual ~game_coil()
    {
        //delete _fsm;
        delete _man;
    }

    virtual int create()
    {
        SDL_Renderer* g_ren = _app->getRenderer();
        
        //SDL_RenderSetLogicalSize(g_ren, 568, 320);//1136, 640);

        int rw, rh;
        SDL_GetRendererOutputSize(g_ren, &rw, &rh);

        g_Renderer = g_ren;


        SDL_Renderer* ren = _app->getRenderer();

        SDL_Texture* _whitePixel = SDL_CreateTexture(ren, SDL_PIXELFORMAT_RGB565, SDL_TEXTUREACCESS_STATIC, 1, 1);
        atom::addTexture("@whitePixel", _whitePixel);
        uint16_t pixel = 0xffff;
        SDL_Rect rc = { 0, 0, 1, 1 };
        SDL_UpdateTexture(_whitePixel, &rc, &pixel, sizeof(pixel));
        
        std::ifstream is;
        is.open(atom::getResourcePath() + "assets/textures.json");
        if(is.is_open()) {
            atom::loadTextureLibrary(ren, is);
            is.close();
        }

        std::string fontPath = atom::getResourcePath() + "assets/Lack.ttf";

        is.open(fontPath);
        if(is.is_open()) {
            atom::loadFont("@Lack_16pt", is, 16, ren);
            is.close();
        }
        
        fontPath = atom::getResourcePath() + "assets/Mario-Kart-DS.ttf";
        is.open(fontPath);
        if(is.is_open()) {
            atom::loadFont("@MarioKartDS_24pt", is, 24, ren);
            is.close();
        }


        //imgui_loadFont(_nvg, fontPath.c_str(), "Lack.ttf");

        is.open(atom::getResourcePath() + "assets/ads.json");
        if(is.is_open()) {
            ads::AdManager::instance().cacheFrom(is);
            is.close();
        }
        
        ads::Advertisement* banner = ads::AdManager::instance().getAdByName("admobBanner");
        banner->onAdWasReceived += [&] (ads::Advertisement* ad) {
            //ad->show();
        };
        banner->request();
        
        if(Mix_Init(MIX_INIT_OGG) == 0) {
            printf("Mix_Init() failed: %s\n", Mix_GetError());
            return false;
        }
        

        
        //Initialize SDL_mixer
        if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
        {
            std::cout << Mix_GetError() << std::endl;
            return false;
        }
        
        Mix_AllocateChannels(32);
        
        std::string sndPath = atom::getResourcePath() + "assets/audio.json";
        is.open(sndPath);
        if(is.is_open()) {
            atom::loadAudioLibrary(is);
            is.close();
        }
        
        std::string musPath = atom::getResourcePath() + "assets/music.json";
        is.open(musPath);
        if(is.is_open()) {
            atom::loadMusicLibrary(is);
            is.close();
        }

        
        

        _man->add(new screen_play);

        return 0;
    }

    virtual int destroy()
    {
        return 0;
    }

    virtual void update(int t, int dt)
    {
        _man->update(t, dt);
    }

    virtual void draw()
    {
        SDL_Renderer* ren = _app->getRenderer();
        int rw, rh;
        SDL_GetRendererOutputSize(ren, &rw, &rh);
        SDL_SetRenderDrawColor(ren, 0, 0, 7, 255);
        SDL_RenderClear(ren);
        _man->draw(ren);
        SDL_RenderPresent(ren);
    }

    gamescreen_manager& getGamescreenManager()
    {
        return *_man;
    }
};

extern "C" applet* new_game(application* app) {
    return new game_coil(app);
}
