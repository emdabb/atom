//
//  screen_splash.cpp
//  coil
//
//  Created by emiel Dam on 11/6/15.
//
//

#include "screens.h"
#include "asset_manager.h"
#include <nanovg.h>
#include <SDL.h>

using namespace atom;

screen_splash::screen_splash()
    : _currentScreen(0)
    , _maxScreens(2)
    , _visibleTimeout(1000)
    , _visibleTime(0)
    {
        _time_on = 1000;
        _time_off= 1000;
        
        this->OnExit +=
            [this](gamescreen& screen, gamescreen_manager& man) {
                man.add(new screen_menu);
        };
    }
    
screen_splash::~screen_splash()
    {
    }
    
void screen_splash::load_content()
    {
        gamescreen::load_content();
    }
    
void screen_splash::update(int dt, bool a, bool b)
    {
        static bool coveredByOtherScreen = false;
        
        if(state() == E_STATE_ACTIVE)
        {
            _visibleTime += dt;
            if(_visibleTime >= _visibleTimeout)
            {
                coveredByOtherScreen = true;
            }
        }
        else if(state() == E_STATE_HIDDEN)
        {
            coveredByOtherScreen = false;
            _currentScreen++;
            _visibleTime = 0;
            
        }
        
        if(_currentScreen >= _maxScreens) {
            coveredByOtherScreen = true;
            quit();
        }
        
        gamescreen::update(dt, a, coveredByOtherScreen);
    }
    
void screen_splash::draw(SDL_Renderer* ren)
    {
        std::string g_names[] =
        {
            "@splash_02",
            "@splash_03"
        };
        
        int rw = 0, rh = 0;
        SDL_GetRendererOutputSize(ren, &rw, &rh);
        
        int tw = 0, th = 0;
        //std::map<std::string, int>::iterator it = atom::_nvgTextures.find(g_names[_currentScreen]);
//        if(it != atom::_nvgTextures.end()) {
//            id = (*it).second;
//        }
        
    
        
        
        
                SDL_Texture* tex = atom::getTextureByName(g_names[_currentScreen]);

                SDL_QueryTexture(tex, NULL, NULL, &tw, &th);
        
                SDL_Rect rc = {
                    rw / 2 - tw / 2,
                    rh / 2 - th / 2,
                    std::min(rw, tw),
                    std::min(rh, th)
                };
        
                uint8_t alpha = ((1.0f - transition()) * 255.f);
                SDL_SetTextureAlphaMod(tex, alpha);
        SDL_RenderCopy(ren, tex, NULL, &rc);
        gamescreen::draw(ren);
    }
