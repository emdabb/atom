//
//  GADBase.h
//  0101
//
//  Created by emiel Dam on 12/17/15.
//
//

#ifndef GADBase_h
#define GADBase_h

#include "../Classes/ads.h"

namespace atom {
    class GADBase : public atom::Advertisement {
        std::string mPublisherId;
        std::string mAppId;
    public:
        virtual const std::string& getPublisherId() const {
            return mPublisherId;
        }
        virtual const std::string& getAppId() const {
            return mAppId;
        }
        virtual void setPublisherId(const std::string& val) {
            mPublisherId = val;
        }
        virtual void setAppId(const std::string& val) {
            mAppId = val;
        }
        
        std::string getAdUnitId() const {
            return "ca-app-pub-" + mPublisherId + "/" + mAppId;
        }
    };
}

#endif /* GADBase_h */
