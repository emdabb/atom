//
//  BannerView.h
//  bouncy
//
//  Created by emiel Dam on 12/3/15.
//
//

#ifndef BannerView_h
#define BannerView_h

#include <UIKit/UIKit.h>
#include <GoogleMobileAds/GoogleMobileAds.h>

@interface GADBannerViewController : UIViewController <GADBannerViewDelegate>

@property(nonatomic, strong) GADBannerView* banner;

- (void)viewDidLoad;

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView;

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error;

- (void)adViewWillPresentScreen:(GADBannerView *)bannerView;

- (void)adViewDidDismissScreen:(GADBannerView *)bannerView;

- (void)adViewWillDismissScreen:(GADBannerView *)bannerView;

- (void)adViewWillLeaveApplication:(GADBannerView *)bannerView;

- (void) restoreController;

@end


#endif /* BannerView_h */
