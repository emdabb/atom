//
//  trail.cpp
//  application_name
//
//  Created by emiel Dam on 10/12/15.
//
//

#include "trail.h"
#include <atom/engine/easing.h>
#include <nanovg.h>
#include <math.h>
#include <stdio.h>

void TrailRenderer::mouse_move(int x, int y, int dx, int dy)
{
    if(_trail.size() == 0)
        return;
    
    SDL_Point ap = _trail.back().point;
    
    SDL_Point p = {
        ap.x - x,
        ap.y - y
    };
    
    float dist = sqrt(p.x * p.x + p.y * p.y);
    
    if(dist < 20.f)
        return;
    
    _trail.emplace_back((TrailPoint){
        (SDL_Point) {x, y},
        0
    });
}
void TrailRenderer::mouse_press(int x, int y)
{
    _trail.clear();
    _trail.emplace_back((TrailPoint){
        (SDL_Point){x, y},
        0
    });
}
void TrailRenderer::mouse_release(int x, int y)
{
    printf("trail size: %d\n", (int)_trail.size());
}

void TrailRenderer::update(int dt) {
    for(size_t i=0; i < _trail.size(); i++)
    {
        _trail[i].time += dt;
        
        if(_trail[i].time >= _trailSegmentTimeout)
            _trail.erase(_trail.begin() + i);
    }
}

void TrailRenderer::draw(NVGcontext* vg, const NVGcolor& color) //SDL_Renderer* ren)
{
    //uint8_t r, g, b;
    //SDL_GetRenderDrawColor(ren, &r, &g, &b, NULL);

    
    

    //nvgStrokeWidth(vg, 5.0f);
    
    int nvert = (int)_trail.size();
    
    if(nvert == 0) return;
    
    NVGcolor drawColor = color;
    
    nvgBeginPath(vg);

    float dx = 0.1f;
    
    nvgMoveTo(vg, _trail[0].point.x, _trail[0].point.y);
    
    for(int i=0; i < nvert - 1; i++)
    {
        float fAlpha = 1.0f - (float)_trail[i].time / _trailSegmentTimeout;
        fAlpha = atom::easing::ease_sine_out(fAlpha);
        
//        uint8_t a = (uint8_t)( * 255.f);
//        
        drawColor.a = fAlpha;
        
        nvgStrokeColor(vg, drawColor);
        
        SDL_Point& p0 = _trail[i].point;
        SDL_Point& p1 = _trail[i + 1].point;
#if 0
        SDL_SetRenderDrawColor(ren, r, g, b, a);
        SDL_Point& p0 = _trail[i].point;

        
        SDL_RenderDrawLine(ren, p0.x, p0.y, p1.x, p1.y);
#else
        
        //
        nvgBezierTo(vg,
                    p0.x + dx * 0.5f,
                    p0.y + dx * 0.5f,
                    p1.x + dx * 0.5f,
                    p1.y + dx * 0.5f,
                    p1.x, p1.y);
        //nvgLineTo(vg, p1.x, p1.y);
        //nvgQuadTo(vg, (p0.x + p1.x) * 0.5f, (p0.y + p1.y) * 0.5f, p1.x, p1.y);
#endif
    }
    nvgStroke(vg);

    
//    nvgFill(vg);
}

bool TrailRenderer::onSegment(const SDL_Point& p, const SDL_Point& q, const SDL_Point& r) {
    if (q.x <= std::max(p.x, r.x) && q.x >= std::min(p.x, r.x) &&
        q.y <= std::max(p.y, r.y) && q.y >= std::min(p.y, r.y))
        return true;
    
    return false;
}

int TrailRenderer::orientation(const SDL_Point& p, const SDL_Point& q, const SDL_Point& r)
{
    // See 10th slides from following link for derivation of the formula
    // http://www.dcs.gla.ac.uk/~pat/52233/slides/Geometry1x1.pdf
    int val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);
    
    if (val == 0)
        return 0;  // colinear
    
    return (val > 0)? 1: 2; // clock or counterclock wise
}

bool TrailRenderer::lineIntersect(const SDL_Point& p1,
                   const SDL_Point& q1,
                   const SDL_Point& p2,
                   const SDL_Point& q2)
{
    // Find the four orientations needed for general and
    // special cases
    int o1 = orientation(p1, q1, p2);
    int o2 = orientation(p1, q1, q2);
    int o3 = orientation(p2, q2, p1);
    int o4 = orientation(p2, q2, q1);
    
    // General case
    if (o1 != o2 && o3 != o4)
        return true;
    
    // Special Cases
    // p1, q1 and p2 are colinear and p2 lies on segment p1q1
    if (o1 == 0 && onSegment(p1, p2, q1)) return true;
    
    // p1, q1 and p2 are colinear and q2 lies on segment p1q1
    if (o2 == 0 && onSegment(p1, q2, q1)) return true;
    
    // p2, q2 and p1 are colinear and p1 lies on segment p2q2
    if (o3 == 0 && onSegment(p2, p1, q2)) return true;
    
    // p2, q2 and q1 are colinear and q1 lies on segment p2q2
    if (o4 == 0 && onSegment(p2, q1, q2)) return true;
    
    return false; // Doesn't fall in any of the above cases
}

bool TrailRenderer::isClosedShape() {
    
    int nvert = (int)_trail.size();
    for(int i=0; i < nvert - 1; i++) {
        const SDL_Point& p1 = _trail[i + 0].point;
        const SDL_Point& q1 = _trail[i + 1].point;
        
        for(int j=i+2; j < nvert-1; j++) {
            const SDL_Point& p2 = _trail[j].point;
            const SDL_Point& q2 = _trail[j + 1].point;
            
            if(lineIntersect(p1, q1, p2, q2)) {
                return true;
            }
        }
    }
    return false;
}

int TrailRenderer::containsPoint(const SDL_Point& p)
{
    int nvert = (int)_trail.size();
    int i, j, c = 0;
    for(i=0, j = nvert - 1; i < nvert; j = i++)
    {
        const SDL_Point& v0 = _trail[i].point;
        const SDL_Point& v1 = _trail[j].point;
        
        if( ((v0.y > p.y) != (v1.y > p.y)) &&
           (p.x < (v1.x - v0.x) * (p.y - v0.y) / (v1.y-v0.y) + v0.x)) {
            c = !c;
        }
    }
    return c;
}
