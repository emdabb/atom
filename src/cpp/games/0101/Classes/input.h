#pragma once

#include <atom/core/event_handler.h>

struct SDL_Point;

namespace atom 
{

    const SDL_Point& SDL_GetMousePosition();
    const SDL_Point& SDL_GetMousePressPosition();
    const SDL_Point& SDL_GetMouseReleasePosition();
    const std::string& input_GetLastTextInput();
    const bool SDL_IsMouseDown();
    uint16_t input_GetLastKeyPressed();
    void input_resetKey();
    void input_resetMouseButton();

    void input_OnMouseMoved(const int&, const int&, const int&, const int&);
    void input_OnMousePressed(const int&, const int&);
    void input_OnMouseReleased(const int&, const int&);
    void input_OnKeyPressed(const uint16_t&);
    void input_OnTextInput(const std::string&);
    
    extern event_handler<void(int, int, int, int)> OnMouseMoved;
    extern event_handler<void(int, int)> OnMousePressed;
    extern event_handler<void(int, int)> OnMouseReleased;
    extern event_handler<void(uint16_t)> OnKeyPressed;
    
    
}
