//
//  popup_howtoplay.cpp
//  0101
//
//  Created by emiel Dam on 12/18/15.
//
//

#include <stdio.h>
#include "screens.h"

class screen_howtoplay : public gamescreen {
public:
    virtual void update(int dt, bool otherScreenHasFocus, bool coveredByOtherScreen) {
        gamescreen::update(dt, otherScreenHasFocus, coveredByOtherScreen);
    }
    
    virtual void draw(SDL_Renderer* ren) {
        gamescreen::draw(ren);
    }
};