//
//  screen_gameover.cpp
//  0101
//
//  Created by emiel Dam on 12/21/15.
//
//

#include "screens.h"
#include "http_request.h"
#include <happyhttp/happyhttp.h>
#include "imgui.h"
#include "asset_manager.h"
#include <SDL.h>
#include <stdlib.h>
#include <string.h>

using namespace atom;
using namespace happyhttp;

class screen_gameover : public gamescreen {
    static void OnBegin(const Response* r, void* userdata) {
        std::cout << "[http-post] ** begin => [content-length]=[" << r->getheader("Content-Length") << "]" << std::endl;
    }
    static void OnData(const Response* r, void* userdata, const unsigned char* data, int numbytes) {
        std::cout << "[http-post] ** data  => [data]=[" << data << "]" << std::endl;
    }
    static void OnComplete(const Response* r, void* userdata) {
        std::cout << "[http-post] ** done  => [content-length]=[" << r->getheader("Content-Length") << "]" << std::endl;
    }
    
    void post(const std::string& name, int score) {
        
        const char* headers[] = {
            "Connection", "close",
            "Content-type", "application/x-www-form-urlencoded",
            "Accept", "text/plain",
            0
        };
        
        std::stringstream ss;
        ss << score;
        
        char body[256] = { 0 };
        strcat(body, "player=");
        strcat(body, name.c_str());
        strcat(body, "&score=");
        strcat(body, ss.str().c_str());
        
        try {
            Connection con(HIGHSCORE_IP, HIGHSCORE_PORT);
            con.setcallbacks(OnBegin,OnData,OnComplete,NULL);
            
            con.request("POST", "/0101", headers, (const uint8_t*)body, (int)strlen(body));
            while (con.outstanding()) {
                con.pump();
            }
        } catch(happyhttp::Wobbly& err) {
            std::cout << err.what() << std::endl;
        }

        
    }
    static const char* DEFAULT_TEXT;
    
    char mNameBuffer[256];
    int mScore;
public:
    explicit screen_gameover(int score)
    : mScore(score)
    {
        strcpy(mNameBuffer, DEFAULT_TEXT);
        
        this->OnExit += [&] (const GameScreenEventArgs& args) {
            args.manager->add(GUI_CreateMenuScreen());
        };
        
        if(!score) {
            this->quit();
        }
    }
    
    virtual ~screen_gameover() {
        
    }
    
    virtual void update(int dt, bool a, bool b) {
        gamescreen::update(dt, a, b);
    }
    
    virtual void draw(SDL_Renderer* ren) {
        int rw, rh;
        int tw, th;
        
        SDL_GetRendererOutputSize(ren, &rw, &rh);
        
        sprite_font* font = atom::getFontById(0);
        
        font->measureString(mNameBuffer, &tw, &th);
        
        tw = std::max(tw, rw >> 2);
        th = std::max(th, rh >> 3);
        
        SDL_Rect dst {
            rw / 2 - tw / 2,
            rh / 2 - th / 2,
            tw,
            th
        };
        
        imgui_reset();
        
        {
            SDL_Point textSize;
            std::stringstream ss_;
            ss_ << "your score: " << mScore;
            
            font->measureString(ss_.str(), &textSize.x, &textSize.y);
            
            imgui_label(GEN_ID(), ren, 0, { rw / 2 - textSize.x / 2, textSize.y * 2 }, ss_.str().c_str(), 0);
        }
        
        if(imgui_textinput(GEN_ID(), ren, 0, dst, mNameBuffer)) {
            //memset(buf, 0, 256 * sizeof(char));
            strcpy(mNameBuffer, "");
        }
        
        {
            SDL_Point textSize;
            font->measureString("save", &textSize.x, &textSize.y);
            
            dst.x = rw / 2 - textSize.x / 2;
            dst.y = rh / 2 + textSize.y * 2;
            dst.w = textSize.x;
            dst.h = textSize.y;
            if(imgui_button(GEN_ID(), ren, dst, 0, "save")) {
                post(mNameBuffer, mScore);
                this->quit();
            }
        }
    }
};

gamescreen* GUI_CreateGameOverScreen(int score) {
    return new screen_gameover(score);
}

const char* screen_gameover::DEFAULT_TEXT = "anon";
