#pragma once

#define GRID_W          (15)
#define GRID_H          (15)
#define NUM_PARTICLES   (GRID_W * GRID_H)
#define NUM_CONSTRAINTS ((2 * NUM_PARTICLES) - GRID_W - GRID_H)// <-- fixme

#include <nanovg.h>

struct constraint {
    int a, b;
    float restLen;
};

class Grid
{
    vec2f x[NUM_PARTICLES];
    vec2f oldx[NUM_PARTICLES];
    vec2f a[NUM_PARTICLES];
    constraint spring[NUM_CONSTRAINTS];
    float mass[NUM_PARTICLES];
protected:
    void updatePositions(float fDt)
    {
        for(int i=0; i < NUM_PARTICLES; i++)
        {
            vec2f& x_   = x[i];
            vec2f& oldx_= oldx[i];
            vec2f& a_   = a[i];
            vec2f temp  = x_;

            vec2f vel = x_ - oldx_;
            
            x_ += vel + a_ * fDt * fDt;

            oldx_ = temp;

            a_ = { 0.f, 0.f };
        }
    }
    
    void satisfyConstraints(int numIterations)
    {
        for(int j=0; j < numIterations; j++)
        {
            for(int i=0; i < NUM_CONSTRAINTS; i++)
            {
                const constraint& c = spring[i];
                vec2f& x1           = x[c.a];
                vec2f& x2           = x[c.b];
                float invMass1      = mass[c.a];
                float invMass2      = mass[c.b];

                vec2f delta = { x2.x - x1.x, x2.y - x1.y };
                float deltaLen = std::sqrt(delta.x * delta.x + delta.y * delta.y);

                float dd = deltaLen - c.restLen;

                if(dd < 0.001f * 0.001f) continue;
                if(invMass1 == 0.0f && invMass2 == 0.0f) continue;

                float dn = (deltaLen * (invMass1 + invMass2));
                float ratio = dd / dn;

                vec2f va = delta * (0.5f * ratio * invMass1);
                vec2f vb = delta * (0.5f * ratio * invMass2);

                x1 += va;
                x2 -= vb;
            }
        }
    }

    void createParticle(int i, float px, float py, float mass_) 
    {
        x[i] = oldx[i] = { px, py };
        a[i] = { .0f, .0f };
        mass[i] = mass_;
    }

    void createConstraint(int i, int a, int b) 
    {
        spring[i].a = a;
        spring[i].b = b;
        
        const vec2f& va = x[a];
        const vec2f& vb = x[b];

        vec2f dif = vb - va;

        float len = std::sqrt(vec2f::dot(dif, dif));

        spring[i].restLen = len;
    }
public:
    void create(float dx, float dy)
    {
        int j = 0;
        for(int yy=0; yy < GRID_H; yy++) 
        {
            for(int xx=0; xx < GRID_W; xx++)
            {
                float mass = 1.0f;

                int i = xx + yy * GRID_W;
                
                if(xx == 0 || yy == 0 || xx == GRID_W - 1 || yy == GRID_H - 1)
                {
                    mass = 0.f;
                } else if((xx & 7) == 1 || (yy & 7) == 1) {
                    mass = 0.0f;
                }

                createParticle(i, xx * dx, yy * dy, mass);

                if(xx > 0) 
                {
                    int a = (xx - 1) + yy * GRID_W;
                    int b = xx + yy * GRID_W;
                    createConstraint(j++, a, b);
                }
                if(yy > 0) {
                    int a = xx + (yy - 1) * GRID_W;
                    int b = xx + yy * GRID_W;
                    createConstraint(j++, a, b);
                }
            }
        }
    }

    void update(int dt) 
    {    
        float fDt = (float)dt * 0.1f;
        updatePositions(fDt);
        satisfyConstraints(4);
    }

    virtual void draw(NVGcontext* ren) //SDL_Renderer* ren)
    {
#if 0
        SDL_SetRenderDrawColor(ren, ColorPreset::Base1.r, ColorPreset::Base1.g, ColorPreset::Base1.b, 255);
        
        for(int yy=1; yy < GRID_H; yy++) {
            for(int xx=1; xx < GRID_W; xx++) {

                const vec2f& at = x[xx + yy * GRID_W];

                if(xx > 1) {
                    const vec2f& left = x[(xx - 1) + yy * GRID_W];
                    
                    //SDL_RenderDrawLine(ren, (int)left.x, (int)left.y, (int)at.x, (int)at.y);
                }

                if(yy > 1) {
                    const vec2f& top = x[xx + (yy - 1) * GRID_W];
                    //SDL_RenderDrawLine(ren, (int)top.x, (int)top.y, (int)at.x, (int)at.y);
                }

                if(xx > 1 && yy > 1) {
                    const vec2f& left   = x[(xx-1) + yy * GRID_W];
                    const vec2f& top    = x[xx + (yy-1) * GRID_W];
                    const vec2f& topLeft= x[(xx-1)+(yy-1)*GRID_W];
                    
                    vec2f i0, i1, i2, i3;

                    i0 = (topLeft + top) * 0.5f;
                    i1 = (topLeft + left) * 0.5f;
                    i2 = (left + at) * 0.5f;
                    i3 = (top + at) * 0.5f;

                    //SDL_RenderDrawLine(ren, (int)i0.x, (int)i0.y, (int)i2.x, (int)i2.y);
                    //SDL_RenderDrawLine(ren, (int)i1.x, (int)i1.y, (int)i3.x, (int)i3.y);
                }
            }
        }
#else
#endif
    }

    
    void addExplosiveForce(float f, const vec2f& at, float radius) 
    {
        vec2f vf;
        vec2f vdif;
        for(int i=0; i < NUM_PARTICLES; i++) {

            const vec2f& p = x[i];
            vdif = p - at;

            float dSq = vec2f::dot(vdif, vdif);

            if(dSq < radius * radius) {
                vf = vdif * (f * 100.f / (10000.f + dSq));
                addForceTo(i, vf);
            }
        }
    }
   
    void addForceTo(int i, const vec2f& f) {
        vec2f vf = f * mass[i];
        a[i] += vf;
    } 
    void addDirectionalForce(const vec2f& f, const vec2f& at, float radius) 
    {
        vec2f vf, vdif;
        for(int i=0; i < NUM_PARTICLES; i++)
        {
            vec2f& p = x[i];
            vdif = {
                p.x - at.x,
                p.y - at.y
            };
            float d2 = vdif.x * vdif.x + vdif.y * vdif.y;
            if(d2 < radius * radius)
            {
                vf = {
                    vdif.x * f.x * 100.f / (10000.f + d2),
                    vdif.y * f.y * 100.f / (10000.f + d2)
                };
                addForceTo(i, vf);
            }
        }
    }
};

