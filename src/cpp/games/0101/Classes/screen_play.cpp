//
//  screen_play.cpp
//  0101
//
//  Created by emiel Dam on 12/21/15.
//
//

#include "screens.h"
#include "particles.h"
#include "gui.h"
#include "particle_update.h"
#include "particle_spawn.h"
#include "input.h"
#include <SDL.h>
#include <fstream>
#include <atom/engine/easing.h>
#include <happyhttp/happyhttp.h>
#include <string.h>

#define GRID_W      (10)
#define GRID_H      (750)
#define BLOCKS_W    ((GRID_W) >> 1)
#define BLOCKS_H    (BLOCKS_W)
#define NUM_BLOCKS  ((BLOCKS_W) * (BLOCKS_H))
#define SCORETIME   (500)
#define DX          (std::min(rw, rh) / GRID_W)

using happyhttp::Connection;
using happyhttp::Response;

extern SDL_Renderer* g_Renderer;

static void onbegin(const Response* r, void* userdata) {
    std::cout << "post-begin" << std::endl;
}

static void ondata(const Response* r, void* userdata, const unsigned char* data, int numbytes) {
    std::cout << "post-data:[" << data << "]" << std::endl;
}

static void onend(const Response* r, void* userdata) {
    std::cout << "post-end" << std::endl;
}

void postScore() {
    
    
    const char* headers[] = {
        "Connection", "close",
        "Content-type", "application/x-www-form-urlencoded",
        "Accept", "text/plain",
        0
    };
    
    const char* body = "player=miel&score=500";
    
    Connection con("192.168.1.8", 8181);
    con.setcallbacks(onbegin,ondata,onend,NULL);
    
    con.request("POST", "/0101", headers, (const uint8_t*)body, (int)strlen(body));
    
    try {
        while (con.outstanding()) {
            con.pump();
        }
    } catch(happyhttp::Wobbly& err) {
        std::cout << err.what() << std::endl;
    }
}

class screen_play : public gamescreen {
    SDL_Renderer* mRenderer_;
    
    bool _selected[GRID_W * GRID_H];
    int _grid[GRID_W * GRID_H];
    int _time[GRID_W * GRID_H];
    
    int _fontHandle;
    
    SDL_Point _mousePos;
    SDL_Point _mouseDownPos;
    SDL_Point _mouseUpPos;
    bool _mouseIsDown;
    
    float _offset;
    float _scrollPos;
    //    float _totalOffset;
    
    float _targetPos;
    float _currentPos;
    
    int _oldScore;
    int _score;
    int _scoreTimeout;
    
    int _lastSelected;
    
    int mDangerTime;
    const static int mDangerTimeout;
    
    const static int _offsetTimeout = 1250;
    //int _direction;
    bool _gridCellWasSelected;
    //int _currentRow;
    int _sy;
    bool _willSelect;
    int _maxRowHeight;
    
    atom::event_handler<void()> OnClick;
    
    particles<vec2f>* _particles;
    
    Widget* ui[3];
    
    gamescreen_manager mGamescreenManager;
    
protected:
    void checkForEnclosureOnRow(int row) {
        for(int i=0; i < GRID_W - 2; i++) {
            int at   = (i + 0) + row * GRID_W;
            int next = (i + 1) + row * GRID_W;
            int last = (i + 2) + row * GRID_W;
            
            
            if(_selected[at] && !_selected[next] && _selected[last]) {
                _selected[next] = true;
                //_score += 30;
                score(30);
            }
        }
    }
    
    int toggleCell(SDL_Renderer* ren, int x, int y) {
        int res = 0;
        
        if(x > 0) {
            int i = (x - 1) + y * GRID_W;
            res |= (_selected[i] && _grid[i] != 1) ? 1 << 0 : 0;
        }
        if(x < GRID_W - 1) {
            int i = (x + 1) + y * GRID_W;
            res |= (_selected[i] && _grid[i] != 1) ? 1 << 1 : 0;
        }
        
        if(y > 0) {
            int i = x + (y - 1) * GRID_W;
            res |= (_selected[i] && _grid[i] != 1) ? 1 << 2 : 0;
        }
        
        if(y < GRID_H - 1) {
            int i = x + (y + 1) * GRID_W;
            res |= (_selected[i] && _grid[i] != 1) ? 1 << 3 : 0;
        }
        
        int at = x + y * GRID_W;
        
        if(res != 0) {
            if(!_selected[at]) {
                _selected[at] = !_selected[at];
                _time[at] = 0;
                _lastSelected = at;
                _maxRowHeight = std::max(_maxRowHeight, y);
                
                vec2f pos;
                getScreenPosForGridPos(ren, x, y, &pos.x, &pos.y);
                pos.y += _currentPos;
                burst(pos, 8, M_PI * 2.0f, 0.0f, 1.0f, 0.5f, _particles, 0.5f, 0.5f);
                //Mix_PlayChannelTimed(-1, atom::getSoundByName("@click"), 0, -1);
                
                int top = x + (y+1) * GRID_W;
                if(_grid[top] == 1) {
                    mDangerTime = 0;
                }
                
                return _grid[at];
            }
            return 2;
        }
        return -1;
    }
    
    void score(int n) {
        _scoreTimeout = 0;
        _oldScore = _score;
        _score += n;
    }
    
    void getScreenPosForGridPos(SDL_Renderer* ren, int x, int y, float* px, float* py) {
        int rw, rh;
        //SDL_GetRendererOutputSize(ren, &rw, &rh);
        SDL_RenderGetLogicalSize(ren, &rw, &rh);
        
        int dx = DX;//
        if(py)
            *py = (rh - dx) - (y * dx);
        if(px)
            *px = x * dx;
    }
    
    float getOffsetPosition(SDL_Renderer* ren, int y) {
        int rw, rh;
        //SDL_GetRendererOutputSize(ren, &rw, &rh);
        SDL_RenderGetLogicalSize(ren, &rw, &rh);
        int dx = DX;//std::min(rw, rh) / GRID_W;
        
        return y * dx;
    }
    
    void getGridPosForScreenPos(SDL_Renderer* ren, int x, int y, int* px, int* py)
    {
        int rw, rh;

//        SDL_GetRendererOutputSize(ren, &rw, &rh);
        SDL_RenderGetLogicalSize(getRenderer(), &rw, &rh);
        int dx = DX;//std::min(rw, rh) / GRID_W;
        
        int xx = x / dx;
        int yy = (_currentPos + (rh - dx) - y) / dx;
        
        
        //std::cout << "[x,y] = [" << xx << ", " << yy << "]";
        
        *px = std::max(0, std::min(GRID_W, xx));
        *py = std::max(0, std::min(GRID_H, yy));
        
        //assert(*px < GRID_W && *&& *py < GRID_H && *py >= 0)
    }
    
    //    bool _gridCellWasSelected;
    
    
    
    void randomBlock(int* val, int x0, int y0) {
        int x1 = x0 + 1;
        int y1 = y0 + 1;
        
        int r = (rand() & 3) + 1;
        
        val[x0 + y0 * GRID_W] = r == 1 ? 1 : 0;
        val[x1 + y0 * GRID_W] = r == 2 ? 1 : 0;
        val[x1 + y1 * GRID_W] = r == 3 ? 1 : 0;
        val[x0 + y1 * GRID_W] = r == 4 ? 1 : 0;
    }
    
    void createInitialGrid(int* val) {
        for(int y=0; y < GRID_H; y+=2) {
            for(int x=0; x < GRID_W; x+=2) {
                randomBlock(val, x, y);
            }
        }
        memset(_time, 0, sizeof(int) * GRID_W * GRID_H);
        memset(_selected, 0, sizeof(bool) * GRID_W * GRID_H);
        /**
         * Clear first line.
         */
        for(int i=0; i < GRID_W; i++) {
            //            val[i + 0 * GRID_W] = 0;
            _selected[i + 0 * GRID_W] = true;
            //            val[i + 1 * GRID_W] = 0;
        }
        
    }
    
    void emptyRow(int* val) {
        int y = GRID_H - 1;
        for(int x = 0; x < GRID_W; x++) {
            val[x + y * GRID_W] = 0;
        }
    }
    
    void cycleDown(int* val) {
        static int ccount = 0;
        
        for(int y=GRID_H - 2; y >= 0; y--) {
            for(int x=0; x < GRID_W; x++) {
                std::swap(val[x + y * GRID_W], val[x + (y+1) * GRID_W]);
            }
        }
        
        ccount++;
        if(ccount == 2) {
            ccount = 0;
            for(int x=0; x < GRID_W; x+=2) {
                randomBlock(val, x, 0);
            }
        }
    }
    
    
    SDL_Texture* grey;// = atom::getTextureByName("@grey");
    SDL_Texture* green;// = atom::getTextureByName("@green");
    SDL_Texture* yellow;// = atom::getTextureByName("@yellow");
    SDL_Texture* red;// = atom::getTextureByName("@red");
    SDL_Texture* select;// = atom::getTextureByName("@selector");
    SDL_Texture* diamond;// = atom::getTextureByName("@bombAndDanger");
    SDL_Texture* empty;//  = atom::getTextureByName("@empty");
    SDL_Texture* tex;// = grey;
    SDL_Texture* star;// = atom::getTextureByName("@star");
    SDL_Texture* mBackgroundTexture;
    SDL_Texture* mBlank;
    SDL_Texture* mKraken;
    SDL_Texture* mScorePane;
    SDL_Texture* mBlockIndicator;
    SDL_Texture* mWarning;
    
protected:
    SDL_Renderer* getRenderer() {
        return mRenderer_;
    }
public:
    explicit screen_play(SDL_Renderer* ren)
    : mRenderer_(ren)
    , _offset(0.f)
    , _gridCellWasSelected(false)
    , _currentPos(0.0f)
    , _targetPos(0.0f)
    , _sy(0)
    , _scrollPos(0.0f)
    , _score(0)
    , _maxRowHeight(1)
    , mBackgroundTexture(NULL)
    , mKraken(NULL)
    , mScorePane(NULL)
    , mBlockIndicator(NULL)
    , mDangerTime(mDangerTimeout)
    {
//        int rrw, rrh;
//        SDL_GetRendererOutputSize(mRenderer_, &rrw, &rrh);
        
        this->OnExit += [&] (const GameScreenEventArgs& args) {
            //args.manager->add(GUI_CreateGameOverScreen(_score));
            args.manager->add(GUI_CreateMenuScreen());
        };
        
        int rw, rh;
        
        //SDL_GetRendererOutputSize(getRenderer(), &rw, &rh);
        SDL_RenderGetLogicalSize(getRenderer(), &rw, &rh);
        
        int dx = DX;//std::min(rw, rh) / GRID_W;
        _scrollPos = _currentPos = _targetPos = -dx / 2;

        
        atom::OnMouseMoved += [&] (int x, int y, int rx, int ry) {
            if(state() != E_STATE_ACTIVE) return;
            if(_mouseIsDown) {
                int rw, rh;
                
//                SDL_GetRendererOutputSize(getRenderer(), &rw, &rh);
                SDL_RenderGetLogicalSize(getRenderer(), &rw, &rh);
                int dx = DX;//std::min(rw, rh) / GRID_W;
                
                SDL_Point dif = { x - _mouseDownPos.x, y - _mouseDownPos.y };
                if(dif.x * dif.x + dif.y * dif.y >= dx * dx) {
                    _willSelect = false;
                }
                _scrollPos = std::max((float)dx * -0.5f, _scrollPos + ry);
                
                //std::cout << "dif=@[" << dif.y << "]" << std::endl;
                
                _gridCellWasSelected = true;
            }

        };
        
        atom::OnMouseReleased += [this] (int x, int y) {
            if(state() != E_STATE_ACTIVE)
                return;
            
            if(_mouseIsDown) {
                
                _mouseUpPos = { x, y };
                
                SDL_Point dif = { x - _mouseDownPos.x, y - _mouseDownPos.y };
                
                int rw, rh;
                
                //SDL_GetRendererOutputSize(getRenderer(), &rw, &rh);
                SDL_RenderGetLogicalSize(getRenderer(), &rw, &rh);
                int dx = DX;//std::min(rw, rh) / GRID_W;
                
                if(_willSelect) {
                    //OnClick();
                    int px, py;
                    _gridCellWasSelected = true;
                    getGridPosForScreenPos(getRenderer(), _mousePos.x, _mousePos.y, &px, &py);
                    
                    _sy = py;
                    
                    int res = toggleCell(getRenderer(), px, py);
                    
                    if(res == 1) {
                        //_score = 0;

#if 1
                        this->quit();
//#else
                        createInitialGrid(_grid);
//                        _scrollPos = _targetPos = _currentPos = 0;//dx;//0.0f;
                        _gridCellWasSelected = true;
                        
//                        _offset = 0.0f;
                        
#endif
                        
                    } else if(res == 0) {
                        score(15);
                    }
                    
                    _willSelect = false;
                }
            }
            _offset = 0.0f;
            _mouseIsDown = false;
        };
        
        atom::OnMousePressed += [&] (int x, int y) {
                        if(state() != E_STATE_ACTIVE) return;
            _mousePos.x = x;
            _mousePos.y = y;
            _mouseDownPos = _mousePos;
            _mouseIsDown = true;
            _willSelect = true;
        };
        
//        this->OnExit += [&] (const GameScreenEventArgs& args)
//        {
//            args.manager->add(GUI_CreateGameOverScreen(_score));
//        };
        

    }
    virtual void load_content() {
        _oldScore = 0;
        _scoreTimeout = 0;
        
        _particles = new particles<vec2f>(32, &update_boom);
        
        mBackgroundTexture = atom::getTextureByName("@background");
        star = atom::getTextureByName("@star");
        grey = atom::getTextureByName("@grey");
        green = atom::getTextureByName("@green");
        yellow = atom::getTextureByName("@yellow");
        red = atom::getTextureByName("@red");
        select = atom::getTextureByName("@selector");
        diamond = atom::getTextureByName("@bombAndDanger");
        empty  = atom::getTextureByName("@tile_grid");
        mScorePane = atom::getTextureByName("@scorepane");
        mBlank = atom::getTextureByName("@blank");
        mWarning = atom::getTextureByName("@warning");
        
        //mBlockIndicator = atom::getTextureByName("@block_indicator");
        tex = grey;
        
        int rw, rh;
        SDL_RenderGetLogicalSize(getRenderer(), &rw, &rh);
        
        int dx = DX;

        
        createInitialGrid(_grid);
        
        ui[0] = new Container;
        ui[1] = new Widget;
        ui[2] = new ProgressBar;
        
        
        ((ProgressBar*)ui[2])->setValue(0.0f);
        
        ui[0]->setSize({640,480});
        ui[1]->setSize({32,32});
        ui[2]->setSize({128,128});
        
        ui[1]->setParent(ui[0]);
        ui[2]->setParent(ui[0]);
        
        ((Container*)ui[0])->setLayout(new BoxLayout(WidgetOrientation::Vertical));
        
//        http_request* req = new http_request("pic.tfjoy.com", 3000, "/uploads/Banners/1817-1sl3emj.png");
//        req->OnRequestIsProcessing += [&] (const HttpEventArgs& args) {
//            ((ProgressBar*)ui[2])->setValue(args.progress);
//        };
//        
//        req->OnRequestIsDone += [&] (const HttpEventArgs& args) {
//            SDL_RWops* ops   = SDL_RWFromConstMem(args.req->bytes(), args.req->received());
//            SDL_Surface* img = IMG_Load_RW(ops, 0);
//            SDL_Texture* tex = SDL_CreateTextureFromSurface(_app->getRenderer(), img);
//            atom::addTexture("@remote", tex);
//            SDL_FreeSurface(img);
//        };
//        
//        
//        new std::thread([&](http_request* q) {
//            q->fetch();
//        }, req);
        
//        std::ifstream is;
//        is.open(atom::getResourcePath() + "/assets/Mario-Kart-DS.ttf");
//        if(is.is_open()) {
//            _fontHandle = atom::loadFont("kenpixel_blocks", is, 24, mRenderer);
//            is.close();
//        }

        _fontHandle = atom::getFontId("@mariokart");
        
        //t0->join();
        
    }
    
    virtual void update(int dt, bool a, bool b)
    {
        gamescreen::update(dt, a, b);
        
        if(_gridCellWasSelected)
        {
            float factor = ((float)dt / (float)_offsetTimeout);
            _offset += factor;
            
            _targetPos = _scrollPos;// + getOffsetPosition(_sy);
            
            if(_offset >= 1.f)
            {
                _gridCellWasSelected = false;
                _offset = 0.0f;
            }
        }
        
        for(int i=0; i < GRID_W * GRID_H; i++)
        {
            if(_selected[i])
            {
                _time[i] = std::min(_time[i] + dt, 1000);
            }
        }
        _particles->update(0, dt);
        _scoreTimeout = std::min(_scoreTimeout + dt, SCORETIME);
        
        mDangerTime = std::min(mDangerTime + dt, mDangerTimeout);
    }
    
    void drawBackground(SDL_Renderer* ren, int dx, float ypos) {

        int rw, rh;
        SDL_RenderGetLogicalSize(ren, &rw, &rh);
        int tw, th;
        SDL_QueryTexture(mBackgroundTexture, nullptr, nullptr, &tw, &th);
        
        for(int i=0; i < (GRID_H * dx) / th; i++)
        {
            
            SDL_SetRenderDrawBlendMode(ren, SDL_BLENDMODE_MOD);
            SDL_SetTextureColorMod(mBackgroundTexture, 169, 129, 102);//180, 144, 111);
            
            SDL_Rect rc = {
                //0, (int)(ypos + 0.5f), tw, th
                0,
                rh - (th * i) + (int)(ypos + 0.5f),
                tw,
                th
            };
            SDL_RenderCopy(ren, mBackgroundTexture, nullptr, &rc);
        }

    }
    
    void drawGrid(SDL_Renderer* ren, int dx, int offy, float ypos) {
        for(int y=0; y < GRID_H - 1; y++) {
            int yy = offy - y * dx + ypos;
            
            if(yy < -dx) {
                continue;
            }
            for(int x=0; x < GRID_W; x++) {
                int at = x + y * GRID_W;
                int xx = x * dx;
                
                //
                // draw grid lines for tile
                //
                {
                    SDL_Rect rc = {
                        xx, yy, dx, dx
                    };
                    SDL_SetTextureBlendMode(empty, SDL_BLENDMODE_BLEND);
                    SDL_SetTextureColorMod(empty, 0, 0, 0);
                    SDL_SetTextureAlphaMod(empty, 102);
                    SDL_RenderCopy(ren, empty, nullptr, &rc);
                }

            }
        }
    }
    
    void drawBlockIndicators(SDL_Renderer* ren, int dx, int offy, float ypos) {
        
        const uint8_t alpha = 128;
        
        SDL_SetRenderDrawColor(ren, 98, 35, 39, alpha);
        
        for(int y=0; y < GRID_H - 1; y++) {
            int yy = offy - y * dx + ypos;
            
            if(yy < -dx) {
                continue;
            }
            for(int x=0; x < GRID_W; x++) {
                if((x & 1) && (y & 1)) {
            
                    int by = offy - (y * 2) * dx + ypos;
            
                    SDL_Rect rc = {
                        ((x * 2) * dx) - dx * 2,
                        by + dx,
                        dx * 2,
                        dx * 2
                    };
                    SDL_SetRenderDrawBlendMode(ren, SDL_BLENDMODE_BLEND);
                    SDL_RenderFillRect(ren, &rc);

            
                    SDL_SetTextureColorMod(empty, 0, 0, 0);
                    SDL_SetTextureAlphaMod(empty, alpha);
                    SDL_RenderCopy(ren, empty, nullptr, &rc);
            
            
                } else if(!(x & 1) && !(y & 1)) {
                    int by = offy - (y * 2) * dx + ypos;
            
                    SDL_Rect rc = {
                        ((x * 2) * dx) - dx * 2,
                        by + dx,
                        dx * 2,
                        dx * 2
                    };
                    SDL_SetRenderDrawBlendMode(ren, SDL_BLENDMODE_BLEND);
                    SDL_RenderFillRect(ren, &rc);
            
                    SDL_SetTextureColorMod(empty, 0, 0, 0);
                    SDL_SetTextureAlphaMod(empty, alpha);
                    SDL_RenderCopy(ren, empty, nullptr, &rc);
                }
            }
        }
        SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
    }
    
    void drawVisited(SDL_Renderer* ren, int dx, int offy, float ypos) {
        for(int y=0; y < GRID_H - 1; y++) {
            int yy = offy - y * dx + ypos;
            
            if(yy < -dx) {
                continue;
            }
            for(int x=0; x < GRID_W; x++) {
                int at = x + y * GRID_W;
                int xx = x * dx;
                
                if(_selected[at]) {
                    SDL_Rect dst = {
                        xx, yy,
                        dx, dx
                    };
                    
                    SDL_SetTextureBlendMode(mBlank, SDL_BLENDMODE_NONE);
                    SDL_SetTextureAlphaMod(mBlank, 255);
                    SDL_SetTextureColorMod(mBlank, 99, 157, 255);
                    SDL_RenderCopy(ren, mBlank, nullptr, &dst);
                }
            }
        }
    }
    
    void drawWarnings(SDL_Renderer* ren, int dx, int offy, float ypos) {
        for(int y=0; y < GRID_H - 1; y++) {
            int yy = offy - y * dx + ypos;
            
            if(yy < -dx) {
                continue;
            }
            for(int x=0; x < GRID_W; x++) {
                int at = x + y * GRID_W;
                int xx = x * dx;
                int top = x + (y + 1) * GRID_W;
                

                
                if(_selected[at]) {
                    if(_grid[top] == 1) {
                        
                        float fFactor = (float)_time[at] / 1000.f;
                        float scale = 3.0f - (atom::easing::ease_elastic_out(fFactor) * 2.0f);
                        
                        int tw, th;
                        SDL_QueryTexture(yellow, nullptr, nullptr, &tw, &th);
                        
                        int x0 = xx + dx / 2;
                        int y0 = yy + dx / 2;
                        
                        SDL_Rect rc = {
                            x0 - (int)(tw * 0.5f * scale),
                            y0 - (int)(th * 0.5f * scale),
                            (int)(tw * scale),
                            (int)(th * scale)
                        };
                        SDL_RenderCopyEx(ren, yellow, nullptr, &rc, 0.0f, nullptr, SDL_FLIP_NONE);
                    }
                }
            }
        }
    }
    
    void drawBombs(SDL_Renderer* ren, int dx, int offy, float ypos) {
        for(int y=0; y < GRID_H - 1; y++) {
            int yy = offy - y * dx + ypos;
            
            if(yy < -dx) {
                continue;
            }
            for(int x=0; x < GRID_W; x++) {
                int at = x + y * GRID_W;
                int xx = x * dx;
//                int top = x + (y + 1) * GRID_W;
                
                
                
                if(_selected[at]) {
                    if(_grid[at] == 1) {
                        
                        float fFactor = (float)_time[at] / 1000.f;
                        float scale = 2.0f - (atom::easing::ease_elastic_out(fFactor));
                        
                        int tw, th;
                        SDL_QueryTexture(red, nullptr, nullptr, &tw, &th);
                        
                        int x0 = xx + dx / 2;
                        int y0 = yy + dx / 2;
                        
                        SDL_Rect rc = {
                            x0 - (int)(tw * 0.5f * scale),
                            y0 - (int)(th * 0.5f * scale),
                            (int)(tw * scale),
                            (int)(th * scale)
                        };
                        SDL_SetTextureAlphaMod(red, (int)(255 * fFactor));
                        SDL_RenderCopyEx(ren, red, nullptr, &rc, 0.0f, nullptr, SDL_FLIP_NONE);
                    }
                }
            }
        }
    }
    
    void drawPlayer(SDL_Renderer* ren) {
        
    }
    
    virtual void draw(SDL_Renderer* ren) {
#if 1
        int rw, rh;
        SDL_RenderGetLogicalSize(ren, &rw, &rh);
        //SDL_GetRendererOutputSize(ren, &rw, &rh);
        int dx = DX;
        float ypos = _currentPos = _currentPos + (_targetPos - _currentPos) * 0.3334f;//_totalOffset * dx;
        int offy = rh - dx * 2;
        
        drawBackground(ren, dx, ypos);


        drawBlockIndicators(ren, dx, offy, ypos);
        
        drawVisited(ren, dx, offy, ypos);
        drawGrid(ren, dx, offy, ypos);
        
        drawWarnings(ren, dx, offy, ypos);
        drawBombs(ren, dx, offy, ypos);
        
        
        for(int y=0; y < GRID_H - 1; y++) {
            int yy = offy - y * dx + ypos;
            
            if(yy < -dx) {// || yy > (rh - dx)) {
                continue;
            }
            
            checkForEnclosureOnRow(y);
            for(int x=0; x < GRID_W; x++) {
                
                int xx = x * dx;
                int at  = x + y * GRID_W;
                
                if(_lastSelected == at) {
                    
                    {
                        int tw, th;
                        SDL_QueryTexture(select, nullptr, nullptr, &tw, &th);
                    
                        int x0 = xx - tw / 2 + dx / 2;
                        int y0 = yy - dx / 2;
                    
                        SDL_Rect rc = {
                            x0,
                            y0,
                            (int)(tw),
                            (int)(th)
                        };

                    
                        SDL_SetTextureAlphaMod(select, 255);
                        SDL_RenderCopy(ren, select, nullptr, &rc);
                    }
                    
                    {
                        int tw, th;
                        SDL_QueryTexture(mWarning, nullptr, nullptr, &tw, &th);
                        float flerp = (float)mDangerTime / mDangerTimeout;
                        int x0 = xx - tw / 2 + dx / 2;
                        int y0 = yy - dx - (int)(dx * atom::easing::ease_circle_out(flerp));
                        float fAlpha = 255 * (1.0f - flerp);
                        
                        SDL_Rect rc = {
                            x0,
                            y0,
                            (int)(tw),
                            (int)(th * atom::easing::ease_elastic_out(flerp))
                        };
                        
                        SDL_SetTextureAlphaMod(mWarning, (uint8_t)fAlpha);
                        SDL_RenderCopy(ren, mWarning, nullptr, &rc);
                    }
                    
                }
            }
        }
        
        
        _particles->draw(ren, star, { 0, 0, 0, 0 });
        
        float lerp = (float)_scoreTimeout / SCORETIME;
        float factor  = atom::easing::ease_cubic_in(lerp);
        int currentScore = (int)(_oldScore + (_score - _oldScore) * factor);
        
        {
            int tw, th;
            SDL_QueryTexture(mScorePane, NULL, NULL, &tw, &th);
            
            int realTextureHeight = (int)((float)th * ((float)rw / tw));
            
            SDL_Rect dst = {
                0, rh - realTextureHeight, tw, realTextureHeight
            };
            SDL_RenderCopy(ren, mScorePane, nullptr, &dst);
        }
        
        {
            std::stringstream ss;
            ss << "Score:" << currentScore;
            std::string scoreString = ss.str();
            SDL_Color color = { 255, 255, 255, 255 };
            
            int tw, th;
            atom::getFontById(0)->measureString(scoreString, &tw, &th);
            atom::getFontById(0)->drawString(ren, { dx, rh - dx }, scoreString, &color);
        }
        
        {
            std::stringstream ss;
            ss << "Max Row:" << _maxRowHeight;
            std::string str = ss.str();
            SDL_Color color = { 255, 255, 255, 255 };
            int tw, th;
            atom::getFontById(0)->measureString(str, &tw, &th);
            atom::getFontById(0)->drawString(ren, { rw - dx - tw, rh - dx}, str, &color);
        }
#else
        ui[0]->setPosition({0, 0 });
        ui[0]->setSize({310, 470});
        ui[0]->draw(ren);
#endif
        
        SDL_Rect fullscreen = {
            0, 0,
            rw, rh
        };
        
        if(state() != E_STATE_ACTIVE) {
            SDL_Color col;
            SDL_GetRenderDrawColor(ren, &col.r, &col.g, &col.b, &col.a);
            SDL_SetRenderDrawBlendMode(ren, SDL_BLENDMODE_BLEND);
            SDL_SetRenderDrawColor(ren, 7, 54, 66, (uint8_t)(255.f * transition()));
            SDL_RenderFillRect(ren, &fullscreen);
        }
        
        
        gamescreen::draw(ren);
    }
};

const int screen_play::mDangerTimeout = 1000;

extern "C" gamescreen* GUI_CreatePlayScreen(SDL_Renderer* ren) {
    return new screen_play(ren);
}
