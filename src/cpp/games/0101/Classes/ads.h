#pragma once

#include <atom/core/factory.h>
#include <atom/core/event_handler.h>

namespace atom {

    class Advertisement;
    
struct AdEventArgs {
    Advertisement* ad;
};

class Advertisement {
public:
    virtual ~Advertisement() = default;
    virtual void fetch() = 0;
    virtual void show() = 0;
    virtual void dismiss() = 0;
    virtual bool isReady() = 0;
    virtual const std::string& getPublisherId() const = 0;
    virtual const std::string& getAppId() const = 0;
    virtual void setPublisherId(const std::string&) = 0;
    virtual void setAppId(const std::string&) = 0;
public:
    event_handler<void(const AdEventArgs&)> onAdWasFetched;
    event_handler<void(const AdEventArgs&)> onAdWasShown;
    event_handler<void(const AdEventArgs&)> onAdWasHidden;
    event_handler<void(const AdEventArgs&)> onAdWasDismissed;
//    event_handler<void(const AdEventArgs&)> onAdIsReady;
};

typedef factory<Advertisement> ad_factory;

    class ad_manager : public singleton<ad_manager> {
        DISALLOW_COPY_AND_ASSIGN(ad_manager);
    public:
        ad_manager() {}
        ~ad_manager() {}
        void cacheFrom(std::istream &is);
        Advertisement* getAdByName(const std::string& name);
    };
    
} 
