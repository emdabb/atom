//
//  matrix.h
//  0101
//
//  Created by emiel Dam on 1/11/16.
//
//

#ifndef matrix_h
#define matrix_h

namespace atom {
    static void matmul(float* a, float* b, float* c, int rowsA, int colsB, int colsA)
    {
        for(int i=0; i < rowsA; i++) {
            for(int j=0; j < colsB; j++) {
                
                c[i + j * colsB] = 0.0f;
                
                for(int k=0; k < colsA; k++) {
                    c[i + j * colsB] = c[i + j * colsB] + (a[i + k * colsA] * b[k + j * colsB]);
                }
            }
        }
    }
}

#endif /* matrix_h */
