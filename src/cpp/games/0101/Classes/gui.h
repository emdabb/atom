//
//  gui.h
//  0101
//
//  Created by emiel Dam on 12/17/15.
//
//

#ifndef gui_h
#define gui_h

#include "vec2.h"
#include "asset_manager.h"
#include <stdint.h>

class Container;

class Widget {
    Widget* mParent;
    vec2i   mPosition;
    vec2i   mSize;
    vec2i   mFixedSize;
    bool    mIsVisible;
    bool    mIsValid;
public:
    explicit Widget() : mParent(nullptr) {
        mFixedSize = { 0, 0 };
        mIsVisible = true;
        mIsValid   = true;
    }
    
    virtual ~Widget() = default;
    
    const bool isVisible() const { return mIsVisible != false; }
    const bool isValid() const { return mIsValid != false; }
    
    virtual vec2i getPreferredSize() const {
        return mSize;
    }
    virtual vec2i getFixedSize() const {
        return mFixedSize;
    }
    
    virtual int getWidth() const {
        return mSize.x;
    }
    virtual int getHeight() const {
        return mSize.y;
    }
    
    virtual void validate() {
        mIsValid = true;
    }
    virtual void invalidate() {
        mIsValid = false;
    }
    
    void setPosition(const vec2i& val) {
        mPosition = val;
        invalidate();
    }

    void setSize(const vec2i& val) {
        mSize = val;
        invalidate();
    }
    
    virtual const vec2i& getSize() const {
        return mSize;
    }
    
    const Widget* getParent() const {
        return mParent;
    }
    
    vec2i getPositionOnScreen() const {
        return mParent ? (getParent()->getPositionOnScreen() + mPosition) : mPosition;
    }
    
    virtual void draw(SDL_Renderer* ren) {
        if(!isValid())
            validate();
        
        vec2i pos = getPositionOnScreen();
        vec2i size= getSize();
        
        SDL_Rect rc = {
            pos.x, pos.y,
            size.x, size.y
        };
        
        SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
        SDL_RenderDrawRect(ren, &rc);
    }
    
    void setParent(Widget* obj);
};

class Container;

class ILayoutManager {
public:
    virtual void layoutContainer(Container*) const = 0;
    virtual vec2i getPreferredSize(const Container*) const = 0;
};


class Container : public Widget {
    typedef std::vector<Widget*> list_t;
    list_t mChildren;
    ILayoutManager* mLayout;
public:
    explicit Container() : mLayout(nullptr) {
        
    }
    
    virtual ~Container() {
        
    }
    
    void add(Widget* w) {
        mChildren.push_back(w);
        invalidate();
    }
    
    const list_t& getChildren() const {
        return mChildren;
    }

    virtual vec2i getPreferredSize() const {
        if(mLayout) {
            return mLayout->getPreferredSize(this);
        }
        return Widget::getPreferredSize();
    }
    
    virtual void validate() {
        if(!isValid() ) {
            validateRecursive();
            doLayout();
            Widget::validate();
        }
    }
    virtual void validateRecursive() {
        for(auto w : getChildren()) {
            w->validate();
        }
    }
    virtual void invalidate() {
        Widget::invalidate();
    }

    virtual void doLayout() {
        if(mLayout) {
            mLayout->layoutContainer(this);
        }
    }
    
    virtual void draw(SDL_Renderer* ren) {
        Widget::draw(ren);
        for(auto w : getChildren()) {
            w->draw(ren);
        }
    }
    
    void setLayout(ILayoutManager* val) {
        mLayout = val;
    }
};



class ProgressBar : public Widget {
    float mValue;
public:
    float getValue() { return mValue; }
    void setValue(float val) {
        mValue = val;
    }
    
    virtual vec2i getPreferredSize() const {
        return (vec2i) { 70, 12 };
    }
    
    virtual void draw(SDL_Renderer* ren) {
        Widget::draw(ren);
        
        vec2i sz = getSize();
        
        float val = std::min(std::max(0.0f, mValue), 1.0f);
        int bar   = (int)round((sz.x - 2) * val);
        
        vec2i pos = getPositionOnScreen();
        
        SDL_Rect dst = {
            pos.x + 1, pos.y + 1,
            bar, sz.y - 2
        };
        
        //SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
        SDL_Texture* tex = atom::getTextureByName("@progress");
        SDL_SetTextureColorMod(tex, 255, 255, 255);
        //SDL_RenderDrawRect(ren, &dst);
        SDL_RenderCopy(ren, tex, nullptr, &dst);
    }
};

enum class WidgetAlignment : uint8_t {
    Minimum = 0,
    Middle,
    Maximum,
    Fill
};

enum class WidgetOrientation {
    Horizontal = 0,
    Vertical
};



class BoxLayout : public ILayoutManager {
    WidgetOrientation   mOrientation;
    WidgetAlignment     mAlignment;
    int mMargin;
    int mSpacing;
public:
    BoxLayout(WidgetOrientation orientation, WidgetAlignment alignment = WidgetAlignment::Middle, int margin = 0, int spacing = 0)
    : mOrientation(orientation)
    , mAlignment(alignment)
    , mMargin(margin)
    , mSpacing(spacing)
    {
        
    }
    
    virtual ~BoxLayout() {
        
    }
    
    virtual void layoutContainer(Container* parent) const {
        vec2i fs_w = parent->getFixedSize();
        vec2i containerSize = {
                               fs_w.cell[0] ? fs_w.cell[0] : parent->getWidth(),
                               fs_w.cell[1] ? fs_w.cell[1] : parent->getHeight()
        };
        
        int axis1 = (int) mOrientation, axis2 = ((int) mOrientation + 1) & 1;
        int position = mMargin;
//
//        if (dynamic_cast<Window *>(widget))
//            position += widget->theme()->mWindowHeaderHeight - mMargin/2;
//        
        bool first = true;
        for (auto w : parent->getChildren()) {
            if (!w->isVisible())
                continue;
            if (first)
                first = false;
            else
                position += mSpacing;
            
            vec2i ps = w->getPreferredSize(), fs = w->getFixedSize();
            vec2i targetSize= {
                fs.cell[0] ? fs.cell[0] : ps.cell[0],
                fs.cell[1] ? fs.cell[1] : ps.cell[1]
            };
            vec2i pos = { 0, 0 };
            pos.cell[axis1] = position;
            
            switch (mAlignment) {
                case WidgetAlignment::Minimum:
                    pos.cell[axis2] = mMargin;
                    break;
                case WidgetAlignment::Middle:
                    pos.cell[axis2] = (containerSize.cell[axis2] - targetSize.cell[axis2]) / 2;
                    break;
                case WidgetAlignment::Maximum:
                    pos.cell[axis2] = containerSize.cell[axis2] - targetSize.cell[axis2] - mMargin;
                    break;
                case WidgetAlignment::Fill:
                    pos.cell[axis2] = mMargin;
                    targetSize.cell[axis2] = fs.cell[axis2] ? fs.cell[axis2] : containerSize.cell[axis2];
                    break;
            }
            
            w->setPosition(pos);
            w->setSize(targetSize);
            w->validate();
            position += targetSize.cell[axis1];
        }
    }
    virtual vec2i getPreferredSize(const Container* parent) const {
        vec2i size = {
            2 * mMargin,
            2 * mMargin
        };
        
        bool first = true;
        int axis1 = (int)mOrientation;
        int axis2 = ((int)mOrientation + 1) & 1;
        
        for(auto w : parent->getChildren()) {
            if(!w->isVisible())
                continue;
            if(first)
                first = false;
            else
                size.cell[axis1] += mSpacing;
            
            vec2i ps = w->getPreferredSize();
            vec2i fs = w->getFixedSize();
            vec2i ss = {
                fs.x ? fs.x : ps.x,
                fs.y ? fs.y : ps.y
            };
            
            size.cell[axis1] += ss.cell[axis1];
            size.cell[axis2]  = std::max(size.cell[axis2], ss.cell[axis2] + 2 * mMargin);
            first = false;
        }
        return size;
    }
};

#endif /* gui_h */
