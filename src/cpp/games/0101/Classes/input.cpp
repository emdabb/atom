#include "input.h"
#include <SDL.h>
#include <string>

namespace atom 
{
    bool _mouseDown;
    SDL_Point _mousePosition;
    SDL_Point _mouseReleasePosition;
    SDL_Point _mousePressPosition;
    uint16_t  _keyPressed;
    std::string _text;
    
    event_handler<void(int, int, int, int)> OnMouseMoved;
    event_handler<void(int, int)> OnMousePressed;
    event_handler<void(int, int)> OnMouseReleased;

    const SDL_Point& SDL_GetMousePosition() 
    {
        return _mousePosition;
    }

    const SDL_Point& SDL_GetMousePressPosition()
    {
        return _mousePressPosition;
    }

    const SDL_Point& SDL_GetMouseReleasePosition()
    {
        return _mouseReleasePosition;
    }

    const std::string& input_GetLastTextInput() {
        return _text;
    }

    const bool SDL_IsMouseDown()
    {
        return _mouseDown != false;
    }
    
    uint16_t input_GetLastKeyPressed() {
        return _keyPressed;
    }
    
    void input_resetKey() {
        _keyPressed = 0;
    }
    
    void input_resetMouseButton() {
        _mouseDown = false;
    }

    void input_OnMouseMoved(const int& x, const int& y, const int& dx, const int& dy) 
    {
        _mousePosition.x = x;
        _mousePosition.y = y;
        
        OnMouseMoved(x, y, dx, dy);
    }

    void input_OnMousePressed(const int& x, const int& y) 
    {
    	_mouseDown = true;
        _mousePressPosition.x = x;
        _mousePressPosition.y = y;
        OnMousePressed(x, y);
    }

    void input_OnMouseReleased(const int& x, const int& y)
    {
    	_mouseDown = false;
        _mouseReleasePosition.x = x;
        _mouseReleasePosition.y = y;
        OnMouseReleased(x, y);
    }
    
    void input_OnKeyPressed(const uint16_t& key) {
        _keyPressed = key;
    }

    void input_OnTextInput(const std::string& str) 
    {
        _text = str;
    }

}
