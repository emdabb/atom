//
//  screens.h
//  0101
//
//  Created by emiel Dam on 12/21/15.
//
//

#ifndef screens_h
#define screens_h

#include "gamescreen.h"

struct SDL_Renderer;

extern "C" gamescreen* GUI_CreatePlayScreen(SDL_Renderer*);
extern "C" gamescreen* GUI_CreateGameOverScreen(int);
extern "C" gamescreen* GUI_CreateMenuScreen();
extern "C" gamescreen* GUI_CreateSplashScreen();
extern "C" gamescreen* GUI_CreateHighscoreScreen();

#endif /* screens_h */
