//
//  sprite_sheet.h
//  bouncy
//
//  Created by emiel Dam on 12/4/15.
//
//

#ifndef sprite_sheet_h
#define sprite_sheet_h

#include <map>
#include <vector>

namespace atom {
    template <typename T = int>
    class sprite_sheet {
        std::map<T, SDL_Rect>   _rects_by_name;
        std::vector<SDL_Rect>   _rects;
        SDL_Texture*            _src;
    public:
        int addRect(const T& name, const SDL_Rect& rc)
        {
            typename std::map<T, SDL_Rect>::iterator it = _rects_by_name.find(name);
            if(it == _rects_by_name.end()) {
                _rects_by_name[name] = rc;
                _rects.emplace_back(rc);
                return (int)_rects.size();
            }
            return -1;
        }
        
        void setTexture(SDL_Texture* tex) {
            _src = tex;
        }
        
        const SDL_Rect* getRect(const T& name) {
            typename std::map<T, SDL_Rect>::iterator it = _rects_by_name.find(name);
            if(it != _rects_by_name.end()) {
                return &(*it).second;
            }
            return NULL;
        }
        
        SDL_Texture* getTexture() {
            return _src;
        }
    };

}


#endif /* sprite_sheet_h */
