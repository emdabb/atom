//
//  gui.cpp
//  0101
//
//  Created by emiel Dam on 12/21/15.
//
//

#include "gui.h"


void Widget::setParent(Widget* obj) {
    Container* cobj = dynamic_cast<Container*>(obj);
    if(cobj != nullptr) {
        mParent = obj;
        cobj->add(this);
    }
}