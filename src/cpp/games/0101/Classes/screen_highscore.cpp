//
//  screen_loading.cpp
//  0101
//
//  Created by emiel Dam on 12/21/15.
//
//

#include <stdio.h>
#include <thread>
#include "screens.h"
#include "imgui.h"
#include "gui.h"
#include "http_request.h"
#include <happyhttp/happyhttp.h>
#include <cjson/cJSON.h>
#include "asset_manager.h"


using namespace happyhttp;
using namespace atom;

class screen_highscore : public gamescreen {
    struct entry {
        std::string name;
        int score;
    };
    
    std::vector<entry> mPlayerEntries;
    
    char* data;
    int dataptr;
    int datalen;
    
    bool mAddressUnreachable;
    
    static void OnBegin(const Response* r, void* userdata) {
        std::cout << "[http-get] ** begin => [content-length]=[" << r->getheader("Content-Length") << "]" << std::endl;
        
        screen_highscore* req = (screen_highscore*)userdata;
        req->dataptr = 0;
        req->datalen = atoi(r->getheader("Content-Length"));
        req->data    = new char[req->datalen];
    }
    static void OnData(const Response* r, void* userdata, const unsigned char* data, int numbytes) {
        std::cout << "[http-get] ** data  => [data]=[" << data << "]" << std::endl;
        
        screen_highscore* req = (screen_highscore*)userdata;
        memcpy(&req->data[req->dataptr], data, numbytes);
        req->dataptr += numbytes;
    }
    static void OnComplete(const Response* r, void* userdata) {
        std::cout << "[http-get] ** done  => [content-length]=[" << r->getheader("Content-Length") << "]" << std::endl;
        screen_highscore* req = (screen_highscore*)userdata;
        req->handleJson((const char*)req->data);
    }
    
    void handleJsonNode(const cJSON* json) {
        static std::string currentPlayerEntry;
        
        cJSON* ptr = json->child;
        while(ptr) {
            if(!strcmp(ptr->string, "player")) {
                currentPlayerEntry = ptr->valuestring;
            }
            if(!strcmp(ptr->string, "score")) {
                mPlayerEntries.push_back({ currentPlayerEntry, ptr->valueint} );
            }
            ptr = ptr->next;
        }
    }
    
    void handleJson(const char* raw) {
        cJSON* json = cJSON_Parse(raw);
        if(json) {
            int len = cJSON_GetArraySize(json->child);
            
            for(int i=0; i < len; i++) {
                handleJsonNode(cJSON_GetArrayItem(json->child, i));
            }
        }
        std::reverse(mPlayerEntries.begin(), mPlayerEntries.end());
    }
public:
    screen_highscore()
    : mAddressUnreachable(false)
    {
        this->_popup = true;
        
        this->OnExit += [&] (const GameScreenEventArgs& args_) {
            args_.manager->add(GUI_CreateMenuScreen());
        };
//        this->quit();
        
    }
    
    virtual ~screen_highscore() {
        
    }
    
    virtual void load_content() {

        
        char body[256] = { 0 };
        const char* headers[] = {
            "Connection", "close",
            "Content-type", "application/x-www-form-urlencoded",
            "Accept", "text/plain",
            0
        };
        
        strcat(body, "?reverse=0&limit=10");
        
        try {
            Connection con(HIGHSCORE_IP, HIGHSCORE_PORT);
            con.setcallbacks(OnBegin, OnData, OnComplete, this);
            
            con.request("GET", "/0101", headers, nullptr, 0);//(const uint8_t*)body, (int)strlen(body));
            while (con.outstanding()) {
                con.pump();
            }
        } catch(happyhttp::Wobbly& err) {
            mAddressUnreachable = true;
            std::cout << err.what() << std::endl;
        }
    }
    
    virtual void draw(SDL_Renderer* ren) {
        int rw, rh;
        SDL_GetRendererOutputSize(ren, &rw, &rh);
        
        sprite_font* font = getFontById(0);
        
        imgui_reset();
        imgui_setAlpha(1.0f - transition());
        
        std::vector<entry>::iterator it = mPlayerEntries.begin();
        
        {
            int tw, th;
            font->measureString("highscores", &tw, &th);
            imgui_label(GEN_ID(), ren, 0, { rw / 2 - tw / 2, th * 2 }, "highscores", 0);
        }
        
        if(mAddressUnreachable) {
            int tw, th;
            font->measureString("unavailable", &tw, &th);
            
            imgui_label(GEN_ID(), ren, 0,
                        {
                            rw / 2 - tw / 2,
                            rh / 2 - th / 2
                        }, "unavailable", 0);

        } else {
            int tw, th;
            font->measureString("highscores", &tw, &th);
            int starty = th * 3;
            
        for(int i=0; it != mPlayerEntries.end(); it++, i++) {
            
            std::stringstream ss;
            ss << i + 1 << ".)" << (*it).name << "--" << (*it).score;
            
            int tw, th;
            font->measureString(ss.str(), &tw, &th);
            
            imgui_label(GEN_ID(), ren, 0,
            {
                rw / 2 - tw / 2,
                starty + (i + 1) * th
            }, ss.str().c_str(), 0);
        }
        }
        
        {
            int tw, th;
            font->measureString("ok", &tw, &th);
            SDL_Rect rect {
                rw / 2 - tw / 2,
                rh - th * 2,
                tw, th
            };
            
            if(imgui_button(GEN_ID(), ren, rect, 0, "ok")) {
                if(state() == E_STATE_ACTIVE) {
                    quit();
                }
            }
        }
        
    }
};

gamescreen* GUI_CreateHighscoreScreen() {
    return new screen_highscore;
}
