//
//  screen_splash.cpp
//  0101
//
//  Created by emiel Dam on 12/18/15.
//
//

#include <stdio.h>
#include "screens.h"
#include "asset_manager.h"

static const char* SCREEN_NAMES[] = {
    "@splash00",
    "@splash01",
    "@splash02"
};

class screen_splash : public gamescreen {
    int mCurrentImageToDraw;
    int mTimeToShow;
    int mTime;
    static const int MaxImagesToDraw = 3;
public:
    screen_splash()
    : mCurrentImageToDraw(0)
    , mTimeToShow(500)
    , mTime(0)
    {
        this->_time_on = this->_time_off = 500;
        this->OnExit += [&] (const GameScreenEventArgs& args) {
            args.manager->add(GUI_CreateMenuScreen());
        };
    }
    virtual void update(int dt, bool otherScreenHasFocus, bool b) {
        static bool _coveredByOtherScreen = false;
        
        if(state() == E_STATE_ACTIVE)
        {
            mTime += dt;
            if(mTime >= mTimeToShow)
            {
                _coveredByOtherScreen = true;
            }
        }
        else if(state() == E_STATE_HIDDEN)
        {
            _coveredByOtherScreen = false;
            mCurrentImageToDraw++;
            mTime = 0;
            
        }
        
        if(mCurrentImageToDraw >= MaxImagesToDraw) {
            _coveredByOtherScreen = true;
            quit();
        }
        
        gamescreen::update(dt, otherScreenHasFocus, _coveredByOtherScreen);
    }
    
    virtual void draw(SDL_Renderer* ren) {
        gamescreen::draw(ren);
        
        SDL_Texture* tex = atom::getTextureByName(SCREEN_NAMES[mCurrentImageToDraw]);
        
        int rw, rh;
        int tw, th;
        int width, height;

        //SDL_GetRendererOutputSize(ren, &rw, &rh);
        SDL_RenderGetLogicalSize(ren, &rw, &rh);
        SDL_QueryTexture(tex, nullptr, nullptr, &tw, &th);
        
        if(tw < rw) {
            width = tw;
            height = th;
        } else {
            
            float targetAspect = (float)tw / th;
            

            width = rw >> 1;
            height= (int)(width / targetAspect + 0.5f);
            
            if(height > rh) {
                height = rh;
                width  = (int)(height * targetAspect + 0.5f);
            }
        }


        SDL_Rect dst = {
            rw / 2 - width  / 2,
            rh / 2 - height / 2,
            width,
            height
        };
        uint8_t alpha = ((1.0f - transition()) * 255.f);
        SDL_SetTextureAlphaMod(tex, alpha);

        
        SDL_RenderCopy(ren, tex, nullptr, &dst);
    }
};

gamescreen* GUI_CreateSplashScreen() {
    return new screen_splash;
}

