//
//  game_sweep.cpp
//  application_name
//
//  Created by emiel Dam on 10/22/15.
//
//


#include <atom/atom_platform.h>
#include <atom/engine/application.h>
#include <SDL.h>
#include <SDL_mixer.h>
#include <atom/engine/easing.h>
#include "asset_manager.h"
#include <fstream>
#include <SDL_mixer.h>
#include "particles.h"
#include "particle_spawn.h"
#include "particle_update.h"
#include "ads.h"
#include "gui.h"
#include "http_request.h"
#include "screens.h"
#include "input.h"

class game_sweep : public atom::applet {
    atom::application* _app;
    gamescreen_manager mGameScreenManager;
public:
    explicit game_sweep(atom::application* app)
    : _app(app)
    {
        srand(SDL_GetTicks());
        
        _app->OnMouseDown    += &atom::input_OnMousePressed;
        _app->OnMouseMove    += &atom::input_OnMouseMoved;
        _app->OnMouseRelease += &atom::input_OnMouseReleased;
        _app->OnKeyPressed   += &atom::input_OnKeyPressed;
        
//        _app->OnMouseDown.add(atom::delegate<void(int, int)>::from<game_sweep, &game_sweep::onMouseDown>(this));
//        _app->OnMouseRelease.add(atom::delegate<void(int, int)>::from<game_sweep, &game_sweep::onMouseUp>(this));
//        _app->OnMouseMove.add(atom::delegate<void(int, int, int, int)>::from<game_sweep, &game_sweep::onMouseMove>(this));
//        this->OnClick.add(atom::delegate<void()>::from<game_sweep, &game_sweep::onClick>(this));
    }
    
    virtual int create() {
        std::ifstream is;
        
        SDL_Renderer* ren = _app->getRenderer();
        int rw, rh;
        SDL_GetRendererOutputSize(ren, &rw, &rh);

        int lw, lh;
        SDL_RenderSetLogicalSize(ren, 1024, 1365);
        SDL_RenderGetLogicalSize(ren, &lw, &lh);



        int fontsize = rw / 12;

        std::string path_to_texture_lib(atom::getResourcePath() + "assets/textures.json");

        
        is.open(path_to_texture_lib.c_str());
        if(is.is_open()) {
            atom::loadTextureLibrary(_app->getRenderer(), is);
            is.close();
        }
        
#if defined(__IOS__) || (__ANDROID__) 
        is.open(atom::getResourcePath() + "assets/ads.json");
        if(is.is_open()) {
            atom::ad_manager::instance().cacheFrom(is);
            is.close();
        }
#endif 
        is.open(atom::getResourcePath() + "assets/Mario-Kart-DS.ttf");
        if(is.is_open()) {
            atom::loadFont("@mariokart", is, fontsize, _app->getRenderer());
            is.close();
        }
//        http_request* req = new http_request("pic.tfjoy.com", 3000, "/uploads/Banners/1817-1sl3emj.png");
//        req->OnRequestIsProcessing += [&] (const HttpEventArgs& args) {
//            ((ProgressBar*)ui[2])->setValue(args.progress);
//        };
//        
//        req->OnRequestIsDone += [&] (const HttpEventArgs& args) {
//            SDL_RWops* ops   = SDL_RWFromConstMem(args.req->bytes(), args.req->received());
//            SDL_Surface* img = IMG_Load_RW(ops, 0);
//            SDL_Texture* tex = SDL_CreateTextureFromSurface(_app->getRenderer(), img);
//            atom::addTexture("@remote", tex);
//            SDL_FreeSurface(img);
//        };
//
//        
//        new std::thread([&](http_request* q) {
//            q->fetch();
//        }, req);
        
//        postScore();
        
//        atom::ad_manager::instance().getAdByName("admobBanner")->onAdWasFetched += [&] (const atom::AdEventArgs& args) {
//            args.ad->show();
//        };
        
        if(Mix_Init(MIX_INIT_OGG) == 0) {
            printf("Mix_Init() failed: %s\n", Mix_GetError());
            return false;
        }
        
        //Initialize SDL_mixer
        if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
        {
            std::cout << Mix_GetError() << std::endl;
            return false;
        }
        
        Mix_AllocateChannels(32);
        
        std::string sndPath = atom::getResourcePath() + "assets/audio.json";
        is.open(sndPath);
        if(is.is_open()) {
            atom::loadAudioLibrary(is);
            is.close();
        }

        

        mGameScreenManager.add(GUI_CreatePlayScreen(_app->getRenderer()));
//        mGameScreenManager.add(GUI_CreateSplashScreen());

//        mGameScreenManager.add(GUI_CreateHighscoreScreen());
//        mGameScreenManager.add(GUI_CreateMenuScreen());

        
        return 0;
    }
    
    virtual int destroy() {
        return 0;
    }
    
    virtual void update(int t, int dt) {
        mGameScreenManager.update(t, dt);
    }
    
    virtual void draw() {
        SDL_Renderer* ren = _app->getRenderer();
        SDL_SetRenderDrawColor(ren, 7, 54, 66, 255);
        SDL_RenderClear(ren);
        mGameScreenManager.draw(ren);
        SDL_RenderPresent(ren);
    }
};

extern "C" atom::applet* new_game_sweep(atom::application* app) {
    return new game_sweep(app);
}
