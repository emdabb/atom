//
//  particle_spawn.h
//  application_name
//
//  Created by emiel Dam on 9/30/15.
//
//

#ifndef particle_spawn_h
#define particle_spawn_h

#include "vec2.h"
#include "color.h"
#include <cmath>

namespace {

static float randf() {
    return (float)rand() / (float)RAND_MAX;
}

static void burst(const vec2f& at,
                       size_t count,
                       float spread,
                       float baseAngle,
                       float speed,
                       float speedVariance,
                       particles<vec2f>* pool,
                       float baseScale = 1.f,
                       float scaleVariance = 0.f)
{
    float speedRnd;
    vec2f angleVector;
    float spreadRnd;
    float scaleRnd;
    
    for(size_t i=0; i < count; i++)
    {
        
        speedRnd  = randf() * speedVariance - speedVariance / 2.0f;
        spreadRnd = randf() * spread - spread / 2.0f;
        scaleRnd  = randf() * scaleVariance - scaleVariance / 2.0f;
        
        angleVector.x = std::cos((-baseAngle + spreadRnd)) * speed * (1 + speedRnd);
        angleVector.y = std::sin((-baseAngle + spreadRnd)) * speed * (1 + speedRnd);
        
        SDL_Color color = { 255, 255, 255, 255 };//detail::SDL_MakeColorHSV(rand() % 255, 255, 255);
        
        pool->emit(at, 1000.f + 1000.f * (rand() / (float)RAND_MAX), color, angleVector, baseScale + scaleRnd);
    }
}

void explode(
    const vec2f& at, 
    size_t count, 
    float distanceMultiplier, 
    particles<vec2f>* pool, 
    float randomRange=2.0f, 
    const vec2f& vector = { 0.0f, 0.0f }, 
    float spawnAreaSize = 0.0f) 
{
    for(size_t i=0; i < count; i++) 
    {
        vec2f at_ = { 
            at.x + (randf() - 0.5f) * spawnAreaSize,
            at.y + (randf() - 0.5f) * spawnAreaSize
        };

        vec2f vel = {
            (vector.x + (randf() - .5f) * randomRange) * distanceMultiplier,
            (vector.y + (randf() - .5f) * randomRange) * distanceMultiplier
        };

        SDL_Color color = detail::SDL_MakeColorHSV(rand() % 255, 255, 255);
    
        pool->emit(at_, 1000.f + 1000.f * randf(), color, vel);
    } 
}

}

#endif /* particle_spawn_h */
