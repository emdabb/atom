#define MAX_ENTITIES        1024


struct World;

typedef int(*destroy_fn)(World*, int);

struct Transform {
    SDL_Point pos;
    float scale;
};

struct Tween {
    int id;
    float time;
    float timeout;
    float* dst;
    float minValue, maxValue;
};

struct Timeout {
    int current, max;
    destroy_fn onTimeout;
};

struct Nested {
    int parentId;
};

struct Visual {
    SDL_Color color;
};

enum {
    E_COMPONENT_NIL         = 0,
    E_COMPONENT_TRANSFORM   = 1 << 0,
    E_COMPONENT_TWEEN       = 1 << 1,
    E_COMPONENT_SCORE       = 1 << 2,
    E_COMPONENT_TIMEOUT     = 1 << 3
};

struct World {
    int         mask[MAX_ENTITIES];
    Transform   transform[MAX_ENTITIES];
    Tween       tween[MAX_ENTITIES];
    int         score[MAX_ENTITIES];
    Nested      nested[MAX_ENTITIES];
    destroy_fn  destroy[MAX_ENTITIES];
    Timeout     timeout[MAX_ENTITIES];
    Visual      visual[MAX_ENTITIES];
    
    particles<vec2f>* _boomParticles;
    Grid _grid;


};

int World_createEntity(World* world) {
    for(int i=0; i < MAX_ENTITIES; i++) {
        if(world->mask[i] == E_COMPONENT_NIL)
            return i;
    }
    return MAX_ENTITIES;
}

void World_destroyEntity(World* world, int id) {
    world->mask[id] = E_COMPONENT_NIL;
}

