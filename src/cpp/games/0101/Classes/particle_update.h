#pragma once

#include <atom/engine/easing.h>

static void update_boom(particles<vec2f>* sys, particles<vec2f>::particle& p, int t, int dt) {
    float fDt = (float)dt * 1.f;
    p.pos.x += p.state.x * fDt;
    p.pos.y += p.state.y * fDt;
    
    p.state.x *= 0.85f;
    p.state.y *= 0.85f;
    
    p.percentLife   = std::max(0.0f, p.percentLife - fDt / p.duration);
    p.currentScale  = p.scale * atom::easing::ease_elastic_out(1.0f - p.percentLife);
    p.tint.a        = (uint8_t)(p.percentLife * 255.0f);
    p.angle         = 0.0f;//p.percentLife * M_PI * 2.0f;
}
