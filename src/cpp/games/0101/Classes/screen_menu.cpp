//
//  screen_menu.cpp
//  0101
//
//  Created by emiel Dam on 12/18/15.
//
//

#include <stdio.h>
#include "screens.h"
#include "imgui.h"
#include "asset_manager.h"
#include <SDL.h>

using namespace atom;

class screen_menu : public gamescreen {
    gamescreen* next;
    SDL_Texture* mBackground;
public:
    screen_menu()
    : mBackground(NULL)
    {
        next = nullptr;
    }
    virtual ~screen_menu() {
        next = nullptr;
    }
    virtual void update(int dt, bool otherScreenHasFocus, bool coveredByOtherScreen) {
        gamescreen::update(dt, otherScreenHasFocus, coveredByOtherScreen);
    }
    
    virtual void load_content() {
        

    }
    
    virtual void draw(SDL_Renderer* ren) {
        
        if(!mBackground) {
            mBackground = atom::getTextureByName("@menu_bg");
        }

        if(!next) {
            next = GUI_CreatePlayScreen(ren);
        }
        gamescreen::draw(ren);
        

        
        int rw, rh;
        //SDL_GetRendererOutputSize(ren, &rw, &rh);
        SDL_RenderGetLogicalSize(ren, &rw, &rh);
        
        
        SDL_Rect rc = {
            0, 0, rw, rh
        };
        
        SDL_RenderCopy(ren, mBackground, nullptr, &rc);
        
        atom::sprite_font* font = atom::getFontById(0);
        
        float c = 1.0f - transition();
        
        imgui_reset();
        imgui_setAlpha(c);
        {
        
            int tw, th;
            font->measureString("new game", &tw, &th);
            SDL_Rect rect = {
                rw / 2 - tw / 2,
                rh / 2 - th / 2,
                tw,
                th
            };
            
            SDL_Texture* tex = atom::getTextureByName("@menu_fg");
            int ttw, tth;
            SDL_QueryTexture(tex, NULL, NULL, &ttw, &tth);
            
            int realTextureHeight = (int)((float)tth * ((float)rw / ttw));
            
            SDL_Rect rc_foreground = {
                0,
                rh - tth,
                ttw,
                tth
            };
            
            SDL_RenderCopy(ren, tex, nullptr, &rc_foreground);
            
            if(imgui_button(GEN_ID(), ren, rc_foreground, 0, "")) {
                if(_exiting != true) {
                    this->OnExit += [&] (const GameScreenEventArgs& args) {
                        args.manager->add(next);
                    };
                    quit();
                }
            }
        }
        
//        {
//            int tw, th;
//            font->measureString("highscore", &tw, &th);
//            SDL_Rect rect = {
//                rw / 2 - tw / 2,
//                rh / 2 + th,
//                tw,
//                th
//            };
//            if(imgui_button(GEN_ID(), ren, rect, 0, "highscore")) {
//                if(state() == E_STATE_ACTIVE) {
//                    this->OnExit += [&] (const GameScreenEventArgs& args) {
//                        args.manager->add(GUI_CreateHighscoreScreen());
//                    };
//                    quit();
//                }
//            }
//        }
    }
};


gamescreen* GUI_CreateMenuScreen() {
    return new screen_menu;
}