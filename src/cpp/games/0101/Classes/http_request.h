//
//  http_request.h
//  bouncy
//
//  Created by emiel Dam on 12/10/15.
//
//

#ifndef http_request_h
#define http_request_h

#include <happyhttp/happyhttp.h>
#include <atom/core/event_handler.h>
#include <iostream>
#include <string.h>

#define HIGHSCORE_IP        "52.29.179.230"
#define HIGHSCORE_PORT      8181

class http_request;

struct HttpEventArgs {
    http_request* req;
    int status;
    float progress;
};

class http_request {
    static void OnBegin( const happyhttp::Response* r, void* userdata )
    {
        http_request* req = (http_request*)userdata;
        printf( "BEGIN (%d %s), %s, Content-Length:%s\n", r->getstatus(), r->getreason(), r->getheader("Authorization"), r->getheader("Content-Length"));
        
        
        
        std::string len = r->getheader("Content-Length");
        
        int content_length = std::atoi(len.c_str());
        
        req->reset(content_length);
        req->OnRequestIsStarted( { req, r->getstatus() } );
    }
    
    // invoked to process response body data (may be called multiple times)
    static void OnData( const happyhttp::Response* r, void* userdata, const unsigned char* data, int n )
    {
        http_request* req = (http_request*)userdata;
        
        float val = (float)req->received() / req->size();
        
        printf( "DATA (%d %s) [%d%%]\n", r->getstatus(), r->getreason(), (int)(val * 100.f + 0.5f));
        req->inc(data, n);
        req->OnRequestIsProcessing( { req, r->getstatus(), val } );
    }
    
    // invoked when response is complete
    static void OnComplete( const happyhttp::Response* r, void* userdata )
    {
        printf( "COMPLETE (%d %s)\n", r->getstatus(), r->getreason() );
        http_request* req = (http_request*)userdata;
//        printf( "COMPLETE (%d bytes)\n", count );
        req->OnRequestIsDone( { req, r->getstatus() } );
    }
    
//    static const int datasize = 1024 * 1024 * 8 * sizeof(uint8_t);

    int _nbytesReceived;
    void* _bytes;
    
    std::string _addr;
    int _port;
    std::string _get;
    int _contentLength;
public:
    http_request(const std::string& addr, int port, const std::string& get)
    : _nbytesReceived(0)
    , _bytes(nullptr)
    , _addr(addr)
    , _port(port)
    , _get(get)
    , _contentLength(0)
    {

    }
    
    virtual ~http_request() {
        free(_bytes);
    }
    
    void fetch() {

        
            std::cout << "running GET on " << _addr.c_str() << ":" << _port << _get << std::endl;
            happyhttp::Connection conn(_addr.c_str(), _port);
            conn.setcallbacks(OnBegin, OnData, OnComplete, this);
            conn.request("GET", _get.c_str());
            while(conn.outstanding()) {
                conn.pump();
            }
    }
    
    void reset(int len) {
        _contentLength = len;
        if(_bytes) {
            free(_bytes);
            _bytes = NULL;
        }
        
        _bytes = malloc(_contentLength);
        
        memset(_bytes, 0, _contentLength);
        _nbytesReceived = 0;
    }
    
    void inc(const uint8_t* data, int n) {
        memcpy((uint8_t*)_bytes + _nbytesReceived, data, n);
        _nbytesReceived += n;
    }
    
    const uint8_t* bytes() const {
        return (uint8_t*)_bytes;
    }
    
    const int received() const {
        return _nbytesReceived;
    }
    
    const int size() const {
        return _contentLength;
    }
    
public:
    atom::event_handler<void(const HttpEventArgs&)> OnRequestIsStarted;
    atom::event_handler<void(const HttpEventArgs&)> OnRequestIsProcessing;
    atom::event_handler<void(const HttpEventArgs&)> OnRequestIsDone;
};


#endif /* http_request_h */
