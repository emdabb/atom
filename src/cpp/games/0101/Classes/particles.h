//
//  particles.h
//  application_name
//
//  Created by emiel Dam on 9/30/15.
//
//

#ifndef particles_h
#define particles_h

#include "vec2.h"
#include <functional>
#include <SDL.h>
#include <vector>
#include <nanovg.h>
#include <atom/engine/easing.h>

extern "C" float randf();

template <typename T>
struct particles  {
    struct particle {
        vec2f pos;
        SDL_Color tint;
        float currentScale;
        float scale;
        float duration;
        float percentLife;
        float angle;
        T     state;
    };
    
    typedef std::function<void(particles<T>*, particle&, int, int)> particle_update_fun;
    
    std::vector<particle> _particles;
    size_t _capacity;
    size_t _num;
    size_t _start;
    particle_update_fun _fun;
    
    
    particles(size_t cap, particle_update_fun fun)
    : _capacity(cap)
    , _num(0)
    , _start(0)
    , _fun(fun)
    {
        _particles.resize(_capacity);
        //        for(size_t i=0; i < _particles.size(); i++) {
        //            _particles[i] = new particle;
        //        }
        //memset(&_particles[0], 0, sizeof(particle) * _capacity);
    }
    
    ~particles()
    {
    }
    
    void update(int t, int dt)
    {
        size_t numToRemove = 0;
        for(size_t ii=0; ii < _num; ii++)
        {
            size_t i = index_of(ii);
            particle& p = _particles[i];
            if(_fun != nullptr)
                _fun(this, p, t, dt);
            
            size_t a = index_of(ii - numToRemove);
            size_t b = index_of(ii);
            
            std::swap(_particles[a], _particles[b]);
            
            if(_particles[i].percentLife <= 0.0f)
                numToRemove++;
        }
        _num -= numToRemove;
    }
    
    inline size_t index_of(size_t i)
    {
        return (_start + i) % _capacity;
    }
    
    void emit(const vec2f& at, float duration, const SDL_Color& c) {
        emit(at, duration, c, T::defaultValue);
    }
    
    void emit(const vec2f& at, float duration, const SDL_Color& c, const T& state, float scale = 1.f)
    {
        particle* p = nullptr;
        
        //size_t i;
        
        if(_num == _capacity)
        {
            p = &_particles[index_of(0)];
            _start = (_start + 1) % _capacity;
        }
        else
        {
            p = &_particles[index_of(_num)];
            _num++;
        }
        
        p->pos   = at;
        p->tint  = c;
        p->scale = scale;
        p->currentScale = scale;
        p->duration = duration;
        p->percentLife = 1.0f;
        p->angle = 0.0f;
        p->state = state;
    }
    
    void draw(SDL_Renderer* ren, SDL_Texture* tex, const SDL_Rect& cameraRect)
    {
        int tw, th;
        SDL_QueryTexture(tex, nullptr, nullptr, &tw, &th);
        
        for(size_t i=0; i < _num; i++)
        {
            particle* p = &_particles[index_of(i)];
            
            SDL_Rect dst;
            
            dst.x = (int)(p->pos.x - (tw / 2) * p->currentScale) - cameraRect.x + cameraRect.w / 2;
            dst.y = (int)(p->pos.y - (th / 2) * p->currentScale) - cameraRect.y + cameraRect.h / 2;
            dst.w = (int)(tw * p->currentScale);
            dst.h = (int)(th * p->currentScale);
            
            SDL_SetTextureColorMod(tex, p->tint.r, p->tint.g, p->tint.b);
            SDL_SetTextureAlphaMod(tex, p->tint.a);
            SDL_SetRenderDrawColor(ren, p->tint.r, p->tint.g, p->tint.b, p->tint.a);
            SDL_RenderCopyEx(ren, tex, NULL, &dst, p->angle * 180.f / M_PI, NULL, SDL_FLIP_NONE);
            
            
        }
        
    }
};


#endif /* particles_h */
