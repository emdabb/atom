//
//  InterstitialView.h
//  bouncy
//
//  Created by emiel Dam on 12/3/15.
//
//

#ifndef InterstitialView_h
#define InterstitialView_h

#include <Foundation/Foundation.h>
#include <GoogleMobileAds/GoogleMobileAds.h>

@interface AdmobInterstitialViewController : UIViewController <GADInterstitialDelegate>

@property (nonatomic, strong) GADInterstitial* interstitial;

/// Called when an interstitial ad request succeeded.
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad;

/// Called when an interstitial ad request failed.
- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error;

/// Called just before presenting an interstitial.
- (void)interstitialWillPresentScreen:(GADInterstitial *)ad;

/// Called before the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad;

/// Called just after dismissing an interstitial and it has animated off the screen.
- (void)interstitialDidDismissScreen:(GADInterstitial *)ad;

/// Called just before the app will background or terminate because the user clicked on an
/// ad that will launch another app (such as the App Store).
- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad;

- (void) restoreController;

@end

#endif /* InterstitialView_h */
