set(CMAKE_CXX_FLAGS
    "-std=c++11"
)
link_directories(
    ${ATOM_ROOTDIR}/proj.linux/lib/Debug
)

file(GLOB APP_sources_platform
    proj.x11/*.cpp
    proj.x11/*.c
)

file(GLOB APP_headers_platform
	proj.x11/*.h
)

set(LIBRARY_DEPS
    ${LIBRARY_DEPS}
    dl
    pthread
    X11
    png
)

add_executable(
    ${NAME}
    ${APP_sources}
    ${APP_sources_platform}
)

target_link_libraries(
    ${NAME}
    ${LIBRARY_DEPS}
)
