#ifndef _IMGUI_H_
#define _IMGUI_H_

#define GEN_ID()    (__COUNTER__ + 1024)

#include <string>

struct SDL_Rect;
struct SDL_Point;
struct SDL_Texture;
struct SDL_Renderer;
struct NVGcontext;

namespace atom {
    
    class sprite_font;
    
    int  imgui_loadFont(SDL_Renderer*, const char* path, const char* name);
    bool imgui_button(int id, SDL_Renderer* ren, const SDL_Rect& dst, int fontId, const char* text = "button");
    bool imgui_textinput(int id, SDL_Renderer* ren, int fontId, const SDL_Rect& dst, char* str);
    void imgui_setAlpha(float alpha);
    void imgui_setTextColor(uint8_t, uint8_t, uint8_t);
    void imgui_reset();
    void imgui_label(int id, SDL_Renderer* ren, int fontId, const SDL_Point& at, const char*, float textSize);
    
    bool imgui_button_facebookLike(int id);


}

#endif


