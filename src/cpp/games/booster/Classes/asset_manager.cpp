//
//  assets.cpp
//  application_name
//
//  Created by emiel Dam on 9/29/15.
//
//

#include "asset_manager.h"
#include <map>
#include <string>
#include <iostream>
#include <cjson/cJSON.h>

namespace atom {
    
    static std::map<std::string, void*> _assets;
    static std::map<std::string, int>  _surfaceNames;
    static std::map<std::string, int>  _textureNames;
    static std::map<std::string, int>  _fontNames;
    static std::map<std::string, int>  _musicNames;
    static std::map<std::string, int>  _soundNames;
    
    static std::vector<SDL_Texture*> _textures;
    static std::vector<SDL_Surface*> _surfaces;
    static std::vector<sprite_font*> _fonts;
    static std::vector<Mix_Music*> _music;
    static std::vector<Mix_Chunk*> _sound;
    
    //std::map<std::string, int>           _nvgTextures;
    
    std::string getResourcePath(const std::string &subDir){
        //We need to choose the path separator properly based on which
        //platform we're running on, since Windows uses a different
        //separator than most systems
#ifdef _WIN32
        const char PATH_SEP = '\\';
#else
        const char PATH_SEP = '/';
#endif
        //This will hold the base resource path: Lessons/res/
        //We give it static lifetime so that we'll only need to call
        //SDL_GetBasePath once to get the executable path
        static std::string baseRes;
        if (baseRes.empty()){
            //SDL_GetBasePath will return NULL if something went wrong in retrieving the path
            char *basePath = SDL_GetBasePath();
            if (basePath){
                baseRes = basePath;
                SDL_free(basePath);
            }
            else {
                std::cerr << "Error getting resource path: " << SDL_GetError() << std::endl;
                return "";
            }
            //We replace the last bin/ with res/ to get the the resource path
            //size_t pos = baseRes.rfind("bin");
            //baseRes = baseRes.substr(0, pos) + "res" + PATH_SEP;
        }
        //If we want a specific subdirectory path in the resource directory
        //append it to the base path. This would be something like Lessons/res/Lesson0
        return subDir.empty() ? baseRes : baseRes + subDir + PATH_SEP;
    }
    
    int addMusic(const std::string& name, Mix_Music* mus) {
        _musicNames[name] = (int)_music.size();
        _music.push_back(mus);
        return (int)(_music.size() - 1);
    }
    int addSound(const std::string& name, Mix_Chunk* snd) {
        _soundNames[name] = (int)_sound.size();
        _sound.push_back(snd);
        return (int)(_sound.size() - 1);
    }

    int addTexture(const std::string& name, SDL_Texture* tex)
    {
        _textureNames[name] = (int)_textures.size();
        _textures.push_back(tex);
        return (int)(_textures.size() - 1);
    }
    
     int addSurface(const std::string& name, SDL_Surface* surf) {
        _surfaceNames.insert(std::pair<std::string, int>(name, (int)_surfaces.size()));
        _surfaces.push_back(surf);
        return (int)(_surfaces.size() - 1);
    }
    
    int loadSurface(const std::string& name, std::istream& is) {
        
//        std::string fname = getResourcePath() + filename;
        
        if(_surfaceNames.count(name)) {
            return _surfaceNames[name];
        }
        
        std::istreambuf_iterator<char> eos;
        std::string contents(std::istreambuf_iterator<char>(is),
                             (std::istreambuf_iterator<char>()));
         

        SDL_RWops* ops = SDL_RWFromConstMem(contents.c_str(), (int)contents.length());
        
        SDL_Surface* surf = IMG_Load_RW(ops, 0);//(fname.c_str());
        if(!surf) {
            printf("IMG_Load: %s\n", IMG_GetError());
            // handle error
        }
        
        return addSurface(name, surf);
        
    }
    
    int loadMusic(const std::string& name, std::istream& is) {
        std::istreambuf_iterator<char> eos;
        std::string contents(std::istreambuf_iterator<char>(is),
                             (std::istreambuf_iterator<char>()));
        SDL_RWops* ops = SDL_RWFromConstMem(contents.c_str(), (int)contents.length());
        Mix_Music* mus = Mix_LoadMUS_RW(ops, 0);
        
        if(!mus) {
            std::cout << "Music loading error: " << Mix_GetError() << std::endl;
            return -1;
        }
        
        return addMusic(name, mus);
    }
    int loadSound(const std::string& name, std::istream& is) {
        std::istreambuf_iterator<char> eos;
        std::string contents(std::istreambuf_iterator<char>(is),
                             (std::istreambuf_iterator<char>()));
        SDL_RWops* ops = SDL_RWFromConstMem(contents.c_str(), (int)contents.length());
        Mix_Chunk* snd = Mix_LoadWAV_RW(ops, 0);
        if(!snd) {
            std::cout << "Sound loading error: " << Mix_GetError() << std::endl;
            return -1;
        }
        return addSound(name, snd);
    }
    
    int addFont(const std::string& name, sprite_font* font) {
        _fontNames[name] = (int)_fonts.size();
        _fonts.push_back(font);
        return(int)(_fonts.size() - 1);
    }
    
    int loadFont(const std::string& name, std::istream& is, int size, SDL_Renderer* ren) {
        sprite_font* font = new sprite_font;
        font->build(is, size, ren);
        return addFont(name, font);
    }
    
    int getFontId(const std::string& name) {
        return _fontNames[name];
    }
    
    sprite_font* getFontById(int id) {
        return _fonts[id];
    }
    
    sprite_font* getFontByName(const std::string& name) {
        return getFontById(getFontId(name));
    }
    
    int getSurfaceId(const std::string& name) {
        return _surfaceNames[name];
    }
    
    SDL_Surface* getSurfaceById(int id) {
        return _surfaces[id];
    }
    
    SDL_Surface* getSurfaceByName(const std::string& name) {
        return getSurfaceById(getSurfaceId(name));
    }
    
    int getTextureId(const std::string& name) {
        return _textureNames[name];
    }
    
    SDL_Texture* getTextureById(int id) {
        return _textures[id];
    }
    
    SDL_Texture* getTextureByName(const std::string& name) {
        return getTextureById(getTextureId(name));
    }
    
    int getMusicId(const std::string& name) {
        return _musicNames[name];
    }
    Mix_Music* getMusicById(int id) {
        return _music[id];
    }
    
    Mix_Music* getMusicByName(const std::string& name) {
        return getMusicById(getMusicId(name));
    }
    
    int getSoundId(const std::string& name) {
        return _soundNames[name];
    }
    Mix_Chunk* getSoundById(int id) {
        return _sound[id];
    }
    Mix_Chunk* getSoundByName(const std::string& name) {
        return getSoundById(getSoundId(name));
    }
    
    void parseJsonNode(cJSON* node, std::map<std::string, std::string>* list) {
        
        if(node->valuestring) {
            list->insert(std::pair<std::string, std::string>(node->string, node->valuestring));
        }
        
        cJSON* ptr = node->next;
        while(ptr) {
            parseJsonNode(ptr, list);
            ptr = ptr->next;
        }
        
        ptr = node->child;
        while(ptr) {
            parseJsonNode(ptr, list);
            ptr = ptr->next;
        }
    }
    
    int loadJson(std::istream& src, std::map<std::string, std::string>* dst) {
        std::istreambuf_iterator<char> eos;
        std::string contents(std::istreambuf_iterator<char>(src),
                             (std::istreambuf_iterator<char>()));
        
        cJSON* root = cJSON_Parse(contents.c_str());
        if(root) {
            parseJsonNode(root, dst);
            return 0;
        }
        return -1;
    }
    
    void loadMusicLibrary(std::istream& is) {
        std::map<std::string, std::string> names;
        if(!loadJson(is, &names)) {
            
            std::map<std::string, std::string>::iterator it = names.begin();
            
            for(; it != names.end(); it++) {
                //SDL_Surface* surf = IMG_Load((*it).second.c_str());
                //Mix_Chunk* snd = Mix_LoadWAV((*it).second.c_str());
                Mix_Music* mus = Mix_LoadMUS((*it).second.c_str());
                
                if(mus != NULL) {
                    std::cout << "Adding sound \"" << (*it).first << "\" from file: \"" << (*it).second << "\"" << std::endl;
                    //addTexture((*it).first, SDL_CreateTextureFromSurface(ren, surf));
                    // SDL_FreeSurface(surf);
                    addMusic((*it).first, mus);
                    
                } else {
                    std::cout << "Error loading \"" << (*it).second << "\" : " << Mix_GetError() << std::endl;
                }
            }
        }

    }
    
    void loadAudioLibrary(std::istream& is) {
        
        std::map<std::string, std::string> names;
        if(!loadJson(is, &names)) {

            std::map<std::string, std::string>::iterator it = names.begin();
            
            for(; it != names.end(); it++) {
                //SDL_Surface* surf = IMG_Load((*it).second.c_str());
                Mix_Chunk* snd = Mix_LoadWAV((*it).second.c_str());
                
                if(snd != NULL) {
                    std::cout << "Adding sound \"" << (*it).first << "\" from file: \"" << (*it).second << "\"" << std::endl;
                    //addTexture((*it).first, SDL_CreateTextureFromSurface(ren, surf));
                   // SDL_FreeSurface(surf);
                    addSound((*it).first, snd);
                    
                } else {
                    std::cout << "Error loading \"" << (*it).second << "\" : " << Mix_GetError() << std::endl;
                }
            }
        }
    }

    void loadTextureLibrary(SDL_Renderer* ren, std::istream& is) {
        std::map<std::string, std::string> names;
        if(!loadJson(is, &names)) {
            std::map<std::string, std::string>::iterator it = names.begin();

            for(; it != names.end(); it++) {
                SDL_Surface* surf = IMG_Load((*it).second.c_str());
                if(surf != NULL) {
                    std::cout << "Adding texture \"" << (*it).first << "\" from file: \"" << (*it).second << "\"" << std::endl;
                    addTexture((*it).first, SDL_CreateTextureFromSurface(ren, surf));
                    SDL_FreeSurface(surf);
                } else {
                    std::cout << "Error loading \"" << (*it).second << "\"" << std::endl;
                }
            }
        }
    }
}
