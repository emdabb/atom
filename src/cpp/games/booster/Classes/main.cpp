//
//  main.cpp
//  booster
//
//  Created by emiel Dam on 12/30/15.
//
//

#include <atom/engine/application.h>
#include <atom/engine/coro.h>
#include <stdio.h>

using namespace atom;

extern "C" applet* new_game(application*);


class WaitForSeconds : public coroutine {
public:
    int operator () (int count) {
        CORO_REENTER(this) {

            do {
                CORO_YIELD return count - 1;
            } while(count > 0);
        }
    }
};


int main(int argc, char* argv[]) { 
#if 1
    atom::application app;
    app.setApplet(new_game(&app));
    //app.resize(1024, 1024);
    app.create();
    return app.start();//app.start();


#else

    WaitForSeconds wait;

    while(1) {
        printf("%d\n", wait(10));
    }

    return 0;

#endif





}
