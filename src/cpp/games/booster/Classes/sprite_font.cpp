//
//  sprite_font.cpp
//  application_name
//
//  Created by emiel Dam on 10/8/15.
//
//

#include <stdio.h>
#include "sprite_font.h"

namespace atom {

    struct node {
        node() : left(NULL), right(NULL), img(-1) {}
        node*    left;
        node*    right;
        SDL_Rect rc;
        int      img;
    };

    static node* node_insert(node* at, const SDL_Rect& rc) {
        if(at->left || at->right) {
            node* n = node_insert(at->left, rc);
            return n ? n : node_insert(at->right, rc);
        } else {
            if(at->img != -1)
                return NULL;

            if(SDL_RectGetArea(&at->rc) < SDL_RectGetArea(&rc))
                return NULL;

            if(SDL_RectGetArea(&at->rc) == SDL_RectGetArea(&rc))
                return at;


            at->left  = new node;
            at->right = new node;

            const SDL_Rect& _rc = at->rc;

            int w = rc.w;
            int h = rc.h;
            int dw= _rc.w - w;
            int dh= _rc.h - h;



            if(dw > dh) {
                at->left->rc    = {
                    _rc.x,
                    _rc.y,
                    w,
                    _rc.h
                };
                at->right->rc   = {
                    _rc.x + w,
                    _rc.y,
                    _rc.w - w,
                    _rc.h};
            } else {
                at->left->rc    = {
                    _rc.x,
                    _rc.y,
                    _rc.w,
                    h
                };

                at->right->rc   = {
                    _rc.x,
                    _rc.y + h,
                    _rc.w,
                    _rc.h - h
                };
            }
            return node_insert(at->left, rc);
        }
        return NULL;
    }


    void sprite_font::build(std::istream &is, int size, SDL_Renderer *ren)
    {
            if(TTF_Init() != 0) {
                // error
                std::cout << "TTF_Init() error" << std::endl;
            }

            std::istreambuf_iterator<char> eos;
            string_t rw(std::istreambuf_iterator<char>(is), {});

            int sz = (int)rw.length();

            SDL_RWops* ops = SDL_RWFromMem((void*)rw.c_str(), sz);

            if((_font = TTF_OpenFontRW(ops, 0, size)) == NULL) {
                // error
                std::cout << "TTF_OpenFontRW() error" << std::endl;
            }

            //SDL_Surface* glyph[NUM_GLYPHS];

            int maxw = 0;
            int maxh = 0;


            for(int i=0; i < NUM_GLYPHS; i++) {

                uint16_t c = i + 32;
                if(!TTF_GlyphIsProvided(_font, c))
                    continue;



                TTF_GlyphMetrics(_font,
                                 c,
                                 &_glyph[i].metric.minx,
                                 &_glyph[i].metric.maxx,
                                 &_glyph[i].metric.miny,
                                 &_glyph[i].metric.maxy,
                                 &_glyph[i].metric.advance);

                int ww, hh;

                TTF_GlyphSize(_font, c, &ww, &hh);

                maxw = std::max(maxw, ww);
                maxh = std::max(maxh, hh);
            }

            int textureDimension = texture_size(NUM_GLYPHS, (float)maxw, (float)maxh);
            //            textureDimension >>= 1;

            SDL_Surface* surf = SDL_CreateRGBSurface(0, textureDimension, textureDimension, 32, SDL_RMASK, SDL_GMASK, SDL_BMASK, SDL_AMASK);

            node* root = new node;
            root->img   = -1;
            root->left  = root->right = NULL;
            root->rc    = { 0, 0, textureDimension, textureDimension };

            for(int i=0; i < NUM_GLYPHS; i++)
            {
                uint16_t c = i + 32;

                if(!TTF_GlyphIsProvided(_font, c)) {
                    continue;
                }

                int ww, hh;

                TTF_GlyphSize(_font, c, &ww, &hh);

                if(ww == 0 && hh == 0)
                    continue;

                SDL_Surface* temp = TTF_RenderGlyph_Blended(_font, c, {255, 255, 255});

                node* n = node_insert(root, {0, 0, temp->w, temp->h});
                n->img = c;

                _sheet->addRect(c, n->rc);

                SDL_BlitSurface(temp, NULL, surf, &n->rc);

                SDL_FreeSurface(temp);
            }

            SDL_Texture* tex = SDL_CreateTextureFromSurface(ren, surf);

            SDL_FreeSurface(surf);

            _sheet->setTexture(tex);

        }

    void sprite_font::drawString(SDL_Renderer* ren, const SDL_Point& p, const string_t& str, const SDL_Color* color)
    {
        SDL_Texture* tex  = _sheet->getTexture();

        SDL_SetTextureColorMod(tex, color->r, color->g, color->b);

        int x = 0;
        for(size_t i=0; i < str.length(); i++)
        {
            const SDL_Rect* rc = _sheet->getRect(str[i]);
            if(rc)
            {
                SDL_Rect dst = { p.x + x, p.y, rc->w, rc->h };

                SDL_RenderCopy(ren, tex, rc, &dst);

                x += rc->w;
            }
        }
    }

    int sprite_font::measureString(const string_t& str, int* w, int* h) {
        return TTF_SizeUNICODE(_font, (Uint16*)str.c_str(), w, h);
    }
}
