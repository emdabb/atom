#include "imgui.h"
#include "input.h"
#include "sprite_font.h"
#include "asset_manager.h"
#include <SDL.h>
#include <cstring>

namespace atom 
{
    static struct guistate {
        int hot;
        int active;
        SDL_Color baseColor;
        SDL_Color hotColor;
        SDL_Color activeColor;
        SDL_Color textColor;
        uint16_t keyChar;
        int kbditem;
    } uistate;

    int  imgui_loadFont(SDL_Renderer* vg, const char* path, const char* name) {
//        int res = nvgCreateFont(vg, name, path);
        //return res;
        return 0;
    }
    bool SDL_IntersectRectAndPoint(const SDL_Rect& rc, const SDL_Point* p)
    {
        
        return ((p->x >= rc.x) &&
                (p->x <= (rc.x + rc.w)) &&
                (p->y >= rc.y) &&
                (p->y <= (rc.y + rc.h)));
    }
    
    void imgui_setAlpha(float alpha) {
        uistate.hotColor.a      = (uint8_t)(alpha * 255.f);
        uistate.textColor.a     = (uint8_t)(alpha * 255.f);
        uistate.activeColor.a   = (uint8_t)(alpha * 255.f);
        uistate.baseColor.a     = (uint8_t)(alpha * 255.f);
    }
    
    void imgui_setTextColor(uint8_t r, uint8_t g, uint8_t b) {
        uistate.textColor = { r, g, b, 255} ;
    }

    void imgui_reset() 
    {
        uistate.hot = uistate.active = 0;
        uistate.baseColor = {181,137,  0, 255};
        uistate.hotColor  = {220, 50, 47, 255};
        uistate.activeColor= {211, 54,130, 255};
        uistate.textColor  ={0,0,0, 255};
        uistate.keyChar = input_GetLastKeyPressed();
        uistate.kbditem = -1;
    }
    
    void imgui_label(int id, SDL_Renderer* ren, int fontId, const SDL_Point& at, const char* text, float size) {
        //sprite_font* font = asset_manager
//        nvgBeginPath(ren);
//        nvgFontSize(ren, size);
//        nvgFillColor(ren, uistate.textColor);
//        nvgFontFaceId(ren, fontId);
//        nvgTextAlign(ren, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
//        nvgText(ren, at.x, at.y, text, NULL);
        sprite_font* font = atom::getFontById(fontId);
        font->drawString(ren, {at.x, at.y}, text, &uistate.baseColor);
        
    }

    bool imgui_button(int id, SDL_Renderer* ren, const SDL_Rect& dst, int fontId, const char* text)
    {
        const SDL_Point& mousep = SDL_GetMousePosition();
        if(SDL_IntersectRectAndPoint(dst, &mousep)) {
            uistate.hot = id;
            if(uistate.active == 0 && SDL_IsMouseDown()) 
            {
                uistate.active = id;
                input_resetMouseButton();
            }
        }
        
        SDL_Color col;

        if(uistate.hot == id)
            if(uistate.active == id)
                col  = uistate.activeColor;//SDL_SetTextureColor(tex, &uistate.activeColor);
            else
                col  = uistate.hotColor;//SDL_SetTextureColor(tex, &uistate.hotColor);
        else
             col  = uistate.baseColor;//SDL_SetTextureColor(tex, &uistate.baseColor);
        
        
        sprite_font* font = atom::getFontById(fontId);
        
        SDL_Rect rc = dst;
        SDL_Point textSize;
        font->measureString(text, &textSize.x, &textSize.y);
        
        
        SDL_Point center = {
            rc.w / 2 - textSize.x / 2,
            rc.h / 2 - textSize.y / 2
        };
        
        
        SDL_SetRenderDrawBlendMode(ren, SDL_BLENDMODE_BLEND);
        SDL_SetRenderDrawColor(ren, col.r, col.g, col.b, col.a);
        SDL_RenderDrawRect(ren, &rc);

        font->drawString(ren, {dst.x + center.x, dst.y + center.y}, text, &col);
        
        return uistate.hot == id && uistate.active == id;
    }

    bool imgui_textinput(int id, SDL_Renderer* ren, int fontId, const SDL_Rect& dst, char* str)
    {
        const SDL_Point& p = SDL_GetMousePosition();
        if(SDL_IntersectRectAndPoint(dst, &p)) {
            uistate.hot = id;
            if(uistate.active == 0 && SDL_IsMouseDown())
            {
                uistate.active = id;
                SDL_StartTextInput();
            }
        } else {
            SDL_StopTextInput();
            uistate.keyChar = 0;
        }
        
        if(uistate.kbditem == -1) uistate.kbditem = id;
        
        size_t len = strlen(str);
        
        if(uistate.kbditem == id) {
            if(uistate.keyChar == 8)
            {
                str[--len] = 0;
            }
            
            if(uistate.keyChar == SDL_SCANCODE_SPACE) {
                
            }
            
            if(uistate.keyChar >= 32 && uistate.keyChar < 127) {
                str[len++] = uistate.keyChar;
                str[len] = 0;
            }
            
            uistate.keyChar = 0;
            input_resetKey();
        }
        
        SDL_Color col;
        
        if(uistate.hot == id)
            if(uistate.active == id)
                col = uistate.activeColor;//SDL_SetRenderDrawColor(ren, uistate.activeColor.r, uistate.activeColor.g, uistate.activeColor.b, 255 );
            else
                col = uistate.hotColor;//SDL_SetRenderDrawColor(ren, uistate.hotColor.r, uistate.hotColor.g, uistate.hotColor.b, 255 );
        else
            col = uistate.baseColor;//SDL_SetRenderDrawColor(ren, uistate.baseColor.r, uistate.baseColor.g, uistate.baseColor.b, 255);

        sprite_font* font = atom::getFontById(fontId);
        
        int tw, th;
        font->measureString(str, &tw, &th);
        
        SDL_Rect box = {
            dst.x,
            dst.y,
            std::max(dst.w, tw),
            std::min(dst.h, th)
        };
        
        SDL_SetRenderDrawBlendMode(ren, SDL_BLENDMODE_BLEND);
        SDL_SetRenderDrawColor(ren, uistate.baseColor.r, uistate.baseColor.g, uistate.baseColor.b, uistate.baseColor.a);
        SDL_RenderDrawRect(ren, &box);
        font->drawString(ren,
        {
            box.x + (box.w / 2 - tw / 2),
            box.y + (box.h / 2 - th / 2)
        }, str, &uistate.hotColor);
     
        
        return uistate.active == id && uistate.hot == id && uistate.kbditem == id;
    }
}
