//
//  game.cpp
//  booster
//
//  Created by emiel Dam on 12/30/15.
//
//

#include <stdio.h>
#include <atom/core/math/math.h>
#include <atom/engine/application.h>
#include "gamescreen.h"
#include "world.h"

using namespace atom;

void matmul(const float* a, const float* b, size_t ra, size_t ca, size_t cb, float* res) {
    for(int r=0; r < ra; r++)
    {
        for(int c=0; c < cb; c++)
        {
            res[r * ra + c] = 0.0f;
            for(int k=0; k < ca; k++)
            {
                res[r * ra + c] += a[r * ca + k] * b[k * cb + c];
            }
        }
    }
}

class game : public applet {
    application* mApplication;
    gamescreen_manager mGamescreenManager;
    World mWorld;

public:
    game(application* app)
    : mApplication(app)
    {
//        test_fbool();
    }

    virtual ~game() {
    }

    void mat_reduce(const float* src, float* dst, size_t ra, size_t ca) {
        for(int y=0; y < ra; y++) {
            for(int x=0; x < ca; x++) {

            }
        }
    }

    enum {
        STATE_IDLE,
        STATE_SELLER_OFFERING,
        STATE_BUYER_COUNTER,
        STATE_SELLER_COUNTER,
        STATE_SELLER_ACCEPTED,
        STATE_SELLER_DECLINED
    };

    int buyerAccepts(int initialPrice, int currentPrice, int bid) {

        float ratio_bid_initial = (float)bid / initialPrice;
        float ratio_bid_current = (float)bid / currentPrice;
        float ratio_price = (float)currentPrice / initialPrice;

        std::cout << "bid ratio[init]:  " << ratio_bid_initial << std::endl;
        std::cout << "bid ratio[cur] :  " << ratio_bid_current << std::endl;
        std::cout << "price ratio    :  " << ratio_price << std::endl;

        return STATE_SELLER_COUNTER;
    }

    int calculateInitialPrice(int minPrice, int maxPrice) {
        return minPrice + (rand() % (maxPrice - minPrice));
    }

    virtual int create() {
        SDL_Renderer* ren = mApplication->getRenderer();
        int rw, rh;
        SDL_GetRendererOutputSize(ren, &rw, &rh);

        int state = STATE_IDLE;
        const int maxPrice  = 1000;
        const int minPrice  = 100;

        int initialPrice;
        int currentPrice;
        int bid;

        while(1) {
            switch(state) {
                case STATE_IDLE:
                    state = STATE_SELLER_OFFERING;
                    initialPrice = currentPrice = calculateInitialPrice(minPrice, maxPrice);
                    break;
                case STATE_SELLER_OFFERING:
                    std::cout << "Good tidings stranger! I'm offering this McGuffin for a mere " << initialPrice << " gold coins; whadayasay?" << std::endl;
                    std::cin >> bid;
                    state = buyerAccepts(initialPrice, currentPrice, bid);
                    break;
                case STATE_SELLER_COUNTER:
                    //currentPrice /= 2;
                    std::cout << "Well.. that's an interesting offer.. However, " << bid << " coins is a bit low for me. How about " << currentPrice << " coins?" << std::endl;
                    std::cin >> bid;
                    state = buyerAccepts(initialPrice, currentPrice, bid);
                    break;
                case STATE_SELLER_ACCEPTED:
                    std::cout << "Aye, you drive a hard bargain; but fair's fair. Here's the McGuffin." << std::endl;
                    state = STATE_IDLE;
                    break;
                case STATE_SELLER_DECLINED:
                    std::cout << "GET OUTTA HERE YA FILTHY PEASANT! BEFORE I CALL THE COPS ON YA, OR I SHOOT YOU MESELF!!" << std::endl;
                    state = STATE_IDLE;
                    break;
                default:
                    break;
            }
        }


        srand(SDL_GetTicks());

        float best = 0.0f;

        for(int i=0; i < 1024; i++) {
            float p = 1.0f;//(float)rand() / RAND_MAX;
            float q = (float)rand() / RAND_MAX;

            float R[] = { p, 1.0f - p };

            float M[] = {
                7.0f, 3.0f,
                2.0f, 5.0f
            };

            float C[] = {  q, 1.0f - q };

            float e = 0.0f;
            float temp[3] = { 0.f };

            matmul(R, M, 1, 2, 2, temp);
            matmul(temp, C, 1, 2, 1, &e);

            float s[2] = { 0.0f, 0.0f };
            matmul(R, M, 1, 2, 2, s);

            std::cout << "--- Game " << i << " ---" << std::endl;
            std::cout << "R = [" << R[0] << ", " << R[1] << "]" << std::endl;
            std::cout << "C = [" << C[0] << ", " << C[1] << "]" << std::endl;
            std::cout << "R vs C expected payoff: " << e << std::endl;
            best = std::max(e, best);
        }

        std::cout << "--- Result ---" << std::endl;
        std::cout << "best strategy yields: " << best << std::endl;

        return 0;
    }

    virtual int destroy() {
        return -1;
    }

    virtual void update(int, int dt) {
        static int t = 0;
        t += dt;

    }
    virtual void draw() {
        SDL_Renderer* ren = mApplication->getRenderer();

        SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
        SDL_RenderClear(ren);

        SDL_RenderPresent(ren);
    }
};

extern "C" applet* new_game(application* app) {
    return new game(app);
}
