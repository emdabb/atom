//
//  world.cpp
//  coil
//
//  Created by emiel Dam on 11/6/15.
//
//

#include "world.h"


void World_clear(World* world)
{
    for(int i=0; i < MAX_ENTITIES; i++)
    {
        World_destroyEntity(world, i);
    }
}

intptr_t World_createEntity(World* world) {
    for(int i=0; i < MAX_ENTITIES; i++) {
        if(world->mask[i] == E_COMPONENT_NIL) {
            world->cleanup[i] = false;
            return i;
        }
    }
    return MAX_ENTITIES;
}

void World_destroyEntity(World* world, intptr_t id) {
    
    world->collider[id].OnCollisionEnter.clear();
    world->collider[id].OnCollisionLeave.clear();

    world->mask[id] = E_COMPONENT_NIL;
    world->cleanup[id] = true;
}

void World_cleanup(World* world) {
    for(int i=0; i < MAX_ENTITIES; i++) {
        if(world->cleanup[i] != false) {
            if(World_hasComponent(world, i, E_COMPONENT_PHYSICS)) {
                if(World_hasComponent(world, i, E_COMPONENT_COLLIDER)) {

                    b2Fixture* ptr = world->physics[i].body->GetFixtureList();
                    while(ptr) {
                        world->physics[i].body->DestroyFixture(ptr);
                        ptr = ptr->GetNext();
                    }
                }
                world->sim->DestroyBody(world->physics[i].body);
                world->physics[i].body  = NULL;
                world->cleanup[i]       = false;
            }
            
//            if(world->particles[i] != NULL) {
//                delete world->particles[i];
//                world->particles[i] = NULL;
//            }
        }
    }
}

bool World_hasComponent(World* world, intptr_t id, uint32_t mask) {
    return (world->mask[id] & mask) == mask;
}

void World_addComponent(World* world, intptr_t id, uint32_t mask) {
    world->mask[id] |= mask;
}

void World_removeComponent(World* world, intptr_t id, uint32_t mask) {
    world->mask[id] &= ~mask;
}

/****************************************
 * CollisionHandler implementation      *
 ****************************************/
#include <iostream>
#define PRINT_METHOD()  std::cout << __PRETTY_FUNCTION__ << std::endl

CollisionHandler::CollisionHandler(World* world_)
: _world(world_)
{
    
}
CollisionHandler::~CollisionHandler(){
    
}

void CollisionHandler::BeginContact(b2Contact* contact){

    
    const b2Fixture* fA = contact->GetFixtureA();
    const b2Fixture* fB = contact->GetFixtureB();
    
    intptr_t objA = (intptr_t)fA->GetUserData();
    intptr_t objB = (intptr_t)fB->GetUserData();
    
    b2WorldManifold man;
    contact->GetWorldManifold(&man);
    
    if((_world->mask[objA] & E_COMPONENT_COLLIDER) == E_COMPONENT_COLLIDER) {
        Collider* c = &_world->collider[objA];
        c->OnCollisionEnter({_world,
            objA,
            objB, {
                man.normal.x,
                man.normal.y
            }
        });
    }
    if((_world->mask[objB] & E_COMPONENT_COLLIDER) == E_COMPONENT_COLLIDER) {
        Collider* c = &_world->collider[objB];

        c->OnCollisionEnter({ _world,
            objB,
            objA, {
                man.normal.x,
                man.normal.y
            }
        });
    }
}

void CollisionHandler::EndContact(b2Contact* contact){

}

void CollisionHandler::BeginContact(b2ParticleSystem* particleSystem, b2ParticleBodyContact* particleBodyContact){
    PRINT_METHOD();
}

void CollisionHandler::EndContact(b2Fixture* fixture, b2ParticleSystem* particleSystem, int32 index) {
    PRINT_METHOD();
}

void CollisionHandler::BeginContact(b2ParticleSystem* particleSystem, b2ParticleContact* particleContact){
    PRINT_METHOD();
}

void CollisionHandler::EndContact(b2ParticleSystem* particleSystem, int32 indexA, int32 indexB)
{
    PRINT_METHOD();
}