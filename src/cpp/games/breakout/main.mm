
#include <atom/engine/application.h>

extern "C" atom::applet* new_game_breakout(atom::application*);


struct uikit_eventhandler {

static int HandleAppEvents(void *userdata, SDL_Event *event)
{
    switch (event->type)
    {
        case SDL_APP_TERMINATING:
            std::cout << "SDL_APP_TERMINATING" << std::endl;
            /* Terminate the app.
             Shut everything down before returning from this function.
             */
            return 0;
        case SDL_APP_LOWMEMORY:
            std::cout << "SDL_APP_LOWMEMORY" << std::endl;
            /* You will get this when your app is paused and iOS wants more memory.
             Release as much memory as possible.
             */
            return 0;
        case SDL_APP_WILLENTERBACKGROUND:
            std::cout << "SDL_APP_WILLENTERBACKGROUND" << std::endl;
            /* Prepare your app to go into the background.  Stop loops, etc.
             This gets called when the user hits the home button, or gets a call.
             */
            return 0;
        case SDL_APP_DIDENTERBACKGROUND:
            std::cout << "SDL_APP_DIDENTERBACKGROUND" << std::endl;
            /* This will get called if the user accepted whatever sent your app to the background.
             If the user got a phone call and canceled it, you'll instead get an    SDL_APP_DIDENTERFOREGROUND event and restart your loops.
             When you get this, you have 5 seconds to save all your state or the app will be terminated.
             Your app is NOT active at this point.
             */
            return 0;
        case SDL_APP_WILLENTERFOREGROUND:
            std::cout << "SDL_APP_WILLENTERFOREGROUND" << std::endl;
            /* This call happens when your app is coming back to the foreground.
             Restore all your state here.
             */
            return 0;
        case SDL_APP_DIDENTERFOREGROUND:
            std::cout << "SDL_APP_DIDENTERFOREGROUND" << std::endl;
            /* Restart your loops here.
             Your app is interactive and getting CPU again.
             */
            return 0;
        case SDL_APP_VIEWDIDLOAD: {
            /**
             * View loaded.
             */
            std::cout << "SDL_APP_VIEWDIDLOAD" << std::endl;
            //UIViewController* vc = [UIApplication sharedApplication].keyWindow.rootViewController;
            //admob_viewcontroller* avc = [admob_viewcontroller alloc];
            //[avc loadInterstitial];
            
            
            //[vc addChildViewController:avc];
            return 0;
        }
        default:
            /* No special processing, add it to the event queue */
            return 1;
    }
    return 0;
}
    
};

int main(int argc, char* argv[]) {
    SDL_SetEventFilter(&uikit_eventhandler::HandleAppEvents, NULL);
    
    atom::application app;
    app.setApplet(new_game_breakout(&app));
    //app.resize(1024, 1024);
    app.create();
    return app.start();//app.start();

}

