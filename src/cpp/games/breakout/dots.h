#pragma once

#define DOT_NORMAL_SCORE    60


static int destroy_NormalDot(World* world, int id)
{
    vec2f at = {
        (float)world->transform[id].pos.x,
        (float)world->transform[id].pos.y
    };
    
    //burst(at, 128, 360.0f * M_PI / 180.0f, 0.0f, 0.075f, 0.1f, world->_boomParticles);
    float baseAngle = 0.0f;
    burst(at,                    // at
          128,                    // count
          360.0f * M_PI / 180.f,  // spread
          baseAngle,
          0.1f,                   // speed
          2.0f,                   // speedVariance
          world->_boomParticles,
          1.f,                    // scale
          .5f                     // scaleVariance
    );
    
    world->_grid.addExplosiveForce(512.f, at, 150.f);
    

    std::cout << "boom!" << std::endl;
    return world->score[id];
}

int World_createNormalDot(World* world, int x, int y)
{
    int id = World_createEntity(world);
    world->mask[id] =   E_COMPONENT_TRANSFORM   | 
                        E_COMPONENT_TWEEN       | 
                        E_COMPONENT_SCORE       | 
                        E_COMPONENT_TIMEOUT;
    
    world->score[id] = DOT_NORMAL_SCORE;
    world->tween[id].id = atom::easing::elastic_out;//circle_in_out;
    world->tween[id].time = 0.f;
    world->tween[id].timeout = 6000.f;
    world->tween[id].dst = &world->transform[id].scale;
    world->tween[id].minValue = 0.1f;
    world->tween[id].maxValue = 2.f;
    
    world->transform[id].pos.x = x;
    world->transform[id].pos.y = y;
    world->transform[id].scale = 0.0f;
    
    world->destroy[id] = &destroy_NormalDot;

    world->timeout[id].current = 0;
    world->timeout[id].max = 6000;
    //world->timeout[id].onTimeout = &World_destroyEntity;

    world->visual[id].color = ColorPreset::Green;//detail::SDL_MakeColor(0, 255, 0);

    return id;
}


int World_createBadDot(World* world, int x, int y) 
{
    int id = World_createNormalDot(world, x, y);
    
    world->score[id] = -world->score[id];
    world->visual[id].color = ColorPreset::Red;//detail::SDL_MakeColor(255, 0, 0);
    world->timeout[id].max = world->tween[id].timeout = 4000;

    return id;
}
