//
//  math.h
//  application_name
//
//  Created by emiel Dam on 10/8/15.
//
//

#ifndef math_h
#define math_h

#include <cmath>
#include <functional>

namespace math {
    //using sqrt = std::sqrt;
    
    template <typename T>
    inline T next_pow2(T n)
    {
        size_t k = 1;
        --n;
        do {
            n |= n >> k;
            k <<= 1;
        } while(n & (n + 1));
        return n + 1;
    }
    
    template <>
    inline int next_pow2<int>(int x) {
        if(x < 0)
            return 0;
        --x;
        x |= x >>  1;
        x |= x >>  2;
        x |= x >>  4;
        x |= x >>  8;
        x |= x >> 16;
        return x + 1;
    }
    
    template <typename T>
    inline T modulus(T x, T mul) {
        return *((T*)NULL);
    }
    
    template <>
    inline float modulus(float x, float mul) {
        return fmodf(x, mul);
    }
    
    template <>
    inline int modulus(int x, int mul) {
        return x % mul;
    }
    
    template <typename T>
    inline T round_up(const T& x, const T& mul) {

        return x + mul - modulus<T>(x, mul);
    }
}

#endif /* math_h */
