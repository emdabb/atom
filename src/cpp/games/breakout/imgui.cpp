#include "imgui.h"
#include "input.h"
#include "sprite_font.h"
#include <SDL.h>
#include <cstring>
#include <nanovg.h>

namespace atom 
{
    static struct guistate {
        int hot;
        int active;
        NVGcolor baseColor;
        NVGcolor hotColor;
        NVGcolor activeColor;
        uint16_t keyChar;
        int kbditem;
    } uistate;

    int  imgui_loadFont(NVGcontext* vg, std::istream* is) {
        return 0;
    }
    bool SDL_IntersectRectAndPoint(const SDL_Rect* rc, const SDL_Point* p) 
    {
        
        return ((p->x >= rc->x) && 
                (p->x <= (rc->x + rc->w)) &&
                (p->y >= rc->y) &&
                (p->y <= (rc->y + rc->h)));
    }

    void imgui_reset() 
    {
        uistate.hot = uistate.active = 0;
        uistate.baseColor = nvgRGBA( 253, 246, 227, 255 );
        uistate.hotColor  = nvgRGBA( 203, 75,   22, 255 );
        uistate.activeColor=nvgRGBA( 220, 50,   47, 255 );
        uistate.keyChar = input_GetLastKeyPressed();
        uistate.kbditem = -1;
    }

    bool imgui_button(int id, NVGcontext* ren, const SDL_Rect* dst, int fontId, const char* text)
    {
        const SDL_Point& mousep = SDL_GetMousePosition();
        if(SDL_IntersectRectAndPoint(dst, &mousep)) {
            uistate.hot = id;
            if(uistate.active == 0 && SDL_IsMouseDown()) 
            {
                uistate.active = id;
                input_resetMouseButton();
            }
        }
        
        NVGcolor col;

        if(uistate.hot == id)
            if(uistate.active == id)
                col  = uistate.activeColor;//SDL_SetTextureColor(tex, &uistate.activeColor);
            else
                col  = uistate.hotColor;//SDL_SetTextureColor(tex, &uistate.hotColor);
        else
             col  = uistate.baseColor;//SDL_SetTextureColor(tex, &uistate.baseColor);
        
        nvgBeginPath(ren);
        nvgFillColor(ren, col);
        nvgRoundedRect(ren, dst->x, dst->y, dst->w, dst->h, 5.0f);
        nvgFill(ren);
        
        nvgFontFaceId(ren, fontId);
        nvgTextAlign(ren, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
        nvgFillColor(ren, nvgRGBA(0, 0, 0, 255));
        nvgText(ren, dst->x + dst->w / 2, dst->y + dst->h / 2, text, NULL);

        return uistate.hot == id && uistate.active == id;
    }

    bool imgui_textinput(int id, NVGcontext* ren, int fontId, const SDL_Rect* src, const SDL_Rect* dst, char* str)
    {
        const SDL_Point& p = SDL_GetMousePosition();
        if(SDL_IntersectRectAndPoint(dst, &p)) {
            uistate.hot = id;
            if(uistate.active == 0 && SDL_IsMouseDown())
            {
                uistate.active = id;
                SDL_StartTextInput();
            }
        } else {
            SDL_StopTextInput();
            uistate.keyChar = 0;
        }
        
        if(uistate.kbditem == -1) uistate.kbditem = id;
        
        size_t len = strlen(str);
        
        if(uistate.kbditem == id) {
            if(uistate.keyChar == 8)
            {
                str[--len] = 0;
            }
            
            if(uistate.keyChar == SDL_SCANCODE_SPACE) {
                
            }
            
            if(uistate.keyChar >= 32 && uistate.keyChar < 127) {
                str[len++] = uistate.keyChar;
                str[len] = 0;
            }
            
            uistate.keyChar = 0;
            input_resetKey();
        }
        
        NVGcolor col;
        
        if(uistate.hot == id)
            if(uistate.active == id)
                col = uistate.activeColor;//SDL_SetRenderDrawColor(ren, uistate.activeColor.r, uistate.activeColor.g, uistate.activeColor.b, 255 );
            else
                col = uistate.hotColor;//SDL_SetRenderDrawColor(ren, uistate.hotColor.r, uistate.hotColor.g, uistate.hotColor.b, 255 );
        else
            col = uistate.baseColor;//SDL_SetRenderDrawColor(ren, uistate.baseColor.r, uistate.baseColor.g, uistate.baseColor.b, 255);

        int tw, th;

        //font->measure_string(str, &tw, &th);
        //SDL_Rect box = { dst->x, dst->y, dst->w, dst->h };
        //SDL_RenderDrawRect(ren, &box);
        //font->draw_string(ren, {dst->x, dst->y}, str, &uistate.hotColor);
        
        return uistate.active == id && uistate.hot == id && uistate.kbditem == id;
    }
}
