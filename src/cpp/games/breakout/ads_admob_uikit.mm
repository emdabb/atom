//
//  admob_uikit.m
//  application_name
//
//  Created by emiel Dam on 10/14/15.
//
//

#import <Foundation/Foundation.h>
#include <GoogleMobileAds/GoogleMobileAds.h>
#include "ads.h"
#include <map>

@interface admob_viewcontroller : UIViewController

@property(nonatomic, strong) GADInterstitial *interstitial;

@end

namespace ads {
    class AdmobAd : public Ad {
        admob_viewcontroller* _viewcontroller;
        
        
    public:
        AdmobAd();
        virtual ~AdmobAd();
        
        virtual void request();
        virtual void show();
        virtual void hide();
        virtual bool isReady();
        
        static Ad* create();
    };

}



@interface admob_viewcontroller () <GADInterstitialDelegate, UIAlertViewDelegate>
@end

@implementation admob_viewcontroller
ads::Ad* _ad;

- (void) initWithAd : (ads::Ad*)ad{
    _ad = ad;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
    [mainWindow addSubview:self.view];
}

- (BOOL) isReady {
    return [self.interstitial isReady];
}

- (void)loadInterstitial:(NSString*)str {
    NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
    self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:str];
    self.interstitial.delegate = self;
    GADRequest *request = [GADRequest request];
    // Requests test ads on test devices.
    request.testDevices = @[@"7a6f80ec7bf34abd68a374108d996d4c"];
    [self.interstitial loadRequest:request];
}

- (void)presentInterstitial {
    [self.interstitial presentFromRootViewController:self];
}

- (void) dismiss {
    [self.view removeFromSuperview];
}

#pragma mark UIAlertViewDelegate implementation

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    //[self startNewGame];
}

#pragma mark GADInterstitialDelegate implementation

- (void)interstitial:(GADInterstitial *)interstitial
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"interstitialDidFailToReceiveAdWithError: %@", [error localizedDescription]);
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    NSLog(@"interstitialDidDismissScreen");
    [self.view removeFromSuperview];
    _ad->onAdIsDismissed(_ad);
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
    _ad->onAdIsReady(_ad);
}

// Sent just before presenting an interstitial.  After this method finishes the
// interstitial will animate onto the screen.  Use this opportunity to stop
// animations and save the state of your application in case the user leaves
// while the interstitial is on screen (e.g. to visit the App Store from a link
// on the interstitial).
- (void)interstitialWillPresentScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillPresentScreen");
}

// Sent before the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
}

// Sent just before the application will background or terminate because the
// user clicked on an ad that will launch another application (such as the App
// Store).  The normal UIApplicationDelegate methods, like
// applicationDidEnterBackground:, will be called immediately before this.
- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}


@end

#include "ads.h"

namespace ads {
    AdmobAd::AdmobAd() {
        _viewcontroller = [admob_viewcontroller alloc];
        
        [_viewcontroller initWithAd:this];
    }
    AdmobAd::~AdmobAd() {
        
    }
    
    void AdmobAd::request() {
        [_viewcontroller loadInterstitial:[NSString stringWithUTF8String:_appId.c_str()]];
    }
    
    void AdmobAd::show() {
        
        [_viewcontroller presentInterstitial];
    }
    void AdmobAd::hide() {
        [_viewcontroller dismiss];
    }
    bool AdmobAd::isReady() {
        return [_viewcontroller isReady] != NO;
    }
    
    Ad* AdmobAd::create() {
        return new AdmobAd;
    }
    
    static struct registerFactory {
        registerFactory() {
            AdFactory::instance().register_class("admob.interstitial", &AdmobAd::create);
        }
    } _;
}

