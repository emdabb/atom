#ifndef _IMGUI_H_
#define _IMGUI_H_

#include <string>

struct SDL_Rect;
struct SDL_Texture;
struct SDL_Renderer;
struct NVGcontext;

namespace atom {
    
    class sprite_font;
    
    int  imgui_loadFont(NVGcontext*, std::istream*);
    bool imgui_button(int, NVGcontext*, const SDL_Rect*, int, const char* text = "button");
    bool imgui_textinput(int, NVGcontext*, int, const SDL_Rect*, const SDL_Rect*, char*);
    void imgui_reset();


}

#endif


