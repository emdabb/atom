//
//  assets.h
//  application_name
//
//  Created by emiel Dam on 9/29/15.
//
//

#ifndef assets_h
#define assets_h

#include <SDL.h>
#include <SDL_image.h>
#include <atom/core/singleton.h>
#include <map>
#include <string>

namespace atom {
    
    class sprite_font;
    
    struct assets : public singleton<assets> {
        
    };
    
    static std::map<std::string, void*>         _assets;
    static std::map<std::string, SDL_Surface*>  _surfaces;
    static std::map<std::string, SDL_Texture*>  _textures;
    static std::map<std::string, sprite_font*>  _fonts;
    
    
    std::string getResourcePath(const std::string &subDir = "");
    
    static void add_texture(const std::string& name, SDL_Texture* tex) {
        _textures[name] = tex;
    }
    
    template <typename T>
    static T* load(const std::string&, const std::string& fn) {

    }
    
    template <>
    inline SDL_Surface* load<SDL_Surface>(const std::string& name, const std::string& str) {
        
        std::string fname = getResourcePath() + str;
        
        if(_surfaces.count(name)) {
            return _surfaces[name];
        }
        
        SDL_Surface* surf = IMG_Load(fname.c_str());
        if(!surf) {
            printf("IMG_Load: %s\n", IMG_GetError());
            // handle error
        }

        _assets[name]   = reinterpret_cast<void*>(surf);
        _surfaces[name] = surf;
        return load<SDL_Surface>(name, str);
    }
    
    
    
}


#endif /* assets_h */
