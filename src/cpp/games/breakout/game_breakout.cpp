#include <atom/engine/application.h>

#include "particles.h"
#include "particle_spawn.h"
#include "vec2.h"
#include "asset_manager.h"
#include <atom/engine/easing.h>
#include "color.h"

using namespace atom;

#include <SDL.h>
#include <SDL_main.h>
#include <SDL_image.h>
#include <iostream>

#include "vec2.h"

#include <cassert>
#include <vector>
#include <cmath>
#include <atom/engine/ecs/kult.hpp>
#include <atom/engine/easing.h>
#include <atom/engine/application.h>
#include <Box2D/Box2D.h>

#ifndef _PTM_RATIO
#define _PTM_RATIO  (40.0f)
#define _WORLD_TO_SCREEN(n) ((n) * _PTM_RATIO)
#define _SCREEN_TO_WORLD(n) ((n) / _PTM_RATIO)
#endif

extern "C" atom::applet* new_game(atom::application*);

enum {
    E_TYPE_NIL   = 0,
    E_TYPE_BALL  = 1 << 0,
    E_TYPE_BRICK = 1 << 1,
    E_TYPE_WALL  = 1 << 2,
    E_TYPE_PLAYER= 1 << 3,
    E_TYPE_BOTTOM= 1 << 4
};

#define MAX_PLAYERS     1
#define MAX_BALLS       5
#define MAX_BLOCKS      128
#define MAX_ENTITIES    (MAX_PLAYERS + MAX_BALLS + MAX_BLOCKS)

float g_GlobalScale = 1.0f;
float g_GlobalMaxScale = 2.0f;
float g_GlobalTweenTimeout = 500;
float g_GlobalTweenTime = 0.0f;

static void update_burst(particles<vec2f>* sys, particles<vec2f>::particle& p, int t, int dt) {
    float fDt = (float)dt * 1.f;
    p.pos.x += p.state.x * fDt;
    p.pos.y += p.state.y * fDt;
    
    p.state.x *= 0.98f;
    p.state.y *= 0.98f;
    
    p.percentLife   = std::max(0.0f, p.percentLife - fDt / p.duration);
    p.currentScale  = p.scale * atom::easing::ease_elastic_out(1.0f - p.percentLife);
    p.tint.a        = (uint8_t)(p.percentLife * 255.0f);
    p.angle         = p.percentLife * M_PI * 2.0f;
}

struct GameEntity {
    b2Fixture*  _fixture;
    b2Body*     _body;
    bool        _visible;
    
    GameEntity()
    : _fixture(nullptr)
    , _body(nullptr)
    {
        
    }
    
    virtual ~GameEntity()
    {
        
    }
    
    virtual void update(int, int) = 0;
    virtual void draw(SDL_Renderer*) = 0;//SDL_Renderer*) = 0;
    
    virtual void onCollision(GameEntity*, const vec2f&) = 0;
    virtual const int getType() const = 0;
    
    virtual void setPosition(vec2f const&) = 0;
    virtual void getPosition(vec2f*) = 0;
    
    virtual const bool isVisible() const { return _visible != false; }
    virtual void setVisible(bool val) { _visible = val; }
};


class Ball : public GameEntity {
    float _radius;
    float _scaleTime;
    float _scaleTimeout;
    float _scale;
    particles<int>*   _trail;
public:
    Ball(b2World* world, const vec2f& at, float radius)
    : _radius(radius)
    , _scaleTime(0.0f)
    , _scaleTimeout(1500.0f)
    {
        b2BodyDef bd;
        bd.type = b2_dynamicBody;
        bd.userData = this;
        bd.allowSleep = false;

        bd.position.Set(_SCREEN_TO_WORLD(at.x), _SCREEN_TO_WORLD(at.y));
        _body = world->CreateBody(&bd);
        
        b2Shape* shape = new b2CircleShape;
        ((b2CircleShape*)shape)->m_radius = _SCREEN_TO_WORLD(radius);
        
        b2FixtureDef fd;
        fd.density = 1.0f;
        fd.friction = 0.0f;
        fd.restitution = 1.0f;
        fd.shape = shape;
        fd.userData = this;
        fd.filter.categoryBits  = E_TYPE_BALL;
        fd.filter.maskBits      = E_TYPE_WALL | E_TYPE_BRICK | E_TYPE_BOTTOM | E_TYPE_PLAYER;
        _fixture = _body->CreateFixture(&fd);
        

        _trail = new particles<int>(32, nullptr);
        
    }
    virtual void update(int t, int dt) {
        _scaleTime = std::min(_scaleTime + (float)dt, _scaleTimeout);
        
        _scale = atom::easing::ease_elastic_out(_scaleTime / _scaleTimeout);
        

        
        if((t & 63) == 0) {
            const b2Vec2& vec = _body->GetPosition();
            vec2f at = {
                _WORLD_TO_SCREEN(vec.x),
                _WORLD_TO_SCREEN(vec.y)
            };
            
            _trail->emit(at, 1.f, {255,255,255,255}, 0, 5.f);
        }
    
    }
    virtual void draw(SDL_Renderer* ren) { 

        _trail->draw(ren, atom::_textures["@whitePixel"]);

        const b2Vec2& vec = _body->GetPosition();
        vec2f at = {
            _WORLD_TO_SCREEN(vec.x),
            _WORLD_TO_SCREEN(vec.y)
        };
        SDL_Rect rc = {
            (int)(at.x -(_radius * _scale) * 0.5f),
            (int)(at.y -(_radius * _scale) * 0.5f),
            (int)((_radius * _scale) * 2.0f),
            (int)((_radius * _scale) * 2.0f)
        };
        
        b2Vec2 vel = _body->GetLinearVelocity();
        float angle = atan2f(vel.y, vel.x);
        
        SDL_Texture* tex = atom::_textures["@whitePixel"];
        SDL_SetTextureColorMod(tex, 255,255,255);
        SDL_SetTextureAlphaMod(tex, 255);
        SDL_RenderCopyEx(ren, tex, NULL, &rc, angle * (180.0f / M_PI), NULL, SDL_FLIP_NONE);
    }
    
    virtual void onCollision(GameEntity* other, const vec2f& normal) {
        _scaleTime = 0.0f;
        
        if(other && other->getType() == E_TYPE_PLAYER) {
            vec2f playerPos, myPos;
            this->getPosition(&myPos);
            other->getPosition(&playerPos);
            
            float dx = myPos.x - playerPos.x;
            
            const b2AABB& aabb = _fixture->GetAABB(0);
            float w = _WORLD_TO_SCREEN(aabb.GetExtents().x * 4.0f);
            
            float lerpPos = dx / w;
            
            std::cout << "dx=" << dx << ", w=" << w << ", lerp=" << lerpPos << std::endl;
            
            float angle = M_PI + (lerpPos);
            
            b2Vec2 vec = { dx, 2.f * fabsf(dx) };
            vec.Normalize();

            this->_body->SetLinearVelocity({vec.x * 10.f, vec.y * 10.f});
            
        }
        
        float angle = atan2f(-normal.y, normal.x);
        
        
    }
    virtual const int getType() const {
        return E_TYPE_BALL;
    }
    
    virtual void setPosition(vec2f const& val) {
        _body->SetTransform({
            _SCREEN_TO_WORLD(val.x),
            _SCREEN_TO_WORLD(val.y)
        }, 0.0f);
    }
    
    virtual void getPosition(vec2f* val) {
        assert(val != NULL);
        const b2Vec2& t = _body->GetPosition();
        val->x = _WORLD_TO_SCREEN(t.x);
        val->y = _WORLD_TO_SCREEN(t.y);
    }
    
    virtual void setVelocity(const vec2f& val) {
        _body->SetLinearVelocity({
            val.x,
            val.y
        });
    }
};

class Block : public GameEntity {
    vec2f _halfSize;
    particles<vec2f>* _particles;
    int _health;
public:
    Block(b2World* world, const vec2f& at, const vec2f& size)
    : _halfSize(size)
    , _health(100)
    {
        _halfSize.x *= 0.5f;
        _halfSize.y *= 0.5f;

        b2BodyDef bd;
        bd.type = b2_staticBody;
        bd.position.Set(_SCREEN_TO_WORLD(at.x), _SCREEN_TO_WORLD(at.y));
        _body = world->CreateBody(&bd);
        
        b2Shape* shape = new b2PolygonShape;
        ((b2PolygonShape*)shape)->SetAsBox(_SCREEN_TO_WORLD(size.x * 0.5f), _SCREEN_TO_WORLD(size.y * 0.5f));
        
        b2FixtureDef fd;
        fd.shape = shape;
        fd.userData = this;
        fd.filter.categoryBits  = E_TYPE_BRICK;
        fd.filter.maskBits      = E_TYPE_BALL;
        
        _fixture = _body->CreateFixture(&fd);
        
        _particles = new particles<vec2f>(1024, &update_burst);
    }
    virtual void update(int t, int dt) {
        _particles->update(t, dt);
    }
    virtual void draw(SDL_Renderer* ren) {
        
        _particles->draw(ren, atom::_textures["@smoke"]);
        
        const b2Vec2& vec = _body->GetPosition();
        
        vec2f pos = {
            _WORLD_TO_SCREEN(vec.x),
            _WORLD_TO_SCREEN(vec.y)
        };
        
        SDL_Rect rc = {
            (int)(pos.x -_halfSize.x),
            (int)(pos.y -_halfSize.y),
            (int)(_halfSize.x * 2.0f),
            (int)(_halfSize.y * 2.0f)
        };
        SDL_SetRenderDrawColor(ren, 255, 255, 0, 255);
        SDL_RenderFillRect(ren, &rc);

    }
    
    virtual void onCollision(GameEntity*, const vec2f& normal)
    {
        b2Vec2 b2vec = this->_body->GetPosition();
        
        vec2f pos = {
            _WORLD_TO_SCREEN(b2vec.x),
            _WORLD_TO_SCREEN(b2vec.y)
        };
        
        float angle = atan2(-normal.y, normal.x);
        
        burst(pos,                    // at
              128,                    // count
              180.0f * M_PI / 180.f,  // spread
              angle,
              0.1f,                   // speed
              2.0f,                   // speedVariance
              _particles,
              1.f,                    // scale
              .5f                     // scaleVariance
              );
        _health -= 100;
        if(_health <= 0) 
        {
            b2Filter disableFilter;
            disableFilter.categoryBits = 0;
            disableFilter.maskBits = 0;
            _fixture->SetFilterData(disableFilter);
            setVisible(false);
           
        }
    }
    virtual const int getType() const {
        return E_TYPE_NIL;
    }
    
    virtual void setPosition(vec2f const&) {
        
    }
    
    virtual void getPosition(vec2f* val) {
        assert(val != NULL);
        const b2Transform& t = _body->GetTransform();
        val->x = _WORLD_TO_SCREEN(t.p.x);
        val->y = _WORLD_TO_SCREEN(t.p.y);
    }
};

class Player : public GameEntity {
    vec2f _size;
    particles<vec2f>* _particles;
    SDL_Texture* _eyeTex;
    float _eyeRadius;
    vec2f _targetPos;
public:
    Player(b2World* world, const vec2f& at, const vec2f& size)
    : _size(size)
    {
        
        b2BodyDef bd;
        bd.userData = this;
        bd.type     = b2_kinematicBody;
        bd.position.Set(_SCREEN_TO_WORLD(at.x), _SCREEN_TO_WORLD(at.y));
        _body = world->CreateBody(&bd);
        
        b2Shape* shape = new b2PolygonShape;
        ((b2PolygonShape*)shape)->SetAsBox(_SCREEN_TO_WORLD(size.x * 0.5f), _SCREEN_TO_WORLD(size.y * 0.5f));
        
        b2FixtureDef fd;
        fd.shape = shape;
        fd.userData = this;
        fd.filter.categoryBits  = E_TYPE_PLAYER;
        fd.filter.maskBits      = E_TYPE_BALL;
        
        _fixture = _body->CreateFixture(&fd);
        
        _particles = new particles<vec2f>(1024, &update_burst);
        
        _eyeTex = atom::_textures["@eye"];
        _eyeRadius = size.y * 0.75f;

    }
    virtual void update(int t, int dt) {
        float fDt = (float)dt * 0.01f;
        _particles->update(t, dt);
        
        const b2Vec2& pos0 = _body->GetPosition();
        b2Vec2 pos1 = { _SCREEN_TO_WORLD(_targetPos.x), _SCREEN_TO_WORLD(_targetPos.y) };
        
        vec2f val;
        val.x = pos0.x + (pos1.x - pos0.x) * fDt;
        val.y = pos0.y;
        
        _body->SetTransform({
            val.x,
            val.y
        }, 0.0f);
        
        
    }
    
    void drawMouth(NVGcontext* ren, const vec2f& at)
    {
//        SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
        // draw the mouth texture here, scaled with the velocity of the ball.
    }
    
    void drawEye(SDL_Renderer* ren, const vec2f& at) {
        SDL_Rect dst {
            (int)(at.x - _eyeRadius * 0.5f),
            (int)(at.y - _eyeRadius * 0.5f),
            (int)(_eyeRadius),
            (int)(_eyeRadius)
        };
        
        SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
        SDL_SetTextureColorMod(_eyeTex, 255, 255, 255);
        SDL_RenderCopy(ren, _eyeTex, NULL, &dst);
        
        dst.x = (int)(at.x - _eyeRadius * 0.25f);
        dst.y = (int)(at.y - _eyeRadius * 0.25f);
        dst.w = (int)(_eyeRadius * 0.5f);
        dst.h = (int)(_eyeRadius * 0.5f);
        
        SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
        SDL_SetTextureColorMod(_eyeTex, 0, 0, 0);
        SDL_RenderCopy(ren, _eyeTex, NULL, &dst);

    }
    
    void drawEyes(SDL_Renderer* ren, const vec2f& at, float sep) {
        drawEye(ren, {at.x + sep, at.y });
        drawEye(ren, {at.x - sep, at.y });
    }
    
    virtual void draw(SDL_Renderer* ren) {
        vec2f at;
        getPosition(&at);
        _particles->draw(ren, atom::_textures["@whitePixel"]);
        
        /**
         scale = ||targetPos - currentPos||
         */
        
        SDL_Rect rc = {
            (int)(at.x - _size.x * 0.5f),
            (int)(at.y - _size.y * 0.5f),
            (int)(_size.x),
            (int)(_size.y)
        };
        
        SDL_SetRenderDrawColor(ren, 0xcf, 0x37, 0x46, 255);
        SDL_RenderFillRect(ren, &rc);
        
        drawEyes(ren, at, _size.x * 0.35f);
    }
    
    virtual void onCollision(GameEntity*, const vec2f& normal)
    {
        b2Vec2 b2vec = this->_body->GetPosition();
        
        vec2f pos = {
            _WORLD_TO_SCREEN(b2vec.x),
            _WORLD_TO_SCREEN(b2vec.y)
        };
        
        float angle = atan2(-normal.y, normal.x);
        
        burst(pos,                    // at
              128,                    // count
              180.0f * M_PI / 180.f,  // spread
              angle,
              0.1f,                   // speed
              2.0f,                   // speedVariance
              _particles,
              5.f,
              0.f);

    }
    virtual const int getType() const {
        return E_TYPE_PLAYER;
    }
    
    virtual void setPosition(vec2f const& val) {
        _targetPos = val;
    }
    
    virtual void getPosition(vec2f* val) {
        assert(val != NULL);
        const b2Transform& t = _body->GetTransform();
        val->x = _WORLD_TO_SCREEN(t.p.x);
        val->y = _WORLD_TO_SCREEN(t.p.y);
    }
};

class Walls : public GameEntity
{
    vec2f _size;
public:
    Walls(b2World* world, const vec2f& at, const vec2f& size)
    : _size(size)
    {
        b2BodyDef bd;
        bd.position.Set(_SCREEN_TO_WORLD(at.x), _SCREEN_TO_WORLD(at.y));
        
        _body = world->CreateBody(&bd);
        
        b2EdgeShape shape;
        
        b2FixtureDef fd;
        fd.userData = this;
        fd.restitution = 1.0f;
        fd.shape = &shape;
        fd.filter.categoryBits  = E_TYPE_WALL;
        fd.filter.maskBits      = E_TYPE_BALL;
        
        shape.Set({0.f, 0.f},
                  {_SCREEN_TO_WORLD(size.x), 0.f}
        );
        _fixture = _body->CreateFixture(&fd);
        
        fd.userData = nullptr;
        
        shape.Set({.0f, .0f},
                  {.0f, _SCREEN_TO_WORLD(size.y)}
        );
        _body->CreateFixture(&fd);
        
        shape.Set({.0f, _SCREEN_TO_WORLD(size.y)},
                  {_SCREEN_TO_WORLD(size.x), _SCREEN_TO_WORLD(size.y)}
        );
        _body->CreateFixture(&fd);
        
        shape.Set({_SCREEN_TO_WORLD(size.x), _SCREEN_TO_WORLD(size.y)},
                  {_SCREEN_TO_WORLD(size.x), .0f}
        );
        _body->CreateFixture(&fd);
        
        
    }
    
    virtual void update(int, int) {
        
    }
    virtual void draw(SDL_Renderer* ren) {
        const b2Vec2& at = _body->GetPosition();
        
        SDL_Rect rc = {
            (int)(_WORLD_TO_SCREEN(at.x)),
            (int)(_WORLD_TO_SCREEN(at.y)),
            (int)(_size.x),
            (int)(_size.y)
        };
        SDL_SetRenderDrawColor(ren, 255, 0, 255, 255);
        SDL_RenderDrawRect(ren, &rc);
        
    }
    virtual void onCollision(GameEntity*, const vec2f&) {
        
    }
    virtual const int getType() const {
        return E_TYPE_WALL;
    }
    
    virtual void setPosition(vec2f const&) {}
    

    virtual void getPosition(vec2f* val) {
        assert(val != NULL);
        const b2Transform& t = _body->GetTransform();
        val->x = _WORLD_TO_SCREEN(t.p.x);
        val->y = _WORLD_TO_SCREEN(t.p.y);
    }
};

class ContactListener : public b2ContactListener
{
public:
    /// Called when two fixtures begin to touch.
    virtual void BeginContact(b2Contact* contact) {
        const b2Fixture* f_A = contact->GetFixtureA();
        const b2Fixture* f_B = contact->GetFixtureB();
        
        GameEntity* e_A = static_cast<GameEntity*>(f_A->GetUserData());
        GameEntity* e_B = static_cast<GameEntity*>(f_B->GetUserData());
        
        b2WorldManifold m;
        contact->GetWorldManifold(&m);

        if(e_A)
            e_A->onCollision(e_B, {m.normal.x, m.normal.y});
        if(e_B)
            e_B->onCollision(e_A, {m.normal.x, m.normal.y});
    }
    
    /// Called when two fixtures cease to touch.
    virtual void EndContact(b2Contact* contact) {
        //B2_NOT_USED(contact);
    }
};




class game : public applet {
    application* _app;
    SDL_Texture* tex;
    SDL_Texture* _whitePixel;
    
    
    vec2<int> _mousePos;
    
    b2World* _world;
    
    Walls* _wall;
    
    GameEntity* _player[MAX_PLAYERS];
    GameEntity* _balls[MAX_BALLS];
    GameEntity* _blocks[MAX_BLOCKS];
    
    void onMouseDown(int x, int y) {
        _mousePos.x = x;
        _mousePos.y = y;
    }
    
    void onMouseMove(int x, int y, int dx, int dy) {

        _mousePos.x = x;
        _mousePos.y = y;
    }
public:
    explicit game(application* app_)
    : _app(app_)
    {
        auto d1 = atom::delegate<void(int, int)>::from<game, &game::onMouseDown>(this);
        _app->OnMouseDown.add(d1);//delegate<void(int)>::from<game, &game::onMouseDown>(this));
        _app->OnMouseMove.add(delegate<void(int, int, int, int)>::from<game, &game::onMouseMove>(this));

    }

    virtual ~game() 
    {
    }

    virtual int create() {
        SDL_Renderer* ren = _app->getRenderer();

        atom::load<SDL_Surface>("@test",  "assets/test.png");
        atom::load<SDL_Surface>("@smoke", "assets/smoke.png");
        atom::load<SDL_Surface>("@eye", "assets/circle.png");
        
        atom::add_texture("@eye", SDL_CreateTextureFromSurface(ren, atom::_surfaces["@eye"]));
        atom::add_texture("@smoke", SDL_CreateTextureFromSurface(ren, atom::_surfaces["@smoke"]));
    
        tex = SDL_CreateTextureFromSurface(ren, atom::_surfaces["@smoke"]);
        
        _whitePixel = SDL_CreateTexture(ren, SDL_PIXELFORMAT_RGB565, SDL_TEXTUREACCESS_STATIC, 1, 1);
        atom::add_texture("@whitePixel", _whitePixel);
        uint16_t pixel = 0xffff;
        SDL_Rect rc = { 0, 0, 1, 1 };
        SDL_UpdateTexture(_whitePixel, &rc, &pixel, sizeof(pixel));

        //emitter = new particles<vec2f> (1024, &update_burst);
       
        _world = new b2World({.0f, .0f});
        _world->SetContactListener(new ContactListener);

        memset(_player, 0, sizeof(GameEntity) * MAX_PLAYERS);
        memset(_blocks, 0, sizeof(GameEntity) * MAX_BLOCKS);
        memset(_balls, 0, sizeof(GameEntity) * MAX_BALLS);

        int w = _app->getWidth();
        int h = _app->getHeight();
        
        _mousePos.x = w / 2;
        _mousePos.y = h - 64;
        
        _player[0] = new Player(_world, vec2f::convert(_mousePos), {128, 32});
        _player[0]->setVisible(true);
        
        //for(int i=0; i < MAX_BALLS; i++) {
        //    _balls[i] = new Ball(_world, {randf() * w, randf() * h}, 10.f);
        //    _balls[i]->setVisible(true);
        //}
        _balls[0] = new Ball(_world, { w * 0.5f, 0.f }, 10.0f);
        _balls[0]->setPosition({w * 0.5f, (float)h - 64 - 64});
        _balls[0]->_body->ApplyLinearImpulse({-2.f, -2.f }, _balls[0]->_body->GetWorldCenter(), true);
        _balls[0]->setVisible(true);
        
        _wall = new Walls(_world, {.0f, .0f}, {(float)w, (float)h});
        
        int cols = 8;
        int rows = 5;
        float gap= 5.0f;
        
        float blockWidth = w / cols;


        for(int row = 0; row < rows; row++) {
            for(int col = 0; col < cols; col++) {
                _blocks[col + row * cols] = new Block(_world,
                                     {
                                         col * blockWidth + blockWidth * 0.5f,
                                         row * 32.f + (16.f * row)
                                     },
                                     {
                                         blockWidth - gap,
                                         32
                                     }
                                     );
                _blocks[col + row * cols]->setVisible(true);
            }
        }

        return 0;
    }

    virtual int destroy() {
        return 0;
    }

    virtual void update(int t, int dt) {
        float fDt = static_cast<float>(dt) * 0.001f;
        
        _player[0]->setPosition(vec2f::convert(_mousePos));
        
        for(int i=0; i < MAX_BALLS; i++) {
            if(_balls[i]) {
                _balls[i]->update(t, dt);
            }
        }
        
        for(int i=0; i < MAX_BLOCKS; i++) {
            if(_blocks[i]) {
                _blocks[i]->update(t, dt);
            }
        }
        
        for(int i=0; i < MAX_PLAYERS; i++) {
            if(_player[i]) {
                _player[i]->update(t, dt);
            }
        }
        
        _world->Step(fDt, 8, 3, 0);
    }

    virtual void draw() {
        SDL_Renderer* ren = _app->getRenderer();
        SDL_SetRenderDrawBlendMode(ren, SDL_BLENDMODE_BLEND);

        SDL_SetRenderDrawColor(ren, 18, 18, 36, 255);
        SDL_RenderClear(ren);
        
        
        for(int i=0; i < MAX_BALLS; i++) {
            if(_balls[i]) {
                if( _balls[i]->isVisible())
                    _balls[i]->draw(ren);
            }
        }
        
        for(int i=0; i < MAX_BLOCKS; i++) {
            if(_blocks[i]) {
                if( _blocks[i]->isVisible())
                    _blocks[i]->draw(ren);
            }
        }
        
        for(int i=0; i < MAX_PLAYERS; i++) {
            if(_player[i]) {
                if( _player[i]->isVisible())
                    _player[i]->draw(ren);
            }
        }
    
        _wall->draw(ren);
    
        SDL_RenderPresent(_app->getRenderer());
    }
};

extern "C" applet* new_game(application* app)
{
    return new game(app);
}
