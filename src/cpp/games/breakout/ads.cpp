//
//  ads.m
//  application_name
//
//  Created by emiel Dam on 10/14/15.
//
//

//#import <Foundation/Foundation.h>
#include <stdint.h>
#include <cstring>
//#include <GoogleMobileAds/GoogleMobileAds.h>
typedef unsigned long int size_t;
#include <cjson/cJSON.h>
#include "ads.h"
#include <iostream>

namespace ads {
    
    std::map<std::string, Ad*> _ads;
    
    static void AdManager_cacheNext(Ad* ad) {
        ad->request();
    }
    
    void AdManager_buildAd(cJSON* json, Ad* ad) {
        cJSON* ptr = json;
        while(ptr) {
            if(!strcmp(ptr->string, "location")) {
                ad->setLocation(ptr->valuestring);
            }
            if(!strcmp(ptr->string, "publisher-id")) {
                ad->setPublisherId(ptr->valuestring);
            }
            if(!strcmp(ptr->string, "app-id")) {
                ad->setAppId(ptr->valuestring);
                ad->request();
                ad->onAdIsDismissed.add(atom::delegate<void(Ad*)>(&AdManager_cacheNext));
            }
            ptr = ptr->next;
        }
    }
    
    void AdManager_Parse(cJSON* json) {
        static std::string name;
        if(json->string) {
            if(!json->valuestring) {
                name = json->string;
            }
            if(!strcmp(json->string, "type")) {
                std::cout << "creating ad: " << name << std::endl;
                Ad* ad = AdFactory::instance().create(json->valuestring);
                AdManager_buildAd(json, ad);
                //_ads.push_back(ad);
                _ads[name] = ad;
            }

        }
        
        if(json->child) {
            AdManager_Parse(json->child);
        }
        
        if(json->next) {
            AdManager_Parse(json->next);
        }
    }
    

    
    void AdManager::cacheFrom(std::istream &is) {
        std::istreambuf_iterator<char> eos;
        std::string rw(std::istreambuf_iterator<char>(is), {});
        
        cJSON* json = cJSON_Parse(rw.c_str());
        if(!json)
            std::cout << "cJSON error: " << cJSON_GetErrorPtr() << std::endl;
        else
            AdManager_Parse(json);
    }
    
    Ad* AdManager::getAdByName(const std::string& name) {
        std::map<std::string, Ad*>::iterator it = _ads.find(name);
        if(it != _ads.end()) {
            return (*it).second;
        }
        return NULL;
    }
}
