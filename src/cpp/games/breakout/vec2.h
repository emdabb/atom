//
//  vec2.h
//  application_name
//
//  Created by emiel Dam on 9/30/15.
//
//

#ifndef vec2_h
#define vec2_h


template <typename T>
struct vec2 {
    T x, y;
    
    template <typename S>
    static vec2<T> convert(const vec2<S>& val) {
        vec2<T> res;
        res.x = (T)val.x;
        res.y = (T)val.y;
        return res;
    }
    
    static T dot(const vec2& a, const vec2& b) {
        return a.x * b.x + a.y * b.y;
    }
    
    static T to_angle(const vec2& a) {
        return atan2f(-a.y, a.x);
    }
    
    static vec2<T> cross(const vec2& a, const vec2& b) {
        return a.X * b.Y - b.X * a.Y;
    }

    vec2& operator -= (const vec2& other) {
        x -= other.x;
        y -= other.y;
        return *this;
    }

    vec2 operator - (const vec2& other) const {
        vec2 copy(*this);   
        copy -= other;
        return copy;
    }

    vec2 operator + (const vec2& other) const {
        vec2 copy(*this);
        copy += other;
        return copy;
    }

    vec2& operator += (const vec2& other) {
        this->x += other.x;
        this->y += other.y;
        return *this;
    }
   
    vec2 operator * (float val) const {
        vec2 copy(*this);
        copy *= val;
        return copy;
    } 

    vec2& operator *= (float val) {
        this->x *= val;
        this->y *= val;
        return *this;
    }
    
};

typedef vec2<float> vec2f;

template<class T, class S>
inline T& operator <<( T &ostream, const vec2<S> &self ) {
    return ostream << "(x=" << self.x << ",y=" << self.y << ")", ostream;
}



#endif /* vec2_h */
