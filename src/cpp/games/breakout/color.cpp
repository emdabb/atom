//
//  color.cpp
//  application_name
//
//  Created by emiel Dam on 10/13/15.
//
//

#include "color.h"

const SDL_Color ColorPreset::Base03 = {   0,  43,  53, 255 };
const SDL_Color ColorPreset::Base02 = {   7,  54,  66, 255 };
const SDL_Color ColorPreset::Base01 = {  88, 110, 117, 255 };
const SDL_Color ColorPreset::Base00 = { 101, 123, 131, 255 };
const SDL_Color ColorPreset::Base0  = { 131, 148, 150, 255 };
const SDL_Color ColorPreset::Base1  = { 147, 161, 161, 255 };
const SDL_Color ColorPreset::Base2  = { 238, 232, 213, 255 };
const SDL_Color ColorPreset::Base3  = { 253, 246, 227, 255 };
const SDL_Color ColorPreset::Yellow = { 181, 137,   0, 255 };

const SDL_Color ColorPreset::Orange  = { 203,  75,  22, 255 };
const SDL_Color ColorPreset::Red     = { 220,  50,  47, 255 };
const SDL_Color ColorPreset::Magenta = { 211,  54, 130, 255 };
const SDL_Color ColorPreset::Violet  = { 108, 113, 196, 255 };
const SDL_Color ColorPreset::Blue    = {  38, 139, 210, 255 };
const SDL_Color ColorPreset::Cyan    = {  42, 161, 152, 255 };
const SDL_Color ColorPreset::Green   = { 133, 153,   0, 255 };
