//
//  particles.h
//  application_name
//
//  Created by emiel Dam on 9/30/15.
//
//

#ifndef particles_h
#define particles_h

#include "vec2.h"
#include <SDL.h>
#include <vector>
#include <nanovg.h>

struct NVGcontext;

template <typename T>
struct particles {
    
    struct start_ {
        size_t n;
        start_() : n(0) {
            
        }
        void inc(size_t cap) {
            n = (n + 1) % cap;
        }
    };
    
    struct particle {
        vec2f pos;
        SDL_Color tint;
        float currentScale;
        float scale;
        float duration;
        float percentLife;
        float angle;
        T     state;
    };
    
    typedef void(*particle_update_fun)(particles<T>*, particle&, int, int);
    
    
    std::vector<particle> _particles;
    size_t _capacity;
    size_t _num;
    size_t _start;
    particle_update_fun _fun;
    
    
    particles(size_t cap, particle_update_fun fun)
    : _capacity(cap)
    , _num(0)
    , _start(0)
    , _fun(fun)
    {
        _particles.resize(_capacity);
//        for(size_t i=0; i < _particles.size(); i++) {
//            _particles[i] = new particle;
//        }
        //memset(&_particles[0], 0, sizeof(particle) * _capacity);
    }
    
    ~particles()
    {
    }
    
    void update(int t, int dt)
    {
        size_t numToRemove = 0;
        for(size_t ii=0; ii < _num; ii++)
        {
            size_t i = index_of(ii);
            particle& p = _particles[i];
            if(_fun != nullptr)
                _fun(this, p, t, dt);
            
            size_t a = index_of(ii - numToRemove);
            size_t b = index_of(ii);
            
            std::swap(_particles[a], _particles[b]);
            
            if(_particles[i].percentLife <= 0.0f)
                numToRemove++;
        }
        _num -= numToRemove;
    }
    
    inline size_t index_of(size_t i)
    {
        return (_start + i) % _capacity;
    }
    
    void emit(const vec2f& at, float duration, const SDL_Color& c, const T& state, float scale = 1.f)
    {
        particle* p = nullptr;
        
        size_t i;
        
        if(_num == _capacity)
        {
            p = &_particles[index_of(0)];
            _start = (_start + 1) % _capacity;
        }
        else
        {
            p = &_particles[index_of(_num)];
            _num++;
        }
        
        p->pos   = at;
        p->tint  = c;
        p->scale = scale;
        p->currentScale = scale;
        p->duration = duration;
        p->percentLife = 1.0f;
        p->angle = 0.0f;
        p->state = state;
    }
    
    void draw(SDL_Renderer* ren, SDL_Texture* tex)
    {
        
        SDL_SetTextureBlendMode(tex, SDL_BLENDMODE_BLEND);
        int w, h;
        SDL_QueryTexture(tex, NULL, NULL, &w, &h);
        
        for(size_t i=0; i < _num; i++)
        {
            particle* p = &_particles[index_of(i)];
            
            SDL_Rect dst = {

                (int)((p->pos.x + 0.5f) - (p->currentScale) / 2),
                (int)((p->pos.y + 0.5f) - (p->currentScale) / 2),

                (int)(p->currentScale),
                (int)(p->currentScale)
            };
            SDL_SetTextureColorMod(tex, p->tint.r, p->tint.g, p->tint.b);
            SDL_SetTextureAlphaMod(tex, p->tint.a);
            SDL_SetRenderDrawColor(ren, p->tint.r, p->tint.g, p->tint.b, p->tint.a);
            SDL_RenderCopyEx(ren, tex, NULL, &dst, p->angle * 180.f / M_PI, NULL, SDL_FLIP_NONE);
        }

    }
};


#endif /* particles_h */
