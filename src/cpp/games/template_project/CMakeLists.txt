CMAKE_MINIMUM_REQUIRED(VERSION 3.0)
project(orbit)

set(NAME ${PROJECT_NAME})

set(CMAKE_BUILD_TYPE
    Debug
)

include(PlatformConfig.cmake.in)

include_directories(
    "${ATOM_ROOTDIR}/src/cpp"
    "${ATOM_ROOTDIR}/src/cpp/vendor"
    "${ATOM_ROOTDIR}/src/cpp/vendor/nanovg/src"
    "${ATOM_ROOTDIR}/src/cpp/vendor/SDL2/include"
    "${ATOM_ROOTDIR}/src/cpp/vendor/SDL2_image"
    "${ATOM_ROOTDIR}/src/cpp/vendor/SDL2_mixer"
    "${ATOM_ROOTDIR}/src/cpp/vendor/SDL2_ttf"
    "${ATOM_ROOTDIR}/src/cpp/vendor/liquidfun/liquidfun/Box2D"
)

set(LIBRARY_DEPS
    SDL2
    SDL2_image
    SDL2_ttf
    SDL2_mixer
    liquidfun
    liblua
    cJSON
    atom.engine
    nanovg
	atom.net
	happyhttp
)

file(GLOB_RECURSE APP_headers
    Classes/*.h
)
file(GLOB_RECURSE APP_sources
    Classes/*.cpp
    Classes/*.c
)

if(UNIX)
	if(ANDROID)
		add_definitions(-D__ANDROID__)
        	include(proj.android/Android.cmake)
	elseif(IOS)
		add_definitions(-D__IOS__)
        	include(proj.ios/iOS.cmake)
	else(ANDROID)
		add_definitions(-D__X11__)
		include(proj.linux/linux.cmake)
	endif(ANDROID)


elseif(WIN32)
	if(WINDOWS_PHONE)

	else(WINDOWS_PHONE)

	endif(WINDOWS_PHONE)
endif(UNIX)






























