//
//  BannerView.m
//  bouncy
//
//  Created by emiel Dam on 12/3/15.
//
//

//#import <Foundation/Foundation.h>
#include "GADBannerViewController.h"
#include "GADBase.h"

@implementation GADBannerViewController

@synthesize banner;

atom::Advertisement* _bannerAd;

- (void)viewDidLoad {
    NSLog(@"BannerView.viewDidLoad");
    [super viewDidLoad];
}

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    NSLog(@"GADBannerViewController.adViewDidReceiveAd");
    [UIView beginAnimations:@"BannerSlide" context:nil];
    bannerView.frame = CGRectMake(0.0,
                                  self.view.frame.size.height -
                                  bannerView.frame.size.height,
                                  bannerView.frame.size.width,
                                  bannerView.frame.size.height);
    
    self.view = bannerView;
    [UIView commitAnimations];
    atom::AdEventArgs args = { _bannerAd };
    _bannerAd->onAdWasFetched(args);//_bannerAd);
}

- (void) show {
    UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
    UIViewController* rc = [mainWindow rootViewController];
    [rc.view addSubview:self.view];
}

- (void) restoreController {
    [self.view removeFromSuperview];
}

- (UIViewController *)viewControllerForPresentingModalView {
    //return UIWindow.viewController;
    return self;
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"GADBannerViewController.didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

- (void)adViewWillPresentScreen:(GADBannerView *)bannerView {
    NSLog(@"GADBannerViewController.adViewWillPresentScreen");
}

- (void)adViewDidDismissScreen:(GADBannerView *)bannerView {
    NSLog(@"GADBannerViewController.adViewDidDismissScreen");
    atom::AdEventArgs args;
    args.ad = _bannerAd;
    _bannerAd->onAdWasDismissed(args);
}

- (void)adViewWillDismissScreen:(GADBannerView *)bannerView {
    NSLog(@"GADBannerViewController.adViewWillDismissScreen");
}

- (void)adViewWillLeaveApplication:(GADBannerView *)bannerView {
    NSLog(@"GADBannerViewController.adViewWillLeaveApplication");
}

- (void) createAndLoadBanner:(NSString*) str {
    NSLog(@"GADBannerViewController.createAndLoadBanner");
    NSLog(@"Loading ad-unit: %@", str);
    
    UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
    UIViewController* rc = [mainWindow rootViewController];
    

    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    if(orientation == UIDeviceOrientationLandscapeLeft || UIDeviceOrientationLandscapeRight) {
        self.banner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerLandscape];
    } else {
        self.banner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
    }
    

//    self.banner.delegate = self;
    [self.banner setAdUnitID:str];
    [self.banner setRootViewController:rc];
    [self.banner setDelegate:self];
//    [self.banner setNeedsLayout];
    GADRequest* req = [GADRequest request];
    [self.banner loadRequest:req];
}

- (instancetype) initWithBanner:(void*)ad {
    _bannerAd = (atom::Advertisement*)ad;
    return super.init;
}


@end



namespace atom {

    class GADBannerAd : public GADBase {
        GADBannerViewController* _view;
        bool _isReady;
    public:
        GADBannerAd() : _view(nullptr), _isReady(false)
        {
            _view = [[GADBannerViewController alloc] initWithBanner:(this)];
            this->onAdWasFetched += [&] (const AdEventArgs& args) {
                this->_isReady = true;
            };
        }
        
        virtual void fetch() {

            std::string adUnit = this->getAdUnitId();
            [_view createAndLoadBanner:[NSString stringWithUTF8String:adUnit.c_str()]];
        }
        
        virtual void show() {
            [_view show];
        }
        
        virtual void dismiss() {
            this->_isReady = false;
            [_view restoreController];
        }
        
        virtual bool isReady() {
            return this->_isReady = false;
        }
        
    public:
        static Advertisement* create() {
            return new GADBannerAd;
        }
    };
    
    static class RegisterGADBannerAd {
    public:
        RegisterGADBannerAd() {
            atom::factory<Advertisement>::instance().register_class("admob.banner", &GADBannerAd::create);
        }
    } gRegisterGADBannerAd;

}