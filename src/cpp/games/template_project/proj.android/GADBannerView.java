package com.tfjoy.nl.package_name

public class GADBannerView {

    AdView mAdView;

    public void fetch() {
        /* .. */
        notifyBannerLoaded();
    }    

    public native void notifyBannerLoaded();
    public native void notifyBannerDismissed();
    public native void notifyBannerShown();
}
