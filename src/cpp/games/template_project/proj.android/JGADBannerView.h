#pragma once

#include <jni.h>

#define JMETHOD_TAG_VOID "()V"

namespace ads {

class JGADBannerView {
    static const char* jclassname = "com/tfjoy/nl/GADBannerView"; // for env->FindClass
    JavaVM* vm;
    jobject obj;
    jclass  bannerViewClass;
    jmethodID   fetchAdID;
    jmethodID   showAdID;
    jmethodID   hideAdID;
public:
    JGADBannerView(JNIEnv* env, jobject jobj) {
        env->GetJavaVM(&vm);
        obj = env->NewGlobalRef(jobj);
        bannerViewClass = env->GetObjectClass(obj);

        fetchAdID   = env->GetMethodID(bannerViewClass, "fetch", "()V");
        showAdID    = env->GetMethodID(bannerViewClass, "show",  "()V");
        hideAdID    = env->GetMethodID(bannerViewClass, "hide",  "()V");

        env->DeleteLocalRef(bannerViewClass);
    }

    virtual ~JGADBannerView() {
        JNIEnv* env = getEnv();
        env->DeleteGlobalRef(obj);
    }

    virtual void show() {
        getEnv()->CallVoidMethod(obj, showAdID);
    }

    virtual void hide() {
        
    }

    virtual void fetch() {
    }

    JNIEnv* getEnv() const {
        JNIEnv* env = NULL;
        vm->AttachCurrentThread((void**)&env, NULL);
        return env;
    }
    
};

}



