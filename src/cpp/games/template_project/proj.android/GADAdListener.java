package com.tfjoy.nl.package_name

public abstract class AdListener {
    public void onAdLoaded() {
    }
    public void onAdFailedToLoad(int errorCode) {
    }
    public void onAdOpened() {
    }
    public void onAdClosed() {
    }
    public void onAdLeftApplication() {
    }
}
