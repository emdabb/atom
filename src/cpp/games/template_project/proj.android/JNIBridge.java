package com.tfjoy.nl.package_name

class JNIBridge {
    public static native void requestAd(String arg0);
    public static native void showAd();
    public static native void hideAd();
}
