package com.tfjoy.nl.package_name

public class GADInterstitialView {
    InterstitialAd  mInterstitialAd;
    
    public GADInterstitialView(Context context) {
        mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId("");
        
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
                // foo...
            }
        });

        if(mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            
        }
    }

    public void fetch() {
        AdRequest adRequest = new AdRequest.Builder()
            .addTestDevice("")
            .build();
        mInterstitialAd.loadAd(adRequest);
    }
}
