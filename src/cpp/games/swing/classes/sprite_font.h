//
//  sprite_font.h
//  application_name
//
//  Created by emiel Dam on 10/8/15.
//
//

#ifndef sprite_font_h
#define sprite_font_h

#include <SDL.h>
#include <SDL_ttf.h>
#include <ios>
#include <iostream>
#include <string>
#include <map>
#include <string>
#include <vector>
#include "math.h"
#include "sprite_sheet.h"

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
#define SDL_RMASK   0xff000000
#define SDL_GMASK   0x00ff0000
#define SDL_BMASK   0x0000ff00
#define SDL_AMASK   0x000000ff
#else
#define SDL_AMASK   0xff000000
#define SDL_BMASK   0x00ff0000
#define SDL_GMASK   0x0000ff00
#define SDL_RMASK   0x000000ff
#endif

namespace atom {
    
    inline int SDL_RectGetArea(const SDL_Rect* rc) {
        return rc->w * rc->h;
    }
    
    struct glyph_metrics {
        int minx, miny;
        int maxx, maxy;
        int advance;
    };
    
    struct ttf_glyph {
        glyph_metrics metric;
    };
    
    class sprite_font {
        static const int NUM_GLYPHS = 94;
        sprite_sheet<char>* _sheet;
        TTF_Font* _font;
        ttf_glyph _glyph[NUM_GLYPHS];
    protected:
        int texture_size(int nchars, float maxw, float maxh) {
            float area= nchars * maxw * maxh;
            float len = math::round_up(std::sqrt(area), maxw > maxh ? maxw : maxh);
            return math::next_pow2((int)(len + 0.5f));
        }
    public:
        sprite_font()
        : _sheet(new sprite_sheet<char>)
        {
            
        }
        
        virtual ~sprite_font() {
            delete _sheet;
        }
        
        int TTF_GlyphSize(TTF_Font* font, uint16_t c, int* w, int* h) {
            int xmax, xmin;
            int ymax, ymin;
            
            int res = TTF_GlyphMetrics(font, c, &xmin, &xmax, &ymin, &ymax, NULL);
            
            if(w) *w = xmax - xmin;
            if(h) *h = ymax - ymin;
            
            return res;
        }
        
        
        void build(std::istream& is, int size, SDL_Renderer* ren);
        int measureString(const std::string& str, int* w, int* h);
        void drawString(SDL_Renderer* ren, const SDL_Point& p, const std::string& str, const SDL_Color* col);
    };
}


#endif /* sprite_font_h */
