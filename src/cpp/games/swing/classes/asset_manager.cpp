//
//  assets.cpp
//  application_name
//
//  Created by emiel Dam on 9/29/15.
//
//

#include "asset_manager.h"
#include <map>
#include <string>
#include <iostream>

namespace atom {
    
    static std::map<std::string, void*>         _assets;
    static std::map<std::string, int>  _surfaceNames;
    static std::map<std::string, int>  _textureNames;
    static std::map<std::string, int>  _fontNames;
    
    static std::vector<SDL_Texture*> _textures;
    static std::vector<SDL_Surface*> _surfaces;
    static std::vector<sprite_font*> _fonts;
    
    std::map<std::string, int>           _nvgTextures;
    
    std::string getResourcePath(const std::string &subDir){
        //We need to choose the path separator properly based on which
        //platform we're running on, since Windows uses a different
        //separator than most systems
#ifdef _WIN32
        const char PATH_SEP = '\\';
#else
        const char PATH_SEP = '/';
#endif
        //This will hold the base resource path: Lessons/res/
        //We give it static lifetime so that we'll only need to call
        //SDL_GetBasePath once to get the executable path
        static std::string baseRes;
        if (baseRes.empty()){
            //SDL_GetBasePath will return NULL if something went wrong in retrieving the path
            char *basePath = SDL_GetBasePath();
            if (basePath){
                baseRes = basePath;
                SDL_free(basePath);
            }
            else {
                std::cerr << "Error getting resource path: " << SDL_GetError() << std::endl;
                return "";
            }
            //We replace the last bin/ with res/ to get the the resource path
            //size_t pos = baseRes.rfind("bin");
            //baseRes = baseRes.substr(0, pos) + "res" + PATH_SEP;
        }
        //If we want a specific subdirectory path in the resource directory
        //append it to the base path. This would be something like Lessons/res/Lesson0
        return subDir.empty() ? baseRes : baseRes + subDir + PATH_SEP;
    }

    int addTexture(const std::string& name, SDL_Texture* tex)
    {
        _textureNames[name] = (int)_textures.size();
        _textures.push_back(tex);
        return (int)(_textures.size() - 1);
    }
    
     int addSurface(const std::string& name, SDL_Surface* surf) {
        _surfaceNames.insert(std::pair<std::string, int>(name, (int)_surfaces.size()));
        _surfaces.push_back(surf);
        return (int)(_surfaces.size() - 1);
    }
    
     int loadSurface(const std::string& name, const std::string& filename) {
        
        std::string fname = getResourcePath() + filename;
        
        if(_surfaceNames.count(name)) {
            return _surfaceNames[name];
        }
        
        SDL_Surface* surf = IMG_Load(fname.c_str());
        if(!surf) {
            printf("IMG_Load: %s\n", IMG_GetError());
            // handle error
        }
        
        return addSurface(name, surf);
        
    }
    
    int addFont(const std::string& name, sprite_font* font) {
        _fontNames[name] = (int)_fonts.size();
        _fonts.push_back(font);
        return(int)(_fonts.size() - 1);
    }
    
    int loadFont(const std::string& name, std::istream& is, int size, SDL_Renderer* ren) {
        sprite_font* font = new sprite_font;
        font->build(is, size, ren);
        return addFont(name, font);
    }
    
    int getFontId(const std::string& name) {
        return _fontNames[name];
    }
    
    sprite_font* getFontById(int id) {
        return _fonts[id];
    }
    
    sprite_font* getFontByName(const std::string& name) {
        return getFontById(getFontId(name));
    }
    
    int getSurfaceId(const std::string& name) {
        return _surfaceNames[name];
    }
    
    SDL_Surface* getSurfaceById(int id) {
        return _surfaces[id];
    }
    
    SDL_Surface* getSurfaceByName(const std::string& name) {
        return getSurfaceById(getSurfaceId(name));
    }
    
    int getTextureId(const std::string& name) {
        return _textureNames[name];
    }
    
    SDL_Texture* getTextureById(int id) {
        return _textures[id];
    }
    
    SDL_Texture* getTextureByName(const std::string& name) {
        return getTextureById(getTextureId(name));
    }

}
