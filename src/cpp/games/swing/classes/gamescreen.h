#pragma once

#include <vector>
#include <algorithm>
#include <atom/core/event_handler.h>

enum gamescreen_state {
    E_STATE_HIDDEN,
    E_STATE_TRANSITION_ON,
    E_STATE_TRANSITION_OFF,
    E_STATE_ACTIVE
};

struct SDL_Renderer;

class gamescreen_manager;

struct NVGcontext;

class gamescreen 
{
protected:
    int   _state;
    bool  _exiting;
    int   _time_on;
    int   _time_off;
    float _pos;
    bool  _popup;
    gamescreen_manager* _manager;
    std::function<void(void)> _exitCallback;
protected:
    bool update_transition(int dt, int time, int direction);
public:
    gamescreen();
    virtual ~gamescreen();
    virtual void load_content();
    virtual void handle_input();
    virtual void update(int dt, bool otherScreenHasFocus, bool coveredByOtherScreen);
    virtual void draw(SDL_Renderer*);//SDL_Renderer*);
    virtual void unload_content();
    virtual void quit();
    void setOwner(gamescreen_manager* mgr) {
        _manager = mgr;
    }
    
    gamescreen_manager* getOwner() {
        return _manager;
    }

    inline float transition() const {
        return _pos;
    }

    inline const bool exiting() const {
        return _exiting != false;
    }

    inline int state() const 
    {
        return _state;
    }
    inline const bool is_popup() const 
    {
        return _popup != false;
    }

    void setExitCallback(const std::function<void(void)>& fn) {
        _exitCallback = fn;
    }
};

class gamescreen_manager
{
    std::vector<gamescreen*> _screens;
public:
    gamescreen_manager();
    virtual ~gamescreen_manager();
    void add(gamescreen*);
    void remove(gamescreen*);
    void update(int, int);
    void draw(SDL_Renderer*);//SDL_Renderer*);
};

#include <atom/core/factory.h>

typedef atom::factory<gamescreen> gamescreen_factory;
