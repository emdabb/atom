#include <atom/engine/ecs/kult.hpp>
#include <Box2D/Box2D.h>

#ifndef PTM_RATIO
#define PTM_RATIO   (40.f)
#endif

#define WORLD_TO_SCREEN(x)  ((x) * (PTM_RATIO))
#define SCREEN_TO_WORLD(x)  ((x) / (PTM_RATIO))

template <typename T>
struct vec2 {
    T x, y;
};

typedef vec2<float> vec2f;

template <typename T, typename S>
T& operator << (T& os, const vec2<S>& vec) {
    return os << "{ \"x\":" << vec.x << ", \"y\": " << vec.y << "}", os;
}

kult::entity player;

struct EComponent {
    enum {
        None = 0,
        Name = 1 << 0,
        Transform = 1 << 1,
        PhysicsBody = 1 << 2,
        Timeout = 1 << 3
    };
};

typedef struct {
    unsigned int time;
    unsigned int timeout;
} timeout_t;

struct transform {
    vec2f pos;
    float rot;
};

template <typename T>
T& operator << (T& os, const transform& t) {
    return os << "{" << t.pos << ", \"rot\" : " << t.rot << "}", os;
}

kult::component<EComponent::None, std::string>    name;
kult::component<EComponent::Transform, transform> xform;
kult::component<EComponent::PhysicsBody, b2Body*> body;

kult::system<int> to_world = [&] (int) {
    for(auto& e : kult::join(xform, body)) {
        const vec2f& w = e[xform].pos;

        e[body]->SetTransform( {
            SCREEN_TO_WORLD(w.x),
            SCREEN_TO_WORLD(w.y)
        }, e[xform].rot );
    }
};

kult::system<int> to_screen = [&] (int) {
    for(auto& e : kult::join(xform, body)) {
        //b2Vec2& w = e[body]->GetT;
        const b2Transform& t = e[body]->GetTransform();
        const b2Vec2& w = t.p;

        e[xform].pos = { 
            WORLD_TO_SCREEN(w.x), 
            WORLD_TO_SCREEN(w.y) 
        };
        e[xform].rot = e[body]->GetAngle();
    }
};

int main(int argc, char* argv[]) {
    return 0;
}
