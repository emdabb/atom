set(CMAKE_CXX_FLAGS "-std=c++11")

link_directories(
    ${ATOM_ROOTDIR}/proj.linux/lib/${CMAKE_BUILD_TYPE}
)

file(GLOB SRC_platform
    proj.linux/*.c
    proj.linux/*.cpp
)

file(GLOB HDR_platform
    proj.linux/*.h
)

set(LIBRARY_DEPS
    ${LIBRARY_DEPS}
    dl
    pthread
    X11
    z
    GLESv2
)

add_executable(
    ${NAME}
    ${SRC_platform}
    ${APP_sources}
)

target_link_libraries(
    ${NAME}
    ${LIBRARY_DEPS}
)

