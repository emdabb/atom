set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -x objective-c++ -std=c++11 -O0 -g -fobjc-arc")
#set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fobjc-arc")

set(${INCLUDE_DIRECTORIES}
    ~/dev/FacebookSDK/FBAudienceNetwork.framework
    ~/dev/FacebookSDK/FBSDKCoreKit.framework
    ~/dev/FacebookSDK/FBSDKLoginKit.framework
    ~/dev/FacebookSDK/FBSDKMessengerShareKit.framework
    ~/dev/FacebookSDK/FBSDKShareKit.framework
)

set(CMAKE_EXE_LINKER_FLAGS
"-framework AudioToolbox
-framework CoreGraphics
-framework QuartzCore
-framework UIKit
-framework OpenGLES
-framework CoreMotion
-framework GameController
-framework GoogleMobileAds
-framework AdSupport
-framework AudioToolbox
-framework AVFoundation
-framework CoreMedia
-framework CoreTelephony
-framework EventKit
-framework EventKitUI
-framework MessageUI
-framework StoreKit
-framework SystemConfiguration
-framework Chartboost
-framework FBAudienceNetwork
-framework FBSDKCoreKit
-framework FBSDKLoginKit
-framework FBSDKMessengerShareKit
-framework FBSDKShareKit
"
)

link_directories(
#${CMAKE_LIBRARY_OUTPUT_DIRECTORY}
${ATOM_ROOTDIR}/proj.ios/lib/Debug
~/dev/GoogleMobileAdsSdk

)

set(APP_BUNDLE_NAME com.tfjoy.nl.${NAME})
set(APP_NAME ${NAME})
set(MACOSX_BUNDLE_GUI_IDENTIFIER ${APP_BUNDLE_NAME})
set(APP_TYPE MACOSX_BUNDLE)

configure_file(
"${CMAKE_CURRENT_SOURCE_DIR}/gen/Info.plist.in"
"${CMAKE_CURRENT_BINARY_DIR}/Info.plist"
)

message("Generating ${CMAKE_CURRENT_BINARY_DIR}/Info.plist from ${CMAKE_CURRENT_SOURCE_DIR}/gen/Info.plist.in")

file(GLOB resources
${CMAKE_CURRENT_SOURCE_DIR}/assets
)

set_source_files_properties(
    ${resources}
    PROPERTIES
    MACOSX_PACKAGE_LOCATION Resources
)

file(GLOB APP_sources_iOS
    proj.ios/*.mm
    proj.ios/*.m
)

file(GLOB APP_headers_iOS
    proj.ios/*.h
)

add_definitions(-D__APP_NAME=\"${APP_NAME}\")

add_executable(${NAME}
    ${APP_TYPE}
    ${APP_headers}
    ${APP_headers_iOS}
    ${APP_sources}
    ${APP_sources_iOS}
    ${resources}
#${ATOM_ROOTDIR}/src/cpp/atom
#${ATOM_ROOTDIR}/src/cpp/vendor/SDL2/src
    ~/dev/GoogleMobileAdsSdk/GoogleMobileAds.framework
    #~/dev/VungleSDK/VungleSDK.framework

    ~/dev/FacebookSDK/FBAudienceNetwork.framework
    ~/dev/FacebookSDK/FBSDKCoreKit.framework
    ~/dev/FacebookSDK/FBSDKLoginKit.framework
    ~/dev/FacebookSDK/FBSDKMessengerShareKit.framework
    ~/dev/FacebookSDK/FBSDKShareKit.framework

)

target_link_libraries(${NAME}
# other libraries to link
${LIBRARY_DEPS}
)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

# code signing
set_target_properties(${APP_NAME} PROPERTIES
    XCODE_ATTRIBUTE_CODE_SIGN_IDENTITY "iPhone Developer: Xu Hou (GL82E6KX5R)"
    MACOSX_BUNDLE_INFO_PLIST "${CMAKE_CURRENT_BINARY_DIR}/Info.plist"
    XCODE_ATTRIBUTE_TARGETED_DEVICE_FAMILY "1,2"
    XCODE_ATTRIBUTE_PRODUCT_NAME ${NAME}
    XCODE_ATTRIBUTE_INSTALL_PATH /Applications
    XCODE_ATTRIBUTE_FRAMEWORK_SEARCH_PATHS "\
        ~/dev/GoogleMobileAdsSdk \
        ~/dev/FacebookSDK \
        ~/dev/Chartboost"

    XCODE_ATTRIBUTE_ENABLE_BITCODE "NO"
    XCODE_ATTRIBUTE_COMPRESS_PNG_FILES "YES"
    XCODE_ATTRIBUTE_VALIDATE_PRODUCT "YES"
    XCODE_ATTRIBUTE_IPHONEOS_DEPLOYMENT_TARGET "7.0"

    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin"
)

install (TARGETS ${PROJECT_NAME} DESTINATION .)
