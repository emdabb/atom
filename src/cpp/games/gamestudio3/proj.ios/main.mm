//
//  main.m
//  gamestudio3
//
//  Created by emiel Dam on 2/29/16.
//
//

#import <Foundation/Foundation.h>
#include <atom/engine/application.h>
#include "asset_manager.h"
#include "game.h"
#include <SDL.h>

extern "C" atom::applet* new_Game(atom::application*);

static int iosEventCallback(void* userData, SDL_Event* ev) {
    Game* game = (Game*)userData;
    switch(ev->type) {
        case SDL_APP_WILLENTERBACKGROUND:
            game->save(game->_currentSavegameFilename);
            break;
        default:
            break;
    }
    return 1;
}



extern "C" const char* iOS_getDataPath() {
    //NSFileManager *fileManager = [NSFileManager defaultManager];
    //NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory UTF8String];
}

static void copyDir(NSString* directory) {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentDBFolderPath = [documentsDirectory stringByAppendingPathComponent:directory];
    NSString *resourceDBFolderPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:directory];
    
    if (![fileManager fileExistsAtPath:documentDBFolderPath]) {
        //Create Directory!
        [fileManager createDirectoryAtPath:documentDBFolderPath withIntermediateDirectories:NO attributes:nil error:&error];
    } else {
        NSLog(@"Directory exists! %@", documentDBFolderPath);
    }
    
    NSArray *fileList = [fileManager contentsOfDirectoryAtPath:resourceDBFolderPath error:&error];
    for (NSString *s in fileList) {
        NSString *newFilePath = [documentDBFolderPath stringByAppendingPathComponent:s];
        NSString *oldFilePath = [resourceDBFolderPath stringByAppendingPathComponent:s];
        if (![fileManager fileExistsAtPath:newFilePath]) {
            //File does not exist, copy it
            [fileManager copyItemAtPath:oldFilePath toPath:newFilePath error:&error];
        } else {
            NSLog(@"File exists: %@", newFilePath);
        }
    }
}

int main(int argc, char* argvp[]) {
    
    //std::string path = atom::getResourcePath("") + __APP_NAME + ".app/assets";
    copyDir(@"assets");
    copyDir(@"assets/common");
    copyDir(@"assets/common/fonts");
    copyDir(@"assets/savegames");
    copyDir(@"assets/img");
    copyDir(@"assets/tables");
    copyDir(@"assets/translations");
    copyDir(@"assets/sprites");
    

    
    atom::application app;
    atom::applet* game = new_Game(&app);
    app.setApplet(game);
    app.create();
    
    SDL_SetEventFilter(&iosEventCallback, (void*)game);
    
    return app.start();
}