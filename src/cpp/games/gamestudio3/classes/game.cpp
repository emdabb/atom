#include <atom/engine/application.h>
#include <regex>
#include <atom/engine/properties.h>
#include "../classes/variant.h"
#include <cjson/cJSON.h>
#include "../classes/ptree.h"
#include "../classes/variant.h"
#include "../classes/util.h"
#include "gamescreen.h"
#include "rosetta.h"
#include "ui/Gui.h"
#include <atom/engine/easing.h>
#include <atom/core/factory.h>
#include <atom/core/io/instream.h>
#include <atom/core/io/onstream.h>
#include "game_services.h"
#include "asset_manager.h"
#include "sha256.h"
#include "rosetta.h"
#include "csv.h"
#include <stdint.h>
#include <numeric>
#include <limits>

#if !defined(__IOS__)
#include <GLES2/gl2.h>
#else
#include <OpenGLES/ES2/gl.h>
#endif

#define NANOVG_GLES2_IMPLEMENTATION

#include <nanovg.h>
#include <nanovg_gl.h>

#include "fsm.h"
#include "character.h"
#include "project.h"
#include "company.h"


using namespace ui;

extern gamescreen* new_phase_screen(project&, int);
struct gameplay_screen;

extern "C" gamescreen* new_gameplay_screen(company& co);
extern "C" gamescreen* new_project_properties_screen(company& co);
extern "C" gamescreen* new_post_development_screen(project&);
extern "C" gamescreen* new_mainmenu_screen(company&);
extern "C" gamescreen* new_end_of_life_screen(project&);

float Character_chanceToSpawnPoint(int xp);
float Character_chanceToSpawnIdeaPoint(int it);
float Character_chanceToSpawnBugPoint(int it);
int Character_numPointsToSpawn(int xp);
int Character_tickTimeout(int xp);

extern "C" void Company_save(const company* co);

cJSON* ptree_serialize(const ptree& pt) {
     cJSON* json = cJSON_CreateObject();
     for(auto it = pt._map.begin(); it != pt._map.end(); it=pt._map.upper_bound(it->first)) {
         auto a = pt._map.lower_bound(it->first);
         auto b = pt._map.upper_bound(it->first);
         
         if(std::distance(a, b) > 1) {
             cJSON* arr = nullptr;
             cJSON_AddItemToObject(json, it->first.c_str(), arr = cJSON_CreateArray());
             for(auto i=a; i!=b; i++) {
                 cJSON_AddItemToArray(arr, cJSON_CreateString(i->second.c_str()));
             }
         } else {
             cJSON_AddItemToObject(json, it->first.c_str(), cJSON_CreateString(it->second.c_str()));
         } 
     }
     for(auto it = pt._tree.begin(); it != pt._tree.end(); it++) {
         cJSON_AddItemToObject(json, it->first.c_str(), ptree_serialize(*(it->second)));
     }
     return json;
} 



//typedef state<character> character_state;

extern "C" void Character_init(character& ch) {
    ch._name = "<character-name>";//std::string();
    ch._xp = 350;
    for(int i=0; i < 6; i++) {
        ch._focus[i]        = 350;
        ch._gen_tech[i]     = 0;
        ch._gen_design[i]   = 0;
    }
    ch._gender = 0;
    ch._it_bugs = 0;
    ch._it_idea = 0;
    ch._time    = 0;
    ch._timeout = 0;
    ch._project = std::string();
}

void Character_deserialize(character& ch, cJSON* json) {
    if(!strcmp(json->string, "name"))       ch._name    = json->valuestring;
    if(!strcmp(json->string, "xp"))         ch._xp      = json->valueint;
    if(!strcmp(json->string, "gender"))     ch._gender  = json->valueint;
    if(!strcmp(json->string, "it_bugs"))    ch._it_bugs = json->valueint;
    if(!strcmp(json->string, "it_ideas"))   ch._it_idea = json->valueint;
    if(!strcmp(json->string, "time"))       ch._time    = json->valueint;
    if(!strcmp(json->string, "timeout"))    ch._timeout = json->valueint;
    if(!strcmp(json->string, "project"))    ch._project = json->valuestring;
    if(!strcmp(json->string, "focus")) {
        for(int i=0; i < cJSON_GetArraySize(json); i++) {
            ch._focus[i] = cJSON_GetArrayItem(json, i)->valueint;
        }
    }
    if(!strcmp(json->string, "gen_tech")) {
        for(int i=0; i < cJSON_GetArraySize(json); i++) {
            ch._gen_tech[i] = cJSON_GetArrayItem(json, i)->valueint;
        }
    }
    if(!strcmp(json->string, "gen_design")) {
        for(int i=0; i < cJSON_GetArraySize(json); i++) {
            ch._gen_design[i] = cJSON_GetArrayItem(json, i)->valueint;
        }
    }
    if(json->next)
        Character_deserialize(ch, json->next);
    //if(json->child)
        //Character_deserialize(ch, json->child);
}

cJSON* Character_serialize(const character& ch) {
    cJSON* json = cJSON_CreateObject();
    cJSON* arr = nullptr;
    {
        cJSON_AddItemToObject(json, "name", cJSON_CreateString(ch._name.c_str()));
        cJSON_AddItemToObject(json, "xp", cJSON_CreateNumber(ch._xp));
        cJSON_AddItemToObject(json, "focus", arr = cJSON_CreateArray());
        for(int i=0; i < 6; i++) {
            cJSON_AddItemToArray(arr, cJSON_CreateNumber(ch._focus[i]));
        }
        cJSON_AddItemToObject(json, "gender", cJSON_CreateNumber(ch._gender));
        cJSON_AddItemToObject(json, "gen_tech", arr = cJSON_CreateArray());
        for(int i=0; i < 6; i++) {
            cJSON_AddItemToArray(arr, cJSON_CreateNumber(ch._gen_tech[i]));
        }

        cJSON_AddItemToObject(json, "gen_design", arr = cJSON_CreateArray());
        for(int i=0; i < 6; i++) {
            cJSON_AddItemToArray(arr, cJSON_CreateNumber(ch._gen_design[i]));
        }
        cJSON_AddItemToObject(json, "it_bugs", cJSON_CreateNumber(ch._it_bugs));
        cJSON_AddItemToObject(json, "it_ideas", cJSON_CreateNumber(ch._it_idea));
        cJSON_AddItemToObject(json, "time", cJSON_CreateNumber(ch._time));
        cJSON_AddItemToObject(json, "timeout", cJSON_CreateNumber(ch._timeout));
        cJSON_AddItemToObject(json, "project", cJSON_CreateString(ch._project.c_str()));
    }
    return json;
}

struct company;

extern "C" bool Project_hasStaff(const project& p, int f, const std::string& id) {
    auto beg = p._focus[f]._staff.begin();
    auto end = p._focus[f]._staff.end();
    auto it = std::find(beg, end, id);
    return it != end;
    //for(
}

bool Project_countryFactor(project& p) {
    company& co = *p._co;

    if(co._country == static_cast<int>(ECountry::Europe)) {
        return p._properties._genre == static_cast<int>(EGenre::Simulation) 
            || p._properties._genre == static_cast<int>(EGenre::Adventure)
            || p._properties._genre == static_cast<int>(EGenre::Strategy);
    } else if(co._country == static_cast<int>(ECountry::Asia)) {
        return p._properties._genre ==static_cast<int>(EGenre::Action) 
            || p._properties._genre ==static_cast<int>(EGenre::RPG)
            || p._properties._genre ==static_cast<int>(EGenre::MMORPG);
    } else if(co._country == static_cast<int>(ECountry::NorthAmerica)) {
        return p._properties._genre ==static_cast<int>(EGenre::Casual)
            || p._properties._genre ==static_cast<int>(EGenre::Gambling);
    }
    return false;
}

void Project_calculateSales(project& p) {
    std::cout << " calculating sales for [" << p._name << "]" << std::endl;
    company& co = *p._co;

    float up = sqrtf(p._points._tech + p._points._design) * gSystemFactor[p._properties._platform];
    float tm = std::max(3.0f, sqrtf(p._review - 1) * 4.0f);
    float as = p._review * tm * ((p._points._tech + p._points._ideas) * 0.5f) + co._fans;

    float fb = 1.0f - 0.03f * p._points._bugs;
    float finc = 0.01f * as * (p._review - 5.0f);

    p._co->_fans += finc;

    float fh = 1.0f;
    float fy = gYearFactor[p._properties._platform][std::min(20, co._year)]; 
    float ft = p._properties._topic == co._trend[0] && p._properties._genre == co._trend[1] ? 1.3f : 1.0f;
    float fc = Project_countryFactor(p) ? 1.2f : 1.0f;



    p._time     = 0;
    p._timeout  = (int)(tm * MilliSecondsPerGameWeek);

    std::cout << " project timeout=[" << p._timeout << "]" << std::endl;

    p._sales._unitPrice = up;
    p._sales._current   = 0;
    p._sales._max       = as;
    p._sales._factor    = fh * fy * ft * fc;
}

struct project_state_idle : public project_state {
    virtual void enter(project& p) {       
        std::cout << "idle state entering" << std::endl;
    }
    virtual void update(project& p, int dt) {
    }
    virtual void leave(project& p) {
        std::cout << "idle state leaving" << std::endl;
    }
};

void Company_updateStarting(project& p, int dt);  

struct project_state_starting : public project_state {

    static const int TIMEOUT = 5000;

    virtual void enter(project& p) {
        std::cout << " project \"" << p._name << "\" has started" << std::endl;
        p._timeout = TIMEOUT;
        p._time = 0;
        p._state = EProjectState::Starting;
    }

    virtual void update(project& p, int dt) {
        p._time = std::min(p._time + dt, p._timeout);
        if(p._time >= p._timeout) {
            p._fsm.push(p, project_state_factory::instance().create(EProjectState::Phase1));
        }

        Company_updateStarting(p, dt);
    }

    virtual void leave(project&) {}
};

int Company_getCharacter(const company& co, const std::string& name, character** ptr) {
    auto beg = co._staff.begin();
    auto end = co._staff.end();
    for(auto it=beg; it != end; it++) {
        character* ch = const_cast<character*>(&(*it));
        if(ch->_name == name) {
            return *ptr = ch, 0;
        }
    }
    return -1;
}

void Project_updateFocus(project& p, int dt) {
    company& co             = *p._co;
    project::focus_t& focus = p._focus[p._currentFocus];

    for(int i=0; i < focus._staff.size(); i++) {
        character* ch = nullptr;
        Company_getCharacter(co, focus._staff[i], &ch);
        ch->_time += dt;
        int xp  = ch->_focus[p._currentFocus];
        if(ch->_time >= Character_tickTimeout(xp)) {
            ch->_time = 0;
            
            float cb = Character_chanceToSpawnBugPoint(ch->_it_bugs);
            float ci = Character_chanceToSpawnIdeaPoint(ch->_it_idea);
            float s  = Character_chanceToSpawnPoint(xp);
            
            if(util::frand() < cb) {
                ch->_it_bugs = 0;
                p._points._ideas++;
                std::cout << ch->_name << " spawned a bug point!" << std::endl;
            }
            if(util::frand() < ci) {
                ch->_it_idea = 0;
                p._points._ideas++;
                co._ideas++;
                std::cout << ch->_name << " spawned an idea point!" << std::endl;
            }

            if(util::frand() < s) {
                int n = Character_numPointsToSpawn(xp);
                if(util::frand() < gFocusVector[p._currentFocus]) {
                    ch->_gen_tech[p._currentFocus] += n;
                    p._points._tech += n;
                    std::cout << ch->_name << " generated " << n << " tech points!" << std::endl;
                } else {
                    ch->_gen_design[p._currentFocus] += n;
                    p._points._design += n;
                    std::cout << ch->_name << " generated " << n << " design points!" << std::endl;
                }
            }
        }
    }
}

struct project_state_phase1 : public project_state {
    static const int TIMEOUT = 10000;
    //int focusTime = 0;
    //int focusTimeout = 0;
    //int currentFocus = 0;

    virtual void enter(project& p) {
        
        std::cout << " project \"" << p._name << "\" has entered phase 1" << std::endl;

        p._timeout = TIMEOUT;
        p._time = 0;
        p._state = EProjectState::Phase1;
        Company_save(p._co);
        game_services::instance()._gsm.add(new_phase_screen(p, 0));
    }

    virtual void done(project& p) {
         p._fsm.push(p, project_state_factory::instance().create(EProjectState::Phase2));
    }

    virtual void update(project& p, int dt) {
        p._time = std::min(p._time + dt, p._timeout);
        if(p._time >= p._timeout) {
            return done(p);
             
        }

        int to = (int)(p._focus[p._currentFocus]._allocatedTime * p._timeout + 0.5f);
        p._focusTime += dt;
        if(p._focusTime >= to) {
            p._focusTime = 0;
            p._currentFocus++;
            std::cout << " focus switch[" << rosetta::getString(gFocusString[p._currentFocus - 1]) << " -> " << rosetta::getString(gFocusString[p._currentFocus]) << "]" << std::endl;
            std::cout << " focus timeout is now: " << (int)(p._focus[p._currentFocus]._allocatedTime * p._timeout + 0.5f) << std::endl;
        }

        Project_updateFocus(p, dt);

    }

    virtual void leave(project&) {}
};
struct project_state_phase2  : public project_state_phase1 {
    static const int TIMEOUT = 10000;

    virtual void enter(project& p) {
        

        std::cout << " project \"" << p._name << "\" has entered phase 2" << std::endl;

        p._timeout = TIMEOUT;
        p._time = 0;
        p._state = EProjectState::Phase2;
        Company_save(p._co);

        game_services::instance()._gsm.add(new_phase_screen(p, 1));
    }
    virtual void done(project& p) {
        p._fsm.push(p, project_state_factory::instance().create(EProjectState::Finishing));
    }
/**
    virtual void update(project& p, int dt) {
        p._time = std::min(p._time + dt, p._timeout);
        if(p._time >= p._timeout) {
            p._fsm.push(p, project_state_factory::instance().create(EProjectState::Finishing));
        }

        int to = (int)(p._focus[p._currentFocus]._allocatedTime * p._timeout + 0.5f);
        p._focusTime += dt;
        if(p._focusTime >= to) {
            p._focusTime = 0;
            p._currentFocus++;
            std::cout << " focus switch[" << rosetta::getString(gFocusString[p._currentFocus - 1]) << " -> " << rosetta::getString(gFocusString[p._currentFocus]) << "]" << std::endl;
            std::cout << " focus timeout is now: " << (int)(p._focus[p._currentFocus]._allocatedTime * p._timeout + 0.5f) << std::endl;
        }

    }
*/

    void leave(project&) {}

};

void Company_updateFinishing(project&, int, bool);

struct project_state_finishing  : public project_state {
    virtual void enter(project& p) {
        

        std::cout << " project \"" << p._name << "\" has entered finishing phase" << std::endl;
        p._timeout = 5000;
        p._time = 0;
        p._state = EProjectState::Finishing;
        Company_save(p._co);

    }
    virtual void update(project& p, int dt) {
        p._time = std::min(p._time + dt, p._timeout);
        if(p._time >= p._timeout) {
            p._fsm.push(p, project_state_factory::instance().create(EProjectState::Finished));
        }
        Company_updateFinishing(p, dt, false);
    }
    virtual void leave(project&) {
    }
};

extern gamescreen* new_review_screen(company&, project&);

int Company_numProjectsFinished(company& co, std::vector<project*>* list) {
    if(list != nullptr) 
        list->clear();
    
    int sum = 0;
    for(size_t i=0; i < co._projects.size(); i++) {
        project& p = co._projects[i];
        if(p._state >= EProjectState::Finished) {
            if(list != nullptr) 
                list->push_back(&p);
            sum++;
        }
    }
    return sum;
}

    float Project_calculateScore(project& p) {
        std::cout << "calculating score for project=[" << p._name << "]" << std::endl;
        company& co = *p._co;

        float Topt = gOptimalRatio[p._properties._genre];
        float Dopt = 1.0f - Topt;

        int npoints = p._points._tech + p._points._design;
        float Tgen  = (float)p._points._tech / npoints;
        float Dgen  = (float)p._points._design / npoints;

        float f = std::min(Topt, Tgen) + std::min(Dopt, Dgen);
        float a = (float)csv::get_by_index(p._properties._topic, p._properties._genre) / 100.0f;

        std::cout << "f=[" << f << "]" << std::endl;
        std::cout << "a=[" << a << "]" << std::endl;

        game_services::instance()._a.def_property("@game.ratios.tg", std::to_string(a));

        float b = p._hasPlus ? 0.5f : 0.0f;
        float c = npoints > 500 ? 1.0f : 0.0f;

        float s = f * 5.0f * a + b + c;

        std::vector<project*> list;
        int nprojects = Company_numProjectsFinished(co, &list);

        s = nprojects < 10 ? std::min(s, 8.0f) :
            nprojects < 20 ? std::min(s, 9.5f) :
                             std::min(s, 10.0f);
        
        if(nprojects > 1) {
            project& pp = co._projects[nprojects - 1];
            if(pp._name != p._name) {
                if(pp._properties._topic == p._properties._topic) {
                    if(pp._properties._genre == p._properties._genre) {
                        s *= 0.7f;
                    }
                }
            }
        }
        std::cout << "s=[" << s << "]" << std::endl;
        p._review = s;
        return s;    
    }


struct project_state_finished : public project_state {
    
    virtual void enter(project& p) {
        
                
        std::cout << "project \"" << p._name << "\" has entered finished phase." << std::endl;
        //p._timeout  = std::numeric_limits<int>::max();//INT_MAX;
        //p._time     = 0;
        p._state    = EProjectState::Finished;
        
        Project_calculateScore(p);
        Project_calculateSales(p);

        Company_save(p._co);
        game_services::instance()._gsm.add(new_post_development_screen(p));
    }

    virtual void update(project& p, int dt) {
        p._time = std::min(p._time + dt, p._timeout);

        
        if(p._time >= p._timeout) {
            p._time = 0;
            p._fsm.push(p, project_state_factory::instance().create(EProjectState::OnMarket));
        }
        Company_updateFinishing(p, dt, true);
    }
    virtual void leave(project&) {
    }
};
struct project_state_on_market : public project_state {
    virtual void enter(project& p) {       
        std::cout << "project \"" << p._name << "\" has entered on_market phase." << std::endl;

        //Project_calculateScore(p);
        //Project_calculateSales(p);

        p._state = EProjectState::OnMarket;
        Company_save(p._co);


    }

    int getSalesThisTick(project& p) {
        int dif     = p._sales._max - p._sales._current;
        float dt    = (float)p._time / p._timeout;
        float n     = 0.1f * (util::frandn(0.0f, 0.25f) * 0.5f + 0.5f);
        int nsold   = (int)((dt * dt + n) * dif + 0.5f);

        return nsold;
    }

    virtual void update(project& p, int dt) {

        static int t = 0;
        t += dt;

        p._time = std::min(p._time + dt, p._timeout);

        if(0 == (p._time % MilliSecondsPerGameWeek)) {

            int sales = getSalesThisTick(p);
            p._co->_resources += sales * p._sales._unitPrice;
            p._sales._current += sales;
            std::cout << " project \"" << p._name << "\" @(time: " << (int)((float)p._time / p._timeout * 100) << "%, sales: " << (float)p._sales._current / p._sales._max << ")" << std::endl;
        }

        if(p._time >= 3000) {

            if(p._reviewPending) {
                std::cout << "review time!" << std::endl;

                game_services::instance()._gsm.add(new_review_screen(*p._co, p));
                p._reviewPending = false;


                /**
                 * reset characters point counters.
                 */ 

                company& co = *p._co;
                for(int i=0; i < co._staff.size(); i++) {
                    character& ch = co._staff[i];
                    memset(&ch._gen_tech, 0, sizeof(int) * 6);
                    memset(&ch._gen_design, 0, sizeof(int) * 6);
                    ch._it_bugs = 0;
                    ch._it_idea = 0;
                }
                Company_save(p._co);

            }
        }

        if(p._time >= p._timeout) {
            p._fsm.push(p, project_state_factory::instance().create(EProjectState::EndOfLife));
        }
    }
    virtual void leave(project& p) {
        game_services::instance()._gsm.add(new_end_of_life_screen(p));

    }
};
struct project_state_end_of_life : public project_state {
    virtual void enter(project& p) {
        std::cout << "project " << p._name << " is now EOL." << std::endl;
        p._state = EProjectState::EndOfLife;
        Company_save(p._co);

    }
    virtual void update(project&, int) {
    }
    virtual void leave(project&) {
    }
};

static class register_project_state_factories {
public:
    register_project_state_factories() {
        std::cout << "registering project state factories..." << std::endl;

        atom::register_factory<project_state, project_state_idle, int>(EProjectState::Idle);
        atom::register_factory<project_state, project_state_starting, int>(EProjectState::Starting);
        atom::register_factory<project_state, project_state_phase1, int>(EProjectState::Phase1);
        atom::register_factory<project_state, project_state_phase2, int>(EProjectState::Phase2);
        atom::register_factory<project_state, project_state_finishing, int>(EProjectState::Finishing);
        atom::register_factory<project_state, project_state_finished, int>(EProjectState::Finished);
        atom::register_factory<project_state, project_state_on_market, int>(EProjectState::OnMarket);
        atom::register_factory<project_state, project_state_end_of_life, int>(EProjectState::EndOfLife);
    }
} _;

void Project_update(project& p, int dt) {
    p._fsm.update(p, dt);
}

extern "C" void Project_init(project& p) {
    p._name = std::string();
    p._properties._size     = -1;
    p._properties._genre    = -1;
    p._properties._topic    = -1;
    p._properties._platform = -1;

    p._sales._max       = 0;
    p._sales._current   = 0;
    p._sales._factor    = 0.f;
    p._sales._unitPrice = 0.f;

    for(int i=0; i < 6; i++) {
        p._focus[i]._staff.clear();
        p._focus[i]._allocatedTime = 0.0f;
    }

    p._points._bugs     = 0;
    p._points._ideas    = 0;
    p._points._tech     = 0;
    p._points._design   = 0;

    p._time     = 0;
    p._timeout  = 0;
    p._review   = -1337.0f;
    p._state    = EProjectState::Idle;
    p._hasPlus  = false;
    p._reviewPending = false;
    //p._fsm.push(p, new project_state_idle);
    p._co = nullptr;
}

void Project_deserialize(project& p, cJSON* json) {
    if(!strcmp(json->string, "state"))      p._state            = json->valueint;
    if(!strcmp(json->string, "name"))       p._name             = json->valuestring;
    if(!strcmp(json->string, "time"))       p._time             = json->valueint;
    if(!strcmp(json->string, "timeout"))    p._timeout          = json->valueint;
    if(!strcmp(json->string, "review"))     p._review           = json->valuedouble;
    if(!strcmp(json->string, "hasPlus"))    p._hasPlus          = json->valueint;
    if(!strcmp(json->string, "focusTime"))  p._focusTime        = json->valueint;
    if(!strcmp(json->string, "currentFocus")) p._currentFocus   = json->valueint;
    if(!strcmp(json->string, "reviewPending")) p._reviewPending = json->valueint;
    if(!strcmp(json->string, "bugs"))       p._points._bugs     = json->valueint;
    if(!strcmp(json->string, "ideas"))      p._points._ideas    = json->valueint;
    if(!strcmp(json->string, "tech"))       p._points._tech     = json->valueint;
    if(!strcmp(json->string, "design"))     p._points._design   = json->valueint;
   


    if(!strcmp(json->string, "properties")) {
        cJSON* ptr = json->child;
        while(ptr) {
            if(!strcmp("topic", ptr->string))       p._properties._topic    = ptr->valueint;
            if(!strcmp("genre", ptr->string))       p._properties._genre    = ptr->valueint;
            if(!strcmp("platform", ptr->string))    p._properties._platform = ptr->valueint;
            if(!strcmp("size", ptr->string))        p._properties._size     = ptr->valueint;
                       
            ptr = ptr->next;
        }
    }
    if(!strcmp(json->string, "sales")) {
        cJSON* ptr = json->child;
        while(ptr) {
            if(!strcmp("current", ptr->string))   p._sales._current     = ptr->valueint;
            if(!strcmp("max", ptr->string))       p._sales._max         = ptr->valueint;
            if(!strcmp("factor", ptr->string))    p._sales._factor      = ptr->valueint;
            if(!strcmp("unitPrice", ptr->string)) p._sales._unitPrice   = ptr->valueint;

            ptr = ptr->next;
        }
    }
    if(!strcmp(json->string, "focus")) {
        int n1 = cJSON_GetArraySize(json);
        for(int i=0; i < n1; i++) { // n1 = 6

            cJSON* item = cJSON_GetArrayItem(json, i);

            cJSON* ptr = item->child;
            while(ptr) {
                if(!strcmp("time", ptr->string))    
                    p._focus[i]._allocatedTime = (float)ptr->valuedouble;
                if(!strcmp("staff", ptr->string)) {
                    int n2 = cJSON_GetArraySize(ptr);

                    for(int j=0; j < n2; j++) {
                        std::string name = cJSON_GetArrayItem(ptr, j)->valuestring;
                        p._focus[i]._staff.push_back(name);
                    }
                }
                ptr = ptr->next;
            }
            //p._focus[i]._allocatedTime = item->valuedouble;
        }
    }
    if(json->next)
        Project_deserialize(p, json->next);
}


cJSON* Project_serialize(const project_t& p) {
    cJSON* json = cJSON_CreateObject();
    cJSON* obj = nullptr, *arr = nullptr;

    cJSON_AddNumberToObject(json, "state", p._state);

    cJSON_AddStringToObject(json, "name", p._name.c_str());
    cJSON_AddNumberToObject(json, "time", p._time);
    cJSON_AddNumberToObject(json, "timeout", p._timeout);
    cJSON_AddNumberToObject(json, "review", p._review);

//    cJSON_AddNumberToObject(json, "points", arr= cJSON_CreateArray());
    cJSON_AddNumberToObject(json, "bugs", p._points._bugs);
    cJSON_AddNumberToObject(json, "ideas", p._points._ideas);
    cJSON_AddNumberToObject(json, "tech", p._points._tech);
    cJSON_AddNumberToObject(json, "design", p._points._design);

    cJSON_AddItemToObject(json, "properties", obj = cJSON_CreateObject());
    {
        cJSON_AddNumberToObject(obj, "topic", p._properties._topic);
        cJSON_AddNumberToObject(obj, "genre", p._properties._genre);
        cJSON_AddNumberToObject(obj, "platform", p._properties._platform);
        cJSON_AddNumberToObject(obj, "size", p._properties._size);

    }
    cJSON_AddItemToObject(json, "sales", obj = cJSON_CreateObject());
    {
        cJSON_AddNumberToObject(obj, "current", p._sales._current);
        cJSON_AddNumberToObject(obj, "max", p._sales._max);
        cJSON_AddNumberToObject(obj, "factor", p._sales._factor);
        cJSON_AddNumberToObject(obj, "unitPrice", p._sales._unitPrice);
    }
    cJSON_AddNumberToObject(json, "currentFocus", p._currentFocus);
    cJSON_AddNumberToObject(json, "focusTime", p._focusTime);
    cJSON_AddBoolToObject(json, "hasPlus", p._hasPlus);
    cJSON_AddBoolToObject(json, "reviewPending", p._reviewPending);
    cJSON_AddItemToObject(json, "focus", arr = cJSON_CreateArray());
    for(int i=0; i < 6; i++) {
        cJSON* arr_staff = nullptr;
        cJSON* arr_obj   = nullptr;

        cJSON_AddItemToArray(arr, arr_obj = cJSON_CreateObject());


        const project::focus_t& f = p._focus[i];
        cJSON_AddNumberToObject(arr_obj, "time", f._allocatedTime);
        
        cJSON_AddItemToObject(arr_obj, "staff", arr_staff = cJSON_CreateArray());

        for(int j=0; j < f._staff.size(); j++) {
            cJSON_AddItemToArray(arr_staff, cJSON_CreateString(f._staff[j].c_str()));
        }
        //cJSON_AddItemToObject(arr_obj, "staff", arr_staff);


    }
    return json;
}
/**
void Project_assignStaffTo(project& p, int f, const character* ch) {
    auto beg = p._focus[f]._staff.begin();
    auto end = p._focus[f]._staff.end();

    if(std::find(beg, end, ch) == end) {
        //p._focus[f]._staff.push_back(const_cast<character*>(ch));       
    }
}
*/
//struct company_state_id

using namespace ui;

//typedef state<company> company_state;
typedef atom::factory<gamescreen> gamescreen_factory;

extern "C" cJSON* Company_serialize(const company&);




extern "C" void Company_init(company& co) {
    co._projects.clear();
    co._name = "<company-name>";
    co._staff.clear();
    co._topics.clear();
    co._state = ECompanyState::Idle;
    co._country = -1;
    co._fans  = 0;

    co._resources = 10000;
    co._ideas = 0;
    co._time  = 0;
    co._day   = 0;
    co._week  = 0;
    co._month = 0;
    co._year  = 0;

    
    for(int i=0; i < 4; i++) {
        co._book[i]._time       = 0;
        co._book[i]._timeout    = 0;
        co._book[i]._enabled    = false;
        co._book[i]._is_reading = false;
        co._book[i]._staff      = std::string();
    }

    co._topics.resize(20);
    for(int i=0; i < 20; i++) {
        int tmax = csv::get_num_topics();
        int rr   = rand();

        co._topics[i] = (int)(rr % tmax);
    }
//    co._trend[0] = co._trend[1] = -1;


    co._trend[0] = rand() % csv::get_num_topics();
    co._trend[1] = rand() % csv::get_num_genres();

}

extern "C" int Company_getCurrentProject(company& co, project** ptr) {
    if(co._projects.size()) {
        return *ptr = &co._projects.back(), 0;
    }
    return *ptr = nullptr, -1;
}


int Company_getProject(const company* co, const char* name, project** ptr) {
    auto beg = co->_projects.begin();
    auto end = co->_projects.end();
    
    for(auto it=beg; it != end; it++) {
        if((*it)._name == name) {
            return *ptr = const_cast<project*>(&(*it)), 0;
        }
    }
    return *ptr = nullptr, -1;
}

cJSON* Book_serialize(const book& b) {
    cJSON* json = cJSON_CreateObject();
    cJSON* obj = nullptr;
    cJSON_AddNumberToObject(json, "time", b._time);
    cJSON_AddNumberToObject(json, "timeout", b._timeout);
    cJSON_AddBoolToObject(json, "enabled", b._enabled);
    cJSON_AddBoolToObject(json, "isReading", b._is_reading);
    cJSON_AddStringToObject(json, "staff", b._staff.c_str());
    return json;
}

cJSON* Research_serialize(const research* r) {
    cJSON* res = cJSON_CreateObject();
    cJSON_AddNumberToObject(res, "focus",       r->_focus);
    cJSON_AddNumberToObject(res, "req",         r->_req);
    cJSON_AddNumberToObject(res, "bonus",       r->_bonus);
    cJSON_AddNumberToObject(res, "ideas",       r->_ideas);
    cJSON_AddNumberToObject(res, "cost",        r->_cost);
    cJSON_AddBoolToObject(res,   "unlocked",    r->_unlocked);
    cJSON_AddStringToObject(res, "key",         r->_key.c_str());
    return res;
}

void Research_deserialize(research& r, cJSON* json) {
    cJSON* ptr = json;
    while(ptr) {
        if(!strcmp("focus", json->string))     r._focus = ptr->valueint;
        if(!strcmp("req", json->string))       r._req = ptr->valueint;
        if(!strcmp("bonus", json->string))     r._bonus = ptr->valueint;
        if(!strcmp("ideas", json->string))     r._ideas = ptr->valueint;
        if(!strcmp("cost", json->string))      r._cost = ptr->valueint; 
        if(!strcmp("unlocked", json->string))  r._unlocked = ptr->valueint;
        if(!strcmp("key", json->string))       r._key = ptr->valuestring;        
        ptr = ptr->next;
    }
}

cJSON* Company_serialize(const company& co) {
    cJSON* json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "name", co._name.c_str());
    cJSON_AddStringToObject(json, "id", co._id.c_str());
    cJSON_AddItemToObject(json, "state", cJSON_CreateNumber(co._state));
    cJSON_AddNumberToObject(json, "resources", co._resources);
    cJSON_AddNumberToObject(json, "ideas", co._ideas);
    cJSON_AddNumberToObject(json, "fans", co._fans);
    cJSON_AddNumberToObject(json, "time", co._time);
    cJSON_AddNumberToObject(json, "country", co._country);
    cJSON_AddNumberToObject(json, "day", co._day);
    cJSON_AddNumberToObject(json, "week", co._week);
    cJSON_AddNumberToObject(json, "month", co._month);
    cJSON_AddNumberToObject(json, "year", co._year);


    cJSON* arr = nullptr;

    cJSON_AddItemToObject(json, "books", arr = cJSON_CreateArray());
    for(int i=0; i < 4; i++) {
        cJSON_AddItemToArray(arr, Book_serialize(co._book[i]));
    }
    cJSON_AddItemToObject(json, "staff", arr = cJSON_CreateArray());
    for(unsigned int i=0; i < co._staff.size(); i++) {
        const character& ch = co._staff[i];
        cJSON_AddItemToArray(arr, Character_serialize(ch));
    }
    cJSON_AddItemToObject(json, "projects", arr = cJSON_CreateArray());
    for(unsigned int i=0; i < co._projects.size(); i++) {
        const project& p = co._projects[i];
        cJSON_AddItemToArray(arr, Project_serialize(p));
    }

    cJSON_AddItemToObject(json, "topics", arr = cJSON_CreateArray());
    for(size_t i=0; i < co._topics.size(); i++) {
        cJSON_AddItemToArray(arr, cJSON_CreateNumber(co._topics[i]));
    }

    cJSON_AddNumberToObject(json, "trendTopic", co._trend[0]);
    cJSON_AddNumberToObject(json, "trendGenre", co._trend[1]);
    
    return json;
}

void Book_deserialize(book& b, cJSON* json) {
    if(!strcmp(json->string, "time"))   b._time = json->valueint;
    if(!strcmp(json->string, "timeout"))b._timeout = json->valueint;
    if(!strcmp(json->string, "enabled"))b._enabled = json->valueint != 0;
    if(!strcmp(json->string, "isReading")) b._is_reading = json->valueint != 0;
    if(!strcmp(json->string, "staff")) b._staff = json->valuestring;

    if(json->next) 
        Book_deserialize(b, json->next);
}

extern "C" void Company_deserialize(company& co, cJSON* json) {
    if(!strcmp("name", json->string))   co._name    = json->valuestring;
    if(!strcmp("fans", json->string))   co._fans    = json->valueint;
    if(!strcmp("resources", json->string))co._resources  = json->valueint;
    if(!strcmp("ideas", json->string))  co._ideas   = json->valueint;
    if(!strcmp("id", json->string))     co._id      = json->valuestring;
    if(!strcmp("country", json->string))co._country = json->valueint; 
    if(!strcmp("day", json->string))    co._day     = json->valueint;
    if(!strcmp("week", json->string))   co._week    = json->valueint;
    if(!strcmp("month", json->string))  co._month   = json->valueint;
    if(!strcmp("year", json->string))   co._year    = json->valueint;
    if(!strcmp("time", json->string))   co._time    = json->valueint;
                

    if(!strcmp("state", json->string)) {
        co._state = json->valueint;
        //co._fsm.push(co, atom::factory<company_state,int>::instance().create(co._state));
    }

    if(!strcmp("staff", json->string)) {
        int n = cJSON_GetArraySize(json);

        co._staff.resize(n);
        for(int i=n-1; i >= 0; i--) {
            character& ch = co._staff[i];
            Character_init(ch);
            Character_deserialize(ch, cJSON_GetArrayItem(json, i)->child);
            //co._staff.push_back(ch);
        }
    }
    if(!strcmp("projects", json->string)) {
        int n = cJSON_GetArraySize(json);

        //std::vector<project> temp;
        
        co._projects.resize(n);
            
        for(int i=n-1; i >= 0; i--) {
            //project& p = co._projects[i];
            Project_init(co._projects[i]);
            Project_deserialize(co._projects[i], cJSON_GetArrayItem(json, i)->child);
            co._projects[i]._co = &co;
            //co._projects.push_back(p);
            co._projects[i]._fsm.push(co._projects[i], project_state_factory::instance().create(co._projects[i]._state));
        }
    }

    if(!strcmp("books", json->string)) {
        for(int i=0; i < cJSON_GetArraySize(json); i++) {
            book &b = co._book[i];
            Book_deserialize(b, cJSON_GetArrayItem(json, i)->child);
        }
    }

    if(!strcmp("topics", json->string)) {
        int n = cJSON_GetArraySize(json);
        co._topics.clear();
        co._topics.resize(n);

        for(int i=0; i < n; i++) {
            co._topics[i] = cJSON_GetArrayItem(json, i)->valueint;
        }
    }

    if(!strcmp("trendTopic", json->string))     co._trend[0] = json->valueint;
    if(!strcmp("trendGenre", json->string))     co._trend[1] = json->valueint;
    
    //if(json->child) 
    //    Company_deserialize(co, json->child);
    if(json->next) 
        Company_deserialize(co, json->next);
}

extern "C" void Company_save(const company* co) {
    std::cout << " saving company with id=[" << co->_id << "]" << std::endl;
    std::string fn = atom::getResourcePath() + "assets/savegames/" + co->_id;
    
    cJSON* json     = Company_serialize(*co);
    std::string src = cJSON_Print(json);

    std::ofstream os;
    os.open(fn + ".json");
    if(os.is_open()) {
        os << src;
        os.close();
    }

    BYTE hash[32] = { 0 };

    sha256_ctx ct;
    sha256_init(&ct);
    sha256_update(&ct, (const BYTE*)&src[0], src.length());
    sha256_final(&ct, &hash[0]);

    os.open(fn + ".sha");
    if(os.is_open()) {
        os << hash;
        os.close();
    }
}

extern "C" void Company_load(company& co) {
    std::cout << "Company_load" << std::endl;
    std::string fn = atom::getResourcePath() + "assets/savegames/" + co._id;
    
    cJSON* json     = util::file_to_json(fn + ".json");//Company_serialize(co);
    std::string src = cJSON_Print(json);

    BYTE hash[32] = { 0 };
    BYTE in_hash[32] = { 0 };

    sha256_ctx ct;
    sha256_init(&ct);
    sha256_update(&ct, (const BYTE*)&src[0], src.length());
    sha256_final(&ct, &hash[0]);

    std::ifstream is;
    is.open(fn + ".sha");
    if(is.is_open()) {
        is >> in_hash;
        is.close();
    }

    if(memcmp(hash, in_hash, 32 * sizeof(BYTE))) {
        std::cout << "savegame was corrupted!" << std::endl;
        exit(-10);
    }

    Company_deserialize(co, json);
 
}

static const int xpmax = 15000;
static const int xpmin = 350;
static const int lmin = 1;
static const int lmax = 10;
static const int tmin = 2000;
static const int tmax = 333;

int Character_level(int);

float Character_qualityFactor(int xp) {
    float t = (float)Character_level(xp);
    return 0.37f * 0.2f * (float)sqrt((double)t);
}

int Character_tickTimeout(int xp) {  
    float t = (float)Character_level(xp) / lmax;
    float r = (float)tmin + (tmax - tmin) * t;
    return (int)(r + 0.5f);
}

int Character_numPointsToSpawn(int xp) {
    float t = (float)Character_level(xp);
    float A = (float)sqrt((double)t * 2.0);
    float n = std::max(1.0f, std::min(t + 3.0f, util::frandn(A, 1.5f)));
    return (int)(n + 0.5f);
}

float Character_chanceToSpawnPoint(int xp) {
    float t = Character_qualityFactor(xp);
    return 0.33f + 0.65f * t;
}

float Character_chanceToSpawnIdeaPoint(int it) {
    return (float)it * 0.15f + 0.15f;
}

float Character_chanceToSpawnBugPoint(int it) {
        return (float)it * 0.05f + 0.03f;

}

void Company_updateFinishing(project& p, int dt, bool isFinished) {
    company& co = *p._co;

    for(size_t i=0; i < co._staff.size(); i++) {
        character& ch = const_cast<character&>(co._staff[i]);  
        
        ch._time += dt;
        int to = Character_tickTimeout(ch._xp);
        int timeout = isFinished ? to * 50 : to; 
        
        if(ch._time >= timeout) {
            ch._time = 0;
            float t = Character_chanceToSpawnIdeaPoint(ch._it_idea);
            if(util::frand() < t) {
                ch._it_idea = 0;
                co._ideas++;
                p._points._ideas++;
                std::cout << "idea point spawned!" << std::endl;
            }

            float s = Character_chanceToSpawnPoint(ch._xp);
            if(util::frand() < s) {
                
                int n = Character_numPointsToSpawn(ch._xp);

                if(util::frand() < 0.5f) {
                    // spawn a tech point
                    p._points._tech += n;
                    std::cout << "spawned " << n << " tech points!" << std::endl;
                } else {
                    // spawn a design point
                    p._points._design += n;
                    std::cout << "spawned " << n << "design points!" << std::endl;
                }
            }
        }
    } 
}

void Company_updateStarting(project& p, int dt) {  
    company& co = *p._co;

    for(int i=0; i < co._staff.size(); i++) {
        character& ch = const_cast<character&>(co._staff[i]);
        ch._time += dt;
        if(ch._time >= Character_tickTimeout(ch._xp)) {
            ch._time = 0;
            float s = Character_chanceToSpawnBugPoint(ch._it_bugs);
            if(util::frand() < s) {
            // spawn bug
                std::cout << "bug point spawned!" << std::endl;
                p._points._bugs++;
                ch._it_bugs = 0;
            }
        }   
    }
    Company_updateFinishing(p, dt, false);   
}


extern void Company_update(const company& co_, int dt) {
    company& co = const_cast<company&>(co_);
    
    auto beg = co._projects.begin();
    auto end = co._projects.end();

    for(auto it=beg; it != end; it++) {
        Project_update((*it), dt);
    }


    //co._fsm.update(co, dt);
}

extern int Character_xpRequired(int level) { 
    float n   = (float)(level - lmin) / (lmax - lmin);
    float n2  = n * n;
    float res = n2 * (xpmax - xpmin) + xpmin;

    return (int)(res);
}

extern int Character_level(int xp) { 
#if 1
    float n = (float)(xp - xpmin) / (xpmax - xpmin);
    float n2 = sqrtf(n);// * n;
    float fres = n2 * (lmax - lmin) + lmin;
    return (int)(fres + 0.5f);
#else
    
#endif
}



struct game : public atom::applet {
    atom::application* _app;
    company _co;

    game(atom::application* app) 
    : _app(app)
    {
    }

    void create_default_savegame() { 
        std::string path     = atom::getResourcePath() + "assets/savegames/default.json";
        std::string hashpath = atom::getResourcePath() + "assets/savegames/default.sha";
        std::stringstream ss;

        character ch[3];
        company co;
        Company_init(co);       
        Character_init(ch[0]); 
        Character_init(ch[1]); 
        Character_init(ch[2]); 

        ch[0]._name = "player";
        ch[1]._name = "staff_0";
        ch[2]._name = "staff_1";

        co._staff.push_back(ch[0]);
        co._staff.push_back(ch[1]);
        co._staff.push_back(ch[2]);

        ss << cJSON_Print(Company_serialize(co));
        
        std::string content = ss.str();

        BYTE hash[32] = { 0 };
        sha256_ctx ct;
        sha256_init(&ct);
        sha256_update(&ct, (const BYTE*)&content[0], content.length());
        sha256_final(&ct, &hash[0]);

        std::ofstream os;
        os.open(path.c_str());
        if(os.is_open()) {
            os << content;
            os.close();
        }
        os.open(hashpath);
        if(os.is_open()) {
            os << hash;
            os.close();
        }
    }

    void create_savegame(const std::string& id, const std::string& relpath, const std::string& uid) {
        company co;

        Company_init(co);
        co._id = uid;

        
        //character player;
        //Character_init(player);
        //co._staff.push_back(player);
        
        cJSON* jco = Company_serialize(co);
        std::string src = cJSON_Print(jco);

        std::ofstream os;
        os.open(atom::getResourcePath() + relpath + uid + ".json");
        if(os.is_open()) {
            os << src;
            os.close();
        }

        std::string hashpath = relpath + uid + ".sha";
        BYTE hash[32] = { 0 };
        sha256_ctx ct;
        sha256_init(&ct);
        sha256_update(&ct, (const BYTE*)&src[0], src.length());
        sha256_final(&ct, hash);
        os.open(hashpath);
        if(os.is_open()) {
            os << hash;
            os.close();
        }

    }

    void create_savegame_entry(const std::string& id, const std::string& relpath, const std::string& uid) {

        std::string file_path = relpath + uid;

        std::string res_path = atom::getResourcePath() + "assets/savegames";
        cJSON* json = util::file_to_json(res_path + ".json");

        cJSON_AddStringToObject(json, id.c_str(), file_path.c_str());

        util::json_to_file(json, res_path + ".json");

        BYTE hash[32] = { 0 };
        const char* src = cJSON_Print(json);
        sha256_ctx ct;
        sha256_init(&ct);
        sha256_update(&ct, (const BYTE*)&src[0], strlen(src));
        sha256_final(&ct, hash);

        std::ofstream os;
        os.open(res_path + ".sha");
        if(os.is_open()) {
            os << hash;
            os.close();
        }

        create_savegame(id, relpath, uid);

    }

    void create_savegame_cache() {
        std::string path = atom::getResourcePath() + "assets/savegames";
        std::string path_json = path + ".json";
        std::string path_sha  = path + ".sha";

        for(int i=0; i < 4; i++) {
            create_savegame_entry("<empty>", "assets/savegames/", util::rand_string(8));
        }
        BYTE sha[32] = { 0 };
    }   

    int create() {
     
        csv::load(atom::getResourcePath() + "assets/tables/topic_genre.csv");
        rosetta::loadLocales(atom::getResourcePath() + "assets/translations.json");
 
        create_savegame_cache();
        
        game_services::instance()._nvg = nvgCreateGLES2(NVG_ANTIALIAS | NVG_STENCIL_STROKES | NVG_DEBUG);
        ui::Theme* theme = new ui::Theme(game_services::instance()._nvg);
        Screen::instance().setTheme(theme);
        Screen::instance().initialize(_app);//game_services::instance()._nvg);

               /**
         * 1.) Load textures
         */
        {
            util::nvgLoadTextureLib(game_services::instance()._nvg, atom::getResourcePath() + "assets/textures.json");
        } 
        /**
         * 2.) load sprite sheets.
         */   
        { 
            util::nvgLoadSpriteSheetLib(game_services::instance()._nvg, atom::getResourcePath() + "assets/sprites.json");
        }
        /**
         * 3.) create the achievement list
         */
        {

            std::ifstream is;
            /**
            is.open(atom::getResourcePath() + "assets/tables/achievements_games_en-US.csv");
            if(is.is_open()) {
                std::vector<std::string> list;
                if(util::get_next_line_and_split(is, &list, ',')) {
                    
                    while(util::get_next_line_and_split(is, &list, ',')) {
                        for(size_t i=0; i < list.size(); i++) 
                            std::transform(list[i].begin(), list[i].end(), list[i].begin(), ::tolower);
                      achievement& ach = game_services::instance()._a.def_achievement(list[0]);
                      ach._text = list[1];
                      if(!list[2].empty())
                          ach.withCondition("@game.name", ECondition::Equal, list[2]);
                      //ach.withCondition("@game.name", ECondition::Equal, list[3]);
                      if(!list[4].empty()) 
                          ach.withCondition("@game.topic", ECondition::Equal, list[4]);
                      if(!list[5].empty())
                          ach.withCondition("@game.genre", ECondition::Equal, list[5]);
                      
                    }

                }
                is.close();
            }
            */

            is.open(atom::getResourcePath() + "assets/tables/achievements.csv");
            if(is.is_open()) {
                std::vector<std::string> list;
                if(util::get_next_line_and_split(is, &list, '|')) {

                    while(util::get_next_line_and_split(is, &list, '|')) {
                        for(size_t i=0; i < list.size(); i++) 
                            std::transform(list[i].begin(), list[i].end(), list[i].begin(), ::tolower);
                        achievement& ach = game_services::instance()._a.def_achievement(list[0]);
                        ach._text = list[1];
                        util::find_and_replace(list[3], "\"", "");
                        ach.withCondition(list[2], achievement::get_compare_func(list[3]), list[4]);

                    }
                }
                is.close();
            }

        }
        //game_services::instance()._gsm.add(new_gameplay_screen(_co));
        game_services::instance()._gsm.add(new_mainmenu_screen(_co));

        return -1;
    }

    int destroy() {
        return -1;
    }

    void update(int t, int dt) {

        game_services::instance()._gsm.update(t, dt);
    }

    void draw() {
        int rw, rh;
        SDL_Renderer* ren = _app->getRenderer();
        SDL_GetRendererOutputSize(ren, &rw, &rh);
        float ratio = (float)rw / rh;
        SDL_RenderClear(ren);

        NVGcontext* vg = game_services::instance()._nvg;

        nvgBeginFrame(vg, rw, rh, ratio);
        Screen::instance().draw(vg);
        game_services::instance()._gsm.draw(vg);
        nvgEndFrame(vg);
        SDL_RenderPresent(ren);
    }
};

extern "C"
atom::applet* new_Game(atom::application* app) {
    return new game(app);
}
