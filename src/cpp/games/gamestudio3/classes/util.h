#ifndef UTIL_H_
#define UTIL_H_

#include "common.h"
#include <fstream>
#include <iostream>
#include <map>
#include "nanovg_ext.h"
#include <cstdlib>


#define STR(a)  #a
#define CONCAT(a, b) a##b
#define CONCAT2(a, b) CONCAT(a, b)
//#define PACKAGE_NAME    CONCAT2(com_tfjoy_nl_, APP_NAME)
//#define JNIBRIDGE(x) CONCAT2(CONCAT2(CONCAT2(Java_, PACKAGE_NAME), _), x)
#define UI_WIDGET(a)    CONCAT2(STR(ui_widget_), STR(a))

struct NVGrectangle;
struct NVGcontext;
struct cJSON;

struct sprite_sheet {
    std::map<std::string, NVGrectangle> rect;
    int img;
};

namespace util {

    std::string rand_string(size_t size);
    std::string get_file_contents(const std::string& fn);
    cJSON* stream_to_json(std::istream& is);
    cJSON* file_to_json(const std::string& fn);
    void json_to_file(cJSON* json, const std::string& fn);
    std::array<char, 8> utf8(int c);
    int irand(int = 0, int = RAND_MAX);
    float frand(float = 0.f, float = 1.f);
    float frandn(float u, float s);
    int daysInMonth(struct tm* t);
    std::string to_string_with_precision(float in, int precision);
    void find_and_replace(std::string& source, const std::string& find, const std::string& replace);

    
    int get_next_line_and_split(std::istream& is, std::vector<std::string>* res, char delim = ',');

    void nvgLoadTextureLib(NVGcontext*, const std::string&);
    void nvgLoadSpriteSheetLib(NVGcontext*, const std::string&);
    int nvgGetTextureByKey(const std::string&);
    int nvgGetTextureByIndex(size_t);
    int nvgGetTextureIndex(const std::string&);


    sprite_sheet* nvgGetSpriteSheetByKey(const std::string&);
    sprite_sheet* nvgGetSpriteSheetByIndex(size_t);    

    //int nvgGetSpriteByKey(int sheetId, const std::string& spriteName, int* texId, NVGrectangle* rc);
    //int nvgLoadSpriteSheet(const std::string&);
    //int nvgGetSprite(int id, const std::string&, int* img, NVGrectangle*);
}

#endif
