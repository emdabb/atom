#include "gamescreen.h"
#include "ui/Gui.h"
#include "company.h"
#include "character.h"
#include "rosetta.h"
#include "game_services.h"
#include <iomanip>

using namespace ui;

extern "C" void Character_init(character&);
extern "C" gamescreen* new_select_names_screen(company&);

std::string set_leading(const std::string& in, size_t n, char which) {
    return std::string(n - in.length(), which) + in;
}


struct select_character_screen : gamescreen {
    ref<Window> _win;

    select_character_screen(company& co) 
    {
        
        _win = &Screen::instance().add<Window>("select you character");
        _win->withLayout<GroupLayout>();

        auto& contentPane = _win->add<Widget>().withLayout<BoxLayout>(Orientation::Horizontal, Alignment::Fill);

        for(int i=0; i < static_cast<int>(EGender::Max); i++) { // not being sexist is important (and simpler...)

            auto& widget = contentPane.add<Widget>().withLayout<GroupLayout>();            

            std::string name(32, '\0');
            name = i == static_cast<int>(EGender::Male) ?
                "male" : "female";
           
            int idx = (int)util::frand(1, 12);

            std::stringstream ss;
            ss << name << std::setfill('0') << std::setw(2) <<  std::to_string(idx);

            name = ss.str();

            sprite_sheet* sps = util::nvgGetSpriteSheetByKey("@characters");
            int tex_id = util::nvgGetTextureByIndex(sps->img);

            widget.add<Button>(rosetta::getString(gGenderString[i])).withCallback([&, i] (void) {
                this->setExitCallback([&] (const GameScreenEventArgs&) {
                    _win->dispose();
                    game_services::instance()._gsm.add(new_select_names_screen(co));
                });
                character ch;
                Character_init(ch);
                ch._gender = i;
                co._staff.push_back(ch);
                this->quit();
            });

            auto& iv = widget.add<ImageView>(tex_id);
            iv.setPolicy(ImageView::SizePolicy::Expand);
            iv.setSource(sps->rect[name]);
            
        }
        _win->setSize(_win->preferredSize(game_services::instance()._nvg));
        _win->center(game_services::instance()._nvg);

        auto& diag = Screen::instance().add<MessageDialog>(MessageDialog::Type::Information, "tutorial", rosetta::getString("@tutorial.customize"));

    }
};


extern "C" gamescreen* new_select_character_screen(company& co) {
    return new select_character_screen(co);
}

