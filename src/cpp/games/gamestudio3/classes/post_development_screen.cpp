#include "ui/Gui.h"
#include "panel.h"
#include "gamescreen.h"
#include "company.h"
#include "character.h"
#include "project.h"
#include "rosetta.h"
#include "game_services.h"

using namespace ui;

extern int Character_level(int);
extern int Character_xpRequired(int);

extern "C" int Company_getCurrentProject(company&, project**);
int Company_getAllProjectsWithTopic(company& co, int topic, std::vector<project*>* list) {
    if(list)
        list->clear();
    int count = 0;
    for(auto it=co._projects.begin(); it != co._projects.end(); it++) {
        if((*it)._properties._topic == topic) {
            if(list)
                list->push_back(&(*it));
            count++;
        }
    }
    return count;
}

int Company_getAllProjectsWithGenre(company& co, int genre, std::vector<project*>* list) {
    if(list)
        list->clear();
    int count = 0;
    for(auto it=co._projects.begin(); it != co._projects.end(); it++) {
        if((*it)._properties._genre == genre) {
            if(list)
                list->push_back(&(*it));
            count++;
        }
    }
    return count;
}

int Character_generatedPointsForFocus(const character& ch, int f) {
    return ch._gen_tech[f] + ch._gen_design[f];
}

int Character_generatedPoints(const character& ch) {
    int sum = 0;
    for(int i=0; i < 6; i++) {
        //sum += ch._gen_tech[i];
        //sum += ch._gen_design[i];
        sum += Character_generatedPointsForFocus(ch, i);
    }
    return sum;
}


struct post_development_screen : gamescreen {
    struct progress {
        ref<ProgressBar> widget;
        float a;
        float b;
        int xp;
    };

    std::vector<progress> _progress;

    ref<Window> _win;

    int xp_metrics(int xp, int gen, float bonus, float* f_a, float* f_b) {
        int l0 = Character_level(xp);

        int req_a = Character_xpRequired(l0);
        int req_b = Character_xpRequired(l0 + 1);
        int xa = xp;
        int xadd = (int)((float)gen * 0.7f * bonus);
        int xb = xa + xadd;

        *f_a = (float)(xa - req_a) / (req_b - req_a);
        *f_b = (float)(xb - req_a) / (req_b - req_a);

        return xadd;
    }
    
    post_development_screen(project& p) {
        

        _popup = true;
        //project* p = nullptr;
        //Company_getCurrentProject(co, &p);
        
        company& co = *p._co;
                
        int n   = Company_getAllProjectsWithTopic(co, p._properties._topic, nullptr);
        int n2  = Company_getAllProjectsWithGenre(co, p._properties._genre, nullptr);
        bool tm = co._trend[0] == p._properties._topic && co._trend[1] == p._properties._genre;
        float bonus = 1.0f;
        bonus += n == 1 ? 0.5f : 0.0f;
        bonus += tm ? 0.3 : 0.0f;
        bonus += (n == 1 && n2 == 1) ? 1.0f : 0.0f;

        _win = &Screen::instance().add<Window>();
        _win->withLayout<GroupLayout>();
        auto& top = _win->add<Panel>().withLayout<GroupLayout>();
        auto& middle = _win->add<VScrollPanel>().withFixedHeight(Screen::instance().size().y / 2).add<Widget>().withLayout<BoxLayout>(Orientation::Vertical, Alignment::Fill);
        auto& bottom = _win->add<Widget>().withLayout<GroupLayout>();
        auto& _btn = bottom.add<Button>("ok").withCallback([&] (void) {
            this->setExitCallback([&] (const GameScreenEventArgs&) {
                _win->dispose();
            });
            this->quit();

        });

        if(n == 1) {
            top.add<Label>("new topic! (+50%)");
            if(n2 == 1) 
                top.add<Label>("new topic+genre! (+100%)");
        }
        if(tm) 
            top.add<Label>("trend match! (+30%)");


        auto& panel = middle.add<Panel>().withLayout<GroupLayout>();

        for(int i=0; i < co._staff.size(); i++) {
            character& ch = co._staff[i];
            float f_a, f_b;
            ch._xp += xp_metrics(ch._xp, Character_generatedPoints(ch), bonus, &f_a, &f_b);

            auto& contentTop = panel.add<Widget>().withLayout<GroupLayout>();
            panel.add<Label>(ch._name);
            panel.add<Label>("lvl. " + 
                    std::to_string(Character_level(ch._xp)));

            panel.add<Label>(std::string("[") + std::to_string(ch._xp) + "/" + std::to_string(Character_xpRequired(Character_level(ch._xp) + 1)) + "]");
            panel.add<ProgressBar>().withValue(f_b);

            auto& content = panel.add<Widget>().withLayout<GridLayout>(Orientation::Horizontal, 3, Alignment::Fill);
            
            for(int i=0; i < 6; i++) {
                auto& widget = content.add<Widget>().withLayout<GroupLayout>();

                float ff_a, ff_b;
                ch._focus[i] += xp_metrics(ch._focus[i], Character_generatedPointsForFocus(ch, i), bonus, &ff_a, &ff_b); 

                widget.add<Label>(rosetta::getString(gFocusString[i]));
                widget.add<Label>("lvl. " + 
                    std::to_string(Character_level(ch._focus[i])) + 
                    std::string("[") + 
                    std::to_string(ch._focus[i]) + "/" + 
                    std::to_string(Character_xpRequired(Character_level(ch._focus[i]) + 1)) + "]");

                widget.add<ProgressBar>().withValue(ff_b);
            }
        }
        _win->setSize(_win->preferredSize(game_services::instance()._nvg));
        _win->center(game_services::instance()._nvg);
    }
};


extern "C" gamescreen* new_post_development_screen(project& p) {
    return new post_development_screen(p);
}

