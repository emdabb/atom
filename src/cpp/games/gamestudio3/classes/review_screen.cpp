#include "gamescreen.h"
#include "ui/Gui.h"
#include "company.h"
#include "project.h"
#include "csv.h"
#include "util.h"
#include "game_services.h"
#include <atom/engine/easing.h>
#include <cmath>

#define $GAME_SERVICES game_services::instance()

using namespace ui;

struct book_awarded_screen : gamescreen {
    ref<MessageDialog> diag;

    book_awarded_screen(company& co, book* b) {
        _popup = true;
        diag = &Screen::instance().add<MessageDialog>(MessageDialog::Type::Information, "Book Awarded!", "you were awarded a book, go to the home screen to view your books");
        diag->setCallback([&] (int) {
            this->setExitCallback([&] (const GameScreenEventArgs&) {
                diag->dispose();
            });
            this->quit();
        });
    }
};


extern int Company_getAllProjectsWithTopic(company& co, int topic, std::vector<project*>* list);
extern int Company_getAllProjectsWithGenre(company& co, int topic, std::vector<project*>* list);

struct review_screen : gamescreen {
    ref<Window> _window;
    ref<Label> _label[4];
    float _score[4];
    int _time, _timeout;

    review_screen(company& co, project& p) {
        _time = 0;
        _timeout = 3000;
        _popup = true;

        _window = &Screen::instance().add<Window>();
        _window->withLayout<GroupLayout>();

        float fScore = p._review;//
        //calculateScore(co, p);
        int iScore   = (int)(fScore + 0.5f);

        p._review = fScore;

        awardBook(co, fScore);
        
        std::vector<review*> list;
        $GAME_SERVICES.getReviewsByScore(iScore, &list);

        std::vector<int> review_list;
        review_list.resize(4, -1);
        for(int i=0; i < 4; i++) {
            bool unique = false;
            int item = 0;
            do {
                unique = true;
                item = (int)(util::frand(0.0f, (float)list.size() - 1));
                for(int j=0; j < i; j++) {
                    if(review_list[j] == item) {
                        unique = false;
                        break;
                    }
                }
            } while(!unique);
            review_list[i] = item;
        }

        auto& content = _window->add<Widget>().withLayout<GroupLayout>();

        for(int i=0; i < 4; i++) {
            float score = std::max(0.0f, fScore + (util::frand() * 2.0f - 1.0f) * 1.25f);
            const review* r = list[review_list[i]];

            std::string s1 = r->_text;
            util::find_and_replace(s1, "${GAME_NAME}", p._name);
            util::find_and_replace(s1, "${COMPANY_NAME}", co._name);
            
            s1 = std::string("\"") + s1 + std::string("\"");
            
            auto& pane = content.add<Widget>().withLayout<BoxLayout>(Orientation::Horizontal, Alignment::Middle, 10, 15);

            auto& left = pane.add<Widget>().withLayout<GroupLayout>();

            left.add<Label>(r->_name).withFontSize(32.f);
            left.add<Label>(s1).withFixedWidth(Screen::instance().size().x / 2);
            _label[i] = (Label*)(&pane.add<Label>("0.0").withFontSize(48.f));
            _score[i] = score;
        }

        _window->add<Button>("ok").withCallback([&] (void) {
            setExitCallback([&] (const GameScreenEventArgs&) {
                _window->dispose();
            });
            this->quit();
        });

        _window->setSize(_window->preferredSize($GAME_SERVICES._nvg));
        _window->center($GAME_SERVICES._nvg);
    }

    void awardBook(company& co, float s) {
        if(s >= 6.0f) {
            int type = EBookType::Normal; 
            if(s < 8.0f) {

            } else if(s < 9.5f) {
                type = EBookType::Skilled;

            } else {
                type = EBookType::Master;
            }
            book* b = nullptr;

            for(int i=0; i < 4; i++) {

                b = &co._book[i];

                if((b->_enabled != true) && (b->_is_reading != true)) {
                    break;
                }
            }
            if(b != nullptr) {
                b->_enabled = true;
                b->_type    = type;
                b->_time    = time(NULL);
                b->_timeout =   type == EBookType::Normal ?  600 :
                                type == EBookType::Skilled ? 1800 :
                                                             3600;
                $GAME_SERVICES._gsm.add(new book_awarded_screen(co, b));
            }
        }
    }

    
    void update(int dt, bool coveredByOtherScreen, bool otherScreenHasFocus) {

        if(_state == E_STATE_ACTIVE) {
            _time = std::min(_time + dt, _timeout);
            float t  = 1.0f - (1.0f - (float)_time / _timeout);
            float tt = atom::easing::ease_exp_in_out(t);//powf(t, 6.0f);

            for(int i=0; i < 4; i++) {
                //std::string ss(16, '\0');
                //auto written = std::snprintf(&ss[0], ss.size(), "%.2f", _score[i] * tt);
                std::stringstream ss;
                ss << _score[i] * tt;
                auto written = ss.str();

                //ss.resize(written);

                _label[i]->setCaption(written);
            }
        }

        gamescreen::update(dt, coveredByOtherScreen, otherScreenHasFocus);
    }
};

extern gamescreen* new_review_screen(company& co, project& p) {
    return new review_screen(co, p);
}


