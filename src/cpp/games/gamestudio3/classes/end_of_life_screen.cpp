#include "gamescreen.h"
#include "ui/Gui.h"

#include "util.h"
#include "project.h"
#include "company.h"
#include "rosetta.h"
#include "game_services.h"
#include <string>

using namespace ui;

struct end_of_life_screen : gamescreen {
    ref<MessageDialog> _win;

    explicit end_of_life_screen(project& p) 
    {
        _popup = true;

        std::string msg = "game \"${GAME_NAME}\" is now off of the market, it sold ${COPIES} copies, generating ${REVENUE} in revenue.";

        char buf[8] = { 0 };
        sprintf(buf, "%.2f", p._sales._current * p._sales._unitPrice);

        util::find_and_replace(msg, "${GAME_NAME}", p._name);
        util::find_and_replace(msg, "${COPIES}", std::to_string(p._sales._current));
        util::find_and_replace(msg, "${REVENUE}", buf);

        _win = &Screen::instance().add<MessageDialog>(MessageDialog::Type::Information, "game results", msg);

        //_win->add<Label>(msg).withFixedWidth(Screen::instance().size().x / 4);
        //_win->add<Label>

        //_win->setSize(_win->preferredSize(game_services::instance()._nvg));
        _win->center(game_services::instance()._nvg);

        _win->setDisposeCallback([&] (void) {
            this->quit();
        });         
    }
};

extern "C" gamescreen* new_end_of_life_screen(project& p) {
    return new end_of_life_screen(p);
}
