#ifndef PTREE_H_
#define PTREE_H_

#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <sstream>
#include <queue>
#include <cassert>

template <typename T>
T lexical_cast(const std::string& what) {
    std::stringstream ss(what);
    T res;
    ss >> res >> std::ws;
    return res;
}

template <typename T>
std::string lexical_cast(const T& what) {
    std::stringstream ss;
    ss << what;
    return ss.str();
}

template <typename T>
std::string lexical_cast(T& what) {
    std::stringstream ss;
    ss << what;
    return ss.str();
}


struct ptree {
    typedef std::string key_type;
    typedef std::string val_type;
    typedef std::multimap<key_type, val_type> tree_type;
    tree_type _map;
    std::map<key_type, ptree*>       _tree;

    struct path {
        std::queue<std::string> _q;

        path(const char* str) : path(std::string(str)) {}

        path(const std::string& str) { set(str); }

        std::string pop() {
            if(_q.empty()) 
                return std::string();
            std::string res = _q.front();
            _q.pop();
            return res;
        }

        void set(const std::string& str) {
            size_t start = 0;
            while(start < str.size()) {
                size_t pos = str.find(".", start);
                if(pos == std::string::npos) 
                    pos = str.size();
                _q.push(str.substr(start, pos - start));
                start = pos + 1;
            }
        }
        inline const bool empty() const {
            return _q.empty();
        }
    };

    const ptree* tree(const key_type& k) const {
        std::map<key_type, ptree*>::const_iterator it = _tree.find(k);
        return it != _tree.end() ? (*it).second : nullptr;
    }

    ptree* tree(const key_type& k) {
        return const_cast<ptree*>(static_cast<const ptree*>(this)->tree(k));
    }

    template <typename T>
    const void get(const path& path_, std::vector<T>* res) {
        path copy(path_);
        key_type p = copy.pop();
        
        const ptree* is_tree = tree(p);
        if(is_tree) {
            return is_tree->get<T>(copy, res);
        } else {
            tree_type::const_iterator it = _map.find(p);
            if(it != _map.end()) {
                if(copy.empty()) {
                    res->push_back(lexical_cast<T>((*it).second));
                    return;
                }
            }
        }
    }

    template <typename T>
    const T get(const path& path_, T default_) const {
        path copy(path_);
        key_type p = copy.pop();

        const ptree* is_tree = tree(p);
        if(is_tree) {
            /**
             * recursively search sub-trees.
             */
            return is_tree->get<T>(copy, default_);
        } else {
            /**
             * search for data in this node
             */
            tree_type::const_iterator it = _map.find(p);
            if(it != _map.end()) {
                if(copy.empty()) {
                    const val_type& a = (*it).second;
                    return lexical_cast<T>(a);
                } else {
                  return default_;
                }
            }
        }
    }

    template <typename T>
    T get(const path& path_, T default_) {
        return static_cast<const ptree*>(this)->get<T>(path_, default_);
    }

    template <typename T>
    void insert(const ptree::key_type& key, const T& val) {
        _map.insert(std::pair<ptree::key_type, ptree::val_type>(key, lexical_cast<T>(val)));
    }    

    template <typename T, typename... Args>
    void insert(const ptree::key_type & key, const T& first, const Args&... rest) {
        _map.insert(std::pair<ptree::key_type, ptree::val_type>(key, lexical_cast<T>(first)));
        insert(key, rest...);
    }

    template <typename T, typename... Args>
    void put(const ptree::path& path_, const T& first, const Args&... args) {
        path copy(path_);
        key_type p = copy.pop();
        if(copy.empty()) {
            insert(p, first, args...);
        } else {
            ptree* is_tree = tree(p);
            if(!is_tree) {
                is_tree = _tree[p] = new ptree();
            }
            assert(is_tree);
            is_tree->put(copy, first, args...);
        }
    }

    template <typename T>
    void set(const path& path_, const T& val) {
        path copy(path_);
        key_type p = copy.pop();

        if(copy.empty()) {
            auto beg = _map.lower_bound(p);
            auto end = _map.upper_bound(p);
            for(auto it=beg; it != end; it++) {
                (*it).second = lexical_cast<T>(val);
            }
        } else {
            ptree* is_tree = tree(p);
            if(!is_tree) { 
                is_tree = _tree[p] = new ptree();
            }
            assert(is_tree);
            is_tree->set(copy, val);
        }
    }

    const bool is_valid(const path& path_) const {
        path copy(path_);
        
        std::string p = copy.pop();
        tree_type::const_iterator it = _map.find(p);
        if(it != _map.end()) {
            if(copy.empty()) {
                return true;
            } else {
                const val_type& a = (*it).second;
                //const ptree* is_tree = _tree.find(p) != _tree.end() ? _tree[p] : nullptr;
                const ptree* is_tree = tree(p);
                if(is_tree) {
                    return is_tree->is_valid(copy);
                }
            }
        }
    }
};

//typedef basic_ptree<std::string> ptree;


#endif

