#ifndef VARIANT_H_
#define VARIANT_H_

#include <iostream>
#include <utility>
#include <typeinfo>
#include <type_traits>
#include <string>

/**
 * @short:
 *  the type struct holds the static address of a function per type
 */
template <typename T>
struct type {
    static void id() {} 
};
/**
 * this helper function will reinterpret the address of the static
 * function as a size_t, effectively giving a unique id for the type
 */
template <typename T>
size_t type_of() {
        return reinterpret_cast<size_t>(&type<T>::id); 
}

template <size_t arg1, size_t... others>
struct static_max;

template <size_t arg>
struct static_max<arg> {
    static const size_t value = arg;
};

template <size_t arg1, size_t arg2, size_t... args>
struct static_max<arg1, arg2, args...>
{
    static const size_t value = arg1 >= arg2 
        ? static_max<arg1, args...>::value 
        : static_max<arg2, args...>::value;
};

template <typename... Args>
struct variant_helper;

template <typename F, typename... Args>
struct variant_helper<F, Args...> {
    inline static void destroy(size_t id, const void* data) {
        if(id == type_of<F>()) {
            reinterpret_cast<const F*>(data)->F::~F();
            
        } else {
            variant_helper<Args...>::destroy(id, data);
        }
    }

    inline static void move(size_t old_t, const void* old_v, void* new_v) {
        if(old_t == type_of<F>()) 
            new (new_v) F(std::move(*reinterpret_cast<const F*>(old_v)));
        else
            variant_helper<Args...>::move(old_t, old_v, new_v);
    }

    inline static void copy(size_t old_t, const void* old_v, void* new_v) {
        if(old_t == type_of<F>())
            new (new_v) F(*reinterpret_cast<const F*>(old_v));
        else
            variant_helper<Args...>::copy(old_t, old_v, new_v);
    }
};

template <> 
struct variant_helper<> {
    inline static void destroy(size_t, const void*) {}
    inline static void move(size_t, const void*, void*) {}
    inline static void copy(size_t, const void*, void*) {}
};

template <typename... Args>
class variant {
    static const size_t data_size = static_max<sizeof(Args)...>::value;
    static const size_t data_align= static_max<alignof(Args)...>::value;

    using data_t = typename std::aligned_storage<data_size, data_align>::type;
    using helper_t = variant_helper<Args...>;

    static inline size_t invalid_type() {
        return 0;//tdype_id<void>();
    }

    size_t type_id;
    data_t data;
public:
    variant() 
    : type_id(invalid_type()) 
    { 
        //memset(&data, 0, sizeof(data_t));
    }

    variant(const variant<Args...>& old)
    : type_id(old.type_id) 
    {       
        helper_t::copy(old.type_id, &old.data, &data);
    }

    variant(variant<Args...>&& old) 
    : type_id(old.type_id)
    {
        helper_t::move(old.type_id, &old.data, &data);
    }

    ~variant() {
        helper_t::destroy(type_id, &data);
        
    }
#if 1 
    variant<Args...>& operator = (const variant<Args...>& old) {
        std::cout << "copy operator" << std::endl;
        variant<Args...> copy(old);
    
        helper_t::copy(old.type_id, &old.data, &data);

        return *this;
    }

    variant<Args...>& operator = (variant<Args...>&& old) {
        std::cout << "move operator" << std::endl;
        helper_t::move(old.type_id, &old.data, &data);
    }
#else
    variant<Args...>& operator = (variant<Args...> args) {
        std::swap(type_id, args.type_id);
        std::swap(data, args.data);
        return *this;
    }
#endif
    template <typename T>
    const bool is() const {
        return type_id == type_of<T>();//.hash_code();
    }

    const bool is_valid() const {
        return type_id != invalid_type();
    }

    template <typename T, typename... Tail>
    void set(Tail&&... tail) {
        helper_t::destroy(type_id, &data);
        new (&data) T(std::forward<Tail>(tail)...);
        type_id = type_of<T>();//.hash_code();
    }

    template <typename T>
    const T& get() const {
        if(type_id == type_of<T>())//(T).hash_code())
            return *reinterpret_cast<const T*>(&data);
        else 
            throw std::bad_cast();
    }

    template <typename T>
    const T& as() const {
        return get<T>();
    }
};

#endif
