#include "rosetta.h"
#include "asset_manager.h"
#include <map>
#include <string>
#include <iostream>
#include <cjson/cJSON.h>
#include <cstring>
#include "util.h"
#include <algorithm>


namespace rosetta {
    template <typename T>
    struct dictionary {
        typedef std::map<std::string, T> type;
    };

    std::string _locale;

    //dictionary<std::string, dictionary<std::string>::type>::type translations;
    std::map<std::string, std::map<std::string, std::string> > translations;

    void loadLocales(const std::string& fn) {
        cJSON* json = util::file_to_json(fn.c_str())->child;
        while(json) {
            if(!strcmp(json->string, "locales")) {
                for(int i=0; i < cJSON_GetArraySize(json); i++) {

                    cJSON* node = cJSON_GetArrayItem(json, i)->child;

                    std::string locale  = node->string;
                    std::string abspath = atom::getResourcePath() + node->valuestring;
                    std::ifstream is;
                    is.open(abspath.c_str());
                    if(is.is_open()) {
                        rosetta::loadTranslations(locale, is);
                        is.close();
                    }
                }
            }
            if(!strcmp(json->string, "default")) {
                rosetta::setLocale(json->valuestring);
            }
            json = json->next;
        }
    }

    void setLocale(const std::string& ll) {
        //std::wcout << "User-preferred locale setting is " << std::locale("").name().c_str() << std::endl;
        std::wcout << "Locale setting is " << ll.c_str() << std::endl;

        _locale = ll;
    }

    void loadTranslations(const std::string& locale, std::istream& is) {
        std::istreambuf_iterator<char> eos;
        std::string contents(std::istreambuf_iterator<char>(is),
                             (std::istreambuf_iterator<char>()));

        cJSON* json = cJSON_Parse(contents.c_str())->child;
        while(json) {

            std::map<std::string, std::string>& dict = translations[locale];
            std::string key = json->string;
            std::string val = json->valuestring;
            std::transform(key.begin(), key.end(), key.begin(), ::tolower);
            std::transform(val.begin(), val.end(), val.begin(), ::tolower);
            dict[key] = val;//json->valuestring;

            json = json->next;
        }




    }

    const std::string& getString(const std::string& key, const std::string& defaultValue) {
        return translations[_locale].count(key) ? translations[_locale][key] : defaultValue;
    }
}
