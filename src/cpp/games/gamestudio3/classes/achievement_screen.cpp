#include "achievement.h"
#include "gamescreen.h"
#include "ui/Gui.h"

using namespace ui;

struct achievement_screen : gamescreen {
    ref<MessageDialog> _diag;

    achievement_screen(achievement* a) {

        _popup = true;

        _diag = &Screen::instance().add<MessageDialog>(MessageDialog::Type::Information, 
                "achievement!",
                a->_name,
                a->_text);

        _diag->setDisposeCallback([&] (void) {
            this->setExitCallback([&] (const GameScreenEventArgs&) {
                _diag->dispose();
            });
            this->quit();
        });
    }
};

extern "C" gamescreen* new_achievement_screen(achievement* a) {
    return new achievement_screen(a);
}
