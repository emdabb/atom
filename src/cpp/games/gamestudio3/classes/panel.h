#ifndef PANEL_H_
#define PANEL_H_

#include "ui/Widget.h"

namespace ui {

struct Panel : public Widget {

    bool  mHasCustomColor;
    Color mBackgroundColor;

    Panel(Widget* parent)
        : Widget(parent) 
        , mHasCustomColor(false)
        {
        }
    virtual void draw(NVGcontext* ctx) {

        NVGcolor gradTop = mHasCustomColor ? mBackgroundColor : mTheme->mButtonGradientTopUnfocused;
        NVGcolor gradBot = mTheme->mButtonGradientBotUnfocused;

        NVGpaint bg = nvgLinearGradient(ctx, mPos.x, mPos.y, mPos.x, mPos.y + mSize.y, gradTop, gradBot);
        
        nvgBeginPath(ctx);
        nvgRoundedRect(ctx, mPos.x + 1, mPos.y + 1.0f, mSize.x - 2, mSize.y - 2, mTheme->mButtonCornerRadius * 2 - 1);

        nvgFillPaint(ctx, bg);
        nvgFill(ctx);

        nvgBeginPath(ctx);
        nvgRoundedRect(ctx, mPos.x + 0.5f, mPos.y + 1.5f, mSize.x - 1, mSize.y - 1 - 1, mTheme->mButtonCornerRadius * 2);
        nvgStrokeColor(ctx, mTheme->mBorderLight);
        nvgStroke(ctx);
        

        Widget::draw(ctx);
    }

    void setBackgroundColor(const Color& col) {
        mHasCustomColor = true;
        mBackgroundColor = col;
    }
};
} //ui

#endif
