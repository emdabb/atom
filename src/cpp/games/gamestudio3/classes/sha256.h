#ifndef SHA256_H_
#define SHA256_H_

#include <stdlib.h>
#include <memory.h>
#include <stdint.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#define SHA256_BLOCK_SIZE   32

typedef unsigned char       BYTE;
typedef unsigned int        WORD;
typedef unsigned long long  DWORD;

typedef struct {
    BYTE  data[64];
    WORD  datalen;
    DWORD bitlen;
    WORD  state[8];
} sha256_ctx;

#ifdef __cplusplus
extern "C" {
#endif

void sha256_init(sha256_ctx* ctx);
void sha256_update(sha256_ctx* ctx, const BYTE data[], size_t len);
void sha256_final(sha256_ctx* ctx, BYTE hash[]);

#ifdef __cplusplus
}
#endif

#endif
