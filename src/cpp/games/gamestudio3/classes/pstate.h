#ifndef PSTATE_H_
#define PSTATE_H_

struct pstate {
    typedef ParticleEmitter<pstate> emitter_type;
    typedef emitter_type::Particle  particle_type;

    int     _amount;
    vec2i   _a;
    vec2i   _b;
    int     _type;    
};

void Particle_spawnPoint(pstate::emitter_type& em, const vec2f& at, int type, int amount) {
    pstate::particle_type p;
    p._pos              = at;
    p._time             = 0;
    p._timeout          = type == 0 ? 1000 : type == 1 ? 1000 : type == 2 ? 5000 : 5000;
    p._state._amount    = amount;
    p._state._a         = {(int)at.x, (int)at.y};
    p._state._b         = type == 0 ? (vec2i){(int)at.x, (int)at.y - 100} : 
                          type == 1 ? (vec2i){(int)at.x, (int)at.y + 100} :
                          type == 2 ? (vec2i){(int)at.x + 100, (int)at.y }:
                                      (vec2i){(int)at.x - 100, (int)at.y }; 
    p._state._type      = type;
                                
    em.emit(p);
}


#endif
