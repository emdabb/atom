#include "gamescreen.h"
#include "ui/Gui.h"
#include "company.h"
#include "asset_manager.h"
#include "sha256.h"
#include "game_services.h"


using namespace ui;

extern "C" void Company_deserialize(company&, cJSON*);
extern "C" gamescreen* new_gameplay_screen(company&);
extern "C" gamescreen* new_country_screen(company&);
extern "C" void Company_init(company&);

struct mainmenu_screen : gamescreen {
    ref<Window> _win;
    company& _co;

    struct savegame {
        std::string id;
        std::string path;
    };

    std::vector<savegame>   _dict;
    std::vector<Button>     _savegame;

    void load(const std::string& path) {
        BYTE sha[32];
        std::string json = util::get_file_contents(path + ".json");
        std::string ssha = util::get_file_contents(path + ".sha");
        memcpy(&sha[0], &ssha[0], 32);

        BYTE hash[32] = { 0 };
        sha256_ctx ct;
        sha256_init(&ct);
        sha256_update(&ct, (const BYTE*)&json[0], json.length());
        sha256_final(&ct, &hash[0]);

        if(memcmp(sha, hash, 32)) {
            std::cout << "error: savegame corrupted!" << std::endl;
            exit(-10);
        } else {
            cJSON* j = cJSON_Parse(json.c_str());
            Company_deserialize(_co, j->child);
        }
    } 

    void load_savegame_cache() {
        cJSON* json = util::file_to_json(atom::getResourcePath() + "assets/savegames.json");

        const char* buf = cJSON_Print(json);        
        //BYTE hash[32] = { 0 };
        std::string hash(32, '\0');

        sha256_ctx ct;
        sha256_init(&ct);
        sha256_update(&ct, (const BYTE*)&buf[0], strlen(buf));
        sha256_final(&ct, (BYTE*)&hash[0]);

        std::string check = util::get_file_contents(atom::getResourcePath() + "assets/savegames.sha");
        if(memcmp(&hash[0], &check[0], 32 * sizeof(BYTE))) {
            auto& diag = Screen::instance().add<MessageDialog>(MessageDialog::Type::Warning, "savegame cache was corrupted!");
            return;
        }

        cJSON* ptr = json->child;
        while(ptr != nullptr) {           
            savegame s;
            s.id    = ptr->string;
            s.path  = ptr->valuestring;

            _dict.push_back(s);
            ptr = ptr->next;
        }
    }

    mainmenu_screen(company& co) 
    : _co(co)
    { 
        _win = &Screen::instance().add<Window>();
        _win->withLayout<GroupLayout>();
        _win->setSize(Screen::instance().size());

        load_savegame_cache();

        for(auto it=_dict.begin(); it != _dict.end(); it++) {
            _savegame.push_back(_win->add<Button>((*it).id).withCallback([&, it] (void) {
                std::cout << (*it).id << std::endl;

                std::cout << "loading game with id=[" << (*it).path << "]" << std::endl;

                

                if((*it).id == "<empty>") {
                    std::cout << "creating new savegame..." << std::endl;
                    Company_init(co);
                    //co._id = (*it).path.substr((*it).path.find_last_of('/') + 1, std::string::npos);
                    
                    load((*it).path);
                    
                    this->setExitCallback([&] (const GameScreenEventArgs&) {
                        _win->dispose();
                        game_services::instance()._gsm.add(new_country_screen(co));
                    });
                    this->quit();
                } else {
                                        
                    this->setExitCallback([&, it] (const GameScreenEventArgs&) {
                        std::cout << "disposing window..." << std::endl;
                        _win->dispose();
                        game_services::instance()._gsm.add(new_gameplay_screen(co));
                        load((*it).path);


                    });
                    this->quit();
                }
            }));
        }
    }
};

extern "C" gamescreen* new_mainmenu_screen(company& co) {
    return new mainmenu_screen(co);
}
