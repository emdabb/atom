#include "cJSON_ext.h"
#include <cjson/cJSON.h>
#include <stdio.h>

void cJSON_Hash(const char* src, BYTE hash_out[32]) {
    sha256_ctx ct;
    sha256_init(&ct);
    sha256_update(&ct, (const BYTE*)&src[0], strlen(src));
    sha256_final(&ct, hash_out);

}

cJSON* cJSON_ParseSHA2(const char* src, BYTE hash_out[32]) {
    cJSON_Hash(src, hash_out);    
    return cJSON_Parse(src);
}

const char* cJSON_PrintSHA2(cJSON* src, const BYTE* hash_in) {
    BYTE hash[32] = { 0 };
    const char* res = cJSON_Print(src);
    cJSON_Hash(res, hash);
    if(!memcmp(hash, hash_in, sizeof(BYTE) * 32)) {
        return res;
    }
    return "";
}

void cJSON_SaveToFileSHA2(const char* basepath, cJSON* json) {
    static const int BUFFER_SIZE = 512;
    char bufp_json[BUFFER_SIZE];
    strcat(bufp_json, basepath);
    strcat(bufp_json, ".json");

    char bufp_sha[BUFFER_SIZE];
    strcat(bufp_sha, basepath);
    strcat(bufp_sha, ".sha");

    BYTE hash[32] = { 0 };
    const char* src = cJSON_PrintSHA2(json, hash);

    FILE* fp = fopen(bufp_json, "w");
    if(fp != NULL) {
        fputs(src, fp);
        fclose(fp);
    }

    fp = fopen(bufp_sha, "w");
    if(fp != NULL) {
        fputs(hash, fp);
        fclose(fp);
    }
}

cJSON* cJSON_LoadFromFileSHA2(const char* basepath) {
    cJSON* res;
    static const int BUFFER_SIZE = 512;
    char bufp_json[BUFFER_SIZE];
    strcat(bufp_json, basepath);
    strcat(bufp_json, ".json");

    char bufp_sha[BUFFER_SIZE];
    strcat(bufp_sha, basepath);
    strcat(bufp_sha, ".sha");



    //FILE* fp = ::fopen(
    return res;
}
