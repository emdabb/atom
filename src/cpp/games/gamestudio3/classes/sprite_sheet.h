//
//  sprite_sheet.h
//  bouncy
//
//  Created by emiel Dam on 12/4/15.
//
//

#ifndef sprite_sheet_h
#define sprite_sheet_h

#include <map>
#include <vector>
#include <atom/engine/properties.h>
#include <SDL.h>
#include "types.h"
/**
namespace atom {
    template <typename T = int>
    class sprite_sheet {
        std::map<T, SDL_Rect>   _rects_by_name;
        std::vector<SDL_Rect>   _rects;
        SDL_Texture*            _src;
    public:
        virtual ~sprite_sheet() {

        }

        int addRect(const T& name, const SDL_Rect& rc)
        {
            typename std::map<T, SDL_Rect>::iterator it = _rects_by_name.find(name);
            if(it == _rects_by_name.end()) {
                _rects_by_name[name] = rc;
                _rects.emplace_back(rc);
                return (int)_rects.size();
            }
            return -1;
        }


        const bool has(const T& key) const {
            typename std::map<T, SDL_Rect>::iterator it = _rects_by_name.find(key);
            return it != _rects_by_name.end();
        }

        const SDL_Rect& getRect(const T& name) {
            typename std::map<T, SDL_Rect>::iterator it = _rects_by_name.find(name);
            if(it == _rects_by_name.end()) {
                //std::wcout << "sprite key not found: " << name << std::endl;
                throw std::runtime_error(std::string("sprite key not found"));
            }
            return (*it).second;
        }

        void setTexture(SDL_Texture* tex) {
            _src = tex;
        }

        SDL_Texture* getTexture() {
            return _src;
        }

    };

}
*/

#endif /* sprite_sheet_h */
