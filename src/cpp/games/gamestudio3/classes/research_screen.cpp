#include "gamescreen.h"
#include "ui/Gui.h"
#include "company.h"
#include "game_services.h"
#include "rosetta.h"

using namespace ui;

extern "C" gamescreen* new_mainmenu_screen(company&);
extern "C" gamescreen* new_home_screen(company&);
extern "C" gamescreen* new_research_screen(company&);
extern "C" gamescreen* new_history_screen(company&);




struct research_screen : gamescreen {
    ref<Window> _win;
    ref<Widget> _researchPanel;
    company& _co;

    research_screen(company& co) 
    : _co(co)
    {
        
        _win = &Screen::instance().add<Window>();
        _win->withLayout<GroupLayout>();

        auto& contentPane = _win->add<Widget>();//.withLayout<BorderLayout>();
        auto& north  = contentPane.add<Widget>().withLayout<GroupLayout>();
        auto& east   = contentPane.add<Widget>().withLayout<GroupLayout>();
        auto& south  = contentPane.add<Widget>().withLayout<GroupLayout>();
        auto& west   = contentPane.add<Widget>().withLayout<GroupLayout>();
        auto& center = contentPane.add<Widget>().withLayout<GroupLayout>();

        BorderLayout* layout = reinterpret_cast<BorderLayout*>(contentPane.layout());
        layout->set(&north, BorderLayout::Location::North);
        layout->set(&east,  BorderLayout::Location::East);
        layout->set(&south, BorderLayout::Location::South);
        layout->set(&west,  BorderLayout::Location::West);
        layout->set(&center,BorderLayout::Location::Center);

        

        north.add<Label>("research lab");
        auto& southPane = south.add<Widget>().withLayout<GridLayout>(Orientation::Horizontal, 4, Alignment::Fill);

        _researchPanel = &west.add<Widget>().withLayout<GroupLayout>();
        auto& vscroll = _researchPanel->add<VScrollPanel>().add<Widget>().withLayout<GridLayout>(Orientation::Horizontal, 2, Alignment::Fill);
        for(int j=0; j < 6; j++) {
            for(int i=0; i < 5; i++) {
                research& r = co._research[j][i];
                auto& widget = vscroll.add<Widget>().withLayout<BoxLayout>(Orientation::Vertical, Alignment::Fill);
            
                widget.add<Label>(rosetta::getString(r._key));

                auto& horizontal = widget.add<Widget>().withLayout<BoxLayout>(Orientation::Horizontal, Alignment::Fill);
                horizontal.add<ImageView>();
                horizontal.add<Button>(rosetta::getString("@research"));
            }
        }

        add_buttons(southPane);
    }

    void add_buttons(Widget& w) {
        w.add<Button>("home").withCallback([&] (void) {
            this->setExitCallback([&] (const GameScreenEventArgs&) {
                game_services::instance()._gsm.add(new_home_screen(_co));
                _win->dispose();
            }); 
            this->quit();
        });
        w.add<Button>("history").withCallback([&] (void) {
            this->setExitCallback([&] (const GameScreenEventArgs&) {
                game_services::instance()._gsm.add(new_history_screen(_co));   
                _win->dispose();
            }); 
            this->quit();
        });
        w.add<Button>("research").withCallback([&] (void) {
            this->setExitCallback([&] (const GameScreenEventArgs&) {
                game_services::instance()._gsm.add(new_research_screen(_co));   
                _win->dispose();
            }); 
            this->quit();
        });
        w.add<Button>("main menu").withCallback([&] (void) {
            this->setExitCallback([&] (const GameScreenEventArgs&) { 
                game_services::instance()._gsm.add(new_mainmenu_screen(_co));
                _win->dispose();
            }); 
            this->quit(); 
        });
    }
};

extern "C" gamescreen* new_research_screen(company& co) {
    return new research_screen(co);
}
