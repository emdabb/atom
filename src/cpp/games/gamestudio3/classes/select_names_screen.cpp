#include "gamescreen.h"
#include "ui/Gui.h"
#include "asset_manager.h"
#include "company.h"
#include "character.h"
#include "rosetta.h"
#include "game_services.h"
#include "sha256.h"

extern "C" gamescreen* new_gameplay_screen(company&);
extern "C" gamescreen* new_achievement_screen(achievement*);

extern "C" void Company_save(company&);

extern "C" void cJSON_SaveSecure(const char* basepath, cJSON* json);
extern "C" cJSON* cJSON_LoadSecure(const char* basepath);

extern "C" void cJSON_SaveSecure(const char* basepath, cJSON* json) {
    const char* src = cJSON_Print(json);
    BYTE hash[32] = { 0 };
    sha256_ctx ct;
    sha256_init(&ct);
    sha256_update(&ct, (const BYTE*)&src[0], strlen(src));
    sha256_final(&ct, &hash[0]);

    std::ofstream os;

    std::string path = std::string(basepath) + ".json";
    os.open(path.c_str());
    if(os.is_open()) {
        os << src;
        os.close();
    }

    path = std::string(basepath) + ".sha";
    os.open(path.c_str());
    if(os.is_open()) {
        os << hash;
        os.close();
    }
}

extern "C" cJSON* cJSON_LoadSecure(const char* basepath) {
    std::ifstream is;

    std::string path = std::string(basepath) + ".json";
    std::string src;

    is.open(path.c_str());
    if(is.is_open()) {
        is >> src;
        is.close();
    }

    path = std::string(basepath) + ".sha";
    //src = std::string();
    std::string hash_in;
    is.open(path.c_str());
    if(is.is_open()) {
        is >> hash_in;
        is.close();
    }

    BYTE hash[32] = { 0 };
    sha256_ctx ct;
    sha256_init(&ct);
    sha256_update(&ct, (const BYTE*)&src[0], src.length());
    sha256_final(&ct, &hash[0]);

    if(memcmp(&hash_in[0], hash, 32 * sizeof(BYTE))) {
        exit(-10);
    }    

}

using namespace ui;

struct select_names_screen : gamescreen {
    ref<Window> _win;
    ref<TextBox> _tb1;
    ref<TextBox> _tb2;
    ref<Button> _btn;

    std::string _sname;
    std::string _cname;

    void updateSavegameCache(company& co) {
        std::string uid = co._id;
        std::string fn = atom::getResourcePath() + "assets/savegames";

        cJSON* json = util::file_to_json(fn + ".json");
        cJSON* ptr = json->child;
        while(ptr) {
            std::string path = ptr->valuestring;
            std::string id = path.substr(path.find_last_of('/') + 1, std::string::npos);
            if(id == uid) {
                ptr->string = (char*)co._name.c_str();
                break;
            }
            ptr = ptr->next;
        }

        util::json_to_file(json, fn + ".json");

        std::string src = cJSON_Print(json);

        BYTE hash[32] = { 0 };

        sha256_ctx ct;
        sha256_init(&ct);
        sha256_update(&ct, (const BYTE*)&src[0], src.length());
        sha256_final(&ct, &hash[0]);

        std::ofstream os;
        os.open(fn + ".sha");
        if(os.is_open()) {
            os << hash;
            os.close();
        }
        
    }

    select_names_screen(company& co) 
    {
        _win = &Screen::instance().add<Window>("select names");
        _win->withLayout<GroupLayout>();

        auto& w1 = _win->add<Widget>().withLayout<GroupLayout>();//Orientation::Horizontal, Alignment::Fill);
        auto& w2 = _win->add<Widget>().withLayout<GroupLayout>();//Orientation::Horizontal, Alignment::Fill);
        auto& w3 = _win->add<Widget>().withLayout<GroupLayout>();//Orientation::Horizontal, Alignment::Fill);
 
        w1.add<Label>(rosetta::getString("@game.character.name.text"));
        _tb1 = &w1.add<TextBox>();
        _tb1->setFocusCallback([&] (bool val) {
            if(val) {
                _sname = "";
                _tb1->setValue(_sname);
            }
        });

        _tb1->setCallback([&] (const std::string& str) -> bool {
            _sname = str;
            return true;
        });


        w2.add<Label>(rosetta::getString("@game.company.name.text"));
        _tb2 = &w2.add<TextBox>();
        _tb2->setFocusCallback([&] (bool val) {
            if(val) {
                _cname = "";
                _tb2->setValue(_cname);
            }
        });
        _tb2->setCallback([&] (const std::string& str) -> bool { 
            _cname = str;
            return true;
        });


        _btn = &w3.add<Button>("confirm").withCallback([&] (void) {
            this->setExitCallback([&] (const GameScreenEventArgs&) {
                _win->dispose();
                co._staff[0]._name = _tb1->value();
                co._name = _tb2->value();

                game_services::instance()._a.def_property("@company.name", co._name);
                std::vector<achievement*> list;
                game_services::instance()._a.check_achievements(&list);

                Company_save(co);
                updateSavegameCache(co);

                game_services::instance()._gsm.add(new_gameplay_screen(co));

                for(size_t i=0; i < list.size(); i++) {
                    game_services::instance()._gsm.add(new_achievement_screen(list[i]));
                }
                
            });
            this->quit();
        });
        _win->setSize(_win->preferredSize(game_services::instance()._nvg));
        _win->center(game_services::instance()._nvg);
        
        auto& diag = Screen::instance().add<MessageDialog>(MessageDialog::Type::Information, rosetta::getString("@tutorial"), rosetta::getString("@tutorial.names"));
    }

    const bool is_valid() const {
        return !_cname.empty() && !_sname.empty();//(!_tb1->value().empty() && _tb1->value() != "Untitled") && (!_tb2->value().empty() && _tb2->value() != "Untitled");
    }

    virtual void update(int dt, bool otherScreenHasFocus, bool coveredByOtherScreen) {
        
        gamescreen::update(dt, otherScreenHasFocus, coveredByOtherScreen);

        _btn->setEnabled(is_valid());

    }
};

extern "C" gamescreen* new_select_names_screen(company& co) {
    return new select_names_screen(co);
}
