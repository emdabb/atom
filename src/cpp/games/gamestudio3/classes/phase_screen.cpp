#include "gamescreen.h"
#include "project.h"
#include "company.h"
#include "character.h"
#include "game_services.h"

#include "ui/Gui.h"
#include "panel.h"
#include "rosetta.h"
#include <nanovg.h>
#include <atom/engine/easing.h>
#include <float.h>

extern "C" gamescreen* new_gameplay_screen(company& co);
extern "C" bool Project_hasStaff(const project&, int, const std::string&);

namespace ui {

struct TimeAllocationWidget : public Widget {

    TimeAllocationWidget(Widget* parent) 
    : Widget(parent) {
        _red = nvgRGB(220, 50, 47);
        _green = nvgRGB(133, 153, 0);
        _blue = nvgRGB(38, 139, 210);
    }

    NVGcolor _color;
    NVGcolor _red;// = nvgRGB(220, 50, 47);
    NVGcolor _green;// = nvgRGB(133, 153, 0);
    NVGcolor _blue;// = nvgRGB(38, 139, 210);

    float res[3];

    virtual void draw(NVGcontext* ctx) {
        Widget::draw(ctx);
        vec2i pos = absolutePosition();
        vec2i size= this->size();//_window->size() / 2;
        //float res[3] = { 0.f };
        float x = 0.f;

          //NVGpaint paint = nvgCreatePaint();

                    
        for(int i=0; i < 3; i++) {



            NVGcolor gradTop = i == 0 ? _red : i == 1 ? _green : _blue;//nvgRGBA(i == 0 ? 255 : 0, i == 1 ? 255 : 0, i == 2 ? 255 : 0, 255);
            NVGcolor gradBot = nvgRGBA(28, 28, 28, 128);

            NVGpaint bg = nvgBoxGradient(ctx, mPos.x + 1, mPos.y + 1, mSize.x-2, mSize.y, 3, 4, gradTop, Color(0, 92));

            float w = size.x * res[i];              
            nvgStrokeWidth(ctx, 1.0f);
            nvgBeginPath(ctx);
            nvgRect(ctx, mPos.x + x, mPos.y, w, mSize.y + 1);//, 5.0f);
            nvgFillPaint(ctx, bg);
            nvgFill(ctx);
            x += w;
        }
    }        
};
} // ui

using namespace ui;

struct phase_screen 
: gamescreen
{
    std::vector<ref<CheckBox> > _cbox;
    ref<Window> win;
    ref<Panel> _panel[3];
    ref<Slider> _slider[3];
    ref<Button> _radio[3];
    ref<TimeAllocationWidget> _indicator;
    float _alloc[3];
    int _selectedFocus = 0;
    int _time = 0;
    int _timeout = 500;
    vec2i _startPos, _centerPos;

    void normalize_focus(float x, float y, float z, float* res) {
        //std::wcout << "--- starting unit test ---" << std::endl;
        //std::wcout << "[x="<<x<<", y="<<y<<", z="<<z<<"]"<< std::endl;
        float sum = (x + y + z);

        float a = std::min(1.f, std::max(0.1f, x / sum));
        float b = std::min(1.f, std::max(0.1f, y / sum));
        float c = std::min(1.f, std::max(0.1f, z / sum));

        sum =  (a + b + c);


        //std::wcout << "[a="<<a<<", b="<<b<<", c="<<c<<"]"<< std::endl;

        a /= sum;
        b /= sum;
        c /= sum;
        //std::wcout << "[a="<<a<<", b="<<b<<", c="<<c<<"]"<< std::endl;
        //std::wcout << "[sum="<<sum<<"]"<<std::endl;
        res[0]=a;
        res[1]=b;
        res[2]=c;
        //assert((a + b + c) == 1.0f);
    }

    void checkboxes(project& p, int i) {
        company& co = *p._co;
/**
        for(int j=0; j < _cbox.size(); j++) {
            _cbox[j]->setChecked(false);
        }
*/
        for(int j=0; j < co._staff.size(); j++) {
            _cbox[j]->setChecked(Project_hasStaff(p, i, co._staff[j]._name));
        }
    }

    void Project_addCharacterTo(project& p, int i, const std::string& name) {
        p._focus[i]._staff.push_back(name);
        std::cout << "character " << name << " was added to project: " << p._name << std::endl;
    }

    void Project_removeCharacterFrom(project& p, int i, const std::string& name) {
        auto beg = p._focus[i]._staff.begin();
        auto end = p._focus[i]._staff.end();
        auto it = std::find(beg, end, name);
        if(it != end) {
            p._focus[i]._staff.erase(it);
        }
    }

    phase_screen(project& p, int id) {
        _popup = true;
        _time_on = _time_off = 500;
        company* co = p._co; 
        _alloc[0] = _alloc[1] = _alloc[2] = 0.1f;

        win = &Screen::instance().add<Window>();
        win->withLayout<GroupLayout>();
        auto& contentPane = win->add<Widget>().withLayout<GridLayout>(Orientation::Horizontal, 3, Alignment::Minimum);

        {
            auto& west = contentPane.add<Widget>().withLayout<GroupLayout>();

            for(int i=0; i < co->_staff.size(); i++) {
                character* ch = &co->_staff[i];
                std::string name = ch->_name;
                _cbox.push_back(&west.add<CheckBox>(ch->_name).withCallback([this, &p, name, i, id] (bool val) {
                    if(val) {
                        Project_addCharacterTo(p, id * 3 + _selectedFocus, name);
                    } else {
                        Project_removeCharacterFrom(p, id * 3 + _selectedFocus, name);
                    }                
                }));
            }
        }

        auto& center = contentPane.add<Widget>().withLayout<BoxLayout>(Orientation::Vertical, Alignment::Fill, 5, 5);

        for(int i=0; i < 3; i++) {
            _panel[i] = &center.add<Panel>();
            _panel[i]->withLayout<GroupLayout>();

            _panel[i]->add<Label>(rosetta::getString(gFocusString[id * 3 + i]));
            _slider[i] = &_panel[i]->add<Slider>()
                .withValue(0.1f)
                .withHighlightedRange(0.f, 0.1f);

            _slider[i]->setCallback([this, i] (float val) {
                float b     = std::max(0.1f, val);
                _alloc[i]   = b;
                _slider[i]->setValue(b);
                _slider[i]->setHighlightedRange(std::pair<float, float>(0.f, b));
            });
            _radio[i]  = &_panel[i]->add<Button>("select").withCallback([this, i, id, &p] (void) {
                this->_selectedFocus = i;
                checkboxes(p, i + id * 3);
            });
        }

        _indicator = &win->add<TimeAllocationWidget>();
        _indicator->setFixedHeight(16);

        auto& btn_start = win->add<Button>("start").withCallback([this, id, &p] (void) {
            this->setExitCallback([&] (const GameScreenEventArgs& args) {
                win->dispose(); 
            });
            normalize_focus(_alloc[0], _alloc[1], _alloc[2], _indicator->res);

            p._focus[id * 3 + 0]._allocatedTime = _indicator->res[0];
            p._focus[id * 3 + 1]._allocatedTime = _indicator->res[1];
            p._focus[id * 3 + 2]._allocatedTime = _indicator->res[2];

            int fplus = static_cast<int>(gFocusPlus[p._properties._genre]);
            int pi = -1;
            float tmax = -FLT_MAX;
            for(int i=0; i < 3; i++) {
                if(_indicator->res[i] > tmax) {
                    tmax = _indicator->res[i];
                    pi = id * 3 + i;
                }
            }
            p._hasPlus = p._hasPlus ? true : pi == fplus; 
            std::cout << " project plus=[" << (p._hasPlus ? "true" : "false") << "]" << std::endl;
            this->quit();
        });
        win->setSize(win->preferredSize(game_services::instance()._nvg));
        win->center(game_services::instance()._nvg);
        _centerPos = win->position();
        _startPos.x = -win->size().x;
        _startPos.y = _centerPos.y;
        win->setPosition(_startPos);
    }

    void update(int dt, bool otherScreenHasFocus, bool coveredByOtherScreen) {
        gamescreen::update(dt, otherScreenHasFocus, coveredByOtherScreen);

        normalize_focus(_alloc[0], _alloc[1], _alloc[2], _indicator->res);
        //memcpy(_indicator->res, _alloc, sizeof(_alloc));

        for(int i=0; i < 3; i++) {
            if(_selectedFocus == i) {
                ((Panel*)_panel[i])->setBackgroundColor(_panel[i]->theme()->mButtonGradientTopUnfocused);
            } else {
                ((Panel*)_panel[i])->setBackgroundColor(_panel[i]->theme()->mButtonGradientBotUnfocused);
            }
        }

        if(_state != E_STATE_TRANSITION_OFF) {
            _time = std::min(_time + dt, _timeout);
            float t = 1.0f - transition();//(float)_time / _timeout;
            float tt = atom::easing::ease_elastic_out(t);
            win->setPosition(_startPos + (_centerPos - _startPos) * tt);
        }
    }
};

extern gamescreen* new_phase_screen(project& p, int i) {
    return new phase_screen(p, i);
}
