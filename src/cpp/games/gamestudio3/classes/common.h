#ifndef COMMON_H_
#define COMMON_H_

#include <string>
#include <vector>
#include <array>

#ifdef __cplusplus
# define DLL_EXPORT extern "C"
#else
# define DLL_EXPORT extern 
#endif

#define __export__ DLL_EXPORT

//#define GAMEWEEK_IN_MS  (25000)

static const int SecondsPerWeek = 604800;
static const int MilliSecondsPerWeek = SecondsPerWeek * 1000;
static const int SecondsPerGameWeek = 10;//15;//25;
//static const int SecondsPerGameWeek = 4;
static const int MilliSecondsPerGameWeek = SecondsPerGameWeek * 1000;
static const int MilliSecondsPerGameDay  = MilliSecondsPerGameWeek / 7;

static const float SecondsToDays = 86.4f;
static const float SecondsToWeeks = SecondsToDays * 7.0f;
static const float ToGameTime = SecondsToWeeks / (float)SecondsPerGameWeek;
static const int OffsetUntillJan1st2005 = 1104534000;
static const int NumberOfWeeksUntillTrendRenewal = 12;

static const float gYearFactor[][21] = {
    { 1.0f, 1.0f, 1.0f, 1.5f, 1.5f, 2.2f, 3.2f, 8.1, 12.f, 14, 15.5f, 17, 19.5, 20.f, 20.5, 22, 22 }, // PC
    { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 2.0f, 3.0f, 8.f, 10.f, 13, 15.0f, 17, 18.f, 19.f, 20.f, 20, 22 }, // iOS
    { 1.0f, 1.0f, 1.0f, 1.0f, 0.4f, 1.5f, 2.8f, 8.f, 13.f, 16, 18.5f, 20, 21.f, 22.f, 23.f, 24, 24 }  // Android
};

static const float gFocusVector[] {
    1.0f,
    0.0f,
    0.5f,
    0.5f,
    0.7f,
    0.3f
};

enum class EProjectPhase : uint8_t {
    Phase1  = 0,
    Phase2,
    Starting,
    Finishing,
    Finished,
    OnMarket,
    EndOfLife,
    Max
};

static const char* gProjectPhaseString[] = {
    "@project.phase.phase1",
    "@project.phase.phase2",
    "@project.phase.starting",
    "@project.phase.finishing",
    "@project.phase.finished",
    "@project.phase.onMarket"
};

static const int gProjectPhaseTimeout[] = {
    3000,
    3000,
    3000,
    -1,
    -1,
    -1
};

static const int gProjectPhaseTimeoutMin[] = {
    20000,
    20000,
    4000
};

static const int gProjectPhaseTimeoutMax[] = {
    30000,
    30000,
    4000
};

enum class EProjectSize : uint8_t {
    Small,
    Medium,
    Large,
    AAA,
    Max
};


static const int gSizeCosts[] = {
    500,
    5000,
    500000,
    5000000
};

static const char* gProjectSizeString[] = {
    "@size.small",
    "@size.medium",
    "@size.large",
    "@size.aaa"
};

enum class EPlatform : uint8_t {
    Steam,
    iOS,
    Android,
    Max
};

static const int gPlatformCosts[] = {
    500,
    700,//4000,
    500,//5000
};


static const float gSystemFactor[] = {
    1.00f,
    1.15f,
    0.85f
};

static const char* gResearchString[][6] = {
    { "engine+1", "engine+2", "engine+3", "engine+4", "engine+5" },
    { "art+1", "art+2", "art+3", "art+4", "art+5" },
    { "storyline+1", "storyline+2", "storyline+3", "storyline+4", "storyline+5" },
    { "gameplay+1", "gameplay+2", "gameplay+3", "gameplay+4", "gameplay+5" },
    { "ai+1", "ai+2", "ai+3", "ai+4", "ai+5" },
    { "sounds+1", "sounds+2", "sounds+3", "sounds+4", "sounds+5" },

};

static const char* gPlatformString[] = {
    "@platform.steam",
    "@platform.ios",
    "@platform.android"
    };


enum class EGender : uint8_t {
    Male,
    Female,
    Max
};

static const char* gGenderString[] = {
    "@game.character.gender.male",
    "@game.character.gender.female"
};

enum class EFocus : uint8_t {
    Engine,
    Art,
    Storyline,
    Gameplay,
    Ai,
    Sounds,
    Max
};

static const char* gFocusString[] = {
    "@game.development.focus.engine",
    "@game.development.focus.art",
    "@game.development.focus.storyline",
    "@game.development.focus.gameplay",
    "@game.development.focus.ai",
    "@game.development.focus.sounds"
};

enum class EGenre : uint8_t {
    Action,
    Adventure,
    Casual,
    Gambling,
    MMORPG,
    RPG,
    Simulation,
    Strategy,
    Max
};

static const EFocus gFocusPlus[] = {
    EFocus::Engine,
    EFocus::Storyline,
    EFocus::Art,
    EFocus::Ai,
    EFocus::Gameplay,
    EFocus::Storyline,
    EFocus::Ai,
    EFocus::Gameplay
};


static const char* gGenreString[] = {   
    "@game.genre.action",
    "@game.genre.adventure",
    "@game.genre.casual",
    "@game.genre.gambling",
    "@game.genre.mmorpg",
    "@game.genre.rpg",
    "@game.genre.simulation",
    "@game.genre.strategy"
};

enum class ECountry : uint8_t {
    Europe,
    Asia,
    NorthAmerica,
    Max
};

static const char* gCountryString[]  = {
    "@game.country.europe",
    "@game.country.asia",
    "@game.country.north-america"
};

static const float gOptimalRatio[] = {
    0.7f,
    0.3f,
    0.25f,
    0.85f,
    0.5f,
    0.4f,
    0.65f,
    0.6f
};



#endif
