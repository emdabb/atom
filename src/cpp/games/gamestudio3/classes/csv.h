#ifndef CSV_H_
#define CSV_H_

#include <string>

namespace csv {
    void load(const std::string&);
    void save(const std::string&);
    //int get(const std::string& t, const std::string& g);
    int get_by_index(int t, int g);
    
    const std::string& get_topic(size_t);
    const std::string& get_genre(size_t);
    size_t get_num_topics();
    size_t get_num_genres();
}

#endif
