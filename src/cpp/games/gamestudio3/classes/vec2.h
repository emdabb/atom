//
//  vec2.h
//  application_name
//
//  Created by emiel Dam on 9/30/15.
//
//

#ifndef vec2_h
#define vec2_h

#include <math.h>


template <typename T>
struct vec2 {
    static const vec2 defaultValue;
    union {
        struct { T x, y; };
        T cell[2];
    };

    template <typename S>
    static vec2<T> convert(const vec2<S>& val) {
        vec2<T> res;
        res.x = (T)val.x;
        res.y = (T)val.y;
        return res;
    }

    static vec2 normalize(const vec2& src) {
        T r = 1.0f / length(src);
        vec2 dst;
        dst.x = src.x * r;
        dst.y = src.y * r;
        return dst;
    }

    static T length(const vec2& src) {
        return (T)sqrt((double)dot(src, src));
    }

    static T dot(const vec2& a, const vec2& b) {
        return a.x * b.x + a.y * b.y;
    }

    static T to_angle(const vec2& a) {
        return atan2f(-a.y, a.x);
    }

    static vec2<T> cross(const vec2& a, const vec2& b) {
        return a.X * b.Y - b.X * a.Y;
    }
    static vec2 rotate(const vec2& in, const vec2& at, T t) {
        vec2 res;
        T c = cos((double)t);
        T s = sin((double)t);

        res.x = at.x + (in.x * c - in.y * s);
        res.y = at.y + (in.x * s + in.y * c);

        return res;
    }


    vec2& operator -= (const vec2& other) {
        x -= other.x;
        y -= other.y;
        return *this;
    }

    vec2 operator - (const vec2& other) const {
        vec2 copy(*this);
        copy -= other;
        return copy;
    }

    vec2 operator + (const vec2& other) const {
        vec2 copy(*this);
        copy += other;
        return copy;
    }

    vec2& operator += (const vec2& other) {
        this->x += other.x;
        this->y += other.y;
        return *this;
    }

    vec2 operator * (float val) const {
        vec2 copy(*this);
        copy *= val;
        return copy;
    }

    vec2& operator *= (float val) {
        this->x *= val;
        this->y *= val;
        return *this;
    }

    vec2 operator / (float val) const {
        vec2 copy(*this);
        copy /= val;
        return copy;
    }

    vec2& operator /= (float val) {
        this->x /= val;
        this->y /= val;
        return *this;
    }

    T& operator [] (int i) {
        return cell[i];
    }

    // T operator [] (size_t i) const {
    //     return cell[i];
    // }
};

template <typename T>
const vec2<T> vec2<T>::defaultValue = { (T)0, (T)0 };

typedef vec2<float> vec2f;
typedef vec2<int> vec2i;

typedef vec2f Vector2f;
typedef vec2i Vector2i;

template<class T, class S>
inline T& operator <<( T &ostream, const vec2<S> &self ) {
    return ostream << "{ \"x\":" << self.x << ", \"y\":" << self.y << "}", ostream;
}



#endif /* vec2_h */
