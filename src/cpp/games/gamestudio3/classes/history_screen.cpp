#include "gamescreen.h"
#include "ui/Gui.h"
#include "company.h"

using namespace ui;

struct history_screen : gamescreen {
    ref<Window> _win;
    company& _co;
        
    history_screen(company& co) 
    : _co(co)
    {
    }

    
};

extern "C" gamescreen* new_history_screen(company& co) {
    return new history_screen(co);
}
