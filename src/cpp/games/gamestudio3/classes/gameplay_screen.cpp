#include "ui/Gui.h"
#include "gamescreen.h"
#include "company.h"
#include "project.h"
#include "rosetta.h"
#include "game_services.h"

using namespace ui;

extern "C" gamescreen* new_project_properties_screen(company& co);
extern "C" gamescreen* new_home_screen(company&);
extern "C" gamescreen* new_achievement_screen(achievement*);
extern "C" int Company_getCurrentProject(company&, project**);
extern void Company_update(const company&, int);


struct gameplay_screen 
: gamescreen 
{
    ref<Window> window;
    company& _co;
    ref<Button> _newProject;
    ref<ProgressBar> _projectProgress;
    ref<Label> _stateLabel;
    ref<Button> _releaseButton;
    ref<Button> _homeButton;

    gameplay_screen(company& co) 
        : _co(co)
    {
        std::vector<achievement*> list;
        game_services::instance()._a.check_achievements(&list);
        for(unsigned int i=0; i < list.size(); i++ ) {
            game_services::instance()._gsm.add(new_achievement_screen(list[i]));
        }

        std::cout << "gameplay screen created" << std::endl;

        //co._projects[i]._fsm.push(co._projects[i], project_state_factory::instance().create(co._projects[i]._state));


//        _time_on = 0;
//        _time_off= 0;
        window = &Screen::instance().add<Window>("");
        window->withLayout<GroupLayout>();
        window->setSize(Screen::instance().size());

        
        _newProject = &window->add<Button>("new project").withCallback([&] (void) { 
            this->setExitCallback([&] (const GameScreenEventArgs&) {
                window->dispose();
                this->getOwner()->add(new_project_properties_screen(co));
            });
            this->quit();
        });
        _projectProgress = &window->add<ProgressBar>();
        _stateLabel = &window->add<Label>("state");

        _releaseButton = &window->add<Button>("release").withCallback([&] (void) {
            project* p = nullptr;
            Company_getCurrentProject(co, &p);
            p->_time = p->_timeout;
            p->_reviewPending = true;            
        });

        _homeButton = &window->add<Button>("home").withCallback([&] (void) {
            this->getOwner()->add(new_home_screen(co));
        });
    }

    const bool projectIsActive(project* p) {
        if(p != nullptr) {
            return (p->_state >= EProjectState::Starting) && (p->_state <= EProjectState::Finished);
        }
        return false;
    }

    const bool projectIsInPhase(project* p) {
        if(nullptr != p) {
            return (p->_state == EProjectState::Phase1) || (p->_state == EProjectState::Phase2);
        }
        return false;
    }

    const bool projectIsFinished(project* p) {
        if(p != nullptr) {
            return (p->_state == EProjectState::Finished);
        }
        return false;
    }

    void set_ui_visibility() {
        project* p = nullptr;

        if(!Company_getCurrentProject(_co, &p)) {
            _projectProgress->setValue((float)p->_time / p->_timeout);
        }
        bool isActive = projectIsActive(p);

        if(isActive) {
            _stateLabel->setCaption(rosetta::getString(gProjectStateString[p->_state]));
        } 

        if(projectIsInPhase(p)) {
            _stateLabel->setCaption(rosetta::getString(gFocusString[p->_currentFocus]));
        }
        _releaseButton->setVisible(projectIsFinished(p));
        _projectProgress->setVisible(isActive);
        _stateLabel->setVisible(isActive);
        _newProject->setEnabled(!isActive);
    }

    void update(int dt, bool otherScreenHasFocus, bool coveredByOtherScreen) {

        static int t = 0;

        t += dt;
        if(t >= 1000) {
            _co._time++;
        }

        if(0 == (t % MilliSecondsPerGameWeek)) {
            _co._week++;
            if(0 == (_co._week % 12)) {
                // renew trend here
            }
        }
       
        if(_state == E_STATE_ACTIVE && !coveredByOtherScreen && !otherScreenHasFocus) {
            Company_update(_co, dt);
        }
        set_ui_visibility();
        gamescreen::update(dt, otherScreenHasFocus, coveredByOtherScreen);
    }
};

extern "C" gamescreen* new_gameplay_screen(company& co) {
    return new gameplay_screen(co);
}

