#ifndef CHARACTER_H_
#define CHARACTER_H_

#include <string>


struct character {
    std::string _name;
    int         _xp;
    int         _focus[6];
    int         _gender;
    int         _gen_tech[6];
    int         _gen_design[6];
    int         _it_bugs; // bug iterator
    int         _it_idea; // idea iterator
    int         _time;
    int         _timeout;
    bool        _hasBoost;
    bool        _hasBoostPlus;
    std::string _project;
    //fsm<character> _fsm;
};

#endif

