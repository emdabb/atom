#include "nanovg_ext.h"
#include <nanovg.h>

APICALL int nvgRectangleIsValid(struct NVGrectangle* rc) {
    if(!rc) 
        return -1;
    if(rc->w <= 0 && rc->h <= 0)
        return -2;
    return 0;
}


APICALL void nvgRenderSprite(struct NVGcontext* vg, int img, struct NVGrectangle* dst, struct NVGrectangle* src) {
    int ix, iy;

    float rx = (float)dst->w / src->w;
    float ry = (float)dst->h / src->h;

    float ox = (float)dst->x - src->x * rx;
    float oy = (float)dst->y - src->y * ry;
    
    nvgImageSize(vg, img, &ix, &iy);

    float fx = ox;// * rx;//dst.x - src.x;//(float)src.x - dst.x;
    float fy = oy;// * ry;//dst.x - src.x;//(float)src.y - dst.h;
    float fw = ix * rx;
    float fh = iy * ry;
  
    NVGpaint paint = nvgImagePattern(vg, fx, fy, fw, fh, 0.0f, img, 1.0f);
    nvgBeginPath(vg);
    nvgRect(vg, dst->x, dst->y, dst->w, dst->h);//dst.x, dst.x, dst.w, dst.h);
    nvgFillPaint(vg, paint);
    nvgFill(vg);
#if 0
    nvgBeginPath(vg);
    nvgStrokeColor(vg, nvgRGBA(255,0,255,255));
    nvgRect(vg, dst->x, dst->y, dst->w, dst->h);
    nvgStroke(vg);
#endif
}
