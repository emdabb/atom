#include "util.h"
#include "nanovg_ext.h"
#include <nanovg.h>
#include <cjson/cJSON.h>
#include <cmath>
#include <random>
#include <functional>
#include <iomanip>
#include <sstream>
#include <cassert>
#include <cstring>
#include <map>
#include <string>
#include "asset_manager.h"

namespace util {

std::vector<sprite_sheet> sprite;
std::vector<int> texture;

std::map<std::string, size_t> sprite_id;
std::map<std::string, size_t> texture_id;

void find_and_replace(std::string& source, const std::string& find, const std::string& replace) {
    for(std::string::size_type i = 0; (i = source.find(find, i)) != std::string::npos;) {
        source.replace(i, find.length(), replace);
        i += replace.length();
    }
}

int nvgGetTextureByKey(const std::string& key) {
    std::map<std::string, size_t>::iterator it = texture_id.find(key);;//std::find(texture_id.begin(), texture_id.end(), key);
    if(it == texture_id.end())  
        return -1;
    return nvgGetTextureByIndex((*it).second);
}

int nvgGetTextureByIndex(size_t index) {
    return texture[index];
}

sprite_sheet* nvgGetSpriteSheetByKey(const std::string& key) {
    std::map<std::string, size_t>::iterator it = sprite_id.find(key);
    if(it == sprite_id.end())
        return nullptr;
    return nvgGetSpriteSheetByIndex((*it).second);
}

sprite_sheet* nvgGetSpriteSheetByIndex(size_t id) {
    return &sprite[id];
}


void nvgLoadTextureLib(NVGcontext* ctx, const std::string& fn) {
    cJSON* json = file_to_json(fn);

    int n = cJSON_GetArraySize(json);
    for(int i=0; i < n; i++) {
        cJSON* item = cJSON_GetArrayItem(json, i);

        cJSON* key = item->child;
       
        std::wcout << "  loading: " << key->string << "(\"" << atom::getResourcePath().c_str() << key->valuestring << "\")" << std::endl;
        std::string name = key->string;
        std::string path = atom::getResourcePath() + key->valuestring;
        int id = nvgCreateImage(ctx, path.c_str(), 0);//NVG_IMAGE_GENERATE_MIPMAPS);

        texture.push_back(id);
        texture_id[name] = texture.size() - 1;
    }
}

int nvgGetTextureIndex(const std::string& key) {
    return texture_id[key];
}


sprite_sheet nvgLoadSpriteSheet(const std::string& fn) {
    //sprite_sheet res;
    
    cJSON* json = file_to_json(fn);

    cJSON* ptr = json->child;

    sprite_sheet ss;

    while(ptr) {
        if(!strcmp(ptr->string, "image")) 
            ss.img = nvgGetTextureIndex(ptr->valuestring);

        if(!strcmp(ptr->string, "rects")) {
            int n = cJSON_GetArraySize(ptr);
            for(int i=0; i < n; i++) {
                cJSON* item = cJSON_GetArrayItem(ptr, i);
                cJSON* pp   = item->child;
                std::string  name;
                NVGrectangle rc;
                while(pp) {
                    if(!strcmp("name", pp->string)) 
                        name = pp->valuestring;
                    if(!strcmp("x", pp->string)) 
                        rc.x = pp->valueint;
                    if(!strcmp("y", pp->string))
                        rc.y = pp->valueint;
                    if(!strcmp("width", pp->string))
                        rc.w = pp->valueint;
                    if(!strcmp("height", pp->string))
                        rc.h = pp->valueint;

                    pp = pp->next;
                }
                ss.rect[name] = rc;
            }
        }
        ptr = ptr->next;
    }

    /**
    int n = cJSON_GetArraySize(json);
    for(int i=0; i < n; i++) {

        cJSON* item = cJSON_GetArrayItem(json, i);
        cJSON* ptr = item->child;
        while(ptr) {
            if(!strcmp("name", json->string)) {
            }
            if(!strcmp("x", json->string)) {
            }
            if(!strcmp("y", json->string)) {
            }
            if(!strcmp("width", json->string)) {
            }
            if(!strcmp("height", json->string)) {
            }
            ptr = ptr->next;
        }
    }
    */
    return ss;
}

void nvgLoadSpriteSheetLib(NVGcontext* ctx, const std::string& fn) {
    cJSON* json = file_to_json(fn);
    int n = cJSON_GetArraySize(json);
    for(int i=0; i < n; i++) {
        cJSON* item = cJSON_GetArrayItem(json, i);
        cJSON* jj   = item->child;

        std::string key = jj->string;
        std::string val = jj->valuestring;

        //cJSON* data = file_to_json(val);
         
        sprite.push_back(nvgLoadSpriteSheet(val));
        sprite_id[key] = sprite.size() - 1;
    }
}




int nvgGetSprite(int id, const std::string& name, int* img, NVGrectangle* rc) {
    //sprite_sheet& sprite = sprites[id]; 
    //NVGrectangle& rect  = sprite.rect[name];
    //*img = sprite.img;
    //rc->x = rect.x;
    //rc->y = rect.y;
    //rc->w = rect.w;
    //rc->h = rect.h;
    return 0;
}

    
int get_next_line_and_split(std::istream& is, std::vector<std::string>* res, char delim) {
    res->clear();
    std::string line;
    std::getline(is, line);

    size_t last = 0;
    size_t next = 0;

    while((next = line.find(delim, last)) != std::string::npos) {
        std::string word = line.substr(last, next - last);
//        std::wcout << word.c_str() << std::endl;
        res->push_back(word);
        last = next + 1;
    }
    std::string word = line.substr(last);
//    std::wcout << word.c_str() << std::endl;

    res->push_back(word);
    return (int)res->size() - 1;
}

    std::string to_string_with_precision(float in, int precision) {
        std::stringstream ss;
        ss << std::setprecision(precision) << in;
        return ss.str();
    }

    std::string rand_string(size_t size)
    {
        std::string str;
        str.resize(size);

        static const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJK...";
        if (size) {
            //--size;
            for (size_t n = 0; n < size; n++) {
                int key = rand() % (int) (sizeof charset - 1);
                str[n] = charset[key];
            }
        }
        return str;
    }

std::string get_file_contents(const std::string& fn) {
    std::string res;
    std::ifstream is;
    is.open(fn.c_str());
    if(is.is_open()) {
        res = std::string(std::istreambuf_iterator<char>(is), (std::istreambuf_iterator<char>()));;
        is.close();
    }
    return res;
}

cJSON* stream_to_json(std::istream& is) {
    std::string res(std::istreambuf_iterator<char>(is), (std::istreambuf_iterator<char>()));
    if(!res.empty()) {
        return cJSON_Parse(res.c_str());
    }
    return nullptr;
}

cJSON* file_to_json(const std::string& fn) {
    std::string content = get_file_contents(fn);
    if(content.empty()) {
        return cJSON_CreateObject();
    }
    return cJSON_Parse(content.c_str());
}

void json_to_file(cJSON* json, const std::string& fn) {
    std::ofstream os;
    os.open(fn.c_str());
    if(os.is_open()) {
        //json_to_stream(json, os);
        os << cJSON_Print(json);
        os.close();
    }
}

std::array<char, 8> utf8(int c) {
    std::array<char, 8> seq;
    int n = 0;
    if (c < 0x80) n = 1;
    else if (c < 0x800) n = 2;
    else if (c < 0x10000) n = 3;
    else if (c < 0x200000) n = 4;
    else if (c < 0x4000000) n = 5;
    else if (c <= 0x7fffffff) n = 6;
    seq[n] = '\0';
    switch (n) {
        case 6: seq[5] = 0x80 | (c & 0x3f); c = c >> 6; c |= 0x4000000;
        case 5: seq[4] = 0x80 | (c & 0x3f); c = c >> 6; c |= 0x200000;
        case 4: seq[3] = 0x80 | (c & 0x3f); c = c >> 6; c |= 0x10000;
        case 3: seq[2] = 0x80 | (c & 0x3f); c = c >> 6; c |= 0x800;
        case 2: seq[1] = 0x80 | (c & 0x3f); c = c >> 6; c |= 0xc0;
        case 1: seq[0] = c;
    }
    return seq;
}

float frandn(float u, float s) {
    float u1, u2, w, mult;
    static float x1, x2;
    static int call = 0;

    if(call) {
        call = !call;
        return (u + s * x2);
    }
    do {
        u1 = ((float)rand() / RAND_MAX) * 2.0f - 1.0f;
        u2 = ((float)rand() / RAND_MAX) * 2.0f - 1.0f;
        w  = u1 * u1 + u2 * u2;
    } while(w >= 1.f || w == 0.0f);

    mult = sqrtf((-2.f * logf(w)) / w);
    x1 = u1 * mult;
    x2 = u2 * mult;

    call = !call;
    return u + s * x1;
}

float frand(float a, float b) {

    using std::mt19937;
    static mt19937::result_type seed = time(0);
    static auto real_rand = std::bind(std::uniform_real_distribution<float>(0,1), mt19937(seed));
    float random = real_rand();
    float dif = b - a;
    float r = random * dif;
    return a + r;
}

int daysInMonth(struct tm* t) {
    int mon     = t->tm_mon + 1;
    int year    = t->tm_year + 1900;
    if(mon == 4 || mon == 6 || mon == 9 || mon == 11) {
        return 30;
    } else if(mon == 2) {
        bool isLeapYear = ((year & 3) == 0 && year % 100 != 0) || (year % 400 == 0);
        if(isLeapYear)
            return 29;
        else
            return 28;
    }
    return 31;
}


} // util

extern "C" std::string rand_string(size_t size) {
    return util::rand_string(size);
}

extern "C" std::array<char, 8> utf8(int c) {
    return util::utf8(c);
}
