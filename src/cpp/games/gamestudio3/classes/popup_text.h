#ifndef POPUP_TEXT_H_
#define POPUP_TEXT_H_

struct popup_text {
    typedef ParticleEmitter<popup_text> emitter_type;
    typedef emitter_type::Particle      particle_type;

    int _amount;
    //std::string _text;
    vec2i _a;
    vec2i _b;
//    NVGcolor _color;

    static void updateCallback(emitter_type& em, particle_type& p, int dt) {     
        float t = atom::easing::ease_bounce_out((float)p._time / p._timeout);

        p._pos.x = p._state._a.x + (p._state._b.x - p._state._a.x) * t;
        p._pos.y = p._state._a.y + (p._state._b.y - p._state._a.y) * t;
    }

    static void drawCallback(const particle_type& p, NVGcontext* vg) {
        float fa    = atom::easing::ease_exp_out(1.0f - (float)p._time / p._timeout);
        uint8_t ia  = (uint8_t)(fa * 255.f);
        std::string str = "$" + std::to_string(p._state._amount);
        NVGcolor color  = p._state._amount < 0 ? nvgRGBA(255,0,0,ia) : nvgRGBA(0,255,0,ia);

        nvgFontFace(vg, "sans");
        nvgFontSize(vg, 20.f);
        nvgTextAlign(vg, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
        nvgFillColor(vg, color);
        nvgText(vg, p._pos.x, p._pos.y, str.c_str(), nullptr);
    }
};


#endif
