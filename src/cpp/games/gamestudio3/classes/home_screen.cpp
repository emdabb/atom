#include "gamescreen.h"
#include "company.h"
#include "project.h"
#include "character.h"
#include "rosetta.h"
#include "csv.h"
#include "ui/Gui.h"
#include "game_services.h"

using namespace ui;

extern "C" gamescreen* new_mainmenu_screen(company&);
extern "C" gamescreen* new_home_screen(company&);
extern "C" gamescreen* new_research_screen(company&);
extern "C" gamescreen* new_history_screen(company&);
extern "C" gamescreen* new_achievement_overview_screen(company& co);

struct home_screen : gamescreen {

    ref<Window> _win;
    company& _co;

    home_screen(company& co) 
    : _co(co)
    {
        _popup = true;

        _win = &Screen::instance().add<Window>();
        _win->withLayout<GroupLayout>();

        auto& contentPane = _win->add<Widget>().withLayout<BorderLayout>();
        auto& north  = contentPane.add<Widget>().withLayout<GroupLayout>();
        auto& east   = contentPane.add<Widget>().withLayout<GroupLayout>();
        auto& south  = contentPane.add<Widget>().withLayout<GroupLayout>();
        auto& west   = contentPane.add<Widget>().withLayout<GroupLayout>();
        auto& center = contentPane.add<Widget>().withLayout<GroupLayout>();

        BorderLayout* layout = reinterpret_cast<BorderLayout*>(contentPane.layout());
        layout->set(&north, BorderLayout::Location::North);
        layout->set(&east,  BorderLayout::Location::East);
        layout->set(&south, BorderLayout::Location::South);
        layout->set(&west,  BorderLayout::Location::West);
        layout->set(&center,BorderLayout::Location::Center);

        north.add<Label>("home");
        north.add<Label>("trend=[" + rosetta::getString(csv::get_topic(co._trend[0])) + ", " + rosetta::getString(csv::get_genre(co._trend[1])) + "]");
        auto& southPane = south.add<Widget>().withLayout<GridLayout>(Orientation::Horizontal, 4, Alignment::Fill);

        auto& btn_achievements = north
            .add<Button>("achievements")
            .withCallback([&] (void) {
                game_services::instance()._gsm.add(new_achievement_overview_screen(co));
        });

        add_buttons(southPane);

        _win->setSize(_win->preferredSize(game_services::instance()._nvg));
        _win->center(game_services::instance()._nvg);

    }

    void add_buttons(Widget& w) {
        w.add<Button>("home").withCallback([&] (void) {
            this->setExitCallback([&] (const GameScreenEventArgs&) {
                game_services::instance()._gsm.add(new_home_screen(_co));
                _win->dispose();
            }); 
            this->quit();
        });
        w.add<Button>("history").withCallback([&] (void) {
            this->setExitCallback([&] (const GameScreenEventArgs&) {
                game_services::instance()._gsm.add(new_history_screen(_co));   
                _win->dispose();
            }); 
            this->quit();
        });
        w.add<Button>("research").withCallback([&] (void) {
            this->setExitCallback([&] (const GameScreenEventArgs&) {
                game_services::instance()._gsm.add(new_research_screen(_co));   
                _win->dispose();
            }); 
            this->quit();
        });
        w.add<Button>("main menu").withCallback([&] (void) {
            this->setExitCallback([&] (const GameScreenEventArgs&) { 
                game_services::instance()._gsm.add(new_mainmenu_screen(_co));
                _win->dispose();
            }); 
            this->quit(); 
        });
    }

};

extern "C" gamescreen* new_home_screen(company& co) {
    return new home_screen(co);
}
