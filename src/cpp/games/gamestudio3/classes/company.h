#ifndef COMPANY_H_
#define COMPANY_H_

#include "fsm.h"
#include <vector>
#include <string>

struct project;
struct character;

struct EBookType { 
    enum {
        Normal,
        Skilled,
        Master,
        Max
    };
};

struct ECompanyState {
    enum {
        Idle,
        StartingNewProject,
        WorkingOnProject,
        FinishingProject
    };
};

typedef struct book {
    unsigned long long _time;
    unsigned long long _timeout;
    int _type;
    bool _enabled;
    bool _is_reading;
    std::string _staff;

} book_t;

typedef struct research {
    int32_t _focus;
    int32_t _req;
    int32_t _bonus;
    int32_t _ideas;
    int32_t _cost;
    bool    _unlocked;
    std::string _key;
} research_t;


struct company {
    std::vector<project>    _projects;
    std::vector<character>  _staff; 
    research   _research[6][5];
   
    std::vector<int> _topics; 

    std::string _name;
    std::string _id;
    int _country;
    int _state;
    int _fans;

    int _trend[2];
    int _resources;
    int _ideas;
    book _book[4];
    int _time;
    int _day;
    int _week;
    int _month;
    int _year;

    //fsm<company>    _fsm;
};

#ifdef __cplusplus
extern "C" {
#endif
int Company_getProject(const company*, const char*, project**);
#ifdef __cplusplus
}
#endif

#endif

