#include "auth.h"
#include <chrono>
#include "sha256.h"
#include <sstream>


namespace auth {
    namespace provider {
        unsigned long long timeout() {
            return 300000;
        }
        unsigned long long now() {
            auto epoch = std::chrono::system_clock::now().time_since_epoch();
            return std::chrono::duration_cast<std::chrono::milliseconds>(epoch).count();
        }

        std::string hash(const std::string& src) {
            BYTE buf[32] = { 0 };
            sha256_ctx ctx;
            sha256_init(&ctx);
            sha256_update(&ctx, (BYTE*)&src[0], src.length());
            sha256_final(&ctx, &buf[0]);
            return std::string((const char*)buf);
        }

        std::string salt() {
            auto n = now() - rand();
            std::stringstream ss;
            ss << n;

            return hash(ss.str());
        }

        std::string serialize(const session& s) {
            return std::string();
        }

        
    }
}
