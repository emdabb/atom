#ifndef ARC4_H_
#define ARC4_H_

#include <string>

namespace vault {
    std::string arc4(const std::string& text, const std::string& pass);
}

#endif
