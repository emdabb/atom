#ifndef PROJECT_H_
#define PROJECT_H_

#include <string>
#include <vector>
#include "fsm.h"

struct company;

struct EProjectState {
    enum {
        Idle,
        Starting,
        Phase1,
        Phase2,
        Finishing,
        Finished,
        OnMarket,
        EndOfLife,
        Max
    };
};

static const char* gProjectStateString[] = {
    "@project.phase.idle",
    "@project.phase.starting",
    "@project.phase.phase1",
    "@project.phase.phase2",
    "@project.phase.finishing",
    "@project.phase.finished",
    "@project.phase.onMarket",
    "@project.phase.endOfLife"
};

typedef struct project {
    //ptree pt;
    typedef struct {
        int _platform;
        int _topic;
        int _genre;
        int _size;
    } properties_t;

    typedef struct {
        int     _current;
        int     _max;
        float   _unitPrice;
        float   _factor;
    } sales_t;

    typedef struct {
        std::vector<std::string>  _staff;
        float       _allocatedTime;
    } focus_t;

    typedef struct {
        int _bugs;
        int _ideas;
        int _tech;
        int _design;
    } points_t;

    std::string     _name;
    properties_t    _properties;
    sales_t         _sales;
    focus_t         _focus[6];
    points_t        _points;
    int             _time;
    int             _timeout;
    int             _state;
    float           _review;
    int             _currentFocus;
    int             _focusTime;
    fsm<project>    _fsm;
    company*        _co;
    bool            _hasPlus;
    bool            _reviewPending;
} project_t;

#include <atom/core/factory.h>
typedef ::state<project> project_state;
typedef atom::factory<project_state, int> project_state_factory;

#endif
