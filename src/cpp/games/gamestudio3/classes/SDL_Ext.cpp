#include <SDL.h>

extern "C" bool SDL_RectContains(const SDL_Rect& rc, const SDL_Point& p) {
    return ((p.x >= rc.x) &&
            (p.x <= (rc.x + rc.w)) &&
            (p.y >= rc.y) &&
            (p.y <= (rc.y + rc.h)));
}
