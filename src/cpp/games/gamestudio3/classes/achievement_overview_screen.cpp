#include "gamescreen.h"
#include "ui/Gui.h"
#include "company.h"
#include "achievement.h"
#include "game_services.h"
#include "panel.h"

using namespace ui;

struct achievement_overview_screen : gamescreen {
    ref<Window> _window;

    achievement_overview_screen(company& co) {
        _popup = true;


        int sw = Screen::instance().size().x;
        int sh = Screen::instance().size().y;

        _window = &Screen::instance().add<Window>("achievements");
        _window->withLayout<GroupLayout>();
        _window->withFixedHeight(sh / 2);



        auto& scroll = _window->add<VScrollPanel>().withFixedHeight(sh / 2).add<Widget>().withLayout<GridLayout>(Orientation::Horizontal, 2, Alignment::Fill);

        std::vector<achievement*> list;
        game_services::instance()._a.get_all(&list);

        for(size_t i=0; i < list.size(); i++) {
            auto& panel = scroll
                .add<Panel>()
                .withLayout<GroupLayout>()
                .add<Widget>()
                .withLayout<BoxLayout>(Orientation::Vertical, Alignment::Fill);

            Color color = list[i]->_unlocked ? Color(255, 255) : Color(128, 255);

            panel.add<Label>(list[i]->_name).withColor(color).withFontSize(48).withFixedWidth(sw / 4);
            panel.add<Label>(list[i]->_text).withColor(color).withFixedWidth(sw / 4);
        }

        _window->add<Button>("close").withCallback([&] (void) {
            this->setExitCallback([&] (const GameScreenEventArgs&) {
               _window->dispose();
            });
            this->quit();
        });

        _window->setSize(_window->preferredSize(game_services::instance()._nvg));
        _window->center(game_services::instance()._nvg);
    }
};

extern "C" gamescreen* new_achievement_overview_screen(company& co) {
    return new achievement_overview_screen(co);
}
