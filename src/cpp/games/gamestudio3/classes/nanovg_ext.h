#ifndef NANOVG_EXT_H_
#define NANOVG_EXT_H_

#if defined(__cplusplus)
#define APICALL extern "C" 
#else
#define APICALL
#endif

struct NVGcontext;

struct NVGrectangle {
    int x, y;
    int w, h;
};

typedef struct NVGrectangle NVGrectangle;

APICALL int nvgRectangleIsValid(struct NVGrectangle* rc);
APICALL void nvgRenderSprite(struct NVGcontext* vg, int tex, struct NVGrectangle* dst, struct NVGrectangle* src);

#endif

