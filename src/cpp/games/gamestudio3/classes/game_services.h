#ifndef GAME_SERVICES_H_
#define GAME_SERVICES_H_

#include <atom/core/singleton.h>
#include <nanovg.h>
#include "gamescreen.h"
#include "achievement.h"
#include <map>

typedef struct review {
    int _score;
    std::string _name;
    std::string _text;
    
} review_t;

struct game_services : public atom::singleton<game_services> {
    NVGcontext* _nvg;
    gamescreen_manager _gsm;
    std::multimap<int, review> _review;
    achieve _a;

//protected:
    game_services();

    void getReviewsByScore(int, std::vector<review*>*);
    
};



#endif


