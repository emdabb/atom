#include "csv.h"
#include "util.h"
#include <sstream>
#include <fstream>
#include <vector>
#include <cassert>
#include <map>
#include <cstdlib>
#include "types.h"
#include <cstring>
#include <iostream>
#include <algorithm>
#include <iostream>
#include <string>
#include <cctype>
namespace csv {


    enum EGameTopic : uint8_t {
        Airplane,
        Bread,
        Business,
        Celebrity,
        City,
        Comic,
        Cowboy,
        Cyberpunk,
        Detective,
        Dinosaurs,
        Dungeon,
        Fantasy,
        Farm,
        Fashion,
        Fruit,
        GameDev,
        Goat,
        Government,
        History,
        Horror,
        Hospital,
        Law,
        Love,
        MartialArts,
        Medieval,
        Military,
        Monsters,
        Music,
        Mystery,
        Mythology,
        Ninja,
        Pet,
        Pinball,
        Pirate,
        Plumbing,
        PostApocalyptic,
        Puzzle,
        Racing,
        Restaurant,
        Robot,
        Romance,
        Samurai,
        School,
        SciFi,
        Space,
        Sports,
        Spy,
        Stickman,
        Superheroes,
        Survival,
        TimeTravel,
        Transport,
        Vikings,
        Vocabulary,
        Zombies,
        MaxGameTopic
    };

   
    int numTopics, numGenres;
//    std::vector<int> values;
    std::vector<std::string> topics;
    std::vector<std::string> genres;

    //int** val;
    std::vector<std::vector<int> > val;



// table["ninja"]["action"]

void load(const std::string& fn) {
    /**
     * TODO: index topic/genre table by int instead of strings
     */
    //values.clear();
    topics.clear();
    genres.clear();

    std::ifstream ifs;
    ifs.open(fn.c_str());
    if(ifs.is_open()) {
        /**
         * read first line to get genres.
         */
//        int** val = nullptr;
        
        
        if(util::get_next_line_and_split(ifs, &genres, ',')) {
            val.resize(genres.size());
            
            for(size_t i=0; i < genres.size(); i++)
                std::transform(genres[i].begin(), genres[i].end(), genres[i].begin(), ::tolower);

            std::vector<std::string> temp;
            int i=0;
            while(util::get_next_line_and_split(ifs, &temp, ',')) {

                std::transform(temp[0].begin(), temp[0].end(), temp[0].begin(), ::tolower);
                topics.push_back(temp[0]);
                


                //val[i++].resize(temp.size() - 1);// = (int*)::malloc(sizeof(int) * (temp.size() - 1));
                
                //for(int j=1; j < temp.size(); j++) {
                for(int k=0; k < genres.size(); k++) {

                    
                        val[k].push_back(std::atoi(temp[k+1].c_str()));// = std::atoi(temp[j].c_str());
                    }
                //}
            }
        }
        ifs.close();
    }
}

int get_by_index(int t, int g) {
    return val[g][t];//t * get_num_genres() + g];
    
}

    const std::string& get_topic(size_t i) {
        return topics[i];


    //    return "";
    }

    const std::string& get_genre(size_t i) {
        return genres[i];
        //return "";
    }
    size_t get_num_topics() {
        return topics.size();
    }
    size_t get_num_genres() {
        //return 0;
        return genres.size();
    }

}
