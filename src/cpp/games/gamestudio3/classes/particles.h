#pragma once

#include "vec2.h"
#include <cstdint>
#include <vector>
#include <functional>
#include <nanovg.h>

template <typename T, size_t N>
class static_array {
    typedef typename std::aligned_storage<sizeof(T), alignof(T)>::type data_t;

    data_t data[N];
    size_t _size;
public:
    template <typename... Args> 
    void emplace_back(Args&&... args) {
        if(_size >= N) 
            throw std::bad_alloc {};
        new (data + _size) T(std::forward<Args>(args)...);
        _size++;
    }

    const T& operator [] (size_t pos) const {
        return *reinterpret_cast<const T*>(data + pos);
    }

    ~static_array() {
        for(size_t i=0; i < _size; i++) 
            reinterpret_cast<const T*>(data + i)->T::~T();
    }
};


template <typename T>
class circular_array {
    std::vector<T>  mList;
    size_t          mStart;
    size_t          mCount;
public:
    circular_array(size_t cap) {
        mList.resize(cap);
    }

    inline void sinc() { mStart = (mStart + 1) % mList.size(); }
    inline void cinc() { mCount++; }


    size_t start() const { return mStart; }
    void setStart(size_t val) { mStart = val % mList.size(); }
    size_t count() const { return mCount; }
    void setCount(size_t val) { mCount = val; }
    size_t capacity() const { return mList.size(); }
   

    inline const size_t index_of(const size_t i) const { return (mStart + i) % mList.size(); }

    T& operator [] (const size_t i) {
        return mList[index_of(i)];//(mStart + i) % mList.size()];
    }

    const T& operator [] (const size_t i) const {
        return mList[index_of(i)];//(mStart + i) % mList.size()];
    }
};

template <typename T>
class ParticleEmitter {
public:
    typedef T state_type;

    struct Particle {
        vec2f       _pos;
        state_type  _state;
        int         _time;
        int         _timeout;
    };
    typedef circular_array<Particle> list_type;
    typedef std::function<void(ParticleEmitter&, Particle&, int)> function_type;
    typedef std::function<void(const Particle&, NVGcontext*)> draw_function;
    typedef Particle particle_type;
public:
    ParticleEmitter(uint32_t capacity)
    : _particle(capacity)
    {
        //_particle.resize(_capacity);
    }

    ~ParticleEmitter()
    {
    }

    void setUpdateCallback(const function_type& fn) { _fun = fn; }

    void setDrawCallback(const draw_function& fn) { _drawfun= fn; }

    void draw(NVGcontext* ren) {
        for(int i=0; i < _particle.count(); i++) {
            const Particle& ref = _particle[i];
            _drawfun(ref, ren);
        }
    }

    void update(int dt) {
        size_t removalCount = 0;
        for(size_t i=0; i < _particle.count(); i++) {
            Particle& ref = _particle[i];

            ref._time += dt;//std::min(ref._time + dt, ref._timeout);

            if(_fun != nullptr) 
                _fun(*this, ref, dt);

            swap(_particle, i - removalCount, i);

            ref = _particle[i - removalCount];

            if(ref._time >= ref._timeout)
                removalCount++;
        }
        _particle.setCount(_particle.count() - removalCount);
    }

    void swap(list_type& list, size_t i0, size_t i1) const {
        Particle tmp = list[i0];
        list[i0] = list[i1];
        list[i1] = tmp;

    }

    void emit(const Particle& p) {
        size_t index;
        if(_particle.count() == _particle.capacity()) {
            index = 0;
            _particle.sinc();//setStart(_particle.start() + 1);
        } else {
            index = _particle.count();
            _particle.cinc();//setCount(_particle.count() + 1);
        }

        Particle& ref   = _particle[index];
        ref._pos        = p._pos;
        ref._time       = p._time;
        ref._timeout    = p._timeout;
        ref._state      = p._state;
    }

    list_type& getParticles() {
        return _particle;
    }

private:
    function_type   _fun;
    draw_function   _drawfun;
    //uint32_t        _capacity;
    //uint32_t        _num;
    //uint32_t        _start;
    list_type       _particle;
};
