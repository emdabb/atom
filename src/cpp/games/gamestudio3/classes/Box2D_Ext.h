#ifndef BOX2D_EXT_H_
#define BOX2D_EXT_H_

//include <cjson/cJSON.h>

struct cJSON;
class b2Body;

b2Body* b2Body_deserialize(cJSON*);

#endif
