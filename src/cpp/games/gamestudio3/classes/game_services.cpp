#include "game_services.h"
#include "asset_manager.h"
#include "util.h"
#include <fstream>

game_services::game_services() 
{
    std::ifstream is;
    is.open(atom::getResourcePath() + "assets/tables/reviews_en-US.csv");
    if(is.is_open()) {
        std::vector<std::string> list;
        while(util::get_next_line_and_split(is, &list)) {
            review r;
            r._score = std::atoi(list[0].c_str());
            r._name  = list[1];
            r._text  = list[2];

            this->_review.insert(std::pair<int, review>(r._score, r));
        }
        is.close();
    }
}

void game_services::getReviewsByScore(int score, std::vector<review*>* list) {
    assert(list);
    list->clear();

    auto beg = _review.lower_bound(score);
    auto end = _review.upper_bound(score);

    for(auto it=beg; it != end; it++) {
        list->push_back(&(*it).second);
    }
}

