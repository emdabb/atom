//
//  assets.h
//  application_name
//
//  Created by emiel Dam on 9/29/15.
//
//

#ifndef atom_asset_manager_h
#define atom_asset_manager_h

//#include "sprite_font.h"
//#include "sprite_sheet.h"
#include "types.h"

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>

#include <atom/core/singleton.h>
#include <map>
#include <string>

#if defined(__IOS__)
#define DEFAULT_SUBDIR "Documents"
#else
#define DEFAULT_SUBDIR ""
#endif

//We need to choose the path separator properly based on which
//platform we're running on, since Windows uses a different
//separator than most systems
#ifdef _WIN32
#define PATH_SEP "\\"
#else
#define PATH_SEP  "/"
#endif

struct cJSON;

namespace atom {


    struct assets : public singleton<assets> {

    };


    std::string getDataPath();
    std::string getResourcePath(const std::string &subDir = DEFAULT_SUBDIR);

    int addMusic(const std::string&, Mix_Music*);
    int addSound(const std::string&, Mix_Chunk*);
    //int addAudio(const std::string& name, Mix_Chunk*);
    int addTexture(const std::string& name, SDL_Texture* tex);
    int addSurface(const std::string& name, SDL_Surface* surf);
    //int addFont(const std::string& name, sprite_font* font);


    int loadSurface(const std::string& name, const std::string& filename);
    int loadFont(const std::string& name, std::istream& is, int size, SDL_Renderer* ren);
    int loadMusic(const std::string&, std::istream&);
    int loadSound(const std::string&, std::istream&);
/**
    sprite_sheet<std::string>* loadSpriteSheet(SDL_Renderer*, std::istream&);
    int addSpriteSheet(const std::string& name, sprite_sheet<std::string>*);
    int getSpriteSheetId(const std::string&);
    sprite_sheet<std::string>* getSpriteSheetById(int);
    sprite_sheet<std::string>* getSpriteSheetByName(const std::string&);
*/
    //int getFontId(const std::string& name);
    //sprite_font* getFontById(int id);
    //sprite_font* getFontByName(const std::string& name);

    int getSurfaceId(const std::string& name);
    SDL_Surface* getSurfaceById(int id);
    SDL_Surface* getSurfaceByName(const std::string& name);

    int getTextureId(const std::string& name);
    SDL_Texture* getTextureById(int id);
    SDL_Texture* getTextureByName(const std::string& name);

    int getMusicId(const std::string& name);
    Mix_Music* getMusicById(int);
    Mix_Music* getMusicByName(const std::string&);

    int getSoundId(const std::string&);
    Mix_Chunk* getSoundById(int);
    Mix_Chunk* getSoundByName(const std::string&);

    //int getAudioId(const std::string& name);



    void loadTextureLibrary(SDL_Renderer*, std::istream&);
    void loadAudioLibrary(std::istream&);
    void loadMusicLibrary(std::istream&);

    void parseJsonNode(cJSON* node, std::map<std::string, std::string>* list);
}

//int SDL_LoadTexture(const char*, const std::istream&);
//int SDL_GetTexture(const char*);


#endif /* assets_h */
