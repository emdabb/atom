#include "gamescreen.h"
#include "ui/Gui.h"
#include "company.h"
#include "rosetta.h"
#include "game_services.h"

using namespace ui;

extern "C" gamescreen* new_select_character_screen(company&);

struct country_screen : gamescreen {
    ref<Window> _win;
    ref<Button> _btn;
    company& _co;

    country_screen(company& co) 
    : _co(co)
    {
        
        _win = &Screen::instance().add<Window>();
        _win->withLayout<GroupLayout>();
        int sw = Screen::instance().size().x;
        
        auto& contentPane = _win->add<Widget>();
        contentPane.withLayout<BoxLayout>(Orientation::Horizontal, Alignment::Fill);

        for(int i=0; i < static_cast<int>(ECountry::Max); i++) {
            auto& panel = contentPane.add<Widget>().withLayout<GroupLayout>();
            auto& btn = panel.add<Button>(rosetta::getString(gCountryString[i])).withCallback([&, i] (void) {
                co._country = i;//static_cast<int>(ECountry::Asia);
                this->setExitCallback([&] (const GameScreenEventArgs&) {
                    _win->dispose();
                    game_services::instance()._gsm.add(new_select_character_screen(co));
                });
                this->quit();
            });
            auto& img = panel.add<ImageView>(util::nvgGetTextureByKey("@continents"));
            img.setPolicy(ImageView::SizePolicy::Fixed);
            img.setFixedWidth(sw / 4);
            sprite_sheet* ss = util::nvgGetSpriteSheetByKey("@continents");
            img.setSource(ss->rect[gCountryString[i]]);          
        }

        /**_btn = &_win->add<Button>("ok").withCallback([&] (void) {
                    });
        */

        _win->setSize(_win->preferredSize(game_services::instance()._nvg));
        _win->center(game_services::instance()._nvg);

        auto& diag = Screen::instance().add<MessageDialog>(MessageDialog::Type::Information, "tutorial", rosetta::getString("@tutorial.countryselect"));

    }

    void update(int dt, bool coveredByOtherScreen, bool otherScreenHasFocus) {
        
        gamescreen::update(dt, coveredByOtherScreen, otherScreenHasFocus);
        //_btn->setEnabled(_co._country != -1);
    }
};

extern "C" gamescreen* new_country_screen(company& co) {
    return new country_screen(co);
}
