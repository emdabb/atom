

#pragma once

#include "Widget.h"
#include "../nanovg_ext.h"

namespace ui {

class ImageView : public Widget {
public:
    enum class SizePolicy {
       Fixed,
       Expand
    };

    ImageView(Widget *parent, int image = 0, SizePolicy policy = SizePolicy::Fixed);

    void setImage(int img)      { mImage = img; }
    int  image() const          { return mImage; }

    void       setPolicy(SizePolicy policy) { mPolicy = policy; }
    SizePolicy policy() const { return mPolicy; }

    virtual Vector2i preferredSize(NVGcontext *ctx) const;
    virtual void draw(NVGcontext* ctx);

    void setSource(const NVGrectangle& rc) {
        mSource = rc;
    }

protected:
    int mImage;
    SizePolicy mPolicy;
    NVGrectangle mSource;
};

}
