

#pragma once

#include "Widget.h"

namespace ui {

class ProgressBar : public Widget {
public:
    ProgressBar(Widget *parent);

    float value() { return mValue; }
    void setValue(float value) { mValue = value; }

    virtual Vector2i preferredSize(NVGcontext *ctx) const;
    virtual void draw(NVGcontext* ctx);

    const char* type() const { return "ui.widget.progressBar"; }
    cJSON* serialize();
    
    ProgressBar& withValue(float val) {
        setValue(val);
        return *this;
    }

    // virtual void save(Serializer &s) const;
    // virtual bool load(Serializer &s);
protected:
    float mValue;
};

}
