#ifndef SERIALIZER_H_
#define SERIALIZER_H_

#include <string>
#include <fstream>
#include <sstream>
#include <atom/engine/properties.h>
#include <iostream>
#include "../vec2.h"
#include "Common.h"
#include <cjson/cJSON.h>

namespace ui {

    // template<class T>
    // inline T& operator <<( T &ostream, const vec2<S> &self ) {
    //     return ostream << "(x=" << self.x << ",y=" << self.y << ")", ostream;
    // }

    template <typename ostream>
    inline ostream& operator << (ostream& os, const Color& self) {
        return os << "{" << "r:"<< self._r << ",g:" << self._g<< ",b:" << self._b << ",a:" << self._a<< "}", os;
    }

    template <typename T>
    struct serialization_helper;

    class Serializer {
        std::ofstream mWrite;
    public:


        template <typename T>
        T get(const std::string& valueString) {
            T val = atom::lexical_cast<T>(valueString);
            return val;
        }

        template <typename T>
        bool set(const std::string& key, const T& value) {
            
            return true;
        }

        void write(const std::string& str) {
            std::wcout << str.c_str() << std::endl;
        }
    };

    template <typename T> struct serialization_helper {

    };

    template <> struct serialization_helper<vec2i> {

    };
}

#endif
