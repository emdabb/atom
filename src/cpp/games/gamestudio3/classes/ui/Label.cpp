
#include "Label.h"
#include "Theme.h"
#include "Serializer.h"

using namespace ui;

Label::Label(Widget *parent, const std::string &caption, const std::string &font, int fontSize)
    : Widget(parent), mCaption(caption), mFont(font) {
    mFontSize = fontSize < 0 ? mTheme->mStandardFontSize : fontSize;
    mColor = mTheme->mTextColor;
}

Vector2i Label::preferredSize(NVGcontext *ctx) const {
    if (mCaption == "")
        return (Vector2i) { 0 };
    nvgFontFace(ctx, mFont.c_str());
    nvgFontSize(ctx, fontSize());
    if (mFixedSize.x > 0) {
        float bounds[4];
        nvgTextAlign(ctx, NVG_ALIGN_LEFT | NVG_ALIGN_TOP);
        nvgTextBoxBounds(ctx, mPos.x, mPos.y, mFixedSize.x, mCaption.c_str(), nullptr, bounds);
        return (Vector2i){
            mFixedSize.x,
            (int)(bounds[3]-bounds[1])
        };
    } else {
        nvgTextAlign(ctx, NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE);
        return (Vector2i) {
            (int)(nvgTextBounds(ctx, 0, 0, mCaption.c_str(), NULL,NULL) + 0.5f),
            mTheme->mStandardFontSize
        };
    }
}

void Label::draw(NVGcontext *ctx) {
    Widget::draw(ctx);
    nvgFontFace(ctx, mFont.c_str());
    nvgFontSize(ctx, fontSize());
    nvgFillColor(ctx, mColor);
    if (mFixedSize.x > 0) {
        nvgTextAlign(ctx, NVG_ALIGN_LEFT | NVG_ALIGN_TOP);
        nvgTextBox(ctx, mPos.x, mPos.y, mFixedSize.x, mCaption.c_str(), nullptr);
    } else {
        nvgTextAlign(ctx, NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE);
        nvgText(ctx, mPos.x, mPos.y + mSize.y * 0.5f, mCaption.c_str(), nullptr);
    }
}

void Label::save(Serializer &s) const {
    Widget::save(s);
    s.set("caption", mCaption);
    s.set("font", mFont);
    s.set("color", mColor);
}
//
// bool Label::load(Serializer &s) {
//     if (!Widget::load(s)) return false;
//     if (!s.get("caption", mCaption)) return false;
//     if (!s.get("font", mFont)) return false;
//     if (!s.get("color", mColor)) return false;
//     return true;
// }
