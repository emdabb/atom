
#include "ImageView.h"
//#include <nanogui/opengl.h>

using namespace ui;

ImageView::ImageView(Widget *parent, int img, SizePolicy policy)
: Widget(parent)
, mImage(img)
, mPolicy(policy)
{
    mSource = { -1, -1, -1, -1 };
}

Vector2i ImageView::preferredSize(NVGcontext *ctx) const {
    if (!mImage)
        return (Vector2i){0, 0};
    int w,h;
    if(nvgRectangleIsValid(const_cast<NVGrectangle*>(&mSource))) {
        nvgImageSize(ctx, mImage, &w, &h);
    } else {
        w = mSource.w;
        h = mSource.h;
    }
    return (Vector2i){w, h};
}

void ImageView::draw(NVGcontext* ctx) {
    if (!mImage)
        return;
    Vector2i p = mPos;
    Vector2i s = Widget::size();

    int w, h;
    //nvgImageSize(ctx, mImage, &w, &h);
    vec2i pref = preferredSize(ctx);
    w = pref.x;
    h = pref.y;

    
//#if 0 
    if (mPolicy == SizePolicy::Fixed) {
        if (s.x < w) {
            h = (int) round(h * (float) s.x / w);
            w = s.x;
        }

        if (s.y < h) {
            w = (int) round(w * (float) s.y / h);
            h = s.y;
        }
    } else {    // mPolicy == Expand
        // expand to width
        h = (int) round(h * (float) s.x / w);
        w = s.x;

        // shrink to height, if necessary
        if (s.y < h) {
            w = (int) round(w * (float) s.y / h);
            h = s.y;
        }
    }
#if 0
   #else
    if(nvgRectangleIsValid(&mSource)) {
        NVGpaint imgPaint = nvgImagePattern(ctx, p.x, p.y, w, h, 0, mImage, 1.0);

        nvgBeginPath(ctx);
        nvgRect(ctx, p.x, p.y, w, h);
        nvgFillPaint(ctx, imgPaint);
        nvgFill(ctx);

    } else {

        NVGrectangle dst = {
            p.x, p.y, w, h
        };
        nvgRenderSprite(ctx, mImage, &dst, &mSource);
    }
    
#endif
}
