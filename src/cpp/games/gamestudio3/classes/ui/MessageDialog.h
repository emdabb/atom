
#pragma once

#include "Window.h"
#include <atom/core/event_handler.h>

namespace ui {

class Label;

class MessageDialog : public Window {
public:
    enum class Type {
        Information,
        Question,
        Warning
    };

    MessageDialog(Widget *parent, Type type, const std::string &title = "Untitled",
                  const std::string &message = "Message",
                  const std::string &buttonText = "OK",
                  const std::string &altButtonText = "Cancel", bool altButton = false);

    virtual void draw(NVGcontext*);
    virtual void validate(NVGcontext*);

    Label *messageLabel() { return mMessageLabel; }
    const Label *messageLabel() const { return mMessageLabel; }

    std::function<void(int)> callback() const { return mCallback; }
    void setCallback(const std::function<void(int)> &callback) { mCallback = callback; }

    // const char* type() const { return "ui.widget.messageDialog"; }
    //
    // cJSON* serialize();
protected:
    std::function<void(int)> mCallback;
    Label *mMessageLabel;
};

}
