
#pragma once

#include "Button.h"
#include "Popup.h"
//#include <nanogui/entypo.h>

namespace ui {

class PopupButton : public Button {
public:
    PopupButton(Widget *parent, const std::string &caption = "Untitled",
                int buttonIcon = 0,
                int chevronIcon = ENTYPO_ICON_CHEVRON_SMALL_RIGHT);

    void setChevronIcon(int icon) { mChevronIcon = icon; }
    int chevronIcon() const { return mChevronIcon; }

    Popup *popup() { return mPopup; }
    const Popup *popup() const { return mPopup; }

    virtual void draw(NVGcontext* ctx);
    virtual Vector2i preferredSize(NVGcontext *ctx) const;
    virtual void performLayout(NVGcontext *ctx);

    // virtual void save(Serializer &s) const;
    // virtual bool load(Serializer &s);
protected:
    Popup *mPopup;
    int mChevronIcon;
};

}
