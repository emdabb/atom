
#pragma once

#include "Widget.h"
#include <atom/core/event_handler.h>

namespace ui {

class Button : public Widget {
public:
    /// Flags to specify the button behavior (can be combined with binary OR)
    enum Flags {
        NormalButton = 1,
        RadioButton  = 2,
        ToggleButton = 4,
        PopupButton  = 8
    };

    enum class IconPosition {
        Left,
        LeftCentered,
        RightCentered,
        Right
    };

    Button(Widget *parent, const std::string &caption = "Untitled", int icon = 0);

    const std::string &caption() const { return mCaption; }
    void setCaption(const std::string &caption) { mCaption = caption; }

    const Color &backgroundColor() const { return mBackgroundColor; }
    void setBackgroundColor(const Color &backgroundColor) { mBackgroundColor = backgroundColor; }

    const Color &textColor() const { return mTextColor; }
    void setTextColor(const Color &textColor) { mTextColor = textColor; }

    int icon() const { return mIcon; }
    void setIcon(int icon) { mIcon = icon; }

    int flags() const { return mFlags; }
    void setFlags(int buttonFlags) { mFlags = buttonFlags; }

    IconPosition iconPosition() const { return mIconPosition; }
    void setIconPosition(IconPosition iconPosition) { mIconPosition = iconPosition; }

    bool pushed() const { return mPushed; }
    void setPushed(bool pushed) { mPushed = pushed; }

    /// Set the push callback (for any type of button)
    std::function<void()> callback() const { return mCallback; }
    void setCallback(const std::function<void()> &callback) { mCallback = callback; }

    /// Set the change callback (for toggle buttons)
    std::function<void(bool)> changeCallback() const { return mChangeCallback; }
    void setChangeCallback(const std::function<void(bool)> &callback) { mChangeCallback = callback; }

    /// Set the button group (for radio buttons)
    void setButtonGroup(const std::vector<Button *> &buttonGroup) { mButtonGroup = buttonGroup; }
    const std::vector<Button *> &buttonGroup() const { return mButtonGroup; }

    virtual Vector2i preferredSize(NVGcontext *ctx) const;
    virtual bool mouseButtonEvent(const Vector2i &p, int button, bool down, int modifiers);
    virtual void draw(NVGcontext *ctx);

    void setIconFont(const std::string& name) { mIconFontName = name; }
    const std::string& iconFont() const { return mIconFontName; }

    const char* type() const { return "ui.widget.button"; }

    virtual void save(Serializer &s) const;
    virtual cJSON* serialize();
    // virtual bool load(Serializer &s);

    Button& withCallback(const std::function<void()>& callback) {setCallback(callback);return *this; }
    Button& withIconFont(const std::string& name)   { setIconFont(name);            return *this; }
    Button& withFlags(int f)                        { setFlags(f);                  return *this; }
    Button& withBackgroundColor(const Color& color) { setBackgroundColor(color);    return *this; }
    Button& withEnabled(bool b)                     { setEnabled(b);                return *this; }
    Button& withPushed(bool b)                      { setPushed(b);                 return *this; }
protected:
    std::string mCaption;
    int mIcon;
    IconPosition mIconPosition;
    bool mPushed;
    int mFlags;
    Color mBackgroundColor;
    Color mTextColor;
    atom::delegate<void()> mCallback;
    atom::delegate<void(bool)> mChangeCallback;
    std::vector<Button *> mButtonGroup;
    std::string mIconFontName;
};

}
