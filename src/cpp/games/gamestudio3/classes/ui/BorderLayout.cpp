#include "Layout.h"
#include "Widget.h"
#include "Theme.h"
#include "Window.h"
#include "Serializer.h"
#include <cjson/cJSON.h>

using namespace ui;

extern "C" std::string rand_string(size_t size);

BorderLayout::BorderLayout( Widget* north,
                            Widget* east,
                            Widget* south,
                            Widget* west,
                            Widget* center,
                            int margin,
                            int spacing)
:mMargin(margin)
,mSpacing(spacing)
, fillCorners(true)
{
    set(north,  Location::North);
    set(east,   Location::East);
    set(south,  Location::South);
    set(west,   Location::West);
    set(center, Location::Center);
}
BorderLayout::~BorderLayout() {

}
void BorderLayout::performLayout(NVGcontext *ctx, Widget *parent) const
{

    Vector2i pps = parent->size(), pfs = parent->fixedSize();
    Vector2i parentSize;
    parentSize.x = pfs[0] ? pfs[0] : pps[0];
    parentSize.y = pfs[1] ? pfs[1] : pps[1];



    int top = mMargin;//parent->getInsets().top;
	int bottom = parentSize.y - mMargin;//parent->position().y - parent->getInsets().bottom;
	int left = mMargin;//parent->getInsets().left;
	int right = parentSize.x - mMargin;//parent->position().x - parent->getInsets().right;

    if (dynamic_cast<const Window *>(parent))
        top += parent->theme()->mWindowHeaderHeight - mMargin/2;

	if(north != 0 && north->visible())
	{
        Vector2i ps = north->preferredSize(ctx), fs = north->fixedSize();
        Vector2i targetSize  = {
            fs[0] ? fs[0] : ps[0],
            fs[1] ? fs[1] : ps[1]
        };


		north->setSize({right - left, targetSize.y});
		north->setBounds(left, top, right - left, targetSize.y);
		top += targetSize.y + mSpacing;
	}

	if(south != 0 && south->visible())
	{
        Vector2i ps = south->preferredSize(ctx);
        Vector2i fs = south->fixedSize();
        Vector2i targetSize  = {
            fs[0] ? fs[0] : ps[0],
            fs[1] ? fs[1] : ps[1]
        };
		south->setSize({right - left, targetSize.y});
		south->setBounds(left,bottom - targetSize.y, right - left, targetSize.y);
		bottom -= targetSize.y + mSpacing;
	}

	if(east != 0 && east->visible())
	{
        Vector2i ps = east->preferredSize(ctx);
        Vector2i fs = east->fixedSize();
        Vector2i targetSize  = {
            fs[0] ? fs[0] : ps[0],
            fs[1] ? fs[1] : ps[1]
        };

		east->setSize(targetSize.x, bottom - top);
		east->setBounds(right- targetSize.x,top, targetSize.x, bottom - top);
		right -= targetSize.x + mSpacing;
	}

	if(west != 0 && west->visible())
	{
        Vector2i ps = west->preferredSize(ctx);
        Vector2i fs = west->fixedSize();
        Vector2i targetSize  = {
            fs[0] ? fs[0] : ps[0],
            fs[1] ? fs[1] : ps[1]
        };

		west->setSize(targetSize.x, bottom - top);
		west->setBounds(left, top, targetSize.x, bottom - top);
		left += targetSize.x + mSpacing;
	}

	if(center != 0 && center->visible())
	{
		center->setBounds(left,top,right - left, bottom - top);
	}

	if(!fillCorners)
	{
        const vec2i& npos = north->position();
        const vec2i& wpos = west->position();
		// resize north and south
		if(west != 0 && west->visible())
		{
			if(north != 0 && north->visible())
			{
				north->setBounds(
                    npos.x + wpos.x,
                    npos.y,
                    npos.x - wpos.x,
                    npos.y
                );
			}

			if(south != 0 && south->visible())
			{
				south->setBounds(south->position().x + west->position().x,south->position().y,south->position().x - west->position().x, south->position().y);
			}
		}

		if(east != 0 && east->visible())
		{
			if(north != 0 && north->visible())
			{
				north->setBounds(north->position().x,north->position().y,north->position().x - east->position().x, north->position().y);
			}

			if(south != 0 && south->visible())
			{
				south->setBounds(south->position().x,south->position().y,south->position().x - east->position().x, south->position().y);
			}
		}
	}
}
Vector2i BorderLayout::preferredSize(NVGcontext *ctx, const Widget *widget) const {
    vec2i dim = { 0, 0 };

    if (dynamic_cast<const Window *>(widget))
        dim.y += widget->theme()->mWindowHeaderHeight - mMargin/2;

	if(east != 0 && east->visible())
	{
		dim.x += east->preferredSize(ctx).x + mSpacing;
		dim.y = std::max(east->preferredSize(ctx).y,dim.y);

		if(!fillCorners)
		{
			dim.x += east->preferredSize(ctx).x;
		}
	}
	if(west != 0 && west->visible())
	{
		dim.x += west->preferredSize(ctx).x + mSpacing;
		dim.y = std::max(west->preferredSize(ctx).y, dim.y);

		if(!fillCorners)
		{
			dim.x += west->preferredSize(ctx).x;
		}
	}
	if(center != 0 && center->visible())
	{
		dim.x += center->preferredSize(ctx).x;
		dim.y = std::max(center->preferredSize(ctx).y, dim.y);
	}
	if(north != 0 && north->visible())
	{
		dim.x = std::max(north->preferredSize(ctx).x,dim.x);
		dim.y += north->preferredSize(ctx).y + mSpacing;
	}
	if(south != 0 && south->visible())
	{
		dim.x = std::max(south->preferredSize(ctx).x,dim.x);
		dim.y += south->preferredSize(ctx).y + mSpacing;
	}

	dim.x += mMargin * 2;//parent->getInsets().left + parent->getInsets().right;
	dim.y += mMargin * 2;//parent->getInsets().top + parent->getInsets().bottom;

	return dim;
}

void BorderLayout::set(Widget* widget, Location location) {


    std::string uid = rand_string(16);

    switch(location) {
    case Location::North:
        north = widget;
        if(widget)
            north->setId(std::string("north_") + uid);
        break;
    case Location::East:
        east = widget;
        if(widget)
            east->setId(std::string("east_") + uid);
        break;
    case Location::South:
        south = widget;
        if(widget)
            south->setId(std::string("south_") + uid);
        break;
    case Location::West:
        west = widget;
        if(widget)
            west->setId(std::string("west_") + uid);
        break;
    case Location::Center:
        center = widget;
        if(widget)
            center->setId(std::string("center_") + uid);
        break;
    default:
        break;
    }
    if(widget != nullptr) {
        widget->invalidate();
        if(widget->parent() != nullptr)
            widget->parent()->invalidate();
    }
}

cJSON* BorderLayout::serialize() {
    cJSON* json = Layout::serialize();

    cJSON_AddStringToObject(json, "north", north ? north->id().c_str() : "(null)");
    cJSON_AddStringToObject(json, "east", east ? east->id().c_str() : "(null)");
    cJSON_AddStringToObject(json, "south", south ? south->id().c_str() : "(null)");
    cJSON_AddStringToObject(json, "west", west ? west->id().c_str() : "(null)");
    cJSON_AddStringToObject(json, "center", center ? center->id().c_str() : "(null)");

    // s.set("north", north ? north->id() : "(null)");
    // s.set("east", east ? east->id() : "(null)");
    // s.set("south", south ? south->id() : "(null)");
    // s.set("west", west ? west->id() : "(null)");
    // s.set("center", center ? center->id() : "(null)");
    return json;
}
