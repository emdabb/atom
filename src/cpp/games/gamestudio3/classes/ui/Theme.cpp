#include "Theme.h"
//#include <nanogui/opengl.h>
//#include <nanogui_resources.h>
#include "../asset_manager.h"
#include "../util.h"
#include <cjson/cJSON.h>

using namespace ui;
using namespace atom;

Theme::Theme(NVGcontext *ctx) {
    mStandardFontSize                 = 25;//16;
    mButtonFontSize                   = 29;//20;
    mTextBoxFontSize                  = 25;//16;
    
    mWindowCornerRadius               = 2;
    mWindowHeaderHeight               = 30;
    mWindowDropShadowSize             = 10;
    mButtonCornerRadius               = 2;

    mDropShadow                       = Color(0, 128);
    mTransparent                      = Color(0, 0);
    mBorderDark                       = Color(29, 255);
    mBorderLight                      = Color(92, 255);
    mBorderMedium                     = Color(35, 255);
    mTextColor                        = Color(255, 160);
    mDisabledTextColor                = Color(255, 80);
    mTextColorShadow                  = Color(0, 160);
    mIconColor                        = mTextColor;

    mButtonGradientTopFocused         = Color(64, 255);
    mButtonGradientBotFocused         = Color(48, 255);
    mButtonGradientTopUnfocused       = Color(74, 255);
    mButtonGradientBotUnfocused       = Color(58, 255);
    mButtonGradientTopPushed          = Color(41, 255);
    mButtonGradientBotPushed          = Color(29, 255);

    /* Window-related */
    mWindowFillUnfocused              = Color(43, 230);
    mWindowFillFocused                = Color(45, 255);
    mWindowTitleUnfocused             = Color(220, 160);
    mWindowTitleFocused               = Color(255, 190);

    mWindowHeaderGradientTop          = mButtonGradientTopUnfocused;
    mWindowHeaderGradientBot          = mButtonGradientBotUnfocused;
    mWindowHeaderSepTop               = mBorderLight;
    mWindowHeaderSepBot               = mBorderDark;

    mWindowPopup                      = Color(50, 255);
    mWindowPopupTransparent           = Color(50, 0);

    mProgressBarGradientTop     = Color(38, 139, 210, 255);
    mProgressBarGradientBot     = Color(19, 70, 105);

//    int roboto_regular_ttf_size = mStandardFontSize;
//    int roboto_bold_ttf_size = mStandardFontSize;
//    int entypo_ttf_size = mStandardFontSize;

    mFonts = new int[4];


    {
        std::string path = atom::getResourcePath() + "assets/common/fonts/troika.ttf";
        mFontNormal = nvgCreateFont( ctx, "sans", path.c_str());
    }

    {
        std::string path = atom::getResourcePath() + "assets/common/fonts/animeace2_reg.ttf";
        mFontBold = nvgCreateFont(ctx, "sans-bold", path.c_str());
    }

    {
        std::string path = atom::getResourcePath() + "assets/common/fonts/entypo.ttf";
        mFontIcons = nvgCreateFont(ctx, "icons", path.c_str());
    }

    {
        std::string path = atom::getResourcePath() + "assets/common/fonts/fontawesome-webfont.ttf";
            mFonts[3] = nvgCreateFont(ctx, "icons-awesome", path.c_str());
    }

    if (mFontNormal == -1 || mFontBold == -1 || mFontIcons == -1)
        throw std::runtime_error("Could not load fonts!");

    

    mFonts[0] = mFontNormal;
    mFonts[1] = mFontBold;
    mFonts[2] = mFontIcons;


    util::json_to_file(serialize(), getResourcePath() + "assets/theme-default.json");
}

cJSON* Theme::serialize() {
    cJSON* json = cJSON_CreateObject();

    {
        cJSON_AddStringToObject(json, "name", "theme-name");
    }

    {
        
    }

    return json;
}

void Theme::deserialize(cJSON* json) {

    
}
