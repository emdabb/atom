

#include "MessageDialog.h"
#include "Layout.h"
#include "Button.h"
#include "Label.h"
#include <cjson/cJSON.h>

using namespace ui;

MessageDialog::MessageDialog(Widget *parent, Type type, const std::string &title,
              const std::string &message,
              const std::string &buttonText,
              const std::string &altButtonText, bool altButton)
              : Window(parent, title)
              {
                  setLayout(new BoxLayout(
                  Orientation::Vertical,
                  Alignment::Middle, 10, 10));

                  setModal(true);

                  Widget *panel1 = new Widget(this);
                  panel1->setLayout(new BoxLayout(Orientation::Horizontal,
                                    Alignment::Middle, 10, 15));
    int icon = 0;
    switch (type) {
        case Type::Information: icon = ENTYPO_ICON_CIRCLED_INFO; break;
        case Type::Question: icon = ENTYPO_ICON_CIRCLED_HELP; break;
        case Type::Warning: icon = ENTYPO_ICON_WARNING; break;
    }
    Label *iconLabel = new Label(panel1, std::string(utf8(icon).data()), "icons");
    iconLabel->setFontSize(50);
    mMessageLabel = new Label(panel1, message);
    mMessageLabel->setFixedWidth(200);
    Widget *panel2 = new Widget(this);
    panel2->setLayout(new BoxLayout(Orientation::Horizontal,
                                    Alignment::Middle, 0, 15));

    if (altButton) {
        Button *button = new Button(panel2, altButtonText, ENTYPO_ICON_CIRCLED_CROSS);
        button->setCallback([&] {
            if (mCallback)
                mCallback(1);
            dispose();
        });
    }
    Button *button = new Button(panel2, buttonText, ENTYPO_ICON_CHECK);
    button->setCallback([&] {
        if (mCallback)
            mCallback(0);
            dispose();
    });

    requestFocus();
}

void MessageDialog::validate(NVGcontext* vg) {
    center(vg);
    Window::validate(vg);
}

void MessageDialog::draw(NVGcontext* vg) {
    Window::draw(vg);
}
//
// cJSON* MessageDialog::serialize() {
//     cJSON* json = Window::serialize();
//     //
//     // {
//     //     cJSON_AddStringToObject(json, "messageLabel", mMessageLabel->caption().c_str());
//     // }
//
//     return json;
// }
