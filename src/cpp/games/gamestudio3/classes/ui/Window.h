#ifndef WINDOW_H_
#define WINDOW_H_

#include "Widget.h"
#include <functional>

namespace ui {
    class Window : public Widget {
    friend class Popup;
    std::function<void()> mDisposeCallback;
public:

    Window(Widget *parent, const std::string &title = "Untitled");

    /// Return the window title
    const std::string &title() const { return mTitle; }
    /// Set the window title
    void setTitle(const std::string &title) { mTitle = title; }

    /// Is this a model dialog?
    bool modal() const { return mModal; }
    /// Set whether or not this is a modal dialog
    void setModal(bool modal) { mModal = modal; }

    /// Return the panel used to house window buttons
    Widget *buttonPanel();

    /// Dispose the window
    void dispose();

    /// Center the window in the current \ref Screen
    void center(NVGcontext*);

    /// Draw the window
    virtual void draw(NVGcontext *ctx);

    /// Handle window drag events
    virtual bool mouseDragEvent(const Vector2i &p, const Vector2i &rel, int button, int modifiers);
    /// Handle mouse events recursively and bring the current window to the top
    virtual bool mouseButtonEvent(const Vector2i &p, int button, bool down, int modifiers);
    /// Accept scroll events and propagate them to the widget under the mouse cursor
    virtual bool scrollEvent(const Vector2i &p, const Vector2f &rel);
    /// Compute the preferred size of the widget
    virtual Vector2i preferredSize(NVGcontext *ctx) const;
    /// Invoke the associated layout generator to properly place child widgets, if any
    virtual void performLayout(NVGcontext *ctx);
    virtual void save(Serializer &s) const;
    // virtual bool load(Serializer &s);
    virtual const char* type() const { return "ui.widget.window"; }
    virtual cJSON* serialize();

    void setDisposeCallback(const std::function<void()>& fn) { mDisposeCallback = fn; }
    const std::function<void()>& disposeCallback() {
        return mDisposeCallback;
    }

    void setDragable(bool val) { mIsDragable = val; }
    const bool isDragable() const { return mIsDragable != false; }

    Window& withTitle(const std::string& s) { setTitle(s); return *this; }
protected:
    /// Internal helper function to maintain nested window position values; overridden in \ref Popup
    virtual void refreshRelativePlacement();
protected:
    std::string mTitle;
    Widget *mButtonPanel;
    bool mModal;
    bool mDrag;
    bool mIsDragable;
};
}

#endif
