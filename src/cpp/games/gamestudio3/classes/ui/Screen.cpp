#include "Screen.h"
#include "Window.h"
#include "Popup.h"
#include <SDL.h>

using namespace ui;
using namespace atom;

Screen::Screen()
: Widget(nullptr)
, mLastInteraction(0)
, mMouseState(0)
, mModifiers(0)
, mDragWidget(nullptr)
, mDragActive(false)
, mProcessEvents(true)
{

}

Screen::~Screen() {

}

void Screen::centerWindow(Window* window, NVGcontext* vg) {
    vec2i size = window->size();
    if (size.x == 0 && size.y == 0) {
        window->setSize(window->preferredSize(vg));
        window->performLayout(vg);
    }
    window->setPosition((mSize - window->size()) / 2);
}

void Screen::setGlobalAlpha(float a) {
    float fAlphaClamped = std::max(0.0f, std::min(a, 1.0f));
    mGlobalAlpha = (uint8_t)(fAlphaClamped * 255.f);
}

void Screen::disposeWindow(Window* window) {
    if (std::find(mFocusPath.begin(), mFocusPath.end(), window) != mFocusPath.end())
        mFocusPath.clear();
    if (mDragWidget == window)
        mDragWidget = nullptr;
    removeChild(window);
}

void Screen::initialize(application* app) {
    /**
    event_handler<void(int, int)> OnMouseDown;
    event_handler<void(int, int)> OnMouseRelease;
    event_handler<void(int, int, int, int)> OnMouseMove;
    event_handler<void(std::string)> OnTextInput;
    event_handler<void(uint16_t)> OnKeyPressed;
    */

    setSize({
        app->getWidth(),
        app->getHeight()
    });

    app->OnMouseDown += [&] (int x, int y) {
        Screen* s = Screen::instance_ptr();
        if(!s->mProcessEvents) {
            return;
        }
        s->mouseButtonCallbackEvent(0, 0, 0);
    };

    app->OnMouseRelease += [&] (int x, int y) {
        Screen* s = Screen::instance_ptr();
        if(!s->mProcessEvents) {
            return;
        }
        s->mouseButtonCallbackEvent(0, 1, 0);
    };

    app->OnMouseMove += [&] (int x, int y, int rel_x, int rel_y) {
        Screen* s = Screen::instance_ptr();
        if(!s->mProcessEvents) {
            return;
        }
        s->cursorPosCallbackEvent(x, y);
    };

    app->OnTextInput += [&] (const std::string& str) {
        std::wcout << "text entered " << str.c_str() << std::endl;
        Screen* s = Screen::instance_ptr();
        if(!s->mProcessEvents) {
            return;
        }
        s->charCallbackEvent(str);
    };

    app->OnKeyPressed += [&] (const uint16_t which) {
        std::wcout << "key pressed: " << which << std::endl;
        Screen *s = Screen::instance_ptr();
        if (!s->mProcessEvents)
            return;

        s->keyCallbackEvent(which, which, 0, 0);
    };
}
bool Screen::keyboardEvent(int key, int scancode, int action, int modifiers) {
    if (mFocusPath.size() > 0) {
       for (auto it = mFocusPath.rbegin() + 1; it != mFocusPath.rend(); ++it)
           if ((*it)->focused() && (*it)->keyboardEvent(key, scancode, action, modifiers))
               return true;
   }
   return false;
}

bool Screen::keyboardCharacterEvent(unsigned int codepoint) {
    if (mFocusPath.size() > 0) {
        for (auto it = mFocusPath.rbegin() + 1; it != mFocusPath.rend(); ++it)
            if ((*it)->focused() && (*it)->keyboardCharacterEvent(codepoint))
                return true;
    }
    return false;
}

bool Screen::charCallbackEvent(const std::string& text) {
    mLastInteraction = SDL_GetTicks();
    try {
        int codepoint;

        return keyboardCharacterEvent(*((unsigned int*)&text[0]));
    } catch (const std::exception &e) {
        std::cerr << "Caught exception in event handler: " << e.what()
                  << std::endl;
        abort();
    }
}

bool Screen::keyCallbackEvent(int key, int scancode, int action, int mods) {
    mLastInteraction = SDL_GetTicks();
    try {
        return keyboardEvent(key, scancode, action, mods);
    } catch (const std::exception &e) {
        std::cerr << "Caught exception in event handler: " << e.what() << std::endl;
        abort();
    }
}

bool Screen::mouseButtonCallbackEvent(int button, int action, int modifiers) {
    mModifiers = modifiers;
    mLastInteraction = SDL_GetTicks();
    try {
        if (mFocusPath.size() > 1) {
            const Window *window =
                dynamic_cast<Window *>(mFocusPath[mFocusPath.size() - 2]);
            if (window && window->modal()) {
                if (!window->contains(mMousePos))
                    return false;
            }
        }

        if (action == 0)
            mMouseState |= 1 << button;
        else
            mMouseState &= ~(1 << button);

        auto dropWidget = findWidget(mMousePos);
        if (mDragActive && action == 1 && dropWidget != mDragWidget && mDragWidget != nullptr)
            mDragWidget->mouseButtonEvent(
                mMousePos - mDragWidget->parent()->absolutePosition(), button,
                false, mModifiers);

        if (dropWidget != nullptr && dropWidget->cursor() != mCursor) {
            mCursor = dropWidget->cursor();
            //glfwSetCursor(mGLFWWindow, mCursors[(int) mCursor]);
        }

        if (action == 0 && button == 0) {
            mDragWidget = findWidget(mMousePos);
            if (mDragWidget == this)
                mDragWidget = nullptr;
            mDragActive = mDragWidget != nullptr;
            if (!mDragActive)
                updateFocus(nullptr);
        } else {
            mDragActive = false;
            mDragWidget = nullptr;
        }

        return mouseButtonEvent(mMousePos, button, action == 0, mModifiers);
    } catch (const std::exception &e) {
        std::cerr << "Caught exception in event handler: " << e.what() << std::endl;
        abort();
    }

    return false;
}

bool Screen::cursorPosCallbackEvent(int x, int y) {
    Vector2i p = {
        x, y
    };

    bool ret = false;
    mLastInteraction = SDL_GetTicks();//glfwGetTime();
    try {
        p -= (Vector2i){1, 2};

        if (!mDragActive) {
            Widget *widget = findWidget(p);
            if (widget != nullptr && widget->cursor() != mCursor) {
                mCursor = widget->cursor();
                //glfwSetCursor(mGLFWWindow, mCursors[(int) mCursor]);
            }
        } else {
            ret = mDragWidget->mouseDragEvent(
                p - mDragWidget->parent()->absolutePosition(), p - mMousePos,
                mMouseState, mModifiers);
        }

        if (!ret)
            ret = mouseMotionEvent(p, p - mMousePos, mMouseState, mModifiers);

        mMousePos = p;

        return ret;
    } catch (const std::exception &e) {
        std::cerr << "Caught exception in event handler: " << e.what() << std::endl;
        abort();
    }

    return false;
}

void Screen::updateFocus(Widget* widget) {
    for(auto w : mFocusPath) {
        if(!w->focused()) {
            continue;
        }
        w->focusEvent(false);
    }

    mFocusPath.clear();
    Widget* window= nullptr;
    while(widget) {
        mFocusPath.push_back(widget);
        if(dynamic_cast<Window*>(widget)) {
            window = widget;
        }
        widget = widget->parent();
    }

    for(auto it = mFocusPath.rbegin(); it != mFocusPath.rend(); it++) {
        (*it)->focusEvent(true);
    }

    if(window){
        moveWindowToFront((Window*)window);
    }
}

void Screen::moveWindowToFront(Window* window) {
#if 0
    mChildren.erase(std::remove(mChildren.begin(), mChildren.end(), window), mChildren.end());
    mChildren.push_back(window);
    /* Brute force topological sort (no problem for a few windows..) */
    bool changed = false;
    do {
        size_t baseIndex = 0;
        for (size_t index = 0; index < mChildren.size(); ++index)
            if (mChildren[index] == window)
                baseIndex = index;
        changed = false;
        for (size_t index = 0; index < mChildren.size(); ++index) {
            Popup *pw = dynamic_cast<Popup *>(mChildren[index]);
            if (pw && pw->parentWindow() == window && index < baseIndex) {
                moveWindowToFront(pw);
                changed = true;
                break;
            }
        }
    } while (changed);
#endif

}
