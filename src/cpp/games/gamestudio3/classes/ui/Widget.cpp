

#include "Widget.h"
#include "Layout.h"
#include "Theme.h"
#include "Window.h"
#include "Screen.h"
#include "Serializer.h"
//#include <Opengl.h>
//#include <nanogui/screen.h>
//#include <nanogui/serializer/core.h>
#include <nanovg.h>
#include <iostream>
#include <cjson/cJSON.h>

using namespace ui;

//Widget::Widget()
//: Widget(nullptr)
//{
//
//}

Widget::Widget(Widget *parent)
    : mParent(nullptr)
    , mTheme(nullptr)
    , mLayout(nullptr)
    , mPos({0, 0})
    , mSize({0, 0})
    , mFixedSize({0, 0})
    , mVisible(true)
    , mEnabled(true)
    , mFocused(false)
    , mMouseFocus(false)
    , mTooltip("")
    , mFontSize(-1.0f)
    , mCursor(Cursor::Arrow)
    , mValid(false)
    , mFocusCallback(nullptr)
{
    if (parent) {
        parent->addChild(this);
        mTheme = parent->mTheme;
    }
}

Widget::~Widget() {
    for (auto child : mChildren) {
        if (child)
            child->decRef();
    }
}

bool Widget::isValid() const {
    return this->mValid != false;
}
void Widget::validate(NVGcontext* ctx) {
    if(this->mLayout) {
        this->performLayout(ctx);
    }

    for(auto w : this->mChildren) {
        w->validate(ctx);
    }

    this->mValid = true;
}
void Widget::invalidate() {
    this->mValid = false;
}

int Widget::fontSize() const {
    return mFontSize < 0 ? mTheme->mStandardFontSize : mFontSize;
}

Vector2i Widget::preferredSize(NVGcontext *ctx) const {
    if (mLayout)
        return mLayout->preferredSize(ctx, this);
    else
        return mSize;
}

void Widget::performLayout(NVGcontext *ctx) {
    if (mLayout) {
        mLayout->performLayout(ctx, this);
    } else {
        for (auto c : mChildren) {
            Vector2i pref = c->preferredSize(ctx), fix = c->fixedSize();
            c->setSize({
                fix[0] ? fix[0] : pref[0],
                fix[1] ? fix[1] : pref[1]
            });
            c->performLayout(ctx);
        }
    }
}

Widget* Widget::findWidgetById(const std::string& id_) {
    for(auto it = mChildren.rbegin(); it != mChildren.rend(); it++) {
        Widget* child = *it;
        if(child->visible() && child->id() == id_) {
            return child->findWidgetById(id_);
        }
    }

    return id() == id_ ? this : nullptr;
}

Widget *Widget::findWidget(const Vector2i &p) {
    for (auto it = mChildren.rbegin(); it != mChildren.rend(); ++it) {
        Widget *child = *it;
        if (child->visible() && child->contains(p - mPos))
            return child->findWidget(p - mPos);
    }
    return contains(p) ? this : nullptr;
}

bool Widget::mouseButtonEvent(const Vector2i &p, int button, bool down, int modifiers) {
    for (auto it = mChildren.rbegin(); it != mChildren.rend(); ++it) {
        Widget *child = *it;
        if (child->visible() && child->contains(p - mPos) &&
            child->mouseButtonEvent(p - mPos, button, down, modifiers))
            return true;
    }
    if (button == 0 && down && !mFocused)
        requestFocus();
    return false;
}

bool Widget::mouseMotionEvent(const Vector2i &p, const Vector2i &rel, int button, int modifiers) {
    for (auto it = mChildren.rbegin(); it != mChildren.rend(); ++it) {
        Widget *child = *it;
        if (!child->visible())
            continue;
        bool contained = child->contains(p - mPos), prevContained = child->contains(p - mPos - rel);
        if (contained != prevContained)
            child->mouseEnterEvent(p, contained);
        if ((contained || prevContained) &&
            child->mouseMotionEvent(p - mPos, rel, button, modifiers))
            return true;
    }
    return false;
}

bool Widget::scrollEvent(const Vector2i &p, const Vector2f &rel) {
    for (auto it = mChildren.rbegin(); it != mChildren.rend(); ++it) {
        Widget *child = *it;
        if (!child->visible())
            continue;
        if (child->contains(p - mPos) && child->scrollEvent(p - mPos, rel))
            return true;
    }
    return false;
}

bool Widget::mouseDragEvent(const Vector2i &, const Vector2i &, int, int) {
    return false;
}

bool Widget::mouseEnterEvent(const Vector2i &, bool enter) {
    mMouseFocus = enter;
    return false;
}

bool Widget::focusEvent(bool focused) {
    mFocused = focused;
    if(mFocusCallback != nullptr && mEnabled != false) {
        mFocusCallback(focused);
    }
    return false;
}

bool Widget::keyboardEvent(int, int, int, int) {
    return false;
}

bool Widget::keyboardCharacterEvent(unsigned int) {
    return false;
}

void Widget::addChild(Widget *widget) {
    mChildren.push_back(widget);
    widget->incRef();
    widget->setParent(this);
    invalidate();
}

void Widget::removeChild(const Widget *widget) {
    mChildren.erase(std::remove(mChildren.begin(), mChildren.end(), widget), mChildren.end());
    widget->decRef();
    invalidate();
}

void Widget::removeChild(int index) {
    Widget *widget = mChildren[index];
    mChildren.erase(mChildren.begin() + index);
    widget->decRef();
    invalidate();
}

Window *Widget::window() {
    Widget *widget = this;
    while (true) {
        if (!widget)
            throw std::runtime_error(
                "Widget:internal error (could not find parent window)");
        Window *window = dynamic_cast<Window *>(widget);
        if (window)
            return window;
        widget = widget->parent();
    }
}

void Widget::requestFocus() {
    Screen::instance().updateFocus(this);
}

void Widget::draw(NVGcontext *ctx) {
    if(!isValid()) {
        //std::wcout << "dbg >>> validating widget " << this->id().c_str() << std::endl;
        validate(ctx);
    }
    #if 1 
        nvgStrokeWidth(ctx, 1.0f);
        nvgBeginPath(ctx);
        nvgRect(ctx, mPos.x - 0.5f, mPos.y - 0.5f, mSize.x + 1, mSize.y + 1);
        nvgStrokeColor(ctx, nvgRGBA(255, 0, 0, 64));
        nvgStroke(ctx);
    #endif

    if (mChildren.empty())
        return;

    nvgTranslate(ctx, mPos.x, mPos.y);
    for (auto child : mChildren)
        if (child->visible())
            child->draw(ctx);
    nvgTranslate(ctx, -mPos.x, -mPos.y);
}

cJSON* Widget::serialize() {
    cJSON* json = cJSON_CreateObject();
    cJSON* arr  = nullptr;

    //s.set("class", type());
    cJSON_AddStringToObject(json, "class", type());

    //s.set("position", mPos);
    cJSON_AddItemToObject(json, "position", arr = cJSON_CreateObject());
    cJSON_AddNumberToObject(arr, "x", mPos.x);
    cJSON_AddNumberToObject(arr, "y", mPos.y);


    //s.set("size", mSize);
    cJSON_AddItemToObject(json, "size", arr = cJSON_CreateObject());
    cJSON_AddNumberToObject(arr, "x", mSize.x);
    cJSON_AddNumberToObject(arr, "y", mSize.y);

    //s.set("fixedSize", mFixedSize);

    //s.set("visible", mVisible);
    cJSON_AddNumberToObject(json, "visible", mVisible ? 1 : 0);

    //s.set("enabled", mEnabled);
    cJSON_AddNumberToObject(json, "enabled", mEnabled ? 1 : 0);

    // s.set("focused", mFocused);
    cJSON_AddNumberToObject(json, "focused", mFocused ? 1 : 0);

    // s.set("tooltip", mTooltip);
    cJSON_AddStringToObject(json, "tooltip", mTooltip.c_str());

    // s.set("fontSize", mFontSize);
    cJSON_AddNumberToObject(json, "fontSize", mFontSize);

    // s.set("cursor", (int) mCursor);
    cJSON_AddNumberToObject(json, "cursor", (int)mCursor);

    //if(!id().empty())
        //s.set("id", id());

    if(mLayout.get()) {
        cJSON_AddItemToObject(json, "layout", mLayout->serialize());
    }

    // if(mLayout.get()) {
    //     s.write("layout : {");
    //     const_cast<Layout*>(mLayout.get())->save(s);
    //     s.write("}");
    // }

    //s.write("\"children\" : {" );

    for(auto w : mChildren) {
        cJSON_AddItemToObject(json, "children", w->serialize());
    }

    return json;
}

void Widget::save(Serializer &s) const {

}
//
// bool Widget::load(Serializer &s) {
//     if (!s.get("position", mPos)) return false;
//     if (!s.get("size", mSize)) return false;
//     if (!s.get("fixedSize", mFixedSize)) return false;
//     if (!s.get("visible", mVisible)) return false;
//     if (!s.get("enabled", mEnabled)) return false;
//     if (!s.get("focused", mFocused)) return false;
//     if (!s.get("tooltip", mTooltip)) return false;
//     if (!s.get("fontSize", mFontSize)) return false;
//     int cursor;
//     if (!s.get("cursor", cursor)) return false;
//     mCursor = (Cursor) cursor;
//     return true;
// }
