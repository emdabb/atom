#pragma once

#include "Widget.h"
#include <atom/core/singleton.h>
#include <atom/engine/application.h>
#include <vector>


namespace ui {

    class Window;

    class Screen
    : public Widget
    , public atom::singleton<Screen>
    {
        std::vector<Widget*> mFocusPath;
        int mLastInteraction;
        Vector2i mMousePos;
        int mMouseState;
        int mModifiers;
        Widget* mDragWidget;
        bool mDragActive;
        bool mProcessEvents;
        uint8_t mGlobalAlpha;
    public:
        Screen();
        virtual ~Screen();
        void updateFocus(Widget*);
        void moveWindowToFront(Window*);
        void initialize(atom::application*);
        bool cursorPosCallbackEvent(int, int);
        bool mouseButtonCallbackEvent(int, int, int);
        bool keyCallbackEvent(int key, int scancode, int action, int mods);
        bool charCallbackEvent(const std::string&);
        void centerWindow(Window*, NVGcontext*);
        void disposeWindow(Window*);
        /// Default keyboard event handler
        virtual bool keyboardEvent(int key, int scancode, int action, int modifiers);
        virtual bool keyboardCharacterEvent(unsigned int codepoint);

        const char* type() const { return "ui.widget.screen"; }

        void setGlobalAlpha(float);
        uint8_t globalAlpha() const { return mGlobalAlpha; }
    public:
        void mousePress();
        void mouseRelease();
        void mouseMove();
    };
}
