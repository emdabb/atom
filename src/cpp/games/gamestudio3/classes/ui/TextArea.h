#ifndef TEXTAREA_H_
#define TEXTAREA_H_

#include "TextBox.h"

namespace ui {
    class TextArea : public TextBox {
    public:
        TextArea(Widget*, const std::string& = "Untitled");
        virtual ~TextArea();
        virtual Vector2i preferredSize(NVGcontext *ctx) const;
        virtual void draw(NVGcontext* ctx);

        bool editable() const { return mEditable; }
        void setEditable(bool editable);

    };
}

#endif
