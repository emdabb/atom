

#pragma once

#include "Widget.h"
#include "Layout.h"
#include <atom/core/event_handler.h>

namespace ui {

class Slider : public Widget {
public:
    Slider(Widget *parent, Orientation orientation = Orientation::Horizontal);

    float value() const { return mValue; }
    void setValue(float value) { mValue = value; }

    const Color &highlightColor() const { return mHighlightColor; }
    void setHighlightColor(const Color &highlightColor) { mHighlightColor = highlightColor; }

    std::pair<float, float> highlightedRange() const { return mHighlightedRange; }
    void setHighlightedRange(std::pair<float, float> highlightedRange) { mHighlightedRange = highlightedRange; }

    std::function<void(float)> callback() const { return mCallback; }
    void setCallback(const std::function<void(float)> &callback) { mCallback = callback; }

    std::function<void(float)> finalCallback() const { return mFinalCallback; }
    void setFinalCallback(const std::function<void(float)> &callback) { mFinalCallback = callback; }

    virtual Vector2i preferredSize(NVGcontext *ctx) const;
    virtual bool mouseDragEvent(const Vector2i &p, const Vector2i &rel, int button, int modifiers);
    virtual bool mouseButtonEvent(const Vector2i &p, int button, bool down, int modifiers);
    virtual void draw(NVGcontext* ctx);
    // virtual void save(Serializer &s) const;
    // virtual bool load(Serializer &s);
    virtual cJSON* serialize();

    Slider& withValue(float val) {
        setValue(val);
        return *this;
    }

    Slider& withHighlightedRange(float a, float b) {
        setHighlightedRange(std::pair<float, float>(a, b));
        return *this;
    }

    Slider& withHighlightColor(const Color& c) {
        setHighlightColor(c);
        return *this;
    }

    Slider& withCallback(const std::function<void(float)>& cb) {
        setCallback(cb);
        return *this;
    }

protected:

    float mValue;
    atom::delegate<void(float)> mCallback;
    atom::delegate<void(float)> mFinalCallback;
    std::pair<float, float> mHighlightedRange;
    Color mHighlightColor;
    Orientation mOrientation;
};

}
