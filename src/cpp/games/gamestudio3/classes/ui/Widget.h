#ifndef WIDGET_H_
#define WIDGET_H_

#include "Object.h"
#include "../vec2.h"
#include <vector>
#include <string>
#include <functional>
#include <SDL.h>
#include <atom/core/factory.h>
#include "Common.h"
#include "Color.h"
#include "../util.h"

#if defined __ANDROID__
    
    template <typename T>
    std::string to_string(T val) {
        std::ostringstream ss;
        ss << val;
        return ss.str();
    }

    float stof(const std::string& val) {
        return ::atof(val.c_str());
    }
    int round(float val) {
        return static_cast<int>(val + 0.5f);
    }

#endif


struct NVGcontext;
struct cJSON;

extern "C" bool SDL_RectContains(const SDL_Rect& rc, const SDL_Point& p);

namespace ui {

class Layout;
class Window;
class Theme;
class Serializer;

enum class Cursor;

/**
 * \brief Base class of all widgets
 *
 * \ref Widget is the base class of all widgets in \c nanogui. It can
 * also be used as an panel to arrange an arbitrary number of child
 * widgets using a layout generator (see \ref Layout).
 */
class Widget : public Object {
public:
    //Widget();
    /// Construct a new widget with the given parent widget
    Widget(Widget *parent);

    /// Return the parent widget
    Widget *parent() { return mParent; }
    /// Return the parent widget
    const Widget *parent() const { return mParent; }
    /// Set the parent widget
    void setParent(Widget *parent) {
        if(parent != mParent) {
            mParent = parent;
            invalidate();
        }
    }

    /// Return the used \ref Layout generator
    Layout *layout() { return mLayout; }
    /// Return the used \ref Layout generator
    const Layout *layout() const { return mLayout.get(); }
    /// Set the used \ref Layout generator
    void setLayout(Layout *layout) {
        if(mLayout.get() != layout) {
            mLayout = layout;
            invalidate();
        }
    }

    /// Return the \ref Theme used to draw this widget
    Theme *theme() { return mTheme; }
    /// Return the \ref Theme used to draw this widget
    const Theme *theme() const { return mTheme.get(); }
    /// Set the \ref Theme used to draw this widget
    void setTheme(Theme *theme) { mTheme = theme; }

    /// Return the position relative to the parent widget
    const Vector2i &position() const { return mPos; }
    /// Set the position relative to the parent widget
    void setPosition(const Vector2i &pos) {
        if(mPos.x != pos.x || mPos.y != pos.y) {
            mPos = pos;
            invalidate();
        }
    }

    void setPosition(int x, int y) {
        setPosition({x, y});
    }

    /// Return the absolute position on screen
    Vector2i absolutePosition() const {
        return mParent ?
            (parent()->absolutePosition() + mPos) : mPos;
    }

    /// Return the size of the widget
    const Vector2i &size() const { return mSize; }
    /// set the size of the widget
    void setSize(const Vector2i &size) {
        if(size.x != mSize.x || size.y != mSize.y) {
            mSize = size;
            invalidate();
        }
    }

    void setSize(int w, int h) {
        setSize({w, h});
    }

    /// Return the width of the widget
    int width() const { return mSize.x; }
    /// Set the width of the widget
    void setWidth(int width) {
        if(mSize.x != width) {
            mSize.x= width;
            invalidate();
        }
    }

    void setBounds(int x, int y, int w, int h) {
        setPosition({x, y});
        setSize({w, h});
    }

    /// Return the height of the widget
    int height() const { return mSize.y; }
    /// Set the height of the widget
    void setHeight(int height) {
        if(mSize.y != height) {
            mSize.y = height;
            invalidate();
        }
    }

    /**
     * \brief Set the fixed size of this widget
     *
     * If nonzero, components of the fixed size attribute override any values
     * computed by a layout generator associated with this widget. Note that
     * just setting the fixed size alone is not enough to actually change its
     * size; this is done with a call to \ref setSize or a call to \ref performLayout()
     * in the parent widget.
     */
    void setFixedSize(const Vector2i &fixedSize) {
        setFixedWidth(fixedSize.x);
        setFixedHeight(fixedSize.y);
    }

    /// Return the fixed size (see \ref setFixedSize())
    const Vector2i &fixedSize() const { return mFixedSize; }

    // Return the fixed width (see \ref setFixedSize())
    int fixedWidth() const { return mFixedSize.x; }
    // Return the fixed height (see \ref setFixedSize())
    int fixedHeight() const { return mFixedSize.y; }
    /// Set the fixed width (see \ref setFixedSize())
    void setFixedWidth(int width) {
        if(mFixedSize.x != width) {
            mFixedSize.x = width;
            invalidate();
        }
    }
    /// Set the fixed height (see \ref setFixedSize())
    void setFixedHeight(int height) {
        if(mFixedSize.y != height) {
            mFixedSize.y = height;
            invalidate();
        }
    }

    /// Return whether or not the widget is currently visible (assuming all parents are visible)
    bool visible() const { return mVisible; }
    /// Set whether or not the widget is currently visible (assuming all parents are visible)
    void setVisible(bool visible) {
        if(mVisible != visible) {
             mVisible = visible;
             invalidate();
         }
     }

    /// Check if this widget is currently visible, taking parent widgets into account
    bool visibleRecursive() const {
        bool visible = true;
        const Widget *widget = this;
        while (widget) {
            visible &= widget->visible();
            widget = widget->parent();
        }
        return visible;
    }

    /// Return the number of child widgets
    int childCount() const { return (int) mChildren.size(); }

    /// Return the list of child widgets of the current widget
    const std::vector<Widget *> &children() const { return mChildren; }

    template<typename T, typename... Args>
    T& add( const Args&... args)
    {
        T* widget = new T( this, args... );
        return *widget;
    }

    /**
     * \brief Add a child widget to the current widget
     *
     * This function almost never needs to be called by hand,
     * since the constructor of \ref Widget automatically
     * adds the current widget to its parent
     */
    void addChild(Widget *widget);

    /// Remove a child widget by index
    void removeChild(int index);

    /// Remove a child widget by value
    void removeChild(const Widget *widget);

    // Walk up the hierarchy and return the parent window
    Window *window();

    /// Associate this widget with an ID value (optional)
    void setId(const std::string &id) { mId = id; }
    /// Return the ID value associated with this widget, if any
    const std::string &id() const { return mId; }

    /// Return whether or not this widget is currently enabled
    bool enabled() const { return mEnabled; }
    /// Set whether or not this widget is currently enabled
    void setEnabled(bool enabled) { mEnabled = enabled; }

    /// Return whether or not this widget is currently focused
    bool focused() const { return mFocused; }
    /// Set whether or not this widget is currently focused
    void setFocused(bool focused) { mFocused = focused; }
    /// Request the focus to be moved to this widget
    void requestFocus();

    const std::string &tooltip() const { return mTooltip; }
    void setTooltip(const std::string &tooltip) { mTooltip = tooltip; }

    /// Return current font size. If not set the default of the current theme will be returned
    int fontSize() const;
    /// Set the font size of this widget
    void setFontSize(int fontSize) {
        mFontSize = fontSize;
        invalidate();
    }
    /// Return whether the font size is explicitly specified for this widget
    bool hasFontSize() const { return mFontSize > 0; }

    /// Return a pointer to the cursor of the widget
    Cursor cursor() const { return mCursor; }
    /// Set the cursor of the widget
    void setCursor(Cursor cursor) { mCursor = cursor; }

    /// Check if the widget contains a certain position
    bool contains(const Vector2i &p) const {
        int x, y, w, h;
        SDL_Rect rc = {
            mPos.x, mPos.y,
            mSize.x, mSize.y
        };

        return SDL_RectContains(rc, { p.x, p.y });

        // auto d = (p-mPos).array();
        // return (d >= 0).all() && (d < mSize.array()).all();
        return false;
    }

    /// Determine the widget located at the given position value (recursive)
    Widget *findWidget(const Vector2i &p);

    Widget* findWidgetById(const std::string&);

    /// Handle a mouse button event (default implementation: propagate to children)
    virtual bool mouseButtonEvent(const Vector2i &p, int button, bool down, int modifiers);

    /// Handle a mouse motion event (default implementation: propagate to children)
    virtual bool mouseMotionEvent(const Vector2i &p, const Vector2i &rel, int button, int modifiers);

    /// Handle a mouse drag event (default implementation: do nothing)
    virtual bool mouseDragEvent(const Vector2i &p, const Vector2i &rel, int button, int modifiers);

    /// Handle a mouse enter/leave event (default implementation: record this fact, but do nothing)
    virtual bool mouseEnterEvent(const Vector2i &p, bool enter);

    /// Handle a mouse scroll event (default implementation: propagate to children)
    virtual bool scrollEvent(const Vector2i &p, const Vector2f &rel);

    /// Handle a focus change event (default implementation: record the focus status, but do nothing)
    virtual bool focusEvent(bool focused);

    /// Handle a keyboard event (default implementation: do nothing)
    virtual bool keyboardEvent(int key, int scancode, int action, int modifiers);

    /// Handle text input (UTF-32 format) (default implementation: do nothing)
    virtual bool keyboardCharacterEvent(unsigned int codepoint);

    /// Compute the preferred size of the widget
    virtual Vector2i preferredSize(NVGcontext *ctx) const;

    /// Invoke the associated layout generator to properly place child widgets, if any
    virtual void performLayout(NVGcontext *ctx);

    /// Draw the widget (and all child widgets)
    virtual void draw(NVGcontext *ctx);

    /// Save the state of the widget into the given \ref Serializer instance
    virtual void save(Serializer &s) const;

    virtual cJSON* serialize();

    /// Restore the state of the widget from the given \ref Serializer instance
    //virtual bool load(Serializer &s);

    virtual bool isValid() const;
    virtual void validate(NVGcontext*);
    virtual void invalidate();

    virtual const char* type() const { return "ui.widget.widget"; }

    template<typename LayoutClass,typename... Args>
    Widget& withLayout( const Args&... args)
    {
        auto* layout = new LayoutClass( args... );
        setLayout( layout );
        return *this;
    }

    Widget& withSize(const vec2i& size) { setSize(size); return *this; }

    Widget& withFixedHeight(int h) {
        setFixedHeight(h);
        return *this;
    }

    Widget& withFixedWidth(int w) {
        setFixedWidth(w);
        return *this;
    }

    Widget& withId(const std::string& id) {
        setId(id);
        return *this;
    }
    Widget& withFontSize(int sz) { setFontSize(sz); return *this; }

    void setFocusCallback(const std::function<void(bool)>& fn) { mFocusCallback = fn; }
protected:
    /// Free all resources used by the widget and any children
    virtual ~Widget();

protected:
    Widget *mParent;
    ref<Theme> mTheme;
    ref<Layout> mLayout;
    std::string mId;
    Vector2i mPos, mSize, mFixedSize;
    std::vector<Widget *> mChildren;
    bool mVisible, mEnabled;
    bool mFocused, mMouseFocus;
    bool mValid;
    std::string mTooltip;
    int mFontSize;
    Cursor mCursor;
    std::function<void(bool)> mFocusCallback;
};

typedef atom::factory<Widget> WidgetFactory;

} // ui
#endif
