
#include "Popup.h"
#include "Theme.h"
//#include <nanogui/opengl.h>
//#include <nanogui/serializer/core.h>

using namespace ui;

Popup::Popup(Widget *parent, Window *parentWindow)
    : Window(parent, "")
    , mParentWindow(parentWindow)
    , mAnchorPos({0, 0})
    , mAnchorHeight(30)
{
}

void Popup::performLayout(NVGcontext *ctx) {
    if (mLayout || mChildren.size() != 1) {
        Widget::performLayout(ctx);
    } else {
        mChildren[0]->setPosition({0, 0});
        mChildren[0]->setSize(mSize);
        mChildren[0]->performLayout(ctx);
    }
}

void Popup::refreshRelativePlacement() {
    mParentWindow->refreshRelativePlacement();
    mVisible &= mParentWindow->visibleRecursive();
    mPos = mParentWindow->position() + mAnchorPos - (Vector2i){0, mAnchorHeight};
}

void Popup::draw(NVGcontext* ctx) {
    refreshRelativePlacement();

    if (!mVisible)
        return;

    int ds = mTheme->mWindowDropShadowSize;
    int cr = mTheme->mWindowCornerRadius;

    /* Draw a drop shadow */
    NVGpaint shadowPaint = nvgBoxGradient(
        ctx, mPos.x, mPos.y, mSize.x, mSize.y, cr*2, ds*2,
        mTheme->mDropShadow, mTheme->mTransparent);

    nvgBeginPath(ctx);
    nvgRect(ctx, mPos.x-ds,mPos.y-ds, mSize.x+2*ds, mSize.y+2*ds);
    nvgRoundedRect(ctx, mPos.x, mPos.y, mSize.x, mSize.y, cr);
    nvgPathWinding(ctx, NVG_HOLE);
    nvgFillPaint(ctx, shadowPaint);
    nvgFill(ctx);

    /* Draw window */
    nvgBeginPath(ctx);
    nvgRoundedRect(ctx, mPos.x, mPos.y, mSize.x, mSize.y, cr);

    nvgMoveTo(ctx, mPos.x-15,mPos.y+mAnchorHeight);
    nvgLineTo(ctx, mPos.x+1,mPos.y+mAnchorHeight-15);
    nvgLineTo(ctx, mPos.x+1,mPos.y+mAnchorHeight+15);

    nvgFillColor(ctx, mTheme->mWindowPopup);
    nvgFill(ctx);

    Widget::draw(ctx);
}

// void Popup::save(Serializer &s) const {
//     Window::save(s);
//     s.set("anchorPos", mAnchorPos);
//     s.set("anchorHeight", mAnchorHeight);
// }
//
// bool Popup::load(Serializer &s) {
//     if (!Window::load(s)) return false;
//     if (!s.get("anchorPos", mAnchorPos)) return false;
//     if (!s.get("anchorHeight", mAnchorHeight)) return false;
//     return true;
// }
