#ifndef UI_COLOR_H_
#define UI_COLOR_H_

#include <nanovg.h>
#include <cjson/cJSON.h>

namespace ui {

    /* Cursor shapes */
    enum class Cursor {
        Arrow = 0,
        IBeam,
        Crosshair,
        Hand,
        HResize,
        VResize,
        CursorCount
    };

    extern "C" std::array<char, 8> utf8(int c);

    struct Color {

        float _r, _g, _b, _a;

        Color()
        {
            _r = _g = _b = 0.f;;
            _a = 1.f;
        }

        Color(int intensity, int alpha) {
            _r = _g = _b = (float)intensity / 255.0f;
            _a = (float)alpha / 255.f;
        }

        Color(float intensity, float alpha) {
            _r =_g = _b = intensity;
            _a = alpha;
        }

        Color(int r, int g, int b) {

            _r = (float)r / 255.f;
            _g = (float)g / 255.f;
            _b = (float)b / 255.f;
            _a = 1.0f;
        }

        Color(int r, int g, int b, int a) {
            _r = (float)r / 255.f;
            _g = (float)g / 255.f;
            _b = (float)b / 255.f;
            _a = (float)a / 255.0f;
        }

        NVGcolor toNVGcolor() const {
            NVGcolor res;
            res.r = _r;
            res.g = _g;
            res.b = _b;
            res.a = _a;
            return res;
        }

        operator NVGcolor () const {
            return toNVGcolor();
        }

        cJSON* serialize() {
            cJSON* json = cJSON_CreateObject();
            
            return json;
        }

        void deserialize(cJSON* json) {
        }
    };
}

#endif
