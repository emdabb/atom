#ifndef CJSON_EXT_H_
#define CJSON_EXT_H_

#include "sha256.h"
#include <cjson/cJSON.h>

struct cJSON;

#ifdef __cplusplus
extern "C" {
#endif

cJSON* cJSON_ParseSHA2(const char* src, BYTE hash_out[32]);
const char* cJSON_PrintSHA2(cJSON* src, const BYTE* hash_in);

void cJSON_SaveToFileSHA2(const char* basepath, cJSON* json);
cJSON* cJSON_LoadFromFileSHA2(const char* basepath);

#ifdef __cplusplus
}
#endif


#endif
