#ifndef ROSETTA_H_
#define ROSETTA_H_

#include <string>


namespace rosetta {
    void loadTranslations(const std::string&, std::istream&);
    void setLocale(const std::string&);
    //static const std::string& getString(const std::string&, const std::string&);
    const std::string& getString(const std::string& key, const std::string& defaultValue = "[error]");
    void loadLocales(const std::string&);
};


#endif /* end of include guard: ROSETTA_H_
 */
