#ifndef INPUT_H_
#define INPUT_H_

struct Input : public atom::singleton<Input>
{
    vec2i   _mousePos;
    vec2i   _mousePressPos;
    vec2i   _mouseReleasePos;
    bool    _isDown;
    
    void onPointerMotion(int x, int y) {
        _mousePos.x = x;
        _mousePos.y = y;
    }
    void onPointerPress(int x, int y) {
        _mousePos.x = _mousePressPos.x = x;
        _mousePos.y = _mousePressPos.y = y;
        _isDown = true;
    }
    void onPointerRelease(int x, int y) {
        _mousePos.x = _mouseReleasePos.x = x;
        _mousePos.y = _mouseReleasePos.y = y;
        _isDown = false;
    }
};
#endif
