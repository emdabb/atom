#include "ui/Gui.h"
#include "gamescreen.h"
#include "company.h"
#include "project.h"
#include "character.h"
#include "game_services.h"
#include "csv.h"
#include "fsm.h"
#include "rosetta.h"
#include "common.h"
#include <atom/engine/easing.h>
#include <atom/core/factory.h>

using namespace ui;

extern "C" gamescreen* new_gameplay_screen(company& co);
extern state<company>* new_company_state_developing();
//extern state<project>* new_project_state_starting();
//
extern "C" void Project_init(project& p);


struct project_properties_screen 
: gamescreen 
{
    std::string _name;
    int _topic;
    int _genre;
    int _platform;
    int _size;
    int _platformCosts;
    int _sizeCosts;

    ref<Window> win;
    vec2i _startPos;
    vec2i _centerPos;
    int _time;
    int _timeout;
    ref<Button> btn_done;
    ref<Button> btn_topic;
    ref<Button> btn_genre;
    ref<Button> btn_platform;
    ref<Button> btn_size;
    ref<TextBox> text;

    bool enable_buttons;

    std::string _default;

    project_properties_screen(company& co) 
    : _name(std::string())
    , _topic(-1)
    , _genre(-1)
    , _platform(-1)
    , _size(-1)
    , _platformCosts(0)
    , _sizeCosts(0)
    {
        
        win = &Screen::instance().add<Window>("project properties");
        win->withLayout<GroupLayout>();

        _default = "[game #" +  std::to_string(co._projects.size() + 1) + "]";
        _name = _default;

        text = &win->add<TextBox>(_default);
        text->setFocusCallback([&] (bool val) {
            if(val != false) { 
                text->setValue("");
            }
            _name = text->value();
        });
        text->setCallback([&] (const std::string& val) -> bool {
//                std::cout << "changing textbox value... val=" << val << ", text->value()=" << text->value() << std::endl;
            _name = val;
            return true;
        });

        auto& contentPane = win->add<Widget>().withLayout<GridLayout>(Orientation::Horizontal, 2, Alignment::Fill);

        btn_topic = &contentPane.add<Button>("topic");
        btn_topic->setCallback([&] (void) {
            enable_buttons      = false;
            auto& popup   = Screen::instance().add<Window>();
            popup.withLayout<GroupLayout>();
            auto& scroll = popup
                .add<Widget>().withLayout<GroupLayout>()
                .add<VScrollPanel>()
                    //.withLayout<GroupLayout>()
                    .withFixedHeight(Screen::instance().height() / 2)
                    .add<Widget>()
                        .withLayout<GridLayout>(Orientation::Horizontal, 3, Alignment::Fill);

            for(int i=0; i < co._topics.size(); i++) {
                std::string topicstr = rosetta::getString(csv::get_topic(i));
                scroll.add<Button>(topicstr).withCallback([this, i, &popup] (void) {
                    this->enable_buttons    = true;
                    this->_topic            = i;
                    btn_topic->setCaption(rosetta::getString(csv::get_topic(i)));
                    popup.dispose();                    
                });
            }
            popup.setSize(popup.preferredSize(game_services::instance()._nvg));
            popup.center(game_services::instance()._nvg);
        });

        btn_genre = &contentPane.add<Button>("genre");
        btn_genre->setCallback([&] (void) {
            enable_buttons      = false;

            auto& popup = Screen::instance().add<Window>("select genre");
            popup.withLayout<GroupLayout>();

            auto& contentPane = popup.add<Widget>().withLayout<GridLayout>(Orientation::Horizontal, 3, Alignment::Fill);

            for(int i=0; i < csv::get_num_genres(); i++) {
                std::string genre = rosetta::getString(csv::get_genre(i));
                contentPane.add<Button>(genre).withCallback([this, i, &popup] (void) {
                    this->enable_buttons = true;
                    this->_genre         = i;
                    btn_genre->setCaption(rosetta::getString(csv::get_genre(i)));
                    popup.dispose();
                });
            }
            popup.setSize(popup.preferredSize(game_services::instance()._nvg));
            popup.center(game_services::instance()._nvg); 
        });

        btn_platform = &contentPane.add<Button>("platform");
        btn_platform->setCallback([&] (void) {
            enable_buttons      = false;
            auto& popup = Screen::instance().add<Window>("select platform");
            popup.withLayout<GroupLayout>();

            auto& contentPane = popup.add<Widget>().withLayout<GroupLayout>();//Orientation::Horizontal, 3, Alignment::Fill);

            for(int i=0; i < static_cast<int>(EPlatform::Max); i++) {
                std::string str = rosetta::getString(gPlatformString[i]);
                contentPane.add<Button>(str).withCallback([this, i, &popup] (void) {
                    this->enable_buttons = true;
                    this->_platform      = i;
                    btn_platform->setCaption(rosetta::getString(gPlatformString[i]));
                    _platformCosts = gPlatformCosts[i];
                     
                    popup.dispose();
                });
            }
            popup.setSize(popup.preferredSize(game_services::instance()._nvg));
            popup.center(game_services::instance()._nvg); 
        });

        btn_size = &contentPane.add<Button>("size");
        btn_size->setCallback([&] (void) {
            enable_buttons      = false;
            auto& popup = Screen::instance().add<Window>("select size");
            popup.withLayout<GroupLayout>();

            auto& contentPane = popup.add<Widget>().withLayout<GroupLayout>();//Orientation::Horizontal, 3, Alignment::Fill);

            for(int i=0; i < static_cast<int>(EProjectSize::Max); i++) {
                std::string str = rosetta::getString(gProjectSizeString[i]);
                contentPane.add<Button>(str).withCallback([this, i, &popup] (void) {
                    this->enable_buttons = true;
                    this->_size          = i;
                    btn_size->setCaption(rosetta::getString(gProjectSizeString[i]));
                    _sizeCosts = gSizeCosts[i];

                    popup.dispose();
                });
            }
            popup.setSize(popup.preferredSize(game_services::instance()._nvg));
            popup.center(game_services::instance()._nvg); 
        });

        btn_done = &win->add<Button>("done").withCallback([&] (void) {
            this->setExitCallback([&] (const GameScreenEventArgs&) {
                win->dispose();
                //co._fsm.push(co, new_company_state_developing());
                this->getOwner()->add(new_gameplay_screen(co));
            });
            _name = text->value();
            
            project* ptr = nullptr;
            if(!Company_getProject((const company*)&co, _name.c_str(), &ptr)) {
                auto& dlg = Screen::instance().add<MessageDialog>(
                        MessageDialog::Type::Warning,
                        "oops!",
                        "project name is already taken..."
                        );
                dlg.center(game_services::instance()._nvg);
                return;
            } else {

                project p;
                Project_init(p);
                p._properties._topic = _topic;
                p._properties._genre = _genre;
                p._name  = _name;
                p._properties._platform = _platform;
                p._properties._size = _size;
                p._co = &co;
                p._currentFocus = 0;
                p._focusTime = 0;
                
                project_state* s = project_state_factory::instance().create(EProjectState::Starting);
                p._fsm.push(p, s);
                co._projects.push_back(p);

                /**
                 * Clear all point counters for all staff.
                 */
                for(int i=0; i < co._staff.size(); i++) {
                    character& ch = co._staff[i];
                    for(int j=0; j < 6; j++) {
                        ch._gen_tech[j] = ch._gen_design[j] = 0;
                    }   
                }

                co._resources -= _sizeCosts + _platformCosts;
                this->quit();
            }
        });

        win->setSize(win->preferredSize(game_services::instance()._nvg));
        win->center(game_services::instance()._nvg);

        _centerPos = win->position();
        _startPos.y = _centerPos.y;
        _startPos.x = -win->width();
        win->setPosition(_startPos);

        _time = 0;
        _timeout = 500;
        enable_buttons = true;
    }

    const bool is_valid() const {
        return  (!_name.empty()) &&
                (_topic >= 0) &&
                (_genre >= 0) &&
                (_size  >= 0) &&
                (_platform >= 0);
    }

    virtual void update(int dt, bool coveredByOtherScreen, bool otherScreenHasFocus) {
        if(_state == E_STATE_ACTIVE) {
            _time = std::min(_time + dt, _timeout);
            vec2i pp;
            float t = (float)_time /_timeout;
            float tt = atom::easing::ease_elastic_out(t);
            pp = _startPos + (_centerPos - _startPos) * tt;
            win->setPosition(pp);
        }

        btn_done->setEnabled(enable_buttons && is_valid());
        btn_topic->setEnabled(enable_buttons);
        btn_genre->setEnabled(enable_buttons);
        btn_platform->setEnabled(enable_buttons);
        btn_size->setEnabled(enable_buttons);
        text->setEditable(enable_buttons);
        text->setEnabled(enable_buttons);
        

        win->setSize(win->preferredSize(game_services::instance()._nvg));
        win->center(game_services::instance()._nvg);

        gamescreen::update(dt, coveredByOtherScreen, otherScreenHasFocus);
    }
};

extern "C" gamescreen* new_project_properties_screen(company& co) {
    return new project_properties_screen(co);
}
