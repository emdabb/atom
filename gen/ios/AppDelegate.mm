#import "AppDelegate.h"
#include <atom/base/platform/ios/ios_application.h>
#include <atom/core/threadpool.h>
#include <atom/gfx/graphics_context.h>
#include <iostream>

using namespace atom;

__apicall applet* new_game(application*);
__apicall graphics_context* create_eagl_graphics_context();

@implementation AppDelegate

//@synthesize mainWindow;

-(void) startApp
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    application* app_ = new ios_application;
    app_->set_applet(new_game(app_));
    app_->create();

    
    graphics_context* context = create_eagl_graphics_context();
    context->create(app_->get_display(), app_->get_surface());
    
    app_->start();
    
    app = static_cast<void*>(app_);
}

-(void) applicationDidFinishLaunching:(UIApplication*) application {
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    [self startApp];
}

-(void) applicationWillTerminate {
    //[mainWindow release];
}


@end
